<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "regression" or "type::bug" label:

- https://gitlab.com/clirai/pyralysis/issues?label_name%5B%5D=regression
- https://gitlab.com/clirai/pyralysis/issues?label_name%5B%5D=type::bug

and verify the issue you're about to submit isn't a duplicate.
--->

### Summary

<!-- Summarize the bug encountered concisely. -->

### What is the current *bug* behavior?

<!-- Describe what actually happens. -->

### What is the expected *correct* behavior?

<!-- Describe what you should see instead. -->

### Steps to reproduce

<!-- Describe how one can reproduce the issue - this is very important. Please use an ordered list. -->

### Example Project

<!-- If possible, please create an example project here on GitLab.com that exhibits the problematic
behavior, and link to it here in the bug report. If you are using an older version of GitLab, this
will also determine whether the bug is fixed in a more recent version. -->

### Relevant logs and/or screenshots

<!-- Paste any relevant logs - please use code blocks (```) to format console output, logs, and code
 as it's tough to read otherwise. -->

### Output of checks

<!-- If you are reporting a bug on pyralysis, uncomment below -->

<!-- This bug happens on pyralysis -->
<!-- /label ~"reproduced on pyralysis version 0.0.1" -->

### Environment

<!-- Report operating system, Python version, pip version, and version of your packages -->

### Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem. -->
