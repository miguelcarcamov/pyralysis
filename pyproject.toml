[build-system]
build-backend = "setuptools.build_meta"
requires = ["setuptools>=70.0.0", "setuptools-scm>=8", "wheel"]

[project]
authors = [
  {name = "Miguel Cárcamo", email = "miguel.carcamo@usach.cl"}
]
classifiers = [
  "Programming Language :: Python :: 3",
  "License :: OSI Approved :: GNU GPLv3",
  "Operating System :: OS Independent"
]
dependencies = [
  "astropy==6.0.0",
  "dask==2024.8.2",
  "dafits==1.1.0",
  "xarray==2024.01.1",
  "zarr==2.18.2",
  "dask-ms==0.2.21",
  "distributed==2024.8.2",
  "more-itertools==10.2.0",
  "multimethod==1.11.2",
  "numba==0.59.1",
  "numpy<2.0.0",
  "python-casacore==3.6.1",
  "radio-beam==0.3.7",
  "scipy==1.13.1",
  "spectral-cube==0.6.5"
]
description = "PYthon Radio Astronomy anaLYSis and Image Synthesis"
dynamic = ["version"]
keywords = ["radio", "radio-interferometry", "big data", "big computing", "SKA", "ALMA", "simulation", "optimization", "compressed sensing", "gridding"]
license = {file = "LICENSE"}
maintainers = [
  {name = "Miguel Cárcamo", email = "miguel.carcamo@usach.cl"}
]
name = "pyralysis"
readme = "README.md"
requires-python = ">=3.9,<3.13"

[project.optional-dependencies]
all = ["pyralysis[cupy]"]
cupy = ["cupy==13.2.0"]
test = ["six==1.16.0", "pytest==7.3.1", "pytest-cov==4.1.0"]

[project.urls]
Documentation = "https://pyralysis.readthedocs.io"
Issues = "https://gitlab.com/clirai/pyralysis/-/issues"
Repository = "https://gitlab.com/clirai/pyralysis"
Source = "https://gitlab.com/clirai/pyralysis"

[tool.commitizen]
name = "cz_customize"

[tool.commitizen.customize]
bump_map = {"break" = "MAJOR", "new" = "MINOR", "fix" = "PATCH", "hotfix" = "PATCH"}
bump_pattern = "^(break|new|fix|hotfix)"
change_type_map = {"feature" = "Feat", "bug fix" = "Fix"}
change_type_order = ["BREAKING CHANGE", "feat", "fix", "refactor", "perf"]
changelog_pattern = "^(feature|bug fix|refactor|fix|chg|add)?(!)?"
commit_parser = "^(?P<change_type>feature|bug fix):\\s(?P<message>.*)?"
example = "feature: this feature enable customize through config file"
info = """
This is customized info
"""
info_path = "cz_customize_info.txt"
message_template = "{{change_type}}:{% if show_message %} {{message}}{% endif %}"
schema = "<type>: <body>"
schema_pattern = "(?i)(feature|bug fix|refactor|fix|chg|add):(\\s.*)"

[[tool.commitizen.customize.questions]]
choices = ["feature", "fix", "bug fix", "chg", "refactor"]  # short version
message = "Select the type of change you are committing"
name = "change_type"
type = "list"

[[tool.commitizen.customize.questions]]
message = "Body."
name = "message"
type = "input"

[[tool.commitizen.customize.questions]]
message = "Do you want to add body message in commit?"
name = "show_message"
type = "confirm"

[tool.setuptools]
include-package-data = false

[tool.setuptools.packages.find]
include = ["pyralysis*"]
namespaces = false
where = ["src"]

[tool.setuptools_scm]
local_scheme = "no-local-version"
write_to = "src/pyralysis/_version.py"
