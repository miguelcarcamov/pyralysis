import logging
from dataclasses import dataclass, field
from logging import Logger
from typing import List

import astropy.units as u
import dask.array as da
import numpy as np
import xarray as xr
from astropy.constants import c
from astropy.units import Quantity
from astropy.units.equivalencies import Equivalency

from ..units import lambdas_equivalencies


@dataclass(init=True, repr=True)
class SpectralWindow:
    """Class that represents the Spectral Window table in the Measurement Set.

    Parameters
    ----------
    dataset : List[xarray.Dataset]
              xarray datasets representing the Spectral Window table grouped by rows
    """
    dataset: List[xr.Dataset] = None
    logger: Logger = field(init=False, repr=False)
    ndatasets: int = field(init=False, repr=True, default=0)
    max_nu: Quantity = field(init=False, repr=True, default=0.0 * u.Hz)
    min_nu: Quantity = field(init=False, repr=True, default=0.0 * u.Hz)
    ref_nu: Quantity = field(init=False, repr=True, default=0.0 * u.Hz)
    nchans: np.ndarray = field(init=False, repr=True, default=None)
    ref_freqs: List[Quantity] = field(init=False, repr=True, default=None)
    lambda_max: Quantity = field(init=False, repr=True, default=0.0 * u.m)
    lambda_min: Quantity = field(init=False, repr=True, default=0.0 * u.m)
    lambda_ref: Quantity = field(init=False, repr=True, default=0.0 * u.m)
    equivalencies: List[Equivalency] = field(init=False, repr=True, default=None)

    def __post_init__(self):

        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(logging.INFO)

        if self.dataset is not None:
            self.ndatasets = len(self.dataset)

        if self.dataset is not None:
            max_freqs = []
            min_freqs = []
            ref_freqs = []
            nchans = []
            chan_freqs = []

            for data in self.dataset:
                chans = data.CHAN_FREQ.data
                chan_freqs.append(chans)
                max_freqs.append(da.max(chans))
                min_freqs.append(da.min(chans))
                ref_freqs.append(data.REF_FREQUENCY.data.squeeze())
                nchans.append(data.NUM_CHAN.data.squeeze().compute())

            max_freqs_array = da.array(max_freqs)
            min_freqs_array = da.array(min_freqs)
            ref_freqs_array = da.array(ref_freqs)

            self.max_nu = da.max(max_freqs_array).compute() * u.Hz
            self.min_nu = da.min(min_freqs_array).compute() * u.Hz
            self.ref_nu = ((da.min(ref_freqs_array) + da.max(ref_freqs_array)) / 2).compute() * u.Hz
            self.ref_freqs = (
                ref_freqs_array.compute() * u.Hz
            )  # We compute the reference frequencies
            self.nchans = np.array(nchans)
            self.equivalencies = self.channels_equivalencies(chan_freqs)

            self.lambda_min = c / self.max_nu
            self.lambda_min = self.lambda_min.to(u.m)
            self.lambda_max = c / self.min_nu
            self.lambda_max = self.lambda_max.to(u.m)
            self.lambda_ref = c / self.ref_nu
            self.lambda_ref = self.lambda_ref.to(u.m)

    def channels_equivalencies(self, chans: List[da.Array]) -> List[Equivalency]:
        """Create the equivalencies the channels.

        Given each array with the channel frequency data (without units), broadcast it
        -for its use with visibility data- to a 3-dimensional array with the channels in the second
        dimension, and create the equivalencies for that frequency array.

        Parameters
        ----------
        chans : List[da.Array]
            List of arrays (without units) with the channels frequency data

        Returns
        -------
        List[Equivalency]
            The list of equivalencies, one for each channel data
        """
        chans_mem = da.compute(*chans)
        eq = [self.__channel_equivalency(chan) for chan in chans_mem]
        return eq

    def __channel_equivalency(self, chan: np.ndarray) -> Equivalency:
        # reduce channel dim
        chan_squeeze = chan.squeeze(axis=0)
        # add units to data to get a Quantity array
        chan_broadcast_un = chan_squeeze[np.newaxis, :, np.newaxis] * u.Hz
        # create equivalencies given the frequencies
        return lambdas_equivalencies(restfreq=chan_broadcast_un)
