import logging
from dataclasses import InitVar, dataclass, field
from logging import Logger

import astropy.units as u
import dask.array as da
import xarray as xr
from astropy.units import Quantity

from ..base.observation import Observation
from ..convolution.primary_beam.primary_beam import PrimaryBeam


@dataclass(init=True, repr=True)
class Antenna:
    """Class that represents the antenna MS table of an interferometer.

    Parameters
    ----------
    dataset : xarray.Dataset
              Full antenna table xarray dataset.
    """
    dataset: xr.Dataset = None
    logger: Logger = field(init=False, repr=False)
    primary_beam: PrimaryBeam = field(init=False, repr=False, default=None)
    max_diameter: Quantity = field(init=False, repr=True, default=0.0 * u.m)
    min_diameter: Quantity = field(init=False, repr=True, default=0.0 * u.m)

    def __post_init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(logging.INFO)

        if self.dataset is not None:
            self.max_diameter = self.dataset.DISH_DIAMETER.data.max() * u.m
            self.min_diameter = self.dataset.DISH_DIAMETER.data.min() * u.m

    def create_primary_beam(
        self, observation: Observation, antenna_obs_id: da.Array = None
    ) -> None:
        """Creates the Primary Beam attribute for Antenna.

        Parameters
        ----------
        observation : Observation
            Object with the observation MS table and telescope names
        antenna_obs_id : da.Array, optional
            Array with one observation id for every antenna, by default None
        """
        dish_diameter = self.dataset.DISH_DIAMETER.data
        if observation.ntelescope == 1 or antenna_obs_id is None:
            # if there is only one telescope in observation, use just one to create the PB
            telescope = observation.telescope[0].compute()
        else:
            # otherwise, slice the telescope array with the observations id of every antenna
            telescope = observation.telescope[antenna_obs_id]
        self.primary_beam = PrimaryBeam(dish_diameter=dish_diameter, telescope=telescope)
