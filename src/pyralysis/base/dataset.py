import logging
from dataclasses import dataclass
from dataclasses import field as _field
from logging import Logger
from typing import List, Union

import astropy.units as u
import dask.array as da
import numpy as np
from astropy.units import Quantity
from more_itertools import locate

from ..reconstruction.psf import PSF
from ..units.lambda_units import lambdas_equivalencies
from ..units.units_functions import array_unit_conversion
from .antenna import Antenna
from .baseline import Baseline
from .field import Field
from .observation import Observation
from .polarization import Polarization
from .spectral_window import SpectralWindow
from .subms import SubMS


@dataclass(init=True, repr=True)
class Dataset:
    """Dataset class.

    Parameters
    ----------
    antenna : Antenna
            Full Antenna object
    baseline : Baseline
            Full Baseline object
    field: Field
            Full Field object
    spws: SpectralWindow
            Full Spectral Window object
    polarization: Polarization
            Full Polarization object
    observation: Observation
            Observation object
    ms_list : List[SubMS]
            List of separated main tables by field and spectral windows
    psf : Union[PSF, List[PSF]]
            PSF object of the dataset, or list of PSF objects for each one the Stokes parameters
    do_psf_calculation : bool
            Whether to calculate the PSF for the dataset or not
    """
    antenna: Antenna = None
    baseline: Baseline = None
    field: Field = None
    spws: SpectralWindow = None
    polarization: Polarization = None
    observation: Observation = None
    ms_list: List[SubMS] = None
    psf: Union[PSF, List[PSF]] = None
    do_psf_calculation: bool = True
    corrected_column_present: bool = False

    logger: Logger = _field(init=False, repr=False)
    max_baseline: Quantity = _field(init=False, repr=True, default=0.0 * u.m)
    min_baseline: Quantity = _field(init=False, repr=True, default=0.0 * u.m)
    max_antenna_diameter: Quantity = _field(init=False, repr=True, default=0.0 * u.m)
    min_antenna_diameter: Quantity = _field(init=False, repr=True, default=0.0 * u.m)
    theo_resolution: Quantity = _field(init=False, repr=True, default=0.0 * u.rad)
    fov: Quantity = _field(init=False, repr=True, default=0.0 * u.rad)
    ndatasets: int = _field(init=False, repr=True, default=0)
    corr_weight_sum: dict = _field(init=False, repr=True, default=None)

    def __post_init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(logging.INFO)

        if self.antenna is not None:
            self.max_antenna_diameter = self.antenna.max_diameter
            self.min_antenna_diameter = self.antenna.min_diameter

        if self.baseline is not None:
            self.max_baseline = self.baseline.max_baseline
            self.min_baseline = self.baseline.min_baseline

        if self.antenna is not None and self.baseline is not None and self.spws is not None:
            self.theo_resolution = (self.spws.lambda_min / self.max_baseline) * u.rad
            self.fov = (self.spws.lambda_max / self.max_antenna_diameter) * u.rad

        if self.ms_list is not None:
            self.ndatasets = len(self.ms_list)

        if self.do_psf_calculation:
            self.calculate_psf()

    @staticmethod
    def calc_beam_size(s_uu, s_vv, s_uv) -> tuple:
        """Static method that calculates the size of the PSF given the weighted
        sum of u^2, v^2 and u*v.

        Parameters
        ----------
        s_uu : float
              Weighted sum of u^2.
        s_vv : float
              Weighted sum of v^2.
        s_uv : float
              Weighted sum of u*v.

        Returns
        -------
        tuple
            Beam major, minor and position angle in radians.
        """
        with np.errstate(divide="ignore"):
            uv_squared = s_uv * s_uv
            uu_minus_vv = s_uu - s_vv
            uu_plus_vv = s_uu + s_vv
            sqrt_in = np.sqrt(4.0 * uv_squared + (uu_minus_vv * uu_minus_vv))
            bmaj = (
                2.0 * np.sqrt(np.log(2.0)) / np.pi / np.sqrt(uu_plus_vv - sqrt_in)
            )  # Major axis in radians
            bmin = (
                2.0 * np.sqrt(np.log(2.0)) / np.pi / np.sqrt(uu_plus_vv + sqrt_in)
            )  # Minor axis in radians
            bpa = -0.5 * np.arctan2(2.0 * s_uv, uu_minus_vv)  # Angle in radians
            return bmaj * u.rad, bmin * u.rad, bpa * u.rad

    def sum_weights_stokes(self) -> dict:
        stokes_dict = {"I": 0.0, "Q": 0.0, "U": 0.0, "V": 0.0}
        if self.polarization.feed_kind == "linear":
            stokes_dict["I"] = self.corr_weight_sum["XX"] + self.corr_weight_sum["YY"]
            stokes_dict["Q"] = self.corr_weight_sum["XX"] + self.corr_weight_sum["YY"]
            stokes_dict["U"] = self.corr_weight_sum["XY"] + self.corr_weight_sum["YX"]
            stokes_dict["V"] = self.corr_weight_sum["XY"] + self.corr_weight_sum["YX"]
        elif self.polarization.feed_kind == "circular":
            stokes_dict["I"] = self.corr_weight_sum["LL"] + self.corr_weight_sum["RR"]
            stokes_dict["Q"] = self.corr_weight_sum["RL"] + self.corr_weight_sum["LR"]
            stokes_dict["U"] = self.corr_weight_sum["RL"] + self.corr_weight_sum["LR"]
            stokes_dict["V"] = self.corr_weight_sum["LL"] + self.corr_weight_sum["RR"]
        else:
            stokes_dict["I"] = (
                self.corr_weight_sum["XX"] + self.corr_weight_sum["YY"] +
                self.corr_weight_sum["LL"] + self.corr_weight_sum["RR"]
            )
            stokes_dict["Q"] = (
                self.corr_weight_sum["XX"] + self.corr_weight_sum["YY"] +
                self.corr_weight_sum["RL"] + self.corr_weight_sum["LR"]
            )
            stokes_dict["U"] = (
                self.corr_weight_sum["XY"] + self.corr_weight_sum["YX"] +
                self.corr_weight_sum["RL"] + self.corr_weight_sum["LR"]
            )
            stokes_dict["V"] = (
                self.corr_weight_sum["XY"] + self.corr_weight_sum["YX"] +
                self.corr_weight_sum["LL"] + self.corr_weight_sum["RR"]
            )

        return stokes_dict

    def max_ncorrs(self) -> int:
        return np.max(self.polarization.ncorrs)

    def calculate_weights_sum(self) -> None:

        if self.polarization.feed_kind == "linear":
            weight_corr_dict = {"XX": 0.0, "YX": 0.0, "XY": 0.0, "YY": 0.0}
        elif self.polarization.feed_kind == "circular":
            weight_corr_dict = {"LL": 0.0, "RL": 0.0, "LR": 0.0, "RR": 0.0}
        else:
            weight_corr_dict = {
                "XX": 0.0,
                "YX": 0.0,
                "XY": 0.0,
                "YY": 0.0,
                "LL": 0.0,
                "RL": 0.0,
                "LR": 0.0,
                "RR": 0.0,
            }

        for ms in self.ms_list:
            weight = ms.visibilities.weight.data
            flag = ms.visibilities.flag.data
            pol_id = ms.polarization_id
            spw_id = ms.spw_id
            ncorrs = self.polarization.ncorrs[pol_id]
            nchans = self.spws.nchans[spw_id]
            corr_names = self.polarization.corrs_string[pol_id]
            weight_broadcast = da.repeat(weight, nchans, axis=0)
            flag = flag.reshape((len(flag) * nchans, ncorrs))
            weight_broadcast[flag] = 0.0

            weight_sum = da.sum(weight_broadcast, axis=0)

            for i, corr_name in enumerate(corr_names):
                weight_corr_dict[corr_name] += weight_sum[i]

        weight_corr_dict = da.compute(weight_corr_dict)[0]

        self.corr_weight_sum = weight_corr_dict

    def calculate_psf(self, stokes: Union[List[str], str] = None) -> None:
        """Function that calculates the PSF properties (bmaj, bmin and bpa)
        analytically for different stokes using (u,v) positions and the
        weights."""
        if stokes is None:
            stokes = ["I", "Q", "U", "V"]
        else:
            if isinstance(stokes, str):
                stokes = stokes.split(",")

        stokes.sort()

        self.calculate_weights_sum()

        idx_I = list(locate(stokes, lambda x: x == "I"))
        idx_Q = list(locate(stokes, lambda x: x == "Q"))
        idx_U = list(locate(stokes, lambda x: x == "U"))
        idx_V = list(locate(stokes, lambda x: x == "V"))

        nstokes = len(stokes)

        s_uu = da.zeros(nstokes, dtype=np.float64)
        s_vv = da.zeros(nstokes, dtype=np.float64)
        s_uv = da.zeros(nstokes, dtype=np.float64)

        for ms in self.ms_list:
            pol_id = ms.polarization_id
            spw_id = ms.spw_id
            nchans = self.spws.nchans[spw_id]
            equiv = self.spws.equivalencies[spw_id]
            uvw = ms.visibilities.uvw.data * u.m
            weight = ms.visibilities.imaging_weight.data
            flag = ms.visibilities.flag.data
            ncorrs = self.polarization.ncorrs[pol_id]
            corr_names = self.polarization.corrs_string[pol_id]

            uvw_broadcast = uvw[:, np.newaxis, :]
            uvw_broadcast = da.repeat(uvw_broadcast, nchans, axis=1)
            uvw_lambdas = array_unit_conversion(
                array=uvw_broadcast,
                unit=u.lambdas,
                equivalencies=equiv,
            )

            # uvw_lambdas = uvw_broadcast / chans_broadcast.to(u.m, u.spectral())
            uvw_lambdas = da.map_blocks(lambda x: x.value, uvw_lambdas, dtype=np.float64)

            weight_broadcast = da.repeat(weight[:, np.newaxis, :], nchans, axis=1)
            weight_broadcast *= ~flag

            _u = uvw_lambdas[:, :, 0, np.newaxis]
            _v = uvw_lambdas[:, :, 1, np.newaxis]

            _s_uu = da.sum(weight_broadcast * _u**2, axis=(0, 1))
            _s_vv = da.sum(weight_broadcast * _v**2, axis=(0, 1))
            _s_uv = da.sum(weight_broadcast * _u * _v, axis=(0, 1))

            for i in range(0, ncorrs):
                if corr_names[i] == "XX":
                    if idx_I:
                        s_uu[idx_I[0]] += _s_uu[i]
                        s_vv[idx_I[0]] += _s_vv[i]
                        s_uv[idx_I[0]] += _s_uv[i]

                    if idx_Q:
                        s_uu[idx_Q[0]] += _s_uu[i]
                        s_vv[idx_Q[0]] += _s_vv[i]
                        s_uv[idx_Q[0]] += _s_uv[i]
                elif corr_names[i] == "XY":
                    if idx_U:
                        s_uu[idx_U[0]] += _s_uu[i]
                        s_vv[idx_U[0]] += _s_vv[i]
                        s_uv[idx_U[0]] += _s_uv[i]

                    if idx_V:
                        s_uu[idx_V[0]] += _s_uu[i]
                        s_vv[idx_V[0]] += _s_vv[i]
                        s_uv[idx_V[0]] += _s_uv[i]
                elif corr_names[i] == "YX":
                    if idx_U:
                        s_uu[idx_U[0]] += _s_uu[i]
                        s_vv[idx_U[0]] += _s_vv[i]
                        s_uv[idx_U[0]] += _s_uv[i]

                    if idx_V:
                        s_uu[idx_V[0]] += _s_uu[i]
                        s_vv[idx_V[0]] += _s_vv[i]
                        s_uv[idx_V[0]] += _s_uv[i]
                elif corr_names[i] == "YY":
                    if idx_I:
                        s_uu[idx_I[0]] += _s_uu[i]
                        s_vv[idx_I[0]] += _s_vv[i]
                        s_uv[idx_I[0]] += _s_uv[i]
                    if idx_Q:
                        s_uu[idx_Q[0]] += _s_uu[i]
                        s_vv[idx_Q[0]] += _s_vv[i]
                        s_uv[idx_Q[0]] += _s_uv[i]
                elif corr_names[i] == "LL":
                    if idx_I:
                        s_uu[idx_I[0]] += _s_uu[i]
                        s_vv[idx_I[0]] += _s_vv[i]
                        s_uv[idx_I[0]] += _s_uv[i]
                    if idx_V:
                        s_uu[idx_V[0]] += _s_uu[i]
                        s_vv[idx_V[0]] += _s_vv[i]
                        s_uv[idx_V[0]] += _s_uv[i]
                elif corr_names[i] == "LR":
                    if idx_Q:
                        s_uu[idx_Q[0]] += _s_uu[i]
                        s_vv[idx_Q[0]] += _s_vv[i]
                        s_uv[idx_Q[0]] += _s_uv[i]
                    if idx_U:
                        s_uu[idx_U[0]] += _s_uu[i]
                        s_vv[idx_U[0]] += _s_vv[i]
                        s_uv[idx_U[0]] += _s_uv[i]
                elif corr_names[i] == "RL":
                    if idx_Q:
                        s_uu[idx_Q[0]] += _s_uu[i]
                        s_vv[idx_Q[0]] += _s_vv[i]
                        s_uv[idx_Q[0]] += _s_uv[i]

                    if idx_U:
                        s_uu[idx_U[0]] += _s_uu[i]
                        s_vv[idx_U[0]] += _s_vv[i]
                        s_uv[idx_U[0]] += _s_uv[i]
                elif corr_names[i] == "RR":
                    if idx_I:
                        s_uu[idx_I[0]] += _s_uu[i]
                        s_vv[idx_I[0]] += _s_vv[i]
                        s_uv[idx_I[0]] += _s_uv[i]

                    if idx_V:
                        s_uu[idx_V[0]] += _s_uu[i]
                        s_vv[idx_V[0]] += _s_vv[i]
                        s_uv[idx_V[0]] += _s_uv[i]
                else:
                    raise ValueError("The correlation does not exist")

        weights_per_stokes = self.sum_weights_stokes()
        weights_per_selected_stokes = np.array(list(map(weights_per_stokes.get, stokes)))
        s_uu[weights_per_selected_stokes > 0.0
             ] /= weights_per_selected_stokes[weights_per_selected_stokes > 0.0]
        s_vv[weights_per_selected_stokes > 0.0
             ] /= weights_per_selected_stokes[weights_per_selected_stokes > 0.0]
        s_uv[weights_per_selected_stokes > 0.0
             ] /= weights_per_selected_stokes[weights_per_selected_stokes > 0.0]

        s_uu, s_vv, s_uv = da.compute(s_uu, s_vv, s_uv)
        bmaj, bmin, bpa = self.calc_beam_size(s_uu, s_vv, s_uv)
        self.psf = PSF(major=bmaj, minor=bmin, pa=bpa)

    def __iter__(self):
        attrs = {
            "antenna": self.antenna,
            "baseline": self.baseline,
            "field": self.field,
            "spws": self.spws,
            "polarization": self.polarization,
            "observation": self.observation,
            "ms_list": self.ms_list,
            "do_psf_calculation": self.do_psf_calculation,
            "corrected_column_present": self.corrected_column_present,
        }
        if self.do_psf_calculation:
            attrs["psf"] = self.psf

        return iter(attrs.items())
