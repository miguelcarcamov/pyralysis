import logging
from dataclasses import dataclass, field
from logging import Logger
from typing import List

import numpy as np
import xarray as xr

pol_dict = {
    0: "Undefined",
    1: "I",
    2: "Q",
    3: "U",
    4: "V",
    5: "RR",
    6: "RL",
    7: "LR",
    8: "LL",
    9: "XX",
    10: "XY",
    11: "YX",
    12: "YY",
    13: "RX",
    14: "RY",
    15: "LX",
    16: "LY",
    17: "XR",
    18: "XL",
    19: "YR",
    20: "YL",
    21: "PP",
    22: "PQ",
    23: "QP",
    24: "QQ",
    25: "RCircular",
    26: "LCircular",
    27: "Linear",
    28: "Ptotal",
    29: "Plinear",
    30: "PFtotal",
    31: "PFlinear",
    32: "Pangle",
}


def get_correlation(corr_num):
    return pol_dict.get(corr_num, "Invalid correlation number")


@dataclass(init=True, repr=True)
class Polarization:
    """Class that represents the polarization table in a Measurement Set.

    Parameters
    ----------
    dataset : List[xarray.Dataset]
              xarray datasets with the polarization table grouped by rows
    """
    dataset: List[xr.Dataset] = None

    logger: Logger = field(init=False, repr=False)
    ncorrs: np.ndarray = field(init=False, repr=True, default=None)
    corrs_string: list = field(init=False, repr=True, default=None)
    feed_kind: str = field(init=False, repr=True, default=None)
    ndatasets: int = field(init=False, repr=True, default=0)

    def __post_init__(self):

        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(logging.INFO)

        if self.dataset is not None:
            self.ndatasets = len(self.dataset)
            self.check_feed()

    def check_feed(self):
        linear_feed = ["XX", "XY", "YX", "YY"]
        circular_feed = ["LL", "RL", "LR", "RR"]
        # Here we will convert the corr type from the measurement set to the correlation string.
        # We also check if the feed is linear or circular
        corrs_string_list = []
        ncorrs = []
        feed = []
        for pol in self.dataset:
            corrs = pol.CORR_TYPE.data.squeeze(axis=0).compute()
            corr_string = list(map(get_correlation, corrs))
            corrs_string_list.append(corr_string)
            if all(i in linear_feed for i in corr_string):
                feed.append("linear")
            elif all(i in circular_feed for i in corr_string):
                feed.append("circular")
            else:
                feed.append("mixed")
            ncorrs.append(pol.NUM_CORR.data.squeeze().compute())
        self.ncorrs = np.array(ncorrs)
        self.corrs_string = corrs_string_list
        if all(i == "linear" for i in feed):
            self.feed_kind = "linear"
        elif all(i == "circular" for i in feed):
            self.feed_kind = "circular"
        else:
            self.feed_kind = "mixed"
