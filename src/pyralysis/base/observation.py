import logging
from dataclasses import dataclass, field
from logging import Logger

import dask.array as da
import xarray as xr


@dataclass(init=True, repr=True)
class Observation:
    dataset: xr.Dataset = None
    logger: Logger = field(init=False, repr=False)
    telescope: da.Array = field(init=False, repr=True, default=None)
    ntelescope: int = field(init=False, repr=True, default=None)

    def __post_init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(logging.INFO)

        if self.dataset is not None:
            self.telescope = self.dataset.TELESCOPE_NAME.data
            self.ntelescope = da.unique(self.telescope).compute().size
