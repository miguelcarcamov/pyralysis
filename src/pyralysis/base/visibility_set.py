import logging
from dataclasses import dataclass, field
from logging import Logger

import astropy.units as un
import dask.array as da
import xarray as xr

from ..units import lambdas_equivalencies, xarray_unit_conversion


@dataclass(init=True, repr=True)
class VisibilitySet:
    """Class that represents the main table visibilities on the partitioned
    Measurement Set.

    Parameters
    ----------
    dataset : xarray.Dataset
              xarray Dataset of the main table on the partitioned Measurement Set
    """
    dataset: xr.Dataset = None

    logger: Logger = field(init=False, repr=False)
    nrows: int = field(init=False, repr=True, default=0)
    uvw: xr.DataArray = field(init=False, repr=True, default=None)
    weight: xr.DataArray = field(init=False, repr=True, default=None)
    imaging_weight: xr.DataArray = field(init=False, repr=True, default=None)
    flag: xr.DataArray = field(init=False, repr=True, default=None)
    data: xr.DataArray = field(init=False, repr=True, default=None)
    model: xr.DataArray = field(init=False, repr=True, default=None)
    corrected: xr.DataArray = field(init=False, repr=True, default=None)
    antenna1: xr.DataArray = field(init=False, repr=True, default=None)
    antenna2: xr.DataArray = field(init=False, repr=True, default=None)
    time: xr.DataArray = field(init=False, repr=True, default=None)
    scan_number: xr.DataArray = field(init=False, repr=True, default=None)
    observation: xr.DataArray = field(init=False, repr=True, default=None)
    baseline: xr.DataArray = field(init=False, repr=True, default=None)
    unflagged_rows: da.Array = field(init=False, repr=True, default=None)
    unflagged_chans: da.Array = field(init=False, repr=True, default=None)
    unflagged_corrs: da.Array = field(init=False, repr=True, default=None)
    visibility_number: float = field(init=False, repr=True, default=None)

    def __post_init__(self):

        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(logging.INFO)

        if self.dataset is not None and isinstance(self.dataset, xr.Dataset):
            ms_list_keys = self.dataset.data_vars.keys()
            self.nrows = len(self.dataset.ROWID)
            self.rows = self.dataset.ROWID
            self.uvw = self.dataset.UVW
            self.weight = self.dataset.WEIGHT
            self.imaging_weight = self.dataset.IMAGING_WEIGHTS
            self.flag = self.dataset.FLAG
            self.data = self.dataset.DATA

            if "MODEL_DATA" in ms_list_keys:
                self.model = self.dataset.MODEL_DATA
            else:
                self.model = xr.zeros_like(self.data)

            if "CORRECTED_DATA" in ms_list_keys:
                self.corrected = self.dataset.CORRECTED_DATA
            else:
                self.corrected = xr.zeros_like(self.data)

            self.antenna1 = self.dataset.ANTENNA1
            self.antenna2 = self.dataset.ANTENNA2
            self.observation = self.dataset.OBSERVATION_ID
            self.baseline = self.dataset.BASELINE_ID
            self.time = self.dataset.TIME
            self.scan_number = self.dataset.SCAN_NUMBER
            (
                self.unflagged_rows,
                self.unflagged_chans,
                self.unflagged_corrs,
            ) = da.nonzero(~self.flag.data)
            self.visibility_number = da.count_nonzero(~self.flag.data)
        else:
            raise ValueError("None-type or xarray dataset cannot be read")

    def get_column(self, column=None, as_xarray=True):
        if column is not None:
            if as_xarray:
                return self.dataset[column]
            else:
                return self.dataset[column].data.compute()

    def get_uvw_lambda(self, nu=None, as_xarray: bool = True):
        if nu is None:
            raise ValueError("Error: Input frequency is not present")

        uvw_meters = self.uvw * un.m
        if not as_xarray:
            return uvw_meters.data.compute().to(
                un.lambdas, equivalencies=lambdas_equivalencies(restfreq=nu)
            )
        else:
            # runs function decompose on a lazy manner over all items of the dask array
            uvw = xarray_unit_conversion(uvw_meters, un.lambdas, lambdas_equivalencies(restfreq=nu))
            return uvw

    def get_uvw_distance(self, count_w: bool = True):
        u = self.uvw[:, 0].data * un.m
        v = self.uvw[:, 1].data * un.m
        if count_w:
            w = self.uvw[:, 2].data * un.m
        else:
            w = 0.0 * un.m
        distance = da.sqrt(u**2 + v**2 + w**2)
        return distance
