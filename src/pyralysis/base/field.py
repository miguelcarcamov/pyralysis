import logging
from abc import ABCMeta
from dataclasses import dataclass, field
from logging import Logger
from typing import List, Union

import astropy.units as u
import dask.array as da
import numpy as np
import xarray as xr
from astropy.coordinates import SkyCoord
from astropy.time import Time

from ..utils import radec_to_direction_cosines


@dataclass(init=True, repr=True)
class Field:
    """Class representing the field table in a Measurement Set.

    Parameters
    ----------
    dataset : xarray.Dataset
        xarray datasets of the field table grouped by rows.
    table_keywords : dict, optional
        Table-level keywords.
    column_keywords : dict, optional
        Column-level keywords.
    """
    dataset: Union[xr.Dataset, None] = None
    table_keywords: Union[dict, None] = None
    column_keywords: Union[dict, None] = None

    logger: logging.Logger = field(init=False, repr=False)
    ref_dirs: SkyCoord = field(init=False, default=None)
    ref_direction_cosines: np.ndarray = field(init=False, default=None)
    phase_dirs: SkyCoord = field(init=False, default=None)
    phase_direction_cosines: np.ndarray = field(init=False, default=None)
    center_ref_dir: SkyCoord = field(init=False, default=None)
    center_phase_dir: SkyCoord = field(init=False, default=None)
    is_ms_v3: bool = field(init=False, default=False)

    _ref_dirs: SkyCoord = field(init=False, repr=False, default=None)
    _phase_dirs: SkyCoord = field(init=False, repr=False, default=None)

    _center_ref_dir: SkyCoord = field(init=False, repr=False, default=None)
    _center_phase_dir: SkyCoord = field(init=False, repr=False, default=None)

    def __post_init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(logging.INFO)

        if self.dataset is not None:
            self.__get_coords_from_keywords()

    @property
    def ref_dirs(self):
        """Getter for reference directions."""
        return self._ref_dirs

    @ref_dirs.setter
    def ref_dirs(self, value):
        """Setter for reference directions, updates dependent attributes."""
        if isinstance(value, property):
            value = None
        self._ref_dirs = value
        if self._ref_dirs is not None:
            self.center_ref_dir = self.__get_mean_direction(self._ref_dirs)

    @property
    def phase_dirs(self):
        """Getter for phase directions."""
        return self._phase_dirs

    @phase_dirs.setter
    def phase_dirs(self, value):
        """Setter for phase directions, updates dependent attributes."""
        if isinstance(value, property):
            value = None
        self._phase_dirs = value
        if self._phase_dirs is not None:
            self.center_phase_dir = self.__get_mean_direction(self._phase_dirs)

    @property
    def center_ref_dir(self):
        return self._center_ref_dir

    @center_ref_dir.setter
    def center_ref_dir(self, center_ref_dir):
        if isinstance(center_ref_dir, property):
            center_ref_dir = None
        self._center_ref_dir = center_ref_dir
        if self._center_ref_dir is not None and self.ref_dirs is not None:
            self.ref_direction_cosines = radec_to_direction_cosines(
                self.ref_dirs, self._center_ref_dir
            )

    @property
    def center_phase_dir(self):
        return self._center_phase_dir

    @center_phase_dir.setter
    def center_phase_dir(self, center_phase_dir):
        if isinstance(center_phase_dir, property):
            center_phase_dir = None
        self._center_phase_dir = center_phase_dir
        if self._center_phase_dir is not None and self.phase_dirs is not None:
            self.phase_direction_cosines = radec_to_direction_cosines(
                self.phase_dirs, self._center_phase_dir
            )

    def __get_coords_from_keywords(self):
        """Extract coordinates from dataset keywords and set instance
        attributes."""
        self.is_ms_v3 = self.__is_measurement_set_v3()

        time_units = "1" + self.column_keywords["TIME"]["QuantumUnits"][0]
        time_scale = self.column_keywords["TIME"]["MEASINFO"]["Ref"].lower()
        times_with_units = self.dataset["TIME"].data * u.Quantity(time_units)

        times = Time(times_with_units, format="mjd", scale=time_scale)
        mean_time = Time(np.mean(times_with_units), format="mjd", scale=time_scale)

        ref_dirs_array = self.dataset["REFERENCE_DIR"].data.squeeze(axis=1)
        phase_dirs_array = self.dataset["PHASE_DIR"].data.squeeze(axis=1)

        if self.is_ms_v3:
            ref_dirs_type = self.dataset["RefDir_Ref"].data
            phase_dirs_type = self.dataset["PhaseDir_Ref"].data

            self.ref_dirs = self.__process_ms_v3(
                ref_dirs_array, ref_dirs_type, "REFERENCE_DIR", times
            )
            self.phase_dirs = self.__process_ms_v3(
                phase_dirs_array, phase_dirs_type, "PHASE_DIR", times
            )
        else:
            ref_dirs_type = self.column_keywords["REFERENCE_DIR"]["MEASINFO"]["Ref"]
            phase_dirs_type = self.column_keywords["PHASE_DIR"]["MEASINFO"]["Ref"]

            self.ref_dirs = self.__get_sky_coords_from_coordinates(
                ref_dirs_array, ref_dirs_type,
                self.column_keywords["REFERENCE_DIR"]["QuantumUnits"], times
            )
            self.phase_dirs = self.__get_sky_coords_from_coordinates(
                phase_dirs_array, phase_dirs_type,
                self.column_keywords["PHASE_DIR"]["QuantumUnits"], times
            )

        # Calculate the mean directions
        self.center_ref_dir = self.__get_mean_direction(self.ref_dirs, mean_time)
        self.center_phase_dir = self.__get_mean_direction(self.phase_dirs, mean_time)

    def __is_measurement_set_v3(self) -> bool:
        """Check if the dataset follows Measurement Set version 3."""
        phase_dir_meas_info = self.column_keywords["PHASE_DIR"]["MEASINFO"].keys()
        ref_dir_meas_info = self.column_keywords["REFERENCE_DIR"]["MEASINFO"].keys()

        return {"TabRefTypes", "TabRefCodes"}.issubset(phase_dir_meas_info) and {
            "TabRefTypes", "TabRefCodes"
        }.issubset(ref_dir_meas_info)

    def __process_ms_v3(self, directions_array, directions_type, col_name, times) -> SkyCoord:
        """Process Measurement Set v3 format coordinates."""
        dir_types = self.column_keywords[col_name]["MEASINFO"]["TabRefTypes"]
        dir_codes = self.column_keywords[col_name]["MEASINFO"]["TabRefCodes"]

        return self.__get_sky_coords_from_coordinates_v3(
            directions_array,
            directions_type,
            self.column_keywords[col_name]["QuantumUnits"],
            dir_types,
            dir_codes,
            times,
        )

    @staticmethod
    def __get_mean_direction(directions: SkyCoord, mean_time: Time = None) -> SkyCoord:
        """Calculate the mean direction from a SkyCoord array.

        Parameters
        ----------
        directions : SkyCoord
            Array of direction coordinates.
        mean_time : Time, optional
            The mean observation time. If None, it is computed from the input directions.

        Returns
        -------
        SkyCoord
            Mean direction coordinate.
        """
        if mean_time is None:
            mean_time = Time(np.mean(directions.obstime.mjd), format="mjd", scale="utc")

        return SkyCoord(
            ra=np.mean(directions.ra),
            dec=np.mean(directions.dec),
            obstime=mean_time,
            frame=directions.frame,
        )

    @staticmethod
    def __get_sky_coords_from_coordinates_v3(
        directions_array, directions_type, directions_units, type_strings, type_ids, times
    ) -> SkyCoord:
        """Convert v3 coordinates to SkyCoord objects in ICRS.

        Parameters
        ----------
        directions_array : dask.array
            Array of direction values.
        directions_type : dask.array
            Array of direction types.
        directions_units : list
            Units for direction values.
        type_strings : list
            String representation of direction types.
        type_ids : list
            IDs for direction types.
        times : Time
            Observation times.

        Returns
        -------
        SkyCoord
            Coordinates in ICRS.
        """
        dirs = directions_array.compute()
        dirs_type = directions_type.compute()
        type_strings = np.array(type_strings)

        # Step 1: Get unique type IDs and first occurrence indices.
        unique_ids, first_occurrence = np.unique(type_ids, return_index=True)
        # Step 2: Create a mapping array from type ID to its first occurrence index.
        mapping = np.full(np.max(type_ids) + 1, -1, dtype=int)
        mapping[unique_ids] = first_occurrence
        # Step 3: Map dirs_type to corresponding indices.
        mapped_indices = mapping[dirs_type]

        frames = np.char.lower(type_strings[mapped_indices])
        frames = np.char.replace(frames, "j2000", "fk5")
        frames = np.repeat(frames, len(dirs_type)) if len(frames) == 1 else frames

        ra = dirs[:, 0] * u.Quantity("1." + directions_units[0])
        dec = dirs[:, 1] * u.Quantity("1." + directions_units[1])

        ra_icrs = np.zeros(len(frames)) * u.deg
        dec_icrs = np.zeros(len(frames)) * u.deg

        for frame in np.unique(frames):
            mask = frames == frame
            coord = SkyCoord(ra=ra[mask], dec=dec[mask], obstime=times[mask], frame=frame)
            coord_icrs = coord.icrs

            ra_icrs[mask] = coord_icrs.ra
            dec_icrs[mask] = coord_icrs.dec

        sky_coords = SkyCoord(ra=ra, dec=dec, obstime=times, frame="icrs")
        return sky_coords

    @staticmethod
    def __get_sky_coords_from_coordinates(
        directions_array, directions_type, directions_units, times
    ):
        """Convert coordinates to SkyCoord objects in ICRS.

        Parameters
        ----------
        directions_array : dask.array
            Array of direction values.
        directions_type : dask.array
            Array of direction types.
        directions_units : list
            Units for direction values.
        times : Time
            Observation times.

        Returns
        -------
        SkyCoord
            Coordinates in ICRS.
        """
        frame = directions_type.lower().replace("j2000", "fk5")
        directions_array = directions_array.compute()

        ra = directions_array[:, 0] * u.Quantity("1." + directions_units[0])
        dec = directions_array[:, 1] * u.Quantity("1." + directions_units[1])

        return SkyCoord(ra=ra, dec=dec, obstime=times, frame=frame).icrs
