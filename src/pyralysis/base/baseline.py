import logging
from dataclasses import InitVar, dataclass, field
from logging import Logger
from typing import Union

import astropy.units as u
import dask.array as da
import numpy as np
import xarray as xr
from astropy.units import Quantity

from .antenna import Antenna


@dataclass(init=True, repr=True)
class Baseline:
    """Class that represents the relationship between two antennas.

    The resulting baseline dataset has the following columns:
    - BASELINE_LENGTH
    - ANTENNA1
    - ANTENNA2

    Parameters
    ----------
    antenna : Antenna
              Antenna object.
    """
    antenna: InitVar[Antenna] = None
    logger: Logger = field(init=False, repr=False)
    dataset: xr.Dataset = field(init=False, repr=True, default=None)
    max_baseline: Quantity = field(init=False, repr=True, default=0.0 * u.m)
    min_baseline: Quantity = field(init=False, repr=True, default=0.0 * u.m)

    def __post_init__(self, antenna):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(logging.INFO)

        if antenna is not None:
            ids = antenna.dataset.ROWID.data
            combs = self.find_pairs(ids)

            antenna1 = antenna.dataset.sel(row=combs[:, 0])
            antenna2 = antenna.dataset.sel(row=combs[:, 1])

            baseline = antenna1.POSITION.data - antenna2.POSITION.data

            baseline_length = da.sqrt(baseline[:, 0]**2 + baseline[:, 1]**2 +
                                      baseline[:, 2]**2).astype(np.float32)

            row_id = da.arange(len(combs[:, 0]))
            ant1_id = combs[:, 0]
            ant2_id = combs[:, 1]

            ds = xr.Dataset(
                data_vars=dict(
                    BASELINE_LENGTH=(["row"], baseline_length * u.m),
                    ANTENNA1=(["row"], ant1_id.astype(np.int32)),
                    ANTENNA2=(["row"], ant2_id.astype(np.int32)),
                ),
                coords=dict(ROWID=(["row"], row_id.astype(np.int32)), ),
                attrs=dict(description="Baseline-Antenna relationship"),
            )
            self.dataset = ds
            self.max_baseline = baseline_length.max().compute() * u.m
            self.min_baseline = baseline_length.min().compute() * u.m

    @staticmethod
    def find_pairs(antenna_ids: Union[da.Array, np.array] = None) -> Union[da.Array, np.array]:
        """Static method that find all pairs of antennas from an Antenna
        dataset.

        Parameters
        ----------
        antenna_ids : Union[da.Array, np.array]
                Antenna filtered_dataset

        Returns
        -------
        2D array with the ANTENNA1 and ANTENNA2 combinations without repetition
        """
        if antenna_ids is not None:
            n = len(antenna_ids)
            L = n * (n - 1) // 2
            out = da.empty((L, 2), dtype=antenna_ids.dtype)
            m = ~da.tri(n, dtype=bool)
            out[:, 0] = da.broadcast_to(antenna_ids[:, None], (n, n))[m].compute_chunk_sizes()
            out[:, 1] = da.broadcast_to(antenna_ids, (n, n))[m].compute_chunk_sizes()
            return out
        else:
            raise ValueError("antenna_ids cannot be Nonetype")
