from dataclasses import dataclass
from typing import Tuple

import dask.array as da
import numpy as np

from .c_kernel import CKernel


@dataclass(init=False, repr=True)
class Bicubic(CKernel):
    """
    Bicubic gridding kernel using Catmull–Rom spline interpolation.
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    @staticmethod
    def __cubic_weight(t: np.ndarray) -> np.ndarray:
        """Compute bicubic weights using the Catmull–Rom spline."""
        abs_t = np.abs(t)
        w = np.zeros_like(abs_t)

        mask1 = abs_t <= 1
        mask2 = (abs_t > 1) & (abs_t <= 2)

        w[mask1] = (1.5 * abs_t[mask1]**3) - (2.5 * abs_t[mask1]**2) + 1
        w[mask2] = (-0.5 * abs_t[mask2]**3) + (2.5 * abs_t[mask2]**2) - (4 * abs_t[mask2]) + 2

        return w

    def _compute_raw_kernel(self, imsize: Tuple[int, int]) -> da.Array:
        """
        Compute the raw bicubic kernel (unnormalized) in Fourier space.
        """

        w = imsize[0] if self.w is None else self.w
        os_factor = self.oversampling_factor

        # Effective kernel size (in pixels) in Fourier space.
        effective_size = (imsize[0] * int(os_factor), imsize[1] * int(os_factor))
        effective_uvcellsize = self.uvcellsize / int(os_factor)

        delta_u, delta_v = effective_uvcellsize.value
        i_deltas, j_deltas = self.delta_values(effective_size, deltas=effective_uvcellsize)

        i_values = self.__cubic_weight(i_deltas / (w * delta_v))
        j_values = self.__cubic_weight(j_deltas / (w * delta_u))

        raw_kernel = da.from_array(i_values * j_values[:, np.newaxis])
        return raw_kernel
