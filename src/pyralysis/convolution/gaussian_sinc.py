from dataclasses import dataclass
from typing import Tuple

import dask.array as da
import numpy as np

from .c_kernel import CKernel


@dataclass(init=False, repr=True)
class GaussianSinc(CKernel):
    """
    Gaussian-Sinc gridding kernel.

    Attributes
    ----------
    a : float
        Exponent parameter.
    w_1 : float
        Scaling parameter for the Gaussian (exponential) term.
    w_2 : float
        Scaling parameter for the sinc term.
    """
    a: float
    w_1: float
    w_2: float

    def __init__(self, *, a: float = 2.0, w_1: float = 2.51, w_2: float = 1.55, **kwargs):
        super().__init__(**kwargs)
        self.a = a
        self.w_1 = w_1
        self.w_2 = w_2

    def _compute_raw_kernel(self, imsize: Tuple[int, int]) -> da.Array:
        """
        Compute the raw Gaussian-Sinc kernel (unnormalized) in Fourier space.
        """
        os_factor = self.oversampling_factor

        # Effective kernel size (in pixels) in Fourier space.
        effective_size = (imsize[0] * int(os_factor), imsize[1] * int(os_factor))
        effective_uvcellsize = self.uvcellsize / int(os_factor)

        delta_u, delta_v = effective_uvcellsize.value
        i_deltas, j_deltas = self.delta_values(effective_size, deltas=effective_uvcellsize)

        i_exp = np.exp(-np.abs(i_deltas) / (self.w_1 * delta_v)**self.a)
        j_exp = np.exp(-np.abs(j_deltas) / (self.w_1 * delta_u)**self.a)

        i_sinc = np.sinc(i_deltas / (self.w_2 * delta_v))
        j_sinc = np.sinc(j_deltas / (self.w_2 * delta_u))

        raw_kernel = da.from_array((i_exp * i_sinc)[:, None] * (j_exp * j_sinc)[None, :])
        return raw_kernel
