from dataclasses import dataclass
from typing import Tuple

import dask.array as da
import numpy as np

from .c_kernel import CKernel


@dataclass(init=False, repr=True)
class Pillbox(CKernel):
    """
    Pillbox gridding kernel.
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def _compute_raw_gcf(self, imsize: Tuple[int, int]) -> da.Array:
        """
        Compute the raw Pillbox Gridding Correction Function (GCF) in image space.
        """
        w = imsize[0] if self.w is None else self.w

        delta_x, delta_y = self.cellsize.value
        i_deltas, j_deltas = self.delta_values(imsize, deltas=self.cellsize)

        i_values = np.sinc(i_deltas / (w * delta_y))
        j_values = np.sinc(j_deltas / (w * delta_x))

        raw_gcf = da.from_array(i_values * j_values[:, np.newaxis])
        return raw_gcf
