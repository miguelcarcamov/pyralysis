from abc import abstractmethod
from dataclasses import dataclass
from functools import singledispatchmethod
from typing import Any, Callable, Dict, List, Tuple, Union

import dask.array as da
import numpy as np
import xarray as xr
from astropy import constants as const
from astropy.units.quantity import Quantity
from scipy.special import jn_zeros

from ...convolution.c_kernel import CKernel
from ...fft import fft2
from ...utils.functional_models import airy_disk, gaussian


@dataclass(init=False, repr=True)
class PrimaryBeam(CKernel):
    """Base class for two dimensional primary beams.

    Attributes
    ----------
    amplitude : float
        Amplitude of the function, default is 1.0.
    dish_diameter : list
        Diameter of dish in meters.
    telescope : str, list
        Telescope type or names.

    Examples
    --------
    >>> import dask.array as da
    >>> import numpy as np
    >>> from pyralysis.convolution.primary_beam import PrimaryBeam
    >>> diameter = da.array([12., 16.])
    >>> telescope = da.array(['ALMA', 'VLA'])
    >>> primary_beam = PrimaryBeam(dish_diameter=diameter,telescope=telescope)

    >>> frequencies = da.array([1e8, 1.2e8, 1.3e8])
    >>> beam = primary_beam.beam(frequency=frequencies, imsize=[512, 512])
    >>> beam
    dask.array<mul, shape=(2, 3, 512, 512), dtype=float32, ... >
    """
    amplitude: float = 1.0
    dish_diameter: List[float] = None
    telescope: Union[str, List[str]] = None

    def __init__(
        self,
        /,
        *,
        amplitude: float = 1.0,
        dish_diameter: List[float] = None,
        telescope: Union[str, List[str]] = None,
        **kwargs
    ):
        """Inits PrimaryBeam.

        Parameters
        ----------
        amplitude : float, optional
            Amplitude of the function, by default 1.0
        dish_diameter : List[float], optional
            Diameter of dish in meters, by default None
        telescope : Union[str, List[str]], optional
            Telescope type or names, by default None
        cellsize : Union[float, Tuple[float, float], Quantity, List[Quantity]], optional
            Size of every cell in the image, by default (-1.0, 1.0) rad
        """
        if 'cellsize' not in kwargs:
            kwargs['cellsize'] = 1.0
        super().__init__(**kwargs)

        self.amplitude = amplitude
        self.dish_diameter = dish_diameter
        if isinstance(telescope, str):
            # self.telescope = np.repeat(telescope, self.dish_diameter.size)
            self.telescope = da.repeat(da.array([telescope]), self.dish_diameter.size)
        else:
            self.telescope = telescope

    def beam(
        self,
        frequency: Union[List[float], np.ndarray, da.Array],
        imsize: Union[Tuple[int, int], List[int]],
        imcenter: Union[Tuple[int, int], List[int]] = None,
        x_0: Union[float, int] = None,
        y_0: Union[float, int] = None,
        antenna: List[int] = None,
        cellsize: Union[float, Tuple[float, float], Quantity, List[Quantity]] = None,
        imchunks: Union[int, Tuple[int], Dict, str] = "auto"
    ) -> da.Array:
        """Create an array containing the primary beam.

        The primary beam image is calculated for each frequency and antenna.
        Each antenna has a different modeling function for the primary beam according to the
        telescope it belongs to.

        Parameters
        ----------
        frequency : Union[List[float], np.ndarray, da.Array]
            List of float (dimentionless) with the frequency of the light
        imsize : Union[Tuple[int, int], List[int]]
            List or tuple of 2 integers giving the desired size of the image
        imcenter : Union[Tuple[int, int], List[int]], optional
            List or tuple of 2 integers giving the center of the beam, by default None
        x_0 : Union[int, float]
            x position of the maximum of the Primary beam function
        y_0 : Union[int, float]
            y position of the maximum of the Primary beam function
        antenna : List[int], optional
            List of integers indicating the id of the antennas for which the beam is to be
            calculated, by default None
        cellsize : Union[float, Tuple[float, float], Quantity, List[Quantity]], optional
            Size of every cell in the image, by default None
        imchunks : Union[int, Tuple[int], Dict, str]
            The number of samples on each block on the image dimensions of the resulting array, by
            default "auto"

        Returns
        -------
        da.Array
            A 4D array where the first dimension indexes the antenna id, the second dimension
            indexes the frequency, and the third and fourth dimensions are the rows and columns of
            the image for the primary beam.
        """

        if antenna is None:
            antenna = da.arange(len(self.dish_diameter))
            indices = antenna
            dish_diameter = self.dish_diameter
            telescope = self.telescope
        else:
            # Unique antenna id to avoid redundant computations
            antenna, indices = da.unique(antenna, return_inverse=True)
            antenna.compute_chunk_sizes()
            dish_diameter = self.dish_diameter[antenna]
            telescope = self.telescope[antenna]

        diameters = dish_diameter[:, np.newaxis]  # broadcast diameters
        wavelength = const.c.value / frequency
        theta_fov = wavelength[np.newaxis, :] / diameters  # limit of the angular resolution

        if x_0 is None:
            x_0 = 0

        if y_0 is None:
            y_0 = 0

        if imcenter is None:
            imcenter = [imsize[0] // 2, imsize[1] // 2]

        if cellsize is not None:
            self.cellsize = cellsize

        imchunks = da.core.normalize_chunks(imchunks, shape=imsize, dtype=np.float32)
        # Indices of the image, centering the beam in imcenter
        x = (da.arange(0, imsize[1], chunks=imchunks[1]) - imcenter[0]) * self.cellsize[0].value
        y = (da.arange(0, imsize[0], chunks=imchunks[0]) - imcenter[1]) * self.cellsize[1].value
        # Image grid
        x_grid, y_grid = da.meshgrid(x, y)

        # Vectorized model_function
        vec_model_function = np.vectorize(
            self.model_function,
            otypes=[np.float32],
            excluded=["x_grid", "y_grid"],
            signature="(f),(),(x,y),(x,y),(),()->(f,x,y)"
        )

        # Apply function to every antenna
        beam = da.blockwise(
            self._block_beam,
            ("antenna", "frequency", "im_x", "im_y"),
            theta_fov,
            ("antenna", "frequency"),
            telescope,
            ("antenna", ),
            x_grid,
            ("im_x", "im_y"),
            y_grid,
            ("im_x", "im_y"),
            x_0,
            (),
            y_0,
            (),
            vecfun=vec_model_function,
            dtype=np.float32,
        )

        # Mult with maximum intensity of the pattern at the beam center
        beam *= self.amplitude
        return beam[indices]

    def _compute_raw_gcf(
        self, frequency: Union[float, List[float], np.ndarray, da.Array],
        imsize: Union[Tuple[int, int], List[int]]
    ) -> da.Array:
        """Primary beam of all the antennas for each specified frequency.

        Parameters
        ----------
        frequency : Union[List[float], np.ndarray, da.Array]
            List of float (dimentionless) with the frequency of the light
        imsize : Union[Tuple[int, int], List[int]]
            List or tuple of 2 integers giving the desired size of the image

        Returns
        -------
        da.Array
            A 4D array where the first dimension indexes the antenna id, the second dimension
            indexes the frequency, and the third and fourth dimensions are the rows and columns of
            the image for the primary beam
        """
        if isinstance(frequency, float):
            return self.beam([frequency], imsize)
        else:
            return self.beam(frequency, imsize)

    def model_function(
        self, theta_fov: np.ndarray, telescope: str, x_grid: np.ndarray, y_grid: np.ndarray,
        x_0: Union[float, int], y_0: Union[float, int]
    ) -> np.ndarray:
        """Applies the model function of the antenna according to the
        telescope.

        Parameters
        ----------
        theta_fov : np.ndarray
            Field of view of the antenna. Also known as the limit of the angular resolution/resolving power of the antenna
        telescope : str
            Telescope name
        x_grid : np.ndarray
            Array indexing the 1st dim of the image
        y_grid : np.ndarray
            Array indexing the 2nd dim of the image
        x_0 : Union[int, float]
            x position of the maximum of the Primary beam function
        y_0 : Union[int, float]
            y position of the maximum of the Primary beam function

        Returns
        -------
        np.ndarray
            Primary beam of the antenna
        """
        theta_first_zero = jn_zeros(1, 1)[0] / np.pi  # Approx. 1.22
        if telescope == "ALMA":
            # Scaling factor to match FWHM to 1.13 * λ / D
            scale_factor = 1.13 / theta_first_zero  # This is approx 1.13 / 1.22
            k_aperture = (np.pi / theta_fov) * scale_factor
            return airy_disk(k_aperture, x_grid, y_grid, x_0, y_0)
        else:
            theta_fov = theta_first_zero * theta_fov
            return gaussian(theta_fov, x_grid, y_grid, x_0, y_0)

    def _block_beam(
        self, theta_fov: np.ndarray, telescope: np.ndarray, x_grid: np.ndarray, y_grid: np.ndarray,
        x_0: Union[int, float], y_0: Union[int, float], vecfun: callable
    ) -> np.ndarray:

        beam = vecfun(theta_fov, telescope, x_grid, y_grid, x_0, y_0)

        return beam
