from dataclasses import dataclass
from typing import Tuple

import dask.array as da
import numpy as np

from .c_kernel import CKernel


@dataclass(init=False, repr=True)
class PSWF1(CKernel):
    """
    PSWF1 implements the Prolate Spheroidal Wave Function type 1 for gridding.

    It computes the raw gridding correction function (GCF) in image space using a
    rational polynomial approximation (with coefficients _P_COEFF and _Q_COEFF). The base
    class then uses IFFT2 to obtain the Fourier‐space convolution kernel if needed.

    The native coordinate arrays (from delta_values) are used (without oversampling);
    oversampling is handled in the base class via relative_indexes when performing fractional lookup.
    """

    def __init__(self, **kwargs):
        """
        Initialize a PSWF1 instance.

        Accepts any keyword arguments required by the base class.
        Caches the rational polynomial coefficients for the approximation.
        """
        super().__init__(**kwargs)
        # Coefficients for the numerator (P) of the rational approximation.
        self._P_COEFF = np.array(
            [
                [8.203343e-2, -3.644705e-1, 6.278660e-1, -5.335581e-1, 2.312756e-1],
                [4.028559e-3, -3.697768e-2, 1.021332e-1, -1.201436e-1, 6.412774e-2]
            ]
        )
        # Coefficients for the denominator (Q) of the rational approximation.
        self._Q_COEFF = np.array([[1.0, 8.212018e-1, 2.078043e-1], [1.0, 9.599102e-1, 2.918724e-1]])

    def _compute_raw_gcf(self, imsize: Tuple[int, int]) -> da.Array:
        """
        Compute the raw PSWF Gridding Correction Function (GCF) in image space.

        This raw GCF (unnormalized) is computed using the native coordinate arrays
        (returned by delta_values) and a rational polynomial approximation of the PSWF.
        It does not apply any oversampling directly; oversampling is applied later when
        performing fractional lookup in the gridding process.

        Parameters
        ----------
        imsize : Tuple[int, int]
            The size of the kernel (height, width).

        Returns
        -------
        da.Array
            A Dask array representing the raw (unnormalized) GCF.
        """
        # Determine w (a scaling parameter) from imsize or self.w.
        w = imsize[0] if self.w is None else self.w
        # Get native coordinate arrays from delta_values (do not oversample here)
        x_coords, y_coords = self.delta_values(imsize, deltas=self.cellsize)
        # Compute normalized distances along each axis.
        i_nu = x_coords / (w * self.cellsize.value[1])
        j_nu = y_coords / (w * self.cellsize.value[0])
        # Compute PSWF values along each axis.
        i_values, _ = self.__grdsf(i_nu)
        j_values, _ = self.__grdsf(j_nu)
        # Form the 2D raw gcf by outer product.
        raw_gcf = da.from_array(i_values * j_values[:, np.newaxis])
        return raw_gcf

    def __grdsf(self, nu: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        """
        Compute the gridding spheroidal function using a rational polynomial approximation.

        The function is defined for nu in [0,1]. For nu in [0, 0.75) one set of coefficients
        is used, and for nu in [0.75, 1] another set is used. Values for |nu| > 1 are forced to 0.
        The returned tuple consists of:
          - grdsf: the computed spheroidal function value.
          - kernel_value: defined as (1 - nu**2)*grdsf, representing the value used in gridding.

        Parameters
        ----------
        nu : np.ndarray
            Array of normalized distances (typically in the range [0,1]).

        Returns
        -------
        Tuple[np.ndarray, np.ndarray]
            (grdsf, kernel_value)
        """
        p = self._P_COEFF  # numerator coefficients
        q = self._Q_COEFF  # denominator coefficients
        # Work with the absolute value of nu.
        u = np.abs(nu)

        # Determine which set of coefficients to use.
        part = np.zeros_like(nu, dtype=np.int32)
        part[(nu >= 0.0) & (nu < 0.75)] = 0
        part[(nu >= 0.75) & (nu <= 1.0)] = 1

        # Define the endpoint for the rational approximation.
        nu_end = np.where(part == 0, 0.75, 1.0)
        # Compute the shifted square: nu^2 - nu_end^2.
        del_nu_sq = nu**2 - nu_end**2

        # Evaluate the numerator and denominator polynomials.
        top = np.polyval(p[part, ::-1].T, del_nu_sq)
        bot = np.polyval(q[part, ::-1].T, del_nu_sq)

        # Compute the gridding spheroidal function.
        grdsf = np.where(bot > 0, top / bot, 0.0)
        grdsf[u > 1.0] = 0.0  # Values outside [-1, 1] should be zero

        # Return the gridding correction function (GCF) and the gridding kernel value
        return grdsf, (1 - u**2) * grdsf
