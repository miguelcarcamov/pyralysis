from dataclasses import dataclass
from typing import Tuple

import dask.array as da
import numpy as np
from scipy.special import i0

from .c_kernel import CKernel


@dataclass(init=False, repr=True)
class KaiserBessel(CKernel):
    r"""
    Kaiser‐Bessel gridding kernel.

    This kernel implements the Kaiser‐Bessel function, which is widely used in convolutional
    gridding for radio interferometry. The kernel is defined as:

    .. math::
        K(x) = \frac{I_0\Big(\beta \sqrt{1 - \Big(\frac{2x}{L}\Big)^2}\Big)}{I_0(\beta)}

    for \(|x| \le \frac{L}{2}\) and

    .. math::
        K(x) = 0

    otherwise, where \(I_0\) is the modified Bessel function of order 0, \(\beta\) is a shape
    parameter, and \(L\) is the kernel width.

    **Note:** This implementation currently supports CPU-based computations using Dask arrays.
    GPU/CUDA acceleration is under active development.

    Attributes
    ----------
    beta : float
        The \(\beta\) parameter controlling the shape of the kernel.
    """

    beta: float

    def __init__(self, *, beta: float = 13.9085, **kwargs):
        r"""
        Initialize a KaiserBessel kernel instance.

        Parameters
        ----------
        beta : float, optional
            The \(\beta\) parameter for the Kaiser‐Bessel function (default is 13.9085).
        kwargs :
            Additional keyword arguments passed to the base CKernel class, including:
                - size: Kernel size (int)
                - cellsize: Tuple[float, float] defining the cell size.
                - oversampling_factor: Oversampling factor (int)
                - w: Width parameter for the kernel (float). If None, defaults to the first element of imsize.
        """
        # Initialize the base CKernel with additional parameters
        super().__init__(**kwargs)
        # Set the Kaiser-Bessel beta parameter
        self.beta = beta

    def _compute_raw_kernel(self, imsize: Tuple[int, int]) -> da.Array:
        r"""
        Compute the raw Kaiser‐Bessel kernel (unnormalized) in Fourier space.

        The kernel is computed as a separable 2D function based on the Kaiser‐Bessel formula.
        For each 1D axis, the kernel is given by:

        .. math::
            K(x) = \begin{cases}
                        \frac{I_0\Big(\beta\sqrt{1 - \Big(\frac{2x}{w}\Big)^2}\Big)}{I_0(\beta)} & \text{if } |x| \le \frac{w}{2} \\
                        0 & \text{otherwise}
                  \end{cases}

        where \(w\) is the width parameter (set from the kernel or inferred from \(imsize\)).

        Parameters
        ----------
        imsize : Tuple[int, int]
            The desired output kernel size (height, width).

        Returns
        -------
        da.Array
            A Dask array representing the raw convolution kernel.
        """
        # Determine the width parameter \(w\). If self.w is not set, default to imsize[0].
        w = imsize[0] if self.w is None else self.w

        # Retrieve the oversampling factor.
        os_factor = self.oversampling_factor

        # Compute the effective kernel size (in pixels) in Fourier space:
        # \(\text{effective\_size} = (\text{imsize}[0] \times \text{os\_factor},\, \text{imsize}[1] \times \text{os\_factor})\)
        effective_size = (imsize[0] * int(os_factor), imsize[1] * int(os_factor))

        # Compute the effective UV cell size by scaling the native uvcellsize by \(1/\text{os\_factor}\).
        effective_uvcellsize = self.uvcellsize / int(os_factor)

        # Extract the delta values for the oversampled grid.
        # The 'delta_values' method returns two 1D arrays (for the x and y axes) in the same units as self.cellsize.
        delta_u, delta_v = np.abs(effective_uvcellsize.value)
        i_deltas, j_deltas = self.delta_values(effective_size, deltas=effective_uvcellsize)

        # Define the support masks for each axis.
        # The support is defined as the range where:
        # \(\lvert x \rvert \le \frac{w \cdot \lvert \delta \rvert}{2}\)
        mask_i = np.abs(i_deltas) <= ((w * delta_v) / 2)
        mask_j = np.abs(j_deltas) <= ((w * delta_u) / 2)

        # Initialize arrays for the 1D Kaiser‐Bessel weights with zeros.
        i_vals = np.zeros_like(i_deltas, dtype=np.float32)
        j_vals = np.zeros_like(j_deltas, dtype=np.float32)

        # Compute the Kaiser‐Bessel weight for the first axis for indices within the support.
        # The weight is given by:
        # \[
        # K(x) = \frac{I_0\Big(\beta\sqrt{1 - \Big(\frac{2x}{w \cdot \lvert \delta_v \rvert}\Big)^2}\Big)}{I_0(\beta)}
        # \]
        ratio_i = (2 * i_deltas[mask_i] / (w * delta_v))**2
        sqrt_arg_i = np.clip(1 - ratio_i, 0, 1)
        i_vals[mask_i] = i0(self.beta * np.sqrt(sqrt_arg_i)) / i0(self.beta)

        # Similarly, compute the Kaiser‐Bessel weight for the second axis.
        ratio_j = (2 * j_deltas[mask_j] / (w * delta_u))**2
        sqrt_arg_j = np.clip(1 - ratio_j, 0, 1)
        j_vals[mask_j] = i0(self.beta * np.sqrt(sqrt_arg_j)) / i0(self.beta)

        # Construct the separable 2D kernel as the outer product of the two 1D weight arrays.
        # \[
        # \text{raw\_kernel} = i\_vals \otimes j\_vals
        # \]
        raw_kernel = da.from_array(i_vals * j_vals[:, np.newaxis])

        return raw_kernel
