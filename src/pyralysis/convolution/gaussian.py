from dataclasses import dataclass
from typing import Tuple

import dask.array as da
import numpy as np

from .c_kernel import CKernel


@dataclass(init=False, repr=True)
class Gaussian(CKernel):
    """
    Gaussian gridding kernel defined in image space.
    """
    a: float

    def __init__(self, *, a: float = 2.0, **kwargs):
        super().__init__(**kwargs)
        self.a = a

    def _compute_raw_gcf(self, imsize: Tuple[int, int]) -> da.Array:
        """
        Compute the raw Gaussian Gridding Correction Function (GCF) in image space (unnormalized).
        """
        w = imsize[0] if self.w is None else self.w
        delta_x, delta_y = self.cellsize.value
        i_deltas, j_deltas = self.delta_values(imsize, deltas=self.cellsize)

        i_values = np.exp(-np.power(np.abs(i_deltas) / (w * delta_y), self.a))
        j_values = np.exp(-np.power(np.abs(j_deltas) / (w * delta_x), self.a))

        raw_gcf = da.from_array(i_values * j_values[:, np.newaxis])
        return raw_gcf
