import copy
import operator
from abc import ABCMeta, abstractmethod
from dataclasses import dataclass
from typing import Callable, List, Tuple, Union

import astropy.units as un
import dask.array as da
import numpy as np
from astropy.units.quantity import Quantity
from numpy import ndarray

from ..fft import fft2, ifft2
from ..utils.cellsize import cellsize as cellsize_check_set


@dataclass(init=False, repr=True)
class CKernel(metaclass=ABCMeta):
    size: int
    cellsize: Tuple[float, float]
    oversampling_factor: int
    w: float

    def __init__(
        self,
        *,
        size: int = 3,
        cellsize: Union[float, Tuple[float, float], Quantity, List[Quantity]],
        oversampling_factor: int = None,
        w: float = None,
    ):
        if cellsize is None:
            raise ValueError("For a CKernel instance, the attribute cellsize should not be None")

        self.size = size
        self.cellsize = cellsize_check_set(cellsize)
        self.oversampling_factor = int(
            oversampling_factor
        ) if oversampling_factor is not None else 1
        self.w = w
        self._relative_indexes = None  # Cached property

    @property
    def support(self) -> int:
        """Returns half the kernel size (integer division)."""
        return self.size // 2

    @property
    def cellsize(self) -> Tuple[float, float]:
        return self._cellsize

    @cellsize.setter
    def cellsize(self, value: Union[float, Tuple[float, float], Quantity, List[Quantity]]) -> None:
        # Check cellsize and assign value per type
        self._cellsize = cellsize_check_set(value)

    @property
    def uvcellsize(self) -> Tuple[float, float]:
        """Compute UV cell size as 1/(kernel_size * cellsize)."""
        return Quantity([1 / (self.size * self.cellsize[0]), 1 / (self.size * self.cellsize[1])])

    @property
    def relative_indexes(self) -> Tuple[np.ndarray, np.ndarray]:
        """
        Returns a cached meshgrid of relative indexes for the kernel as integers.

        The native kernel (of size self.size, with support = size//2) has native relative
        indexes given by:
            native = np.arange(-self.support, self.support + 1)
        For example, if support==1 then native = [-1, 0, 1].

        When oversampling_factor > 1, we want to identify the oversampled positions corresponding
        to the native grid. This is done by scaling the native indexes:
            scaled = native * oversampling_factor
        For example, if oversampling_factor == 2 then scaled = [-2, 0, 2].

        The returned meshgrid has shape (2*support+1, 2*support+1) (i.e. the native kernel shape)
        with these scaled values.
        """
        if self._relative_indexes is None:
            native = np.arange(-self.support, self.support + 1, dtype=np.int32)
            scaled = native * self.oversampling_factor
            self._relative_indexes = np.meshgrid(scaled, scaled, indexing="ij")
        return self._relative_indexes

    def _downsample_kernel(self, os_kernel: da.Array) -> da.Array:
        """
        Given an oversampled kernel (computed on a grid),
        extract one sample every os_factor pixels in each dimension
        to recover a kernel on the native grid.

        The oversampling factor is taken from self.oversampling_factor.
        """
        os_factor = int(self.oversampling_factor)
        # Extract samples starting at the central pixel of the first block
        native_kernel = os_kernel[os_factor // 2::os_factor, os_factor // 2::os_factor]
        return native_kernel

    def _compute_raw_kernel(self, imsize: Tuple[int, int]) -> da.Array:
        """
        Compute the Fourier‐space convolution kernel.
        If a subclass implements _compute_raw_kernel, use it; otherwise, compute it from the GCF.
        """
        if hasattr(self, "_compute_raw_gcf") and callable(getattr(self, "_compute_raw_gcf")):
            raw_gcf = self._compute_raw_gcf(imsize)
            # Compute kernel via FFT of the GCF.
            return fft2(
                raw_gcf,
                sign_convention="positive",
                apply_fftshift=True,
                padding_factor=self.oversampling_factor,
                restore_chunks=True
            )
        else:
            raise NotImplementedError(
                "Subclass must implement _compute_raw_kernel or _compute_raw_gcf."
            )

    def _compute_raw_gcf(self, imsize: Tuple[int, int]) -> da.Array:
        """
        Compute the Gridding Correction Function (GCF) in image space.
        If a subclass implements _compute_raw_gcf, use it; otherwise, compute the
        kernel first (using _compute_raw_kernel) and then compute the GCF as ifft2(kernel).
        """
        if hasattr(self, "_compute_raw_kernel") and callable(getattr(self, "_compute_raw_kernel")):
            raw_kernel = self._compute_raw_kernel(imsize)
            if self.oversampling_factor > 1:
                native_kernel = self._downsample_kernel(raw_kernel)
            else:
                native_kernel = raw_kernel
            raw_gcf = ifft2(native_kernel, sign_convention="positive", apply_fftshift=True)
            return raw_gcf
        else:
            raise NotImplementedError(
                "Subclass must implement _compute_raw_kernel or _compute_raw_gcf."
            )

    def gcf(self, imsize: Tuple[int, int]) -> da.Array:
        """
        Returns the gridding correction function (GCF) in image space.
        The raw GCF is normalized by its maximum.
        """
        raw = self._compute_raw_gcf(imsize).astype(np.float32)
        return raw / raw.max()

    def kernel(self, imsize: Tuple[int, int], half: bool = False) -> da.Array:
        """
        Returns the gridding convolution function (kernel) in Fourier space.
        The raw kernel is normalized by its sum.
        """
        # Compute the raw kernel
        raw = self._compute_raw_kernel(imsize).astype(np.complex64)
        raw = da.maximum(raw.real, 0.0) + 1j * da.maximum(raw.imag, 0.0)

        # Determine the normalization factor from the native kernel:
        if self.oversampling_factor > 1:
            # Extract the native kernel from the oversampled one.
            native_kernel = self._downsample_kernel(raw)
            # Compute the normalization factor from the native kernel.
            # (Make sure to do this without further normalization inside _downsample_kernel.)
            global_norm = native_kernel.sum()
        else:
            global_norm = raw.sum()

        # If the imaginary part is negligible compared to the real part, take only the real part.
        if abs(global_norm.imag) < 1e-6 * abs(global_norm.real):
            global_norm = global_norm.real

        # Normalize the full oversampled kernel using the native kernel's sum.
        full_kernel = raw / global_norm

        if half:
            # Return only the quadrant with nonnegative offsets.
            # (Assuming full_kernel is symmetric about its center.)
            center = full_kernel.shape[0] // 2
            half_kernel = full_kernel[center:, center:]
            return half_kernel
        else:
            return full_kernel

    @staticmethod
    def delta_values(size: Union[int, Tuple[int, int]],
                     deltas: Tuple[float, float]) -> Tuple[ndarray, ndarray]:
        """
        Generates native delta grid values (without oversampling) for interpolation.
        Returns two 1D arrays for the x and y directions (no meshgrid).
        The array length equals the kernel size (imsize).
        Works for both odd and even sizes.
        """
        if isinstance(size, int):
            size = (size, size)

        support_x, support_y = size[1] // 2, size[0] // 2
        upper_x, upper_y = support_x + (size[1] % 2), support_y + (size[0] % 2)
        lower_x, lower_y = -support_x, -support_y

        delta_i, delta_j = deltas.value
        return (
            np.arange(lower_x, upper_x, dtype=np.float32) * delta_j,
            np.arange(lower_y, upper_y, dtype=np.float32) * delta_i,
        )
