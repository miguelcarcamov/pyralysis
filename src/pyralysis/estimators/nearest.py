from dataclasses import dataclass
from typing import Union

import dask.array as da
import numpy as np

from ..transformers import ModelVisibilities
from ..utils.decorators import temp_local_save
from ..utils.interpolations import nearest_neighbor


@dataclass(init=False, repr=True)
class NearestNeighbor(ModelVisibilities):
    """
    Subclass of ModelVisibilities that implements the visibility estimation using the nearest-neighbor
    interpolation method.

    Nearest-neighbor interpolation extracts the grid value corresponding to the nearest integer UV coordinate.
    This subclass uses the nearest_neighbor function to obtain the estimated visibilities from the Fourier grid
    (FFT of the image). (Note: Other subclasses may use alternative methods such as degridding or bilinear interpolation.)
    """

    def __init__(self, **kwargs):
        # Initialize the base ModelVisibilities class with any provided keyword arguments.
        super().__init__(**kwargs)

    @temp_local_save
    def estimate_visibility(
        self,
        grid_fft: da.Array,
        uv: tuple,
        kernel: Union[da.Array, np.ndarray] = None
    ) -> da.Array:
        """
        Estimate the model visibilities using nearest-neighbor interpolation.

        This method extracts visibility values directly from the Fourier grid (grid_fft) by selecting the value
        at the grid cell corresponding to each UV coordinate using nearest-neighbor interpolation. The UV arrays
        are reshaped if needed to ensure that they have two dimensions with shape (n, frequency) so that the
        frequency axis aligns with grid_fft. The method then applies Dask's map_blocks to process the data in blocks.

        Parameters
        ----------
        grid_fft : da.Array
            The Fourier transform (FFT) of the image.
        uv : tuple
            A tuple of two arrays representing the u and v coordinates where the visibilities should be extracted.
            These arrays indicate the cell indices on the grid.
        kernel : Union[da.Array, np.ndarray], optional
            Not used in this method. (Included for compatibility with the abstract interface.)

        Returns
        -------
        da.Array
            The estimated visibilities, with the same shape as the UV coordinate arrays.
        """
        # Ensure that the UV coordinate arrays have two dimensions (n, frequency).
        # Reshape the arrays if they are not already 2D.
        if uv[0].ndim != 2:
            uv = tuple(x.reshape((-1, grid_fft.shape[0])) for x in uv)
        elif uv[0].shape[1] != grid_fft.shape[0]:
            uv = tuple(x.reshape((-1, grid_fft.shape[0])) for x in uv)

        # Create an empty meta array to inform Dask of the output shape and data type.
        meta = np.empty(uv[0].shape, dtype=grid_fft.dtype)

        # Use Dask's map_blocks to apply the nearest_neighbor function blockwise.
        # The nearest_neighbor function takes the following arguments:
        #   nearest_neighbor(u, v, im, enforce_sorting, is_hermitian)
        # Here:
        #   - uv[0] and uv[1] are the u and v coordinates,
        #   - grid_fft is the Fourier grid,
        #   - enforce_sorting is set to False,
        #   - self.hermitian_symmetry indicates if the grid has Hermitian symmetry.
        data = da.map_blocks(
            nearest_neighbor,
            uv[0],
            uv[1],
            grid_fft,
            False,  # enforce_sorting flag (not applied here)
            self.hermitian_symmetry,
            dtype=grid_fft.dtype,
            chunks=uv[0].chunks,
            drop_axis=0,  # The function returns a scalar for each block, so drop the extra axis.
            meta=meta
        )

        return data
