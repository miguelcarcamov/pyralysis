from dataclasses import dataclass
from typing import Union

import dask.array as da
import numpy as np

from ..transformers import ModelVisibilities
from ..utils.decorators import temp_local_save
from ..utils.interpolations import degridding


@dataclass(init=False, repr=True)
class Degridding(ModelVisibilities):
    """
    Subclass of ModelVisibilities that implements the visibility estimation using the degridding method.

    Degridding refers to the process of interpolating values from a gridded Fourier transform using a convolution
    kernel. In this class, the estimated visibilities are computed from the FFT of the image by extracting values
    at the given UV coordinates using a half–kernel. (Note: This is one of several possible estimation methods. Other
    subclasses may implement nearest neighbor or bilinear interpolation instead.)
    """

    def __init__(self, **kwargs):
        # Initialize the base ModelVisibilities class with any provided keyword arguments.
        super().__init__(**kwargs)

    @temp_local_save
    def estimate_visibility(
        self,
        grid_fft: da.Array,
        uv: tuple,
        kernel: Union[da.Array, np.ndarray] = None
    ) -> da.Array:
        """
        Estimate the model visibilities using the degridding method.

        This method extracts visibility values from the Fourier grid (grid_fft) by applying a degridding
        algorithm. Degridding uses a half–kernel (which may be oversampled) to perform convolutional extraction
        around each UV coordinate. The abstract method `estimate_visibility` can be implemented with various
        estimation techniques (e.g., nearest neighbor, bilinear interpolation, or degridding); this subclass uses
        degridding.

        Parameters
        ----------
        grid_fft : da.Array
            The Fourier transform of the image (e.g., with shape (frequency, m, n)).
        uv : tuple
            A tuple of two arrays representing the u and v coordinates where the visibilities are to be extracted.
            These arrays can be 0‑d or 1‑d; the core dimension is expected to match the frequency dimension of grid_fft.
        kernel : Union[da.Array, np.ndarray], optional
            The half–kernel used for degridding. The kernel represents only the nonnegative quadrant of the full
            convolution kernel. If None, an identity kernel (i.e. a pass–through) is used.

        Returns
        -------
        da.Array
            The estimated visibilities (an array with the same shape as the provided UV coordinate arrays).

        Notes
        -----
        - The degridding process here involves applying a convolution with a half–kernel over the Fourier grid.
          The full kernel is later reconstructed using the symmetry properties of the convolution kernel.
        - If a kernel object (ckernel_object) is provided and it defines an oversampling factor, that value is used;
          otherwise, the oversampling factor defaults to 1.
        - This method uses Dask's `map_blocks` to apply the degridding function over the blocks of the UV arrays.
        """
        # Ensure that the UV coordinate arrays have two dimensions with shape (n, frequency).
        # If they do not, reshape them accordingly.
        if uv[0].ndim != 2:
            uv = tuple(x.reshape((-1, grid_fft.shape[0])) for x in uv)
        elif uv[0].shape[1] != grid_fft.shape[0]:
            uv = tuple(x.reshape((-1, grid_fft.shape[0])) for x in uv)

        # If no kernel is provided, use an identity kernel (1x1 ones) so that the degridding operation
        # effectively passes through the grid_fft values.
        if kernel is None:
            kernel = np.ones((1, 1), dtype=np.float32)

        # Determine the oversampling factor.
        # If a ckernel_object exists and has an attribute 'oversampling_factor', use it; otherwise, default to 1.
        if self.ckernel_object is not None and hasattr(self.ckernel_object, 'oversampling_factor'):
            os_factor = int(self.ckernel_object.oversampling_factor)
        else:
            os_factor = 1

        # Create an empty metadata array that has the same shape as the UV coordinate array.
        # This array serves to inform Dask about the expected output shape and dtype.
        meta = np.empty(uv[0].shape, dtype=grid_fft.dtype)

        # Apply the top-level degridding function to each block using Dask's map_blocks.
        # The degridding function signature is:
        #   degridding(u, v, im, kernel, oversampling_factor, is_hermitian)
        # where:
        #   - uv[0] and uv[1] provide the u and v coordinates.
        #   - grid_fft is the Fourier grid of the image.
        #   - kernel is the half–kernel.
        #   - os_factor is the oversampling factor.
        #   - self.hermitian_symmetry is a flag indicating if grid_fft is Hermitian symmetric.
        data = da.map_blocks(
            degridding,
            uv[0],
            uv[1],
            grid_fft,
            kernel,
            os_factor,
            self.hermitian_symmetry,
            dtype=grid_fft.dtype,
            chunks=uv[0].chunks,
            drop_axis=
            0,  # The degridding function returns a scalar per block, so drop the extra axis.
            meta=meta
        )

        return data
