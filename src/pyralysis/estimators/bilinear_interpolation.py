from dataclasses import dataclass
from typing import Union

import dask.array as da
import numpy as np

from ..transformers import ModelVisibilities
from ..utils.decorators import temp_local_save
from ..utils.interpolations import bilinear_interpolation


@dataclass(init=False, repr=True)
class BilinearInterpolation(ModelVisibilities):
    """
    Subclass of ModelVisibilities that implements the visibility estimation using
    bilinear interpolation.

    The bilinear interpolation method is used to estimate the model visibilities
    from the Fourier grid (grid_fft) by interpolating between the four nearest grid
    points for each UV coordinate.
    """

    def __init__(self, **kwargs):
        # Initialize the base ModelVisibilities class with any provided keyword arguments.
        super().__init__(**kwargs)

    @temp_local_save
    def estimate_visibility(
        self,
        grid_fft: da.Array,
        uv: tuple,
        kernel: Union[da.Array, np.ndarray] = None
    ) -> da.Array:
        """
        Estimate visibilities using bilinear interpolation over the Fourier grid.

        This method maps the bilinear interpolation function over the blocks of the
        provided UV arrays and the Fourier grid. The UV tuple should contain two arrays
        (for the u and v coordinates, respectively) that indicate the grid cell indices
        where each visibility should be extracted.

        Parameters
        ----------
        grid_fft : da.Array
            The Fourier transform of the image (with shape (channels, m, n)).
        uv : tuple
            A tuple with two arrays, containing the u and v indices (as floats) at which
            the interpolation should be performed. Both arrays are expected to have shape
            (rows, channels).
        kernel : Union[da.Array, np.ndarray], optional
            This parameter is accepted for consistency with other interpolation methods.
            However, for bilinear interpolation, it is not used.

        Returns
        -------
        da.Array
            The estimated visibilities, with shape (rows, channels).

        Notes
        -----
        The decorator @temp_local_save may be used to temporarily persist intermediate
        results or manage temporary file storage, depending on its implementation.
        """
        # Ensure the uv arrays have 2 dimensions with shape (rows, frequency).
        # If they do not, reshape them to have the correct dimensions.
        if uv[0].ndim != 2:  # UV arrays should have dimensions (rows, frequency)
            uv = tuple(x.reshape((-1, grid_fft.shape[0])) for x in uv)
        elif uv[0].shape[1] != grid_fft.shape[0]:
            # If the frequency dimension does not match the number of channels in grid_fft,
            # reshape the uv arrays accordingly.
            uv = tuple(x.reshape((-1, grid_fft.shape[0])) for x in uv)

        # Create an empty metadata array matching the desired output shape.
        meta = np.empty(uv[0].shape, dtype=grid_fft.dtype)

        # Use dask.map_blocks to apply the bilinear interpolation function to each block.
        # The function bilinear_interpolation is applied with:
        #    - uv[0]: the u coordinates for the current block.
        #    - uv[1]: the v coordinates for the current block.
        #    - grid_fft: the Fourier grid from which to interpolate.
        # The result is a Dask array with the same chunk structure as uv[0].
        data = da.map_blocks(
            bilinear_interpolation,
            uv[0],
            uv[1],
            grid_fft,
            dtype=grid_fft.dtype,
            chunks=uv[0].chunks,
            drop_axis=0,  # Remove the first axis from the function output (if applicable)
            meta=meta
        )

        return data
