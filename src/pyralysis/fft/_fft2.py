from typing import Union

import dask.array as da
import numpy as np

from ..utils.padding import apply_padding

__all__ = ["fft2", "ifft2"]


def fft2(
    image: Union[da.Array, np.ndarray],
    sign_convention: str = "negative",
    padding_factor: float = 1.0,
    hermitian_symmetry: bool = False,
    apply_fftshift: bool = True,
    restore_chunks: bool = True
) -> Union[da.Array, np.ndarray]:
    """Fast Fourier Transform 2D (FFT2) that supports different sign
    conventions, padding, hermitian symmetry and FFT shifting.

    By default this function will calculate

    .. math::
        A_{kl} = \\sum_{m=0}^{M-1}\\sum_{n=0}^{N-1} a_{mn}\\exp{\\Biggl\\{-2\\pi i \\Bigl(\\frac{mk}{M}+\\frac{nl}{N}\\Bigr) \\Biggr\\}}\\qquad k=0,...,M-1;\\quad l=0,...,N-1

    If sign_convention parameter is set to True then it will calculate

    .. math::
        A_{kl} = \\sum_{m=0}^{M-1}\\sum_{n=0}^{N-1} a_{mn}\\exp{\\Biggl\\{2\\pi i \\Bigl(\\frac{mk}{M}+\\frac{nl}{N}\\Bigr) \\Biggr\\}}\\qquad k=0,...,M-1;\\quad l=0,...,N-1

    Parameters
    ----------
    image : Union[da.Array, np.ndarray]
        Input signal/image 2-d array.
    sign_convention : str, optional
        Fourier transform sign convention that can take values such as "positive" or "negative", by default "negative".
    padding_factor : float, optional
        Padding factor that increases or decreases image shape, by default 1.0 which does not changes the input shape.
    hermitian_symmetry : bool, optional
        Whether to use rfft2 and return only half of the Fourier grid, by default False.
    apply_fftshift : bool, optional
        Whether to shift the zero-frequency component to the center of the spectrum (center the Fourier DC to (M//2,N//2) or (0, M//2) if hermitian_symmetry is set to True), by default True.
    restore_chunks : bool, optional
        Whether to restore the original chunking after the FFT operation, by default True.

    Returns
    -------
    Union[da.Array, np.ndarray]
        The truncated or zero-padded input, transformed along the last two axes.

    Raises
    ------
    ValueError
        If sign_convention is different from "negative" or "positive"
    """
    image_dtype = image.dtype
    original_chunks = image.chunks

    # Handle padding if required
    if padding_factor > 1.0:
        image, _, original_chunks = apply_padding(image, padding_factor)

    if sign_convention not in ["negative", "positive"]:
        raise ValueError("Sign convention argument can only be 'positive' or 'negative'")

    # Rechunk along the last two axes to a single chunk
    image = image.rechunk({-2: -1, -1: -1})

    if apply_fftshift:
        image = da.fft.ifftshift(image, axes=(-2, -1))

    if sign_convention == "negative":
        if hermitian_symmetry:
            fourier = da.fft.rfft2(image)
        else:
            fourier = da.fft.fft2(image)
    else:
        if hermitian_symmetry:
            fourier = da.fft.rfft2(image).conj()
        else:
            fourier = da.fft.ifft2(image, norm="forward")
    if apply_fftshift:
        if hermitian_symmetry:
            fourier = da.fft.fftshift(fourier, axes=-2)
        else:
            fourier = da.fft.fftshift(fourier, axes=(-2, -1))

    # Optionally restore chunks
    if restore_chunks:
        inferred_chunks = list(original_chunks)
        if hermitian_symmetry:
            # Use the updated _infer_chunks to handle N//2 + 1 cases
            inferred_chunks[-1] = _infer_chunks(original_chunks[-1], full_size=fourier.shape[-1])
        fourier = fourier.rechunk(tuple(inferred_chunks))

    # Cast to appropriate data type
    if image_dtype.type == np.float32 or image_dtype.type == np.complex64:
        fourier = fourier.astype(np.complex64)

    return fourier


def ifft2(
    fourier: Union[da.Array, np.ndarray],
    s: tuple = None,
    sign_convention: str = "negative",
    hermitian_symmetry: bool = False,
    apply_fftshift: bool = True,
    restore_chunks: bool = True
) -> Union[da.Array, np.ndarray]:
    """Inverse Fast Fourier Transform 2D (IFFT2) that supports different sign
    conventions, padding, hermitian symmetry and FFT shifting.

    The inverse DFT is defined and calculated by default as

    .. math::
        a_{mn} = \\frac{1}{MN}\\sum_{k=0}^{M-1}\\sum_{l=0}^{N-1} A_{kl}\\exp{\\Biggl\\{2\\pi i \\Bigl(\\frac{mk}{M}+\\frac{nl}{N}\\Bigr) \\Biggr\\}}\\qquad m=0,...,M-1;\\quad n=0,...,N-1

    If sign_convention parameter is set to True then it will calculate

    .. math::
        a_{mn} = \\frac{1}{MN}\\sum_{k=0}^{M-1}\\sum_{l=0}^{N-1} A_{kl}\\exp{\\Biggl\\{-2\\pi i \\Bigl(\\frac{mk}{M}+\\frac{nl}{N}\\Bigr) \\Biggr\\}}\\qquad m=0,...,M-1;\\quad n=0,...,N-1


    Parameters
    ----------
    fourier : Union[da.Array, np.ndarray]
        Input Fourier transformed 2-d array
    s : tuple, sequence of ints, optional
        Shape (length of each axis) of the output (``s[0]`` refers to axis 0,
        ``s[1]`` to axis 1, etc.).  This corresponds to `n` for ``ifft2(x, n)``.
        Along each axis, if the given shape is smaller than that of the input,
        the input is cropped.  If it is larger, the input is padded with zeros.
        if `s` is not given, the shape of the input along the axes specified
        by `axes` is used.  See notes for issue on `ifft` zero padding, by default None.
    sign_convention : str, optional
        Fourier transform sign convention that can take values such as "positive" or "negative", by default "negative".
    hermitian_symmetry : bool, optional
        Whether to use irfft2 and return a real signal from half of the Fourier grid, by default False.
    apply_fftshift : bool, optional
        Whether to shift the zero-frequency component (Fourier DC) to (0,0) if it is already in (M//2, N//2), by default True.
    restore_chunks : bool, optional
        Whether to restore the original chunking after the IFFT operation, by default True.

    Returns
    -------
    Union[da.Array, np.ndarray]
        The truncated or zero-padded input, transformed along the axes indicated by axes, or the last two axes if axes is not given.

    Raises
    ------
    ValueError
        If sign_convention is different from "negative" or "positive".
    """

    fourier_dtype = fourier.dtype
    original_chunks = fourier.chunks

    if sign_convention not in ["negative", "positive"]:
        raise ValueError("Sign convention argument can only be 'positive' or 'negative'")

    ifft_axes = (-2, -1)

    # Rechunk to single chunk along the axes for IFFT
    fourier = fourier.rechunk({-2: -1, -1: -1})

    if apply_fftshift:
        if hermitian_symmetry:
            fourier = da.fft.ifftshift(fourier, axes=-2)
        else:
            fourier = da.fft.ifftshift(fourier, axes=ifft_axes)

    if sign_convention == "negative":
        if hermitian_symmetry:
            image = da.fft.irfft2(fourier, s=s, axes=ifft_axes)
        else:
            image = da.fft.ifft2(fourier, s=s, axes=ifft_axes)
    else:
        if hermitian_symmetry:
            image = da.fft.irfft2(fourier.conj(), s=s, axes=ifft_axes)
        else:
            image = da.fft.fft2(fourier, s=s, axes=ifft_axes, norm="forward")

    if apply_fftshift:
        image = da.fft.fftshift(image, axes=ifft_axes)

    # Optionally restore chunks
    if restore_chunks:
        inferred_chunks = list(original_chunks)
        if hermitian_symmetry:
            # Calculate exact chunk sizes for the last dimension
            last_dim_chunks = original_chunks[-1]
            full_size = image.shape[-1]
            inferred_last_dim_chunks = _infer_chunks(last_dim_chunks, full_size)
            inferred_chunks[-1] = inferred_last_dim_chunks
        image = image.rechunk(tuple(inferred_chunks))

    # Cast to appropriate data type
    if fourier_dtype.type == np.complex64:
        if image.dtype.type == np.float64:
            image = image.astype(np.float32)
        elif image.dtype.type == np.complex128:
            image = image.astype(np.complex64)

    return image.real


def _infer_chunks(reduced_chunks: tuple, full_size: int) -> tuple:
    """Infer the chunk sizes for the full-size dimension from the reduced-size
    dimension.

    Parameters
    ----------
    reduced_chunks : tuple
        Chunks in the reduced-size dimension (Hermitian symmetry, n//2 + 1).
    full_size : int
        Full size of the output dimension (n).

    Returns
    -------
    tuple
        Inferred chunk sizes for the full-size dimension.
    """
    inferred_chunks = []
    cumulative_size = 0

    for chunk in reduced_chunks:
        inferred_chunk = 2 * (chunk - 1) + 2
        inferred_chunks.append(inferred_chunk)
        cumulative_size += inferred_chunk

        # Adjust the last chunk to match the exact size
        if cumulative_size >= full_size:
            inferred_chunks[-1] -= (cumulative_size - full_size)
            break

    return tuple(inferred_chunks)
