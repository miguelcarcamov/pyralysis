from abc import ABCMeta, abstractmethod
from dataclasses import dataclass
from typing import Optional, Union

import dask.array as da

from ..reconstruction.image import Image
from .intensity_model import IntensityModel


@dataclass(init=True, repr=True)
class PowerLawIntensityModel(IntensityModel):
    """Power-law intensity model with spectral index and curvature.

    Parameters
    ----------
    reference_frequency : float
        Reference frequency for the power-law model.
    I_nu_0_index : int, optional
        Index of \\(I_{\nu_0}\\) in the input image, by default 0.
    alpha_index : Optional[int], optional
        Index of \\(\alpha\\) in the input image. If not specified, \\(\alpha\\) is assumed to be 0, by default 1.
    beta_index : Optional[int], optional
        Index of \\(\beta\\) in the input image. If not specified, \\(\beta\\) is assumed to be 0, by default 2.
    stokes_axis : int, optional
        Axis representing the Stokes parameters, by default 0.
    alpha : Optional[Union[int, float, da.Array]], optional
        Value or array for \\(\alpha\\). Used if not present in the input image. Default is 0.
    beta : Optional[Union[int, float, da.Array]], optional
        Value or array for \\(\beta\\). Used if not present in the input image. Default is 0.
    """
    I_nu_0_index: int = 0
    alpha_index: Optional[int] = 1
    beta_index: Optional[int] = 2
    stokes_axis: int = 0  # Default axis for Stokes parameters
    alpha: Optional[Union[int, float, da.Array]] = None
    beta: Optional[Union[int, float, da.Array]] = None

    def apply_model(self, image: Union[Image, da.Array], frequencies: da.Array) -> da.Array:
        """Apply the power-law model to the given image.

        This method scales the image intensity as a function of frequency using the power-law formula:

        \\[
        I(\nu) = I_{\nu_0} \\left( \frac{\nu}{\nu_\text{ref}} \right)^{\alpha + \beta \\log_{10}\\left( \frac{\nu}{\nu_\text{ref}} \right)}
        \\]

        Parameters
        ----------
        image : Union[Image, da.Array]
            Input image or array.
            - If 1D, it is assumed to contain scalar values for \\(I_{\nu_0}\\), \\(\alpha\\), and \\(\beta\\).
            - If 2D, it is assumed to contain only \\(I_{\nu_0}\\).
            - If 3D, the indices for \\(I_{\nu_0}\\), \\(\alpha\\), and \\(\beta\\) are used to extract components.
            - If 4D, each Stokes component has its own \\(I_{\nu_0}\\), \\(\alpha\\), and \\(\beta\\).
        frequencies : da.Array
            Array of frequencies at which the model is evaluated.

        Returns
        -------
        da.Array
            The image or scalar intensities scaled according to the power-law model.
        """
        if isinstance(image, Image):
            image_data = image.data.data
        elif isinstance(image, da.Array):
            image_data = image
        else:
            raise TypeError("Input image must be of type Image or dask.array")

        # Extract I_nu_0, alpha, and beta based on dimensionality
        I_nu_0, alpha, beta = self._extract_parameters(image_data)

        # Calculate frequency ratio
        freq_ratio = frequencies / self.reference_frequency
        expanded_freq_ratio = da.expand_dims(freq_ratio, axis=tuple(range(1, I_nu_0.ndim + 1)))
        expanded_freq_ratio = da.broadcast_to(
            expanded_freq_ratio, (freq_ratio.shape[0], ) + I_nu_0.shape
        )

        # Calculate scaling factor
        scaling = expanded_freq_ratio**(alpha + beta * da.log10(expanded_freq_ratio))

        # Apply scaling to the reference intensity
        result = I_nu_0 * scaling

        # Reorder axes for 4D case
        if image_data.ndim == 4:
            # After broadcasting, the Stokes axis is at position 1 (freq, stokes, pix, pix)
            current_stokes_axis = 1
            if self.stokes_axis != current_stokes_axis:
                # Move the Stokes axis back to its original position
                result = da.moveaxis(result, current_stokes_axis, self.stokes_axis)

        return result

    def _extract_parameters(self, image_data: da.Array):
        """Extract I_nu_0, alpha, and beta based on image dimensionality."""
        if image_data.ndim == 1:
            return self._extract_1d_parameters(image_data)
        elif image_data.ndim == 2:
            return self._extract_2d_parameters(image_data)
        elif image_data.ndim == 3:
            return self._extract_3d_parameters(image_data)
        elif image_data.ndim == 4:
            return self._extract_4d_parameters(image_data)
        else:
            raise ValueError("Image data must be 1D, 2D, 3D, or 4D.")

    def _extract_1d_parameters(self, image_data: da.Array):
        """Extract parameters for 1D image."""
        I_nu_0 = image_data[self.I_nu_0_index]
        alpha = image_data[self.alpha_index] if self.alpha_index < image_data.size else 0
        beta = image_data[self.beta_index] if self.beta_index < image_data.size else 0
        return I_nu_0, alpha, beta

    def _extract_2d_parameters(self, image_data: da.Array):
        """Extract parameters for 2D image."""
        # Use alpha and beta scalars if provided; otherwise, assume 0
        alpha = self.alpha if self.alpha is not None else 0
        beta = self.beta if self.beta is not None else 0
        return image_data, alpha, beta

    def _extract_3d_parameters(self, image_data: da.Array):
        """Extract parameters for 3D image."""
        I_nu_0 = image_data[self.I_nu_0_index, ...]
        alpha = (
            image_data[self.alpha_index,
                       ...] if self.alpha_index < image_data.shape[0] else da.zeros_like(I_nu_0)
        )
        beta = (
            image_data[self.beta_index,
                       ...] if self.beta_index < image_data.shape[0] else da.zeros_like(I_nu_0)
        )
        return I_nu_0, alpha, beta

    def _extract_4d_parameters(self, image_data: da.Array):
        """Extract parameters for 4D image."""
        param_axis = set(range(image_data.ndim)) - {self.stokes_axis, -2, -1}
        print(param_axis)
        if len(param_axis) < 1:
            raise ValueError("Unable to determine parameter axis.")
        param_axis = param_axis.pop()

        I_nu_0 = da.take(image_data, self.I_nu_0_index, axis=param_axis)
        alpha = (
            da.take(image_data, self.alpha_index, axis=param_axis)
            if self.alpha_index < image_data.shape[param_axis] else da.zeros_like(I_nu_0)
        )
        beta = (
            da.take(image_data, self.beta_index, axis=param_axis)
            if self.beta_index < image_data.shape[param_axis] else da.zeros_like(I_nu_0)
        )
        return I_nu_0, alpha, beta
