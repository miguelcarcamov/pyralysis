from .intensity_model import IntensityModel
from .power_law import PowerLawIntensityModel
