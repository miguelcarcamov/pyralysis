from abc import ABCMeta, abstractmethod
from dataclasses import dataclass
from typing import Union

import dask.array as da

from ..reconstruction.image import Image


@dataclass(init=True, repr=True)
class IntensityModel(metaclass=ABCMeta):
    """Base class for modeling intensity variation with frequency.

    Attributes
    ----------
    reference_frequency : float
        Reference frequency for the intensity model.
    """
    reference_frequency: float

    @abstractmethod
    def apply_model(self, image: Union[Image, da.Array], frequencies: da.Array) -> da.Array:
        """Apply the intensity model to the given image.

        Parameters
        ----------
        image : Union[Image, da.Array]
            Input image or array to which the model is applied.
        frequencies : da.Array
            Array of frequencies at which the model is evaluated.

        Returns
        -------
        da.Array
            The image scaled according to the intensity model.
        """
        pass
