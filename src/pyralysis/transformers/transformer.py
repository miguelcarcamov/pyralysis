import copy
from abc import ABCMeta, abstractmethod
from dataclasses import dataclass, field

from ..base import Dataset


@dataclass(init=True, repr=True)
class Transformer(metaclass=ABCMeta):
    """Class that represents a transformer. The instance of this class must
    perform a transformation to the input dataset.

    Parameters
    ----------
    input_data : Dataset
        The input dataset
    backup : bool
        Boolean that indicates whether to back up the dataset or not
    """
    input_data: Dataset = None
    backup: bool = True

    backup_data: Dataset = field(init=True, repr=True, default=None)

    @abstractmethod
    def transform(self) -> None:
        """Function that performs a tranformation to the dataset."""
        pass

    def apply(self) -> None:
        """Back up the dataset if indicated and applies the transformation."""
        if self.input_data is not None:
            # We first back up the data of the input object
            if self.backup:
                self.backup_data = copy.deepcopy(self.input_data)
            self.transform()
        else:
            raise ValueError("Input dataset object has not been provided")
