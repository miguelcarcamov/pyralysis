from dataclasses import dataclass

from ..transformer import Transformer


@dataclass(init=False, repr=True)
class StokesConverter(Transformer):
    """Class that represents the transformer that convert Stokes to
    correlations and vice versa.

    Parameters
    ----------
    kwargs :
        Transformer object arguments
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def transform(self):
        """Transforms Stokes to correlations and vice versa."""
        pass
