from dataclasses import dataclass

import dask.array as da
import xarray as xr

from .transformer import Transformer


@dataclass(init=True, repr=True)
class HermitianSymmetry(Transformer):
    """Hermitian symmetry class.

    Parameters
    ----------
    kwargs
           Transformer object arguments
    """

    def transform(self):
        """Applies hermitian symmetry to the dataset."""
        for ms in self.input_data.ms_list:
            hermitian_condition = ms.visibilities.uvw[:, 0] > 0.0

            # This is faster than using the indexes as dask arrays
            idx = da.flatnonzero(hermitian_condition).compute()
            # Compute chunk sizes takes 2x times more than computing the indexes
            # idx = da.flatnonzero(hermitian_condition).compute_chunk_sizes()
            if len(idx) > 0:
                ms.visibilities.uvw[idx] *= -1.0
                ms.visibilities.data[idx] = ms.visibilities.data[idx].conj()
                ms.visibilities.model[idx] = ms.visibilities.model[idx].conj()
                ms.visibilities.corrected[idx] = ms.visibilities.corrected[idx].conj()
