from abc import abstractmethod
from dataclasses import dataclass

from .gridder import Gridder
from .transformer import Transformer


@dataclass(init=True, repr=True)
class WeightingScheme(Transformer):
    """Class that represents the interferometry weighting scheme.

    Parameters
    ----------
    gridder : Gridder
        The gridding object in order to grid weights if uniform or briggs weighting
    kwargs :
        Transformer object general arguments
    """
    gridder: Gridder = None

    @abstractmethod
    def transform(self):
        """Transforms the weights of an input dataset."""
        pass
