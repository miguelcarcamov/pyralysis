from dataclasses import dataclass
from typing import List, Tuple, Union

import astropy.units as un
import dask.array as da
import numpy as np
import xarray as xr
from astropy.units import Quantity

from ..base import SubMS
from ..convolution.c_kernel import CKernel
from ..reconstruction import Image
from ..units import lambdas_equivalencies
from ..utils.cellsize import cellsize as cellsize_check_set
from .transformer import Transformer


@dataclass(init=False, repr=True)
class Gridder(Transformer):
    """Class that represents the interferometric gridding.

    Parameters
    ----------
    image : Image
    imsize : Union[List[int], int]
             Image size
    cellsize : Union[List[float], float, Quantity]
               Cell size in Image-space
    padding_factor : float
                     Padding factor
    ckernel_object : CKernel
                     Convolution Kernel
    hermitian_symmetry : bool
                         Indicates whether hermitian symmetry has been applied or not
    kwargs : dict
             General transformer attributes
    """

    image: Image = None
    imsize: Union[List[int], int] = None
    cellsize: Union[List[float], float, Quantity] = None
    padding_factor: float = None
    ckernel_object: CKernel = None
    hermitian_symmetry: bool = None

    def __init__(
        self,
        image: Image = None,
        imsize: Union[List[int], int] = None,
        cellsize: Union[List[float], float, Quantity] = None,
        padding_factor: float = 1.2,
        ckernel_object: CKernel = None,
        hermitian_symmetry: bool = None,
        **kwargs,
    ):
        super().__init__(**kwargs)
        self._grid_size = None
        self._padded_grid_size = None
        self._padded_imsize = None
        self._uvcellsize = None
        self.image = image
        self.padding_factor = max(1.0, padding_factor)
        self.hermitian_symmetry = hermitian_symmetry
        self.imsize = image.imsize if image is not None else imsize
        self.cellsize = cellsize

        if self.cellsize is None:
            # The gridder cellsize is preferred over the image cellsize.
            if image is None:
                raise AttributeError(
                    "The cellsize attribute must be set. Set it at object instance level or passing an Image object."
                )
            else:
                self.cellsize = image.cellsize

        self.ckernel_object = ckernel_object

    @property
    def imsize(self):
        return self._imsize

    @imsize.setter
    def imsize(self, imsize):
        if isinstance(imsize, int):
            self._imsize = [imsize, imsize]
        else:
            self._imsize = imsize
        if self._imsize is not None:
            self.__update_grid_size()

    @property
    def padding_factor(self):
        return self._padding_factor

    @padding_factor.setter
    def padding_factor(self, padding_factor):
        self._padding_factor = max(1.0, float(padding_factor))
        if hasattr(self, 'imsize'):
            # Update padded_grid_size values
            self.__update_grid_size()

    @property
    def cellsize(self):
        return self._cellsize

    @cellsize.setter
    def cellsize(self, cellsize):
        # Check cellsize and assign value per type
        self._cellsize = cellsize_check_set(cellsize)
        # Assign uvcellsize value
        if self._cellsize is not None and self._padded_imsize:
            self._uvcellsize = (
                1. / (self._padded_imsize * self._cellsize.to(un.rad).value)
            ) * un.lambdas

    @property
    def _uvcellsize(self):
        return self.___uvcellsize

    @_uvcellsize.setter
    def _uvcellsize(self, uvcellsize):
        if isinstance(uvcellsize, Quantity):
            if not uvcellsize.isscalar:
                self.___uvcellsize = uvcellsize
            else:
                self.___uvcellsize = Quantity([uvcellsize, uvcellsize])
        elif isinstance(uvcellsize, float):
            self.___uvcellsize = Quantity([uvcellsize * un.lambdas, uvcellsize * un.lambdas])
        elif isinstance(uvcellsize, list):
            if uvcellsize and len(uvcellsize) <= 2:
                self.___uvcellsize = Quantity(uvcellsize * un.lambdas)
            else:
                raise ValueError("The provided list for uvcellsize is too big for Gridder")
        else:
            self.___uvcellsize = uvcellsize

    @property
    def hermitian_symmetry(self):
        return self._hermitian_symmetry

    @hermitian_symmetry.setter
    def hermitian_symmetry(self, hermitian_symmetry):
        if hermitian_symmetry is None:
            self._hermitian_symmetry = False
        else:
            self._hermitian_symmetry = hermitian_symmetry

        if hasattr(self, 'imsize'):
            # Update grid_size and padded_grid_size values
            if self.__check_hermitian_symmetry:
                self.__update_grid_size()

    def __check_hermitian_symmetry(self):
        hermitian_symmetry_applied = False
        imsize = self.imsize

        if hasattr(self, '_grid_size'):
            grid_size = self._grid_size

            if imsize[1] != grid_size[1]:
                hermitian_symmetry_applied = True
        return hermitian_symmetry_applied

    def __update_grid_size(self):
        imsize = self.imsize
        self._padded_imsize = [
            int(imsize[0] * self.padding_factor),
            int(imsize[1] * self.padding_factor)
        ]

        if self.hermitian_symmetry:

            self._grid_size = [imsize[0], imsize[1] // 2 + 1]
            self._padded_grid_size = [
                int(imsize[0] * self.padding_factor),
                int(imsize[1] * self.padding_factor) // 2 + 1,
            ]
        else:
            self._grid_size = imsize
            self._padded_grid_size = self._padded_imsize

    def grid_weights(self, ms: SubMS = None, nu: Quantity = None) -> tuple:
        """Grid the weights given a measurement set and a reference frequency.

        Parameters
        ----------
        ms : SubMS
            The measurement set.
        nu : Quantity
            The reference frequency.

        Returns
        -------
        A tuple containing:
            - the 1D indexes,
            - the gridded weights array,
            - a flag array for non-valid indexes,
            - gridded weight values for each uv coordinate.
        """
        if nu is None:
            raise ValueError("Cannot grid weights without the frequency parameter")

        if ms is not None:
            uvw = ms.visibilities.uvw.data * un.m
            weight = ms.visibilities.weight.data
            original_rows = uvw.shape[0]

            # If hermitian_symmetry flag is False, then we duplicate the data since datasets only provide half of it
            # TODO: Not sure to add these lines here
            if not self.hermitian_symmetry:
                uvw = da.concatenate((uvw, -uvw))
                weight = da.concatenate((weight, weight))
                weight *= 0.5

            u_pix, v_pix = self._calculate_uv_pix(uvw, self._uvcellsize, self._padded_grid_size, nu)
            idx, not_valid_idxs = self._calculate_1d_idx(u_pix, v_pix, self._padded_grid_size)
            # Flag indexes and weights falling outside the grid if any

            idx *= ~not_valid_idxs
            weight_data = weight * ~not_valid_idxs[:, np.newaxis]

            output_size = self._padded_grid_size[0] * self._padded_grid_size[1]

            weights_on_grid_list = [
                da.bincount(idx, weights=weight_data[:, i], minlength=output_size)
                for i in range(weight_data.shape[-1])
            ]
            weights_on_grid = da.stack(weights_on_grid_list, axis=-1)

            w_k = weights_on_grid[idx]  # Gridded weight for each non-gridded u,v coordinate

            if not self.hermitian_symmetry:
                not_valid_idxs = not_valid_idxs[0:original_rows]
                w_k = w_k[0:original_rows]

            return idx, weights_on_grid, not_valid_idxs, w_k

        else:
            raise ValueError("MS cannot be Nonetype")

    def convolve_idx(self, u, v):
        """
        Compute the convolution grid indices and effective kernel offsets for oversampled convolution.

        For each visibility (given by its u,v pixel positions), this function determines:
        - The native grid cell (integer part) where the visibility falls.
        - The oversampling offsets within that native grid cell (i.e., the sub-pixel position).
        - The effective relative indices to select the proper oversampled kernel elements.

        The effective relative indices are computed by adding the native relative kernel indices
        (representing the kernel support) to the oversampling offsets. These indices tell you exactly
        which element of the oversampled kernel to apply for each visibility.

        Parameters
        ----------
        u : dask.array.Array or np.ndarray
            The u fractional pixel coordinates for visibilities.
        v : dask.array.Array or np.ndarray
            The v fractional pixel coordinates for visibilities.

        Returns
        -------
        indexes : dask.array.Array
            A 1D flattened array of grid indices for each kernel element of each visibility.
            (Computed as: n * v_index + u_index, where n is the padded grid width.)
        not_valid_indexes : dask.array.Array (bool)
            A boolean mask indicating which computed indices fall outside the padded grid.
        effective_rel_u : dask.array.Array (int)
            Effective relative u-index for each visibility and each kernel element.
            This is obtained by adding the oversampling offset in u to the native relative u-index.
        effective_rel_v : dask.array.Array (int)
            Effective relative v-index for each visibility and each kernel element.
            This is obtained by adding the oversampling offset in v to the native relative v-index.
        """
        # Get grid dimensions from the padded grid size.
        m = self._padded_grid_size[0]
        n = self._padded_grid_size[1]

        # Compute the integer grid cell positions.
        u_int = (u + 0.5).astype(np.int32)
        v_int = (v + 0.5).astype(np.int32)

        # Compute the fractional offset from the integer grid cell.
        # For example, if u = 100.5, then u_int = 100 and frac_u = 0.5.
        frac_u = (u + 0.5) - u_int
        frac_v = (v + 0.5) - v_int

        # Retrieve the oversampling factor from the convolution kernel.
        os_factor = self.ckernel_object.oversampling_factor

        # Compute the fractional parts.
        offset_u = da.round(os_factor * (frac_u - 0.5)).astype(np.int32)
        offset_v = da.round(os_factor * (frac_v - 0.5)).astype(np.int32)

        # Retrieve the native relative indexes (for example, [-1, 0, 1]) that
        # have already been scaled by the oversampling factor.
        native_rel = self.ckernel_object.relative_indexes
        native_rel_u = native_rel[1].ravel()  # shape: (support*support,)
        native_rel_v = native_rel[0].ravel()

        # Compute the effective relative indexes for each visibility by adding the oversampling offsets.
        effective_rel_u = native_rel_u[None, :] + offset_u[:, None]
        effective_rel_v = native_rel_v[None, :] + offset_v[:, None]

        # Determine the final indexes into the padded grid.
        u_indexes = u_int[:, None] + effective_rel_u
        v_indexes = v_int[:, None] + effective_rel_v

        # Flag indexes that are outside the valid grid range.
        not_valid_indexes = (u_indexes < 0) | (u_indexes >= n) | (v_indexes < 0) | (v_indexes >= m)
        indexes = n * v_indexes + u_indexes

        return indexes, not_valid_indexes, effective_rel_u, effective_rel_v

    @staticmethod
    def _calculate_1d_idx(u, v, imsize):
        u_int, v_int = u.astype(np.int32), v.astype(np.int32)

        not_valid_idxs = (u_int < 0) | (u_int >= imsize[1]) | (v_int < 0) | (v_int >= imsize[0])

        idx = imsize[1] * v_int + u_int

        if isinstance(idx, xr.DataArray):
            return idx.data, not_valid_idxs.data
        else:
            return idx, not_valid_idxs

    @staticmethod
    def _calculate_uv_pix(uvw,
                          uvcellsize,
                          imsize,
                          freq=None,
                          use_rounding=False,
                          keep_dim=False) -> Tuple[da.Array, da.Array]:
        """Find the nearest pixel for each uv coordinate.

        Parameters
        ----------
        uvw :
            uvw positions in Fourier space (last axis holds the coordinates).
        uvcellsize :
            Cell-size in Fourier space.
        imsize :
            Image size.
        freq : optional
            Frequency.
        use_rounding : bool, optional
            Whether to use rounding or not.
        keep_dim : bool, optional
            Whether to preserve dimensionality.
        """
        m = imsize[0]
        n = imsize[1]

        hermitian_symmetry = False

        if m != n:
            hermitian_symmetry = True

        if freq is not None:
            duv = uvcellsize.to(un.m, equivalencies=lambdas_equivalencies(freq))
        else:
            duv = uvcellsize

        uv = uvw[..., 0:2] / duv

        if isinstance(uv, xr.DataArray):
            uv = xr.apply_ufunc(
                lambda x: x.value, uv, dask="parallelized", output_dtypes=[uv.dtype]
            )
        elif isinstance(uv, da.core.Array):
            uv = da.map_blocks(
                lambda x: x.value if isinstance(x, Quantity) else x, uv, dtype=uv.dtype
            )
        else:
            raise TypeError("The array is not a dask nor xarray type")

        if uvw.ndim > 2 and not keep_dim:
            uv = uv.reshape((-1, uv.shape[-1]))

        if hermitian_symmetry:
            u_pix = uv[..., 0]
        else:
            u_pix = uv[..., 0] + n // 2

        v_pix = uv[..., 1] + m // 2

        if use_rounding:
            u_pix += 0.5
            v_pix += 0.5

        return u_pix, v_pix

    def transform(self) -> None:
        """Placeholder for the transform method.

        This should be overridden in subclasses.
        """
        pass
