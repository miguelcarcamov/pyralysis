from dataclasses import dataclass

from ..wscheme import WeightingScheme


@dataclass(init=True, repr=True)
class Natural(WeightingScheme):
    """Natural weighting scheme class.

    Parameters
    ----------
    kwargs :
        WeightingScheme general arguments
    """

    def __post__init__(self):
        self.backup = False

    def transform(self):
        """For natural weights the weights are not transformed, so this
        function does nothing."""
        pass
