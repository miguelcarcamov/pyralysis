from dataclasses import dataclass

from ..wscheme import WeightingScheme


@dataclass(init=True, repr=True)
class Radial(WeightingScheme):
    """Radial weighting scheme class.

    Parameters
    ----------
    kwargs :
        WeightingScheme general arguments
    """
    use_w: bool = False

    def transform(self) -> None:
        """This function calculates the radial weights and transforms them
        according to this scheme,"""
        for ms in self.input_data.ms_list:
            dist = ms.visibilities.get_uvw_distance(self.use_w)
            ms.visibilities.imaging_weight *= dist
