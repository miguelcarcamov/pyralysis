from abc import abstractmethod
from dataclasses import dataclass
from typing import Optional, Union

import astropy.units as un
import dask.array as da
import numpy as np

# Local package imports.
from ..fft import fft2
from ..models import IntensityModel
from ..units import array_unit_conversion
from ..utils.fourier import phase_shift_grid
from ..utils.padding import apply_padding
from .gridder import Gridder


@dataclass(init=False, repr=True)
class ModelVisibilities(Gridder):
    """
    A gridder class that computes and adds model visibilities to a dataset based
    on a model image. The method to estimate the visibility (i.e. how the Fourier
    grid is sampled) is left abstract, allowing for various interpolation schemes
    (e.g., nearest neighbor, bilinear, or more complex degridding).

    Parameters
    ----------
    intensity_model : Optional[IntensityModel]
        An instance of an intensity model used to scale the image as a function
        of frequency.
    **kwargs
        Additional keyword arguments for the base Gridder class.
    """
    intensity_model: Optional[IntensityModel] = None

    def __init__(self, intensity_model: Optional[IntensityModel] = None, **kwargs):
        super().__init__(**kwargs)
        self.intensity_model = intensity_model

    @abstractmethod
    def estimate_visibility(
        self,
        grid_fft: da.Array,
        uv: tuple,
        kernel: Union[da.Array, np.ndarray] = None
    ) -> da.Array:
        """
        Abstract method to estimate the visibility from the Fourier grid.

        This method may be implemented using one of several interpolation
        techniques – such as nearest neighbor, bilinear, or a full degridding
        routine – depending on the subclass requirements.

        Parameters
        ----------
        grid_fft : da.Array
            The Fourier transform (FFT) of the (primary-beam corrected) image cube.
        uv : tuple
            UV coordinates (in pixel units) at which visibilities should be extracted.
        kernel : Union[da.Array, np.ndarray], optional
            The convolution kernel (often a half–kernel) to be used in the estimation.
            For example, when using a degridding technique, the full kernel can be
            reconstructed (using symmetry) from the half–kernel.

        Returns
        -------
        da.Array
            The estimated visibility values.
        """
        pass

    def transform(self) -> None:
        """
        Transform the input image into model visibilities and update the dataset.

        The transformation procedure consists of several steps, some of which are:

        1. **Image Retrieval & Optional Padding:**
           Retrieve the image (a dask.array) from the image object. If a padding factor
           greater than 1.0 is specified, the image is padded and (if needed) rechunked.

        2. **Determine Center Pixel:**
           Compute the center pixel of the image. When padding is applied, adjust the
           center pixel indices accordingly.

        3. **Primary Beam Setup:**
           Configure the primary beam object and update its cell size.

        4. **Coordinate Conversion:**
           Convert the phase direction cosines (l, m) into Cartesian pixel coordinates.

        5. **Kernel Acquisition:**
           If a convolution kernel object (ckernel_object) is provided, request the half–kernel
           (i.e. the nonnegative quadrant) to save memory. Later, the full kernel is reconstructed
           using symmetry. Note that the chosen estimation method (via estimate_visibility) may use
           nearest neighbor, bilinear, or degridding interpolation.

        6. **Processing Each Measurement Set (MS):**
           For every measurement set in the dataset, the following steps are performed:

           a. **Extract Identifiers and Data:**
              Get field, polarization, and spectral window IDs; retrieve uvw coordinates,
              flags, and channel frequencies.

           b. **Field Center Determination:**
              Obtain the field center in both sky coordinates and Cartesian pixel coordinates.

           c. **Image Cube Formation:**
              If an intensity model is provided, apply it to the image across channels.
              Otherwise, simply add a frequency axis.

           d. **Primary Beam Application:**
              Compute the primary beam for the field and multiply it with the image cube.

           e. **Fourier Transform & Phase Shift:**
              Compute the FFT of the (beam–corrected) image cube and apply a phase shift
              to account for the field center offset.

           f. **UV Pixel Calculation:**
              Convert the uvw coordinates (after unit conversion) to pixel coordinates on the grid.

           g. **Visibility Estimation:**
              Estimate the visibilities by calling the abstract method
              `estimate_visibility`. This method may implement any of the following
              estimation schemes: nearest neighbor, bilinear interpolation, or degridding.

           h. **Post–Processing:**
              Repeat the estimated visibilities for all correlations, apply the flag mask,
              and update the measurement set’s dataset with the model visibilities.
        """
        # -------------------------------------------------------
        # 1. Retrieve the image and apply padding if required.
        # -------------------------------------------------------
        image = self.image.data.data  # Extract the underlying dask.array from the image object
        new_chunks = None
        if self.padding_factor > 1.0:
            image, pad_width, new_chunks = apply_padding(image, self.padding_factor)
        if new_chunks:
            image = image.rechunk(new_chunks)

        # -------------------------------------------------------
        # 2. Determine the image center, adjusting for padding.
        # -------------------------------------------------------
        center_pixel = (self.image.center_pixel[0], self.image.center_pixel[1])
        if self.padding_factor > 1.0:
            center_pixel = (
                self.image.center_pixel[0] +
                pad_width[-1][0],  # Column index shift due to left padding.
                self.image.center_pixel[1] +
                pad_width[-2][0],  # Row index shift due to top padding.
            )

        dataset_ms = self.input_data

        # -------------------------------------------------------
        # 3. Set up the primary beam and update its cell size.
        # -------------------------------------------------------
        primary_beam = dataset_ms.antenna.primary_beam
        primary_beam.cellsize = self.cellsize

        # -------------------------------------------------------
        # 4. Convert phase direction cosines (l, m) to Cartesian pixel coordinates.
        # -------------------------------------------------------
        l_direction = dataset_ms.field.phase_direction_cosines[0].value
        m_direction = dataset_ms.field.phase_direction_cosines[1].value

        l_direction_cartesian = (l_direction / self.cellsize[0]).value
        m_direction_cartesian = (m_direction / self.cellsize[1]).value

        # -------------------------------------------------------
        # 5. Obtain the half–kernel representation (if available).
        # This kernel may later be used in the visibility estimation.
        # The abstract estimate_visibility method (which could be based on nearest neighbor,
        # bilinear or degridding methods) will use this kernel to reconstruct the full kernel
        # (if needed) via symmetry.
        # -------------------------------------------------------
        kernel = None
        if self.ckernel_object is not None:
            kernel = self.ckernel_object.kernel(
                imsize=(self.ckernel_object.size, self.ckernel_object.size), half=True
            ).persist()

        # -------------------------------------------------------
        # 6. Process each measurement set (MS) in the dataset.
        # -------------------------------------------------------
        for i, ms in enumerate(dataset_ms.ms_list):
            field_id = ms.field_id
            pol_id = ms.polarization_id
            spw_id = ms.spw_id
            nchans = dataset_ms.spws.nchans[spw_id]

            ms_list_keys = ms.visibilities.dataset.data_vars.keys()
            uvw = ms.visibilities.uvw.data * un.m
            flags = ms.visibilities.flag.data

            chans = dataset_ms.spws.dataset[spw_id].CHAN_FREQ.data.squeeze(axis=0)

            # Get field center in sky coordinates and its Cartesian (pixel) equivalent.
            field_l_center = l_direction[field_id]
            field_m_center = m_direction[field_id]
            field_l_center_cartesian = l_direction_cartesian[field_id]
            field_m_center_cartesian = m_direction_cartesian[field_id]

            # -------------------------------------------------------
            # 6a. Form the image cube.
            # If an intensity model is provided, apply it to the image as a function of frequency.
            # Otherwise, add a singleton frequency axis.
            # -------------------------------------------------------
            if self.intensity_model:
                image_cube = self.intensity_model.apply_model(image, chans)
            else:
                image_cube = image[np.newaxis, :, :]

            # -------------------------------------------------------
            # 6b. Compute the primary beam for this field.
            # -------------------------------------------------------
            beam = primary_beam.beam(
                chans,
                image_cube.shape[-2:],
                antenna=np.array([0]),
                imcenter=center_pixel,
                x_0=field_l_center,
                y_0=field_m_center,
                imchunks=image_cube.chunks[-2:]
            )
            beam = beam[0]  # Select the first beam (if multiple beams exist)

            # Apply the primary beam to the image cube.
            ms_image_cube = image_cube * beam

            # -------------------------------------------------------
            # 6c. Compute the Fourier transform and apply a phase shift.
            # -------------------------------------------------------
            grid_fft = fft2(
                ms_image_cube,
                sign_convention="positive",
                hermitian_symmetry=self.hermitian_symmetry,
                restore_chunks=False
            )
            grid_fft = phase_shift_grid(
                grid_fft, field_l_center_cartesian, field_m_center_cartesian
            )

            # -------------------------------------------------------
            # 6d. Prepare the UV coordinates (pixel indices) for the Fourier grid.
            # -------------------------------------------------------
            ncorrs = dataset_ms.polarization.ncorrs[pol_id]
            uvw_broadcast = uvw[:, np.newaxis, :]
            equiv = dataset_ms.spws.equivalencies[spw_id]

            uvw_broadcast_chunks = da.core.normalize_chunks(
                flags.chunks[:-1] + uvw.chunks[-1:],
                shape=(uvw.shape[0], nchans, 3),
                dtype=uvw.dtype
            )
            uvw_broadcast = da.repeat(uvw_broadcast, nchans, axis=1).rechunk(uvw_broadcast_chunks)
            uvw_lambdas = array_unit_conversion(
                array=uvw_broadcast,
                unit=un.lambdas,
                equivalencies=equiv,
            )

            uv_pixels = self._calculate_uv_pix(
                uvw_lambdas, self._uvcellsize, self._padded_grid_size, keep_dim=True
            )

            # -------------------------------------------------------
            # 6e. Estimate the visibilities.
            # Here, the abstract method 'estimate_visibility' is invoked.
            # Depending on the implementation, this method may use nearest neighbor,
            # bilinear, or degridding interpolation methods.
            # -------------------------------------------------------
            estimated_visibilities = self.estimate_visibility(grid_fft, uv_pixels, kernel)

            # Repeat visibilities across correlation axis and apply flag mask.
            estimated_visibilities = da.repeat(
                estimated_visibilities[:, :, np.newaxis], ncorrs, axis=2
            ).rechunk(flags.chunks) * ~flags

            # -------------------------------------------------------
            # 6f. Update the measurement set with the model visibilities.
            # -------------------------------------------------------
            ms.visibilities.model.data = estimated_visibilities
            if "MODEL_DATA" not in ms_list_keys:
                ms.visibilities.dataset = ms.visibilities.dataset.assign(
                    MODEL_DATA=(["row", "chan", "corr"], ms.visibilities.model.data)
                )
            else:
                ms.visibilities.dataset["MODEL_DATA"] = ms.visibilities.model
