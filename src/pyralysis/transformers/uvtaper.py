from dataclasses import dataclass

import dask.array as da
import numpy as np
from astropy import units as u
from astropy.units import Quantity

from ..units.lambda_units import lambdas_equivalencies
from ..units.units_functions import array_unit_conversion, check_units
from ..utils.gaussian import Gaussian2D
from .transformer import Transformer


@dataclass(init=False, repr=True)
class UVTaper(Transformer, Gaussian2D):
    """Class that represents the UVTaper used in interferometry, the methods of
    this class will transform the weights of the input dataset according to the
    UVTaper features. UVTaper inherits from the Gaussian2D class.

    Parameters
    ----------
    **kwargs :
        Transformer and Gaussian2D arguments
    """

    def __init__(self, input_data=None, **kwargs):
        super().__init__(input_data)
        super(Transformer, self).__init__(**kwargs)

        # Check that sigmas and fwhm are in meters or units of lambdas
        if self.sigma is not None:
            if isinstance(self.sigma, Quantity):
                if (
                    not check_units(self.sigma, u.m) and not check_units(self.sigma, u.lambdas)
                    and not check_units(self.sigma, u.rad)
                ):
                    raise ValueError(
                        "Sigma parameter for uvtaper must be whether in meters, units of lambdas or angular units."
                    )
                else:
                    # Make sure that units are in lambdas and not klambdas, Mlambdas, etc
                    if check_units(self.sigma, u.lambdas):
                        self.sigma = self.sigma.to(u.lambdas)
                    elif check_units(self.sigma, u.m):
                        self.sigma = self.sigma.to(u.m)
                    else:
                        self.sigma = self.sigma.to(u.lambdas, equivalencies=lambdas_equivalencies())
            else:
                raise TypeError("Sigma parameter for uvtaper must be a Quantity instance.")

        if self.fwhm is not None:
            if isinstance(self.fwhm, Quantity):
                if (
                    not check_units(self.fwhm, u.m) and not check_units(self.fwhm, u.lambdas)
                    and not check_units(self.fwhm, u.rad)
                ):
                    raise ValueError(
                        "FWHM parameter for uvtaper must be whether in meters, units of lambdas or angular units"
                    )
                else:
                    # Make sure that units are in lambdas and not klambdas, Mlambdas, etc
                    if check_units(self.fwhm, u.lambdas):
                        self.fwhm = self.fwhm.to(u.lambdas)
                    elif check_units(self.fwhm, u.m):
                        self.fwhm = self.fwhm.to(u.m)
                    else:
                        self.fwhm = self.fwhm.to(u.lambdas, equivalencies=lambdas_equivalencies())
            else:
                raise TypeError("FWHM parameter for uvtaper must be a Quantity instance.")

        # Check that theta is in units equivalent to radians
        if self.theta is not None:
            if not check_units(self.theta, u.rad):
                raise ValueError("Theta angle parameter for uvtaper must be in angular units")

    def transform(self) -> None:
        """Evaluates the taper on the (u,v) positions and tapers the
        weights."""
        for ms in self.input_data.ms_list:
            uvw = ms.visibilities.uvw.data * u.m

            if self.sigma.unit is not u.m:
                spw_id = ms.spw_id
                ref_freq = self.input_data.spws.ref_freqs[spw_id]
                uvw = array_unit_conversion(
                    array=uvw,
                    unit=u.lambdas,
                    equivalencies=lambdas_equivalencies(restfreq=ref_freq),
                )

            u_array = uvw[:, 0]
            v_array = uvw[:, 1]

            u_array = u_array.astype(
                np.float32
            )  # Changing to float32 so the result from evaluate is a float32 array
            v_array = v_array.astype(
                np.float32
            )  # Changing to float32 so the result from evaluate is a float32 array
            gauss = da.map_blocks(
                lambda x, y: self.evaluate(x, y),
                u_array,
                v_array,
                dtype=np.float32,
                enforce_ndim=True
            )

            ms.visibilities.imaging_weight *= gauss[:, np.newaxis]
