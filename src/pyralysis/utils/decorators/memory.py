import functools
import itertools
import os
import tempfile
import uuid

import dask.array as da
import zarr


def temp_local_save(func=None, *, path=None):

    if func is None:
        return functools.partial(temp_local_save, path=path)

    @functools.wraps(func)
    def wrapper_temp_local_save(*args, **kwargs):
        data = func(*args, **kwargs)

        # Return if is not a dask array
        if not isinstance(data, da.Array) or len(data) == 0:
            return data

        # For dask array with data
        if path:
            store_path = path
        else:
            tempdir = tempfile.gettempdir()
            store_path = os.path.join(tempdir, f'{str(uuid.uuid4())}.zarr')

        store = zarr.storage.FSStore(store_path, check=True, create=True)
        stored = da.to_zarr(data, url=store, overwrite=True, compute=True, return_stored=True)

        if not isinstance(stored, da.Array):
            raise TypeError(f"Expected da.Array, got {type(store)}")

        final_data = stored
        return final_data

    return wrapper_temp_local_save


def temp_irreg_local_save(func=None, *, path=None):

    if func is None:
        return functools.partial(temp_irreg_local_save, path=path)

    @functools.wraps(func)
    def wrapper_temp_irreg_local_save(*args, **kwargs):
        data = func(*args, **kwargs)
        # Return if is not a dask array
        if not isinstance(data, da.Array) or len(data) == 0:
            return data

        # For dask array with data
        if path:
            store_path = path
        else:
            tempdir = tempfile.gettempdir()
            store_path = os.path.join(tempdir, f'{str(uuid.uuid4())}.zarr')
        store = zarr.DirectoryStore(store_path)

        seq = []
        for inds in itertools.product(*map(range, data.blocks.shape)):
            chunk = data.blocks[inds]
            z = zarr.empty(
                shape=chunk.shape,
                dtype=chunk.dtype,
                store=store,
                path=str(uuid.uuid4()),
                write_empty_chunks=True
            )
            chunk.store(z, lock=False)

            seq.append(da.from_zarr(z))

        return da.hstack(seq)

    return wrapper_temp_irreg_local_save
