from typing import Union

import astropy.units as un
import dask.array as da
import numpy as np
from astropy.io import fits
from astropy.io.fits import Header
from astropy.wcs import WCS


def get_coordinates(hdul: Union[fits.HDUList, dict] = None, hdu_index: int = None) -> dict:
    """Function that gets coordinates from header.

    Parameters
    ----------
    hdul : fits.HDUList
            HDU List
    hdu_index: int
            HDU index

    Returns
    -------
    coords:
            Dictionary that represents the xarray coordinates
    """
    if isinstance(hdul, fits.HDUList) and hdu_index is not None:
        header = hdul[hdu_index].header
    elif isinstance(hdul, fits.HDUList) and hdu_index is None:
        header = hdul[0].header
    elif isinstance(hdul, dict):
        header = Header(hdul)

    wcs = WCS(header, naxis=2)

    m = header["NAXIS1"]
    n = header["NAXIS2"]

    x_idx = da.arange(0, m, dtype=np.int32)
    y_idx = da.arange(0, n, dtype=np.int32)

    x_grid, y_grid = da.meshgrid(x_idx, y_idx)

    ra, dec = wcs.all_pix2world(x_grid, y_grid, 0) * un.deg

    coords = dict(
        X=(["x", "y"], x_grid.astype(np.int32)),
        Y=(["x", "y"], y_grid.astype(np.int32)),
        RA=(["x", "y"], ra),
        DEC=(["x", "y"], dec),
    )
    return coords
