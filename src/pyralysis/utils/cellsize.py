from typing import List, Tuple, Union

import astropy.units as un
import numpy as np
from astropy.units import Quantity
from multimethod import multimethod


@multimethod
def cellsize(value: Quantity) -> Quantity:
    """Compute cellsize as a Quantity array.

    From a numerical value or an array, returns a Quantity array with 2 elements, containing the
    pixel scale in each axis.

    Parameters
    ----------
    value : Union[Quantity, List, Tuple, np.ndarray, float, property]
        Numerical value of cellsize. If a Quantity or sequence of them, preserves the units. For any
        other valid value type, the result is will have radians as a unit.

    Returns
    -------
    Quantity | None
        A Quantity whose value is an array-like object with two elements that represents the size of
        every cell, specified per-axis. None if the parameter value is a property.

    Raises
    ------
    ValueError
        If the value provided is an array-like object with too many/few elements.
    TypeError
        If the value provided is of an invalid/unsupported type.
    """
    # Quantity is an scalar value
    if value.isscalar:
        return __cellsize_sign(value, value)
    # Quantity is a list
    if len(value) > 2:
        # Error if the list is too long
        raise ValueError("The provided list for cellsize is too long")
    elif len(value) < 2:
        # Error if the list is too short
        raise ValueError("The provided list for cellsize is too short")
    # Continue if list is of length 2 or less
    return __cellsize_sign(value[0], value[1])


@cellsize.register
def _(value: float) -> Quantity:
    return __cellsize_sign(value, value)


@cellsize.register
def _(value: Union[List, Tuple, np.ndarray]) -> Quantity:
    if len(value) > 2:
        # Error if the list is too long
        raise ValueError("The provided list for cellsize is too long")
    elif len(value) < 2:
        # Error if the list is too short
        raise ValueError("The provided list for cellsize is too short")
    # Continue if list is of length 2 or less
    return __cellsize_sign(value[0], value[1])


@cellsize.register
def _(value: property) -> None:
    return None


@cellsize.register
def _(value: None) -> None:
    return None


@cellsize.register
def _(value):
    raise TypeError(f"The provided value is type {type(value)}, which is an invalid type")


@multimethod
def __cellsize_sign(v0: float, v1: float) -> Quantity:
    v0 = -1 * np.abs(v0)
    v1 = np.abs(v1)
    return Quantity([v0, v1] * un.rad)


@__cellsize_sign.register
def _(v0: Quantity, v1: Quantity) -> Quantity:
    # If the Quantity is dimensionless, assume radians
    v0 = -1 * np.abs(v0) if v0.unit != un.dimensionless_unscaled else -1 * np.abs(v0) * un.rad
    v1 = np.abs(v1) if v1.unit != un.dimensionless_unscaled else np.abs(v1) * un.rad
    # Try to convert values to radians
    try:
        return Quantity([v0, v1], un.rad)
    except un.core.UnitConversionError as error:
        # Error if the unit is not an angle measure
        raise TypeError("Units for cellsize should be in units of PhysicalType('angle')") from error
