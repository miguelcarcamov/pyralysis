import numpy as np
from astropy import units as un
from astropy.coordinates import SkyCoord


def calculate_lm(
    right_ascensions: np.ndarray,
    declinations: np.ndarray,
    right_ascension_phase_center: float = 0.0,
    declination_phase_center: float = 0.0
) -> np.ndarray:
    """Function that computes the direction cosines l,m of a direction in the
    sky relative to a phase center.

    Parameters
    ----------
    right_ascensions : np.ndarray
        Right ascension coordinates
    declinations: np.ndarray
        Declination coordinates
    right_ascension_phase_center: float
        Right ascension of the phase center direction
    declination_phase_center: float
        Declination of the phase center direction

    Returns
    -------
    np.ndarray
        Returns the direction cosines of the directions in the sky as a Quantity 2D array in units of radians
    """
    delta_right_ascension = right_ascensions - right_ascension_phase_center
    direction_l = np.cos(declinations) * np.sin(delta_right_ascension)
    direction_m = np.sin(declinations) * np.cos(declination_phase_center) - np.cos(
        declinations
    ) * np.sin(declination_phase_center) * np.cos(delta_right_ascension)

    return np.vstack([direction_l, direction_m]) * un.rad


def calculate_lmn(
    right_ascensions: np.ndarray,
    declinations: np.ndarray,
    right_ascension_phase_center: float = 0.0,
    declination_phase_center: float = 0.0
) -> np.ndarray:
    """Function that computes the direction cosines l,m,n of a direction in the
    sky relative to a phase center.

    Parameters
    ----------
    right_ascensions : np.ndarray
        Right ascension coordinates
    declinations: np.ndarray
        Declination coordinates
    right_ascension_phase_center: float
        Right ascension of the phase center direction
    declination_phase_center: float
        Declination of the phase center direction

    Returns
    -------
    np.ndarray
        Returns the direction cosines of the directions in the sky as a Quantity 2D array in units of radians
    """
    delta_right_ascension = right_ascensions - right_ascension_phase_center
    direction_l = np.cos(declinations) * np.sin(delta_right_ascension)
    direction_m = np.sin(declinations) * np.cos(declination_phase_center) - np.cos(
        declinations
    ) * np.sin(declination_phase_center) * np.cos(delta_right_ascension)
    direction_n = np.sqrt(1.0 - direction_l**2 - direction_m**2)

    return np.vstack([direction_l, direction_m, direction_n]) * un.rad


def calculate_radec(
    lmn_direction_cosines: np.ndarray,
    right_ascension_phase_center: float = 0.0,
    declination_phase_center: float = 0.0
) -> np.ndarray:
    """Function that computes the RA, DEC sky directions from direction cosines
    l,m,n relative to a phase centre.

    Parameters
    ----------
    lmn_direction_cosines : np.ndarray
        Direction cosines coordinates
    right_ascension_phase_center: float
        Right ascension of the phase center direction
    declination_phase_center: float
        Declination of the phase center direction

    Returns
    -------
    np.ndarray
        Returns the RA, DEC coordinates of the sky as a 2D np.ndarray
    """
    direction_l = lmn_direction_cosines[0]
    direction_m = lmn_direction_cosines[1]
    if lmn_direction_cosines.shape[0] == 2:
        direction_n = np.sqrt(1.0 - direction_l**2 - direction_m**2)
    else:
        direction_n = lmn_direction_cosines[2]

    right_ascensions = right_ascension_phase_center + np.arctan(
        direction_l / (
            direction_n * np.cos(declination_phase_center) -
            direction_m * np.sin(declination_phase_center)
        )
    )
    declinations = np.arcsin(
        direction_m * np.cos(declination_phase_center) +
        direction_n * np.sin(declination_phase_center)
    )

    return np.vstack([right_ascensions, declinations]) * un.rad


def radec_to_direction_cosines(coordinates: SkyCoord, phase_center: SkyCoord = None) -> np.ndarray:
    """Function that calculates direction cosines from an astropy SkyCoord
    object relative to a SkyCoord phase centre coordinate.

    Parameters
    ----------
    coordinates: SkyCoord
        Input sky coordinates
    phase_center: SkyCoord
        Phase center coordinates

    Returns
    -------
    np.ndarray
        Returns the direction cosines of the directions in the sky as a Quantity 2D array in units of radians
    """
    if phase_center is None:
        phase_center = SkyCoord(0. * un.deg, 0 * un.deg, frame=coordinates.frame)

    return calculate_lmn(coordinates.ra, coordinates.dec, phase_center.ra, phase_center.dec)


def direction_cosines_to_radec(
    direction_cosines: np.ndarray, phase_center: SkyCoord = None
) -> np.ndarray:
    """Function that calculates RA,DEC equatorial coordinates from direction
    cosines relative to a SkyCoord phase centre coordinate.

    Parameters
    ----------
    direction_cosines: np.ndarray
        Input direction cosines
    phase_center: SkyCoord
        Phase center coordinates

    Returns
    -------
    SkyCoord
        Returns the RA,DEC equatorial coordinates as a SkyCoord object
    """
    if phase_center is None:
        phase_center = SkyCoord(0. * un.deg, 0 * un.deg, frame="icrs")

    radec_coordinates = calculate_radec(
        direction_cosines, phase_center.ra.rad, phase_center.dec.rad
    )

    return SkyCoord(radec_coordinates[0], radec_coordinates[1], frame=phase_center.frame)
