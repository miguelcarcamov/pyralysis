"""
Module for nearest–neighbor interpolation.

Provides functions to perform nearest–neighbor interpolation on a 2D or 3D grid.
"""

from typing import Union

import dask.array as da
import numpy as np

from ...utils.sort import sort_uv  # Assumes sort_uv returns sorted u,v and original indices


def nearest_neighbor(
    u: Union[float, np.ndarray, da.Array],
    v: Union[float, np.ndarray, da.Array],
    im: Union[np.ndarray, da.Array],
    enforce_sorting: bool = False,
    is_hermitian: bool = False
) -> Union[float, np.ndarray, da.Array]:
    """
    Perform nearest–neighbor interpolation for UV coordinates on a 2D or 3D grid.

    Parameters
    ----------
    u : float, np.ndarray, or da.Array
        U coordinates (pixel indices).
    v : float, np.ndarray, or da.Array
        V coordinates (pixel indices).
    im : np.ndarray or da.Array
        Input image (or grid) to interpolate from.
    enforce_sorting : bool, optional
        If True, sort the (u, v) coordinates for monotonic order. Default is False.
    is_hermitian : bool, optional
        If True, the grid is Hermitian symmetric and the DC is assumed at (M//2, 0).

    Returns
    -------
    float, np.ndarray, or da.Array
        The interpolated value(s) at the specified coordinates.
    """
    # Convert input coordinates to arrays.
    u = np.asarray(u)
    v = np.asarray(v)

    # Optionally sort coordinates.
    if enforce_sorting:
        (u, v), original_indices = sort_uv(u, v)
    else:
        original_indices = None

    # For Hermitian grids, round u.
    if is_hermitian:
        u = np.round(u)

    # Compute nearest–neighbor indices.
    u0 = u.astype(np.int32)
    v0 = v.astype(np.int32)

    # Create a boolean mask for points that lie inside the image.
    inside = (u0 >= 0) & (v0 >= 0) & (u0 < im.shape[-1]) & (v0 < im.shape[-2])
    inside &= ~np.isnan(u) & ~np.isnan(v)

    # Multiply by the mask to zero-out indices for out-of-bound points.
    u0_inside = u0 * inside
    v0_inside = v0 * inside

    # Build index tuple. For multidimensional arrays, include indices for other dimensions.
    im_idx = (v0_inside, u0_inside)
    if u0.ndim >= 2:
        _, *freq = np.indices(u0.shape)
        im_idx = (*freq, *im_idx)

    # Use the computed indices to sample the image.
    estimated_data = im[im_idx]

    # For points outside, set the value to 0.
    estimated_data = np.where(inside, estimated_data, 0.0)

    # If sorting was applied, restore the original order.
    if enforce_sorting and original_indices is not None:
        estimated_data = estimated_data[np.argsort(original_indices)]

    return estimated_data


def __nearest_neighbour_1d(
    uv: Union[np.ndarray, da.Array], im: Union[np.ndarray, da.Array]
) -> float:
    """
    1D helper for nearest–neighbor interpolation for a single (u, v) pair.

    Parameters
    ----------
    uv : np.ndarray or da.Array
        A two–element array containing the u and v coordinates.
    im : np.ndarray or da.Array
        2D image (or grid) to sample from.

    Returns
    -------
    float
        The value of the image at the nearest neighbor to (u, v).
    """
    (u, v) = uv

    # Check if the coordinate is out-of-bounds or contains NaN.
    outside = (u < 0) + (v < 0) + (u > im.shape[1] - 1) + (v > im.shape[0] - 1)
    outside += np.isnan(u) + np.isnan(v)

    if outside:
        return 0.0

    # Return the value at the nearest pixel.
    return im[v, u]
