"""
Module for degridding.

Provides a top–level degridding routine that extracts visibilities
from a Fourier grid using a half–kernel convolution. This module
supports both full and Hermitian–symmetric grids, oversampling, and
different numerical types (float32, float64, complex64, complex128).

It also includes unified guvectorized, Numba CPU, and Numba GPU versions.
"""

from typing import Union

import dask
import dask.array as da
import numpy as np
from numba import complex64, complex128, cuda, float32, float64, guvectorize, int32, njit, prange


def degridding(
    u: Union[float, np.ndarray, da.Array],
    v: Union[float, np.ndarray, da.Array],
    im: Union[np.ndarray, da.Array],
    kernel: Union[np.ndarray, da.Array],
    oversampling_factor: int = 1,
    is_hermitian: bool = False
) -> Union[float, np.ndarray, da.Array]:
    """
    Top–level degridding routine.

    This function extracts visibilities from an FFT–shifted Fourier grid.
    The grid is assumed to have its DC at:
      - (M//2, N//2) for a full grid.
      - (M//2, 0) for a Hermitian symmetric grid.

    When an extraction window goes out of bounds, indices are mirrored
    about the DC center and the extracted value is conjugated.

    Parameters
    ----------
    u, v : float, np.ndarray, or da.Array
        Coordinates (can be fractional) in the Fourier grid.
    im : np.ndarray or da.Array
        Fourier grid. For full grids: shape = (M, N);
        For Hermitian grids: shape = (M, N_half) with N_half = N//2 + 1.
    kernel : np.ndarray or da.Array
        The half–convolution kernel (for nonnegative offsets).
    oversampling_factor : int, optional
        Oversampling factor used to compute the kernel. Default is 1.
    is_hermitian : bool, optional
        If True, the grid is Hermitian symmetric and the DC is at (M//2, 0).

    Returns
    -------
    float, np.ndarray, or da.Array
        The degridded visibility (or array of visibilities).
    """
    # Ensure u and v are NumPy arrays.
    u = np.asarray(u)
    v = np.asarray(v)

    # For Hermitian grids, round u to ensure integer sampling.
    if is_hermitian:
        u = np.round(u)

    # Get integer parts of u and v.
    u_int = u.astype(np.int32)
    v_int = v.astype(np.int32)

    # Compute the fractional offset if oversampling is used.
    if oversampling_factor == 1:
        off_u = np.zeros(u_int.shape, dtype=np.int32)
        off_v = np.zeros(v_int.shape, dtype=np.int32)
        s_native = (kernel.shape[0] - 1) // 2
    else:
        off_u = np.round((u - u_int) * oversampling_factor).astype(np.int32)
        off_v = np.round((v - v_int) * oversampling_factor).astype(np.int32)
        s_native = ((kernel.shape[0] - 1) // oversampling_factor) // 2

    # Call the unified guvectorized degridding function.
    # The flag is 1 for Hermitian grids and 0 for full grids.
    estimated_data = __gu_degridding(
        u_int, v_int, im, kernel, off_u, off_v, oversampling_factor, s_native,
        1 if is_hermitian else 0
    )

    return estimated_data


#------------------------------------------------------------------------------
# Unified guvectorized degridding function (for multiple data types)
#------------------------------------------------------------------------------
@guvectorize(
    [
        # Overload for float32
        (
            int32[:], int32[:], float32[:, :], float32[:, :], int32[:], int32[:], int32, int32,
            int32, float32[:]
        ),
        # Overload for float64
        (
            int32[:], int32[:], float64[:, :], float64[:, :], int32[:], int32[:], int32, int32,
            int32, float64[:]
        ),
        # Overload for complex64
        (
            int32[:], int32[:], complex64[:, :], complex64[:, :], int32[:], int32[:], int32, int32,
            int32, complex64[:]
        ),
        # Overload for complex128
        (
            int32[:], int32[:], complex128[:, :], complex128[:, :], int32[:], int32[:], int32,
            int32, int32, complex128[:]
        ),
    ],
    '(),(),(m,n),(p,q),(),(),(),(),()->()',
    nopython=True
)
def __gu_degridding(u, v, im, kernel, off_u, off_v, os_factor, s_native, herm_flag, res):
    """
    Unified guvectorized degridding function.

    Parameters:
      u, v      : 0-D arrays (scalars) containing the integer coordinates
                  for the center of the extraction window.
      im        : 2D Fourier grid (full or Hermitian symmetric).
      kernel    : 2D half–kernel array.
      off_u, off_v : 0-D arrays (scalars) for the fractional offset indices.
      os_factor : Oversampling factor.
      s_native  : Native kernel support size (half–window size).
      herm_flag : Flag (1 for Hermitian grid, 0 for full grid).
      res       : Output scalar (result).

    The function extracts a window of size (2*s_native+1) x (2*s_native+1)
    from im, centered at (v,u). If an index falls out of bounds, it is mirrored
    about the DC center (which is (M//2, N//2) for a full grid or (M//2, 0) for
    a Hermitian grid). Mirrored pixels have their value conjugated.
    """
    M = im.shape[-2]
    N = im.shape[-1]
    if herm_flag == 0:
        center_row = M // 2
        center_col = N // 2
    else:
        center_row = M // 2
        center_col = 0

    window_size = 2 * s_native + 1

    # Allocate a temporary array for the extracted sub-image.
    sub_im = np.empty((window_size, window_size), dtype=im.dtype)
    for i in range(window_size):
        for j in range(window_size):
            # Compute the desired row and column indices.
            row_idx = v[0] - s_native + i
            col_idx = u[0] - s_native + j

            if herm_flag == 1:
                # For Hermitian grids: if either coordinate is out-of-bounds,
                # mirror the index and mark that a mirror occurred.
                if (row_idx < 0 or row_idx >= M) or (col_idx < 0 or col_idx >= N):
                    effective_row = 2 * center_row - row_idx
                    if col_idx < 0:
                        effective_col = -col_idx
                    elif col_idx >= N:
                        effective_col = 2 * (N - 1) - col_idx
                    mirror = True
                else:
                    effective_row = row_idx
                    effective_col = col_idx
                    mirror = False
            else:
                # For full grids: mirror each axis independently.
                if row_idx < 0 or row_idx >= M:
                    effective_row = 2 * center_row - row_idx
                    mirror_row = True
                else:
                    effective_row = row_idx
                    mirror_row = False
                if col_idx < 0 or col_idx >= N:
                    effective_col = 2 * center_col - col_idx
                    mirror_col = True
                else:
                    effective_col = col_idx
                    mirror_col = False
                mirror = mirror_row or mirror_col

            # Retrieve the pixel value; mirror (conjugate) if needed.
            val = im[effective_row, effective_col]
            if mirror:
                val = np.conj(val)
            sub_im[i, j] = val

    # Reconstruct the full convolution kernel from the half–kernel.
    full_kernel = np.empty((window_size, window_size), dtype=kernel.dtype)
    for i in range(window_size):
        native_i = i - s_native
        for j in range(window_size):
            native_j = j - s_native
            if os_factor == 1:
                full_kernel[i, j] = kernel[abs(native_i), abs(native_j)]
            else:
                r = abs(native_i)
                c = abs(native_j)
                # Compute oversampled indices (with clamping).
                row_index = min(r * os_factor + off_v[0], s_native * os_factor)
                col_index = min(c * os_factor + off_u[0], s_native * os_factor)
                full_kernel[i, j] = kernel[row_index, col_index]
    # Compute the degridded value as the sum of the element–wise product.
    res[0] = np.nansum(sub_im * full_kernel)


###############################################################################
# Numba CPU version for half–kernel degridding.
# This version mirrors out-of–bounds indices in the same way as __gu_degridding.
###############################################################################
@njit(parallel=True)
def __numba_degridding_cpu(u, v, im, kernel, off_u, off_v, os_factor, herm_flag):
    """
    Numba CPU degridding function.

    Parameters:
      u, v      : 1D arrays of integer center coordinates.
      im        : 2D Fourier grid.
      kernel    : 2D half–kernel.
      off_u, off_v : 1D arrays of fractional offset indices.
      os_factor : Oversampling factor.
      herm_flag : Integer flag: 1 for Hermitian grid, 0 for full grid.

    Returns
    -------
      res : 1D array of degridded values.

    This function replicates the mirroring logic of __gu_degridding.
    """
    n = u.shape[0]
    if os_factor == 1:
        s_native = (kernel.shape[0] - 1) // 2
    else:
        s_native = ((kernel.shape[0] - 1) // os_factor) // 2
    window_size = 2 * s_native + 1
    M = im.shape[0]
    N = im.shape[1]
    if herm_flag == 0:
        center_row = M // 2
        center_col = N // 2
    else:
        center_row = M // 2
        center_col = 0

    res = np.empty(n, dtype=im.dtype)
    for k_idx in prange(n):
        ui = u[k_idx]
        vi = v[k_idx]
        # Allocate temporary arrays for the extracted window and the full kernel.
        sub_im = np.empty((window_size, window_size), dtype=im.dtype)
        full_kernel = np.empty((window_size, window_size), dtype=kernel.dtype)
        for i in range(window_size):
            native_i = i - s_native
            for j in range(window_size):
                native_j = j - s_native
                # Compute the desired indices in im.
                row_idx = vi - s_native + i
                col_idx = ui - s_native + j
                if herm_flag == 1:
                    if (row_idx < 0 or row_idx >= M or col_idx < 0 or col_idx >= N):
                        effective_row = 2 * center_row - row_idx
                        if col_idx < 0:
                            effective_col = -col_idx
                        elif col_idx >= N:
                            effective_col = 2 * (N - 1) - col_idx
                        else:
                            effective_col = col_idx
                        mirror = True
                    else:
                        effective_row = row_idx
                        effective_col = col_idx
                        mirror = False
                else:
                    if row_idx < 0 or row_idx >= M:
                        effective_row = 2 * center_row - row_idx
                        mirror_row = True
                    else:
                        effective_row = row_idx
                        mirror_row = False
                    if col_idx < 0 or col_idx >= N:
                        effective_col = 2 * center_col - col_idx
                        mirror_col = True
                    else:
                        effective_col = col_idx
                        mirror_col = False
                    mirror = mirror_row or mirror_col
                # Retrieve pixel value and conjugate if mirrored.
                pixel_val = im[effective_row, effective_col]
                if mirror:
                    pixel_val = np.conj(pixel_val)
                sub_im[i, j] = pixel_val

                # Compute the full kernel weight.
                if os_factor == 1:
                    full_kernel[i, j] = kernel[abs(native_i), abs(native_j)]
                else:
                    r = abs(native_i)
                    c = abs(native_j)
                    row_index = r * os_factor + off_v[k_idx]
                    if row_index > s_native * os_factor:
                        row_index = s_native * os_factor
                    col_index = c * os_factor + off_u[k_idx]
                    if col_index > s_native * os_factor:
                        col_index = s_native * os_factor
                    full_kernel[i, j] = kernel[row_index, col_index]
        s_val = 0.0
        for i in range(window_size):
            for j in range(window_size):
                s_val += sub_im[i, j] * full_kernel[i, j]
        res[k_idx] = s_val
    return res


###############################################################################
# Numba GPU version for half–kernel degridding.
# This version mirrors out–of–bounds indices in the same way as __gu_degridding.
###############################################################################
@cuda.jit
def __numba_degridding_gpu(u, v, im, kernel, off_u, off_v, os_factor, herm_flag, res):
    """
    Numba GPU degridding function.

    Parameters:
      u, v, off_u, off_v, res : 1D device arrays.
      im, kernel : 2D device arrays.
      os_factor : Oversampling factor.
      herm_flag : 1 if using a Hermitian grid, 0 for full grid.

    For each index, extracts a window from im and computes the degridded value
    using mirroring (if out–of–bounds) and reconstructing the full kernel.
    """
    k_idx = cuda.grid(1)
    n = u.shape[0]
    if k_idx < n:
        if os_factor == 1:
            s_native = (kernel.shape[0] - 1) // 2
        else:
            s_native = ((kernel.shape[0] - 1) // os_factor) // 2
        window_size = 2 * s_native + 1
        M = im.shape[0]
        N = im.shape[1]
        if herm_flag == 0:
            center_row = M // 2
            center_col = N // 2
        else:
            center_row = M // 2
            center_col = 0

        ui = u[k_idx]
        vi = v[k_idx]

        s_val = 0.0
        for i in range(window_size):
            native_i = i - s_native
            for j in range(window_size):
                native_j = j - s_native
                row_idx = vi - s_native + i
                col_idx = ui - s_native + j
                mirror = False
                if herm_flag == 1:
                    if row_idx < 0 or row_idx >= M or col_idx < 0 or col_idx >= N:
                        effective_row = 2 * center_row - row_idx
                        if col_idx < 0:
                            effective_col = -col_idx
                        elif col_idx >= N:
                            effective_col = 2 * (N - 1) - col_idx
                        else:
                            effective_col = col_idx
                        mirror = True
                    else:
                        effective_row = row_idx
                        effective_col = col_idx
                else:
                    if row_idx < 0 or row_idx >= M:
                        effective_row = 2 * center_row - row_idx
                        mirror_row = True
                    else:
                        effective_row = row_idx
                        mirror_row = False
                    if col_idx < 0 or col_idx >= N:
                        effective_col = 2 * center_col - col_idx
                        mirror_col = True
                    else:
                        effective_col = col_idx
                        mirror_col = False
                    mirror = mirror_row or mirror_col

                # Get the pixel value.
                val = im[effective_row, effective_col]
                if mirror:
                    # For complex numbers, take the conjugate.
                    # (For real numbers, this is a no–op.)
                    val = cuda.conj(val)
                # Compute the kernel weight.
                if os_factor == 1:
                    k_val = kernel[abs(native_i), abs(native_j)]
                else:
                    r = abs(native_i)
                    c = abs(native_j)
                    row_index = r * os_factor + off_v[k_idx]
                    if row_index > s_native * os_factor:
                        row_index = s_native * os_factor
                    col_index = c * os_factor + off_u[k_idx]
                    if col_index > s_native * os_factor:
                        col_index = s_native * os_factor
                    k_val = kernel[row_index, col_index]
                s_val += val * k_val
        res[k_idx] = s_val
