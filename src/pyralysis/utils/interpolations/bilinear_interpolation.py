"""
Module for bilinear interpolation.

Provides functions to perform bilinear interpolation on 2D or 3D grids,
with an option to enforce sorting of the coordinate arrays.
"""

from typing import Union

import dask.array as da
import numpy as np

from ...utils.sort import sort_uv  # sort_uv should sort u and v together


def bilinear_interpolation(
    u: Union[float, np.ndarray, da.Array],
    v: Union[float, np.ndarray, da.Array],
    im: Union[np.ndarray, da.Array],
    enforce_sorting: bool = False,
) -> Union[float, np.ndarray, da.Array]:
    """
    Perform bilinear interpolation for UV coordinates on a 2D or 3D grid.

    Parameters
    ----------
    u : float, np.ndarray, or da.Array
        U coordinates (pixel indices). May be fractional.
    v : float, np.ndarray, or da.Array
        V coordinates (pixel indices). May be fractional.
    im : np.ndarray or da.Array
        Input image (or grid) on which to interpolate.
    enforce_sorting : bool, optional
        If True, sorts the (u, v) coordinates to ensure monotonic ordering.
        Default is False.

    Returns
    -------
    float, np.ndarray, or da.Array
        The interpolated value(s) at the given UV coordinates.
    """
    # Convert inputs to NumPy arrays (or dask arrays) for uniform processing.
    u = np.asarray(u)
    v = np.asarray(v)

    # Optionally sort the coordinates (and remember the original order).
    if enforce_sorting:
        (u, v), original_indices = sort_uv(u, v)
    else:
        original_indices = None

    # Compute integer (floor) indices from the coordinates.
    u0 = u.astype(np.int32)
    v0 = v.astype(np.int32)

    # Determine which indices fall within the image bounds and are not NaN.
    inside = (u0 >= 0) & (v0 >= 0) & (u0 < im.shape[-1]) & (v0 < im.shape[-2])
    inside &= ~np.isnan(u) & ~np.isnan(v)

    # Multiply the integer indices by the boolean mask so that out-of-bound pixels
    # will be zero (this will later be masked out).
    u0_inside = u0 * inside
    v0_inside = v0 * inside

    # Compute the next pixel indices (u0 + 1, v0 + 1) for the bilinear window.
    u1 = u0_inside + 1
    v1 = v0_inside + 1

    # Compute the bilinear weights based on distances from the integer grid.
    wa = (u1 - u) * (v1 - v)
    wb = (u1 - u) * (v - v0_inside)
    wc = (u - u0_inside) * (v1 - v)
    wd = (u - u0_inside) * (v - v0_inside)

    # Ensure that indices are valid integers.
    v0_inside = np.nan_to_num(v0_inside).astype(np.int32)
    u0_inside = np.nan_to_num(u0_inside).astype(np.int32)

    # Determine whether the next indices are within bounds.
    u1_inside = (u1 >= 0) & (u1 < im.shape[-1])
    v1_inside = (v1 >= 0) & (v1 < im.shape[-2])
    uv1_inside = u1_inside & v1_inside

    # Use the inside mask to force out-of-bound indices to 0.
    v1 = (v1 * v1_inside).astype(np.int32)
    u1 = (u1 * u1_inside).astype(np.int32)

    # Determine the row indices for a 3D image. For a 2D image the code is incomplete.
    if im.ndim == 2:
        # (Implementation for 2D case should be added here.)
        idx_0 = ...  # Placeholder
    elif im.ndim == 3:
        # Generate an index array for the first dimension.
        _, idx_0 = np.indices(u0.shape)

    # Extract the four image corners for the bilinear interpolation.
    im_a = im[idx_0, v0_inside, u0_inside] * inside
    im_b = im[idx_0, v1, u0_inside] * v1_inside
    im_c = im[idx_0, v0_inside, u1] * u1_inside
    im_d = im[idx_0, v1, u1] * uv1_inside

    # Compute the weighted sum.
    estimated_data = (wa * im_a + wb * im_b + wc * im_c + wd * im_d).astype(im.dtype)

    # For coordinates outside, set the interpolated value to zero.
    estimated_data = np.where(inside, estimated_data, 0.0)

    # If coordinates were sorted, restore the original ordering.
    if enforce_sorting and original_indices is not None:
        estimated_data = estimated_data[np.argsort(original_indices)]

    return estimated_data


def __bilinear_interpolation_1d(
    uv: Union[np.ndarray, da.Array],
    im: Union[np.ndarray, da.Array],
) -> float:
    """
    Perform bilinear interpolation for a single pair of UV coordinates.

    This helper function assumes uv is a two–element array with u and v.

    Parameters
    ----------
    uv : np.ndarray or da.Array
        A two–element array containing the u and v coordinates.
    im : np.ndarray or da.Array
        The 2D image (or grid) to interpolate from.

    Returns
    -------
    float
        The bilinearly interpolated value.
    """
    (u, v) = uv

    # Compute integer indices.
    u0 = u.astype(np.int32)
    v0 = v.astype(np.int32)

    # Check if the indices are out-of-bounds or NaN.
    outside = (u0 < 0) + (v0 < 0) + (u0 > im.shape[1] - 1) + (v0 > im.shape[0] - 1)
    outside += np.isnan(u0) + np.isnan(v0)

    if outside:
        return 0.0

    # Compute the next indices.
    u1 = u0 + 1
    v1 = v0 + 1

    # Compute interpolation weights.
    wa = (u1 - u) * (v1 - v)
    wb = (u1 - u) * (v - v0)
    wc = (u - u0) * (v1 - v)
    wd = (u - u0) * (v - v0)

    # Check whether u1 and v1 are within bounds.
    u1_outside = (u1 > im.shape[1] - 1)
    v1_outside = (v1 > im.shape[0] - 1)

    im_a = im[v0, u0]
    im_b = 0.0 if v1_outside else im[v1, u0]
    im_c = 0.0 if u1_outside else im[v0, u1]
    im_d = 0.0 if (v1_outside | u1_outside) else im[v1, u1]

    return wa * im_a + wb * im_b + wc * im_c + wd * im_d
