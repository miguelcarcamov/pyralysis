import dask.array as da
import numpy as np


def remove_padding(data, expected_size, padding=None):
    """Removes symmetric or asymmetric padding from the input array.

    Parameters
    ----------
    data : ndarray or dask.array
        The input array with shape (..., height, width).
    expected_size : tuple
        The target shape as (height, width) to which the array should be cropped.
    padding : tuple of tuples, optional
        Padding applied to the data in the format ((top, bottom), (left, right)).
        If not provided, symmetric padding will be assumed.

    Returns
    -------
    ndarray or dask.array
        The array with the padding removed.
    """
    data_height, data_width = data.shape[-2:]
    expected_height, expected_width = expected_size

    if padding is None:
        # Calculate padding differences (symmetric assumption)
        total_pad_y = data_height - expected_height
        total_pad_x = data_width - expected_width

        if total_pad_y < 0 or total_pad_x < 0:
            raise ValueError("Expected size must be smaller or equal to the input size.")

        # Compute start and end indices for cropping
        start_y = total_pad_y // 2
        end_y = start_y + expected_height

        start_x = total_pad_x // 2
        end_x = start_x + expected_width
    else:
        # Handle asymmetric padding using the provided padding tuple
        start_y, end_y = padding[0][0], data_height - padding[0][1]
        start_x, end_x = padding[1][0], data_width - padding[1][1]

    # Slice to remove padding
    depadded = data[..., start_y:end_y, start_x:end_x]

    # Validate the resulting shape matches the expected size
    if depadded.shape[-2:] != expected_size:
        raise ValueError(
            f"Depadded shape {depadded.shape[-2:]} does not match expected size {expected_size}"
        )

    return depadded


def calculate_padding(data, expected_size):
    """Calculates the padding needed to center an image within a target size.

    Parameters:
        data: ndarray or dask array
            The input array with shape (..., height, width).
        expected_size: tuple
            The target shape as (height, width).

    Returns:
        tuple: Padding for the y-axis (height) and x-axis (width)
               in the format ((top, bottom), (left, right)).
    """
    # Extract current dimensions of the data
    data_height = data.shape[-2]
    data_width = data.shape[-1]

    # Extract the target dimensions
    expected_height = expected_size[-2]
    expected_width = expected_size[-1]

    # Calculate padding for the y-axis (height)
    top = (expected_height - data_height) // 2
    bottom = expected_height - top - data_height

    # Calculate padding for the x-axis (width)
    left = (expected_width - data_width) // 2
    right = expected_width - left - data_width

    # Adjust chunks for Dask arrays
    new_chunks = None
    if isinstance(data, da.Array):
        old_chunks = data.chunks

        # Adjust height (y-axis) chunks
        height_chunks = old_chunks[-2]
        num_chunks_height = len(height_chunks)
        new_chunk_height = expected_height // num_chunks_height
        adjusted_height_chunks = tuple(
            new_chunk_height for _ in range(num_chunks_height - 1)
        ) + (expected_height - new_chunk_height * (num_chunks_height - 1), )

        # Adjust width (x-axis) chunks
        width_chunks = old_chunks[-1]
        num_chunks_width = len(width_chunks)
        new_chunk_width = expected_width // num_chunks_width
        adjusted_width_chunks = tuple(
            new_chunk_width for _ in range(num_chunks_width - 1)
        ) + (expected_width - new_chunk_width * (num_chunks_width - 1), )

        # Combine adjusted chunks
        new_chunks = list(old_chunks)
        new_chunks[-2] = adjusted_height_chunks
        new_chunks[-1] = adjusted_width_chunks

    return (top, bottom), (left, right), tuple(new_chunks) if new_chunks else None


def apply_padding(data, padding_factor):
    """Dynamically calculates and applies padding to a data array, preserving
    chunking for Dask arrays.

    Parameters:
        data: ndarray or dask array
            Input array with shape (..., height, width).
        padding_factor: float
            The factor by which to expand the dimensions.

    Returns:
        padded_data: ndarray or dask array
            Padded version of the input data.
        pad_width: list of tuples
            The padding applied to each dimension.
        new_chunks: tuple or None
            New chunk sizes for Dask arrays if applicable.
    """
    # Determine the target padded shape
    padded_image_shape = tuple(int(padding_factor * x) for x in data.shape[-2:])

    # Calculate padding and new chunks
    vertical_pad, horizontal_pad, new_chunks = calculate_padding(data, padded_image_shape)

    # Set up pad_width
    pad_width = [(0, 0)] * data.ndim
    pad_width[-1] = horizontal_pad
    pad_width[-2] = vertical_pad

    # Apply padding
    if isinstance(data, da.Array):
        # Apply padding to Dask array
        padded_data = da.pad(data, pad_width, mode="constant")
        if new_chunks:
            padded_data = padded_data.rechunk(new_chunks)
    else:
        # Apply padding to NumPy array
        padded_data = np.pad(data, pad_width, mode="constant")

    return padded_data, pad_width, new_chunks
