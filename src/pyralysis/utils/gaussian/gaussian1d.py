from abc import ABCMeta
from dataclasses import dataclass

import numpy as np
from numba import vectorize


@vectorize
def evaluate_gaussian1D(x, amp, mu, sigma):
    return amp * np.exp(-0.5 * ((x - mu) / sigma)**2)


@dataclass(init=False, repr=True)
class Gaussian1D(metaclass=ABCMeta):
    """Class that represents a 1D Gaussian :param amplitude: Amplitude of the
    Gaussian :param mu: Center of the Gaussian :param sigma: Standard deviation
    of the Gaussian :param fwhm: FWHM of the Gaussian."""
    amplitude: float
    mu: float
    sigma: float
    fwhm: float

    def __init__(
        self,
        amplitude: float = None,
        mu: float = None,
        sigma: float = None,
        fwhm: float = None,
    ):

        if amplitude is not None:
            self.amplitude = amplitude
        else:
            self.amplitude = 1.0

        if mu is not None:
            self.mu = mu
        else:
            self.mu = 0.0

        if fwhm is None and sigma is not None:
            self.sigma = sigma
        elif fwhm is not None:
            self.fwhm = fwhm

        if sigma is None and fwhm is None:
            self.sigma = 1.0

    @property
    def sigma(self):
        return self.__sigma

    @sigma.setter
    def sigma(self, val):
        self.__sigma = val
        val_fwhm = 2.0 * np.sqrt(2.0 * np.log(2.0))
        self.__fwhm = val * val_fwhm

    @property
    def fwhm(self):
        return self.__fwhm

    @fwhm.setter
    def fwhm(self, val):
        self.__fwhm = val
        val_fwhm = 2.0 * np.sqrt(2.0 * np.log(2.0))
        self.__sigma = val / val_fwhm

    def evaluate(self, x=None):
        if x is not None:
            return evaluate_gaussian1D(x, self.amplitude, self.mu, self.sigma)
