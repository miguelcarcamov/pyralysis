from typing import Union

import dask.array as da
import numpy as np
from scipy.special import j1


def airy_disk(
    k_aperture: np.ndarray,
    x_grid: np.ndarray,
    y_grid: np.ndarray,
    x_0: Union[int, float] = 0,
    y_0: Union[int, float] = 0
) -> np.ndarray:
    """Airy model function.

    Parameters
    ----------
    k_aperture : np.ndarray
        Variable that stores the constant term k (wave number) multiplied by the aperture.
    x_grid : np.ndarray
        x_grid : np.ndarray
    y_grid : np.ndarray
        Array indexing the 2nd dim of the image
    x_0 : Union[int, float], optional
        x position of the maximum of the Airy function, by default 0
    y_0 : Union[int, float], optional
        y position of the maximum of the Airy function, by default 0

    Returns
    -------
    np.ndarray
        Airy disk primary beam

    Notes
    -----
    Model formula:

        .. math:: f(r) = A \\left[
                \\frac{2 J_1(\\pi \\frac{r}{R/R_z})}
                {\\pi\\frac{r}{R/R_z}}
            \\right]^2

    Where :math:`A` is the amplitude of the Airy function, :math:`J_1` is the first order Bessel
    function of the first kind, :math:`r` is radial distance from the center of the disk
    (:math:`r =\\sqrt{x^2 + y^2}`), :math:`R_z = 1.2196698912665045`, and :math:`R` (radius) is
    given approximately by:

        .. math:: R \\approx 1.22 \\frac{\\lambda}{D},

    where :math:`\\lambda` is the observed wavelength and :math:`D` the dish diameter or
    diameter of the aperture.
    """
    k_aperture = k_aperture[:, np.newaxis, np.newaxis]
    radius = da.sqrt((x_grid - x_0)**2 + (y_grid - y_0)**2)
    if not np.isscalar(radius) and isinstance(radius, (da.Array, np.ndarray)):
        radius = radius[np.newaxis, :, :]

    bessel_arg = k_aperture * radius
    mask = bessel_arg == 0
    # Avoid division by zero at r=0
    bessel_arg[mask] = 1e-10
    # Compute with the Airy Disk formula
    beam = (2.0 * j1(bessel_arg) / bessel_arg)**2
    return beam


def gaussian(
    fwhm: np.ndarray,
    x_grid: np.ndarray,
    y_grid: np.ndarray,
    x_0: Union[int, float] = 0,
    y_0: Union[int, float] = 0
) -> np.ndarray:
    """Gaussian function.

    Parameters
    ----------
    fwhm : np.ndarray
        Full width at half maximum (FWHM) of the primary beam. Limit of the angular resolution/resolving power of the antenna
    x_grid : np.ndarray
        Array indexing the 1st dim of the image
    y_grid : np.ndarray
        Array indexing the 2nd dim of the image
    x_0 : Union[int, float], optional
        x position of the maximum of the Gaussian function, by default 0
    y_0 : Union[int, float], optional
        y position of the maximum of the Gaussian function, by default 0

    Returns
    -------
    np.ndarray
        Gaussian 2d primary beam

    Notes
    -----
    Model formula:

        .. math:: f(r) = A e^{-4 ln(2) \\left(
                \\frac{r}{R}
            \\right)^2}

    Where :math:`A` is the amplitude or peak value of the Gaussian, :math:`r` is radial distance
    from the peak (:math:`r = \\sqrt{x^2 + y^2}`), and :math:`R` (radius) is given approximately
    by:

        .. math:: R \\approx 1.22 \\frac{\\lambda}{D},

    where :math:`\\lambda` is the observed wavelength and :math:`D` the dish diameter or
    diameter of the aperture.
    """
    fwhm = fwhm[:, np.newaxis, np.newaxis]
    radius = da.sqrt((x_grid - x_0)**2 + (y_grid - y_0)**2)
    if not np.isscalar(radius) and isinstance(radius, (da.Array, np.ndarray)):
        radius = radius[np.newaxis, :, :]
    sigma = fwhm / (2 * da.sqrt(2 * da.log(2)))
    beam = da.exp(-radius**2 / (2 * sigma**2))
    return beam
