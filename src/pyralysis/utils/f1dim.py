from pyralysis.optimization.objective_function import ObjectiveFunction
from pyralysis.optimization.projection import Projection
from pyralysis.reconstruction.image import Image
from pyralysis.reconstruction.mask import Mask


def f1dim(
    of: ObjectiveFunction, image: Image, projection: Projection, mask: Mask = None
) -> callable:
    """Evaluates a 1-dim value on a function from ObjectiveFunction.

    Evaluates a function with a single float as a parameter based on the results of the objective
    function. The function is primarily used to search for the step to take in gradient descent,
    where the step is the parameter of the created function.

    The created callable evaluates the ObjectiveFunction's function with a new image, which is
    computed as :math:`I - \\alpha G`, where :math:`I` is the image used in the objective function,
    :math:`G` is the gradient obtained from the objective function, and :math:`\\alpha` is the
    parameter of the function.

    Parameters
    ----------
    of : pyralysis.optimization.ObjectiveFunction
        Objective function with precomputed gradient.
    image : pyralysis.reconstruction.Image
        Image used in the objective function to compute the function and gradient.
    projection : pyralysis.optimization.projection.Projection
        Projection to modify the values of an image that meets certain conditions.
    mask : pyralysis.reconstruction.mask.Mask
        Mask for the image used in the objective function to compute the function and gradient.

    Returns
    -------
    callable
        Function that takes a ``float`` as a parameter and returns a ``float`` representing the
        result of evaluating the objective function with an updated image.
    """
    # Store gradient value before updating of's image and resetting its value
    grad = of.dphi
    data = image.data

    def wrapped_f1dim(x: float) -> float:
        image_array = data - x * grad
        image.data = image_array  # Update image data
        projection(image)  # Apply a projection to the new image
        of.fi_image(image)  # Set the image in each Fi
        res = of.calculate_function(mask=mask)
        return res

    return wrapped_f1dim
