from typing import Tuple

import numpy as np


def sort_uv(uv: tuple) -> Tuple[tuple, np.ndarray]:
    """Sort UV coordinates (1D arrays) for monotonicity.

    Parameters
    ----------
    uv : tuple
        Tuple of (u, v) coordinates, both as 1D arrays.

    Returns
    -------
    tuple
        Sorted UV coordinates (u, v).
    np.ndarray
        Original indices for restoring order after sorting.
    """
    u, v = uv

    # Sort using lexicographical order
    indices = np.lexsort((v, u))
    sorted_u = u[indices]
    sorted_v = v[indices]
    return (sorted_u, sorted_v), indices
