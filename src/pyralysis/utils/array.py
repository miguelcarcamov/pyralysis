from math import prod

import dask.array as da
import numpy as np


def ravel_chunks(array: da.Array) -> da.Array:
    """Ravel a dask array chunk by chunk.

    Parameters
    ----------
    array : da.Array
        The shape and data-type of `array` define these same attributes of the
        returned array.

    Returns
    -------
    da.Array
        1-dimensional array of data with the same type as `array`, with shape `(a.size,)`. Each
        chunk has the same size of each chunk in `array`.
    """

    pass
