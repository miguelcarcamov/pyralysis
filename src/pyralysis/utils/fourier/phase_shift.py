from typing import Union

import dask.array as da
import numpy as np


def phase_shift_grid(
    fourier_image_data: da.Array, x0: Union[int, float], y0: Union[int, float]
) -> da.Array:
    """Shifts image to position x0, y0 in a gridded Fourier space.

    Shifts image to position x0, y0 by multiplying its Fourier space by an exponential using the shifting theorem.

    Parameters
    ----------
    fourier_image_data : da.Array
        The Fourier transform of the image with the 0-center on the center of the image (fftshift applied)
    x0 : Union[int, float]
        New position in the horizontal axis (in pixels or fraction of pixels).
    y0 : Union[int, float]
        New position in the vertical axis (in pixels or fraction of pixels).

    Returns
    -------
    da.Array
        Shifted Fourier space
    """
    # We take the last two dimensions
    m, n = fourier_image_data.shape[-2:]

    # If Fourier space is made with rfft2
    if m != n:
        # Here n=image_shape[-1]//2 + 1, so we subtract 1 and multiply by 2
        u_data = da.fft.rfftfreq((n - 1) * 2)
    # If using fft2
    else:
        u_data = da.fft.fftshift(da.fft.fftfreq(n))

    v_data = da.fft.fftshift(da.fft.fftfreq(m))

    ku = u_data[np.newaxis, :]
    kv = v_data[:, np.newaxis]

    shifted_fimage = fourier_image_data * da.exp(
        -1j * 2. * np.pi * (ku * x0 + kv * y0), dtype=fourier_image_data.dtype
    )

    return shifted_fimage
