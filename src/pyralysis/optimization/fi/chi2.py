from dataclasses import dataclass
from typing import List, Tuple, Union

import astropy.units as un
import dask.array as da
import numpy as np
from astropy.units.equivalencies import Equivalency
from multimethod import multimethod

from ...base import Dataset, SubMS
from ...dft import idft2
from ...reconstruction.mask import Mask
from ...transformers import ModelVisibilities
from ...units import array_unit_conversion
from ...units.lambda_units import lambdas_equivalencies
from ...utils.decorators import temp_irreg_local_save, temp_local_save
from .fi import Fi


@dataclass(init=True, repr=True)
class Chi2(Fi):
    """Class to evaluate the chi2 function and gradient over a Dataset.

    Parameters
    ----------
    dataset : pyralysis.base.Dataset, optional
        Dataset to evaluate chi2, by default None
    model_visibility: pyralysis.transformers.ModelVisibilities
        Object that can compute the model visibility of a Dataset, required for evaluating chi2

    Attributes
    ----------
    dataset : pyralysis.base.Dataset
        Dataset to evaluate chi2
    model_visibility: pyralysis.transformers.ModelVisibilities
        Object that can compute the model visibility of a Dataset, required for evaluating chi2

    See Also
    --------
    Fi, Entropy, L1Norm
    """
    dataset: Dataset = None
    model_visibility: ModelVisibilities = None
    _corr: frozenset = frozenset(["XX", "YY", "RR", "LL"])

    def __post_init__(self):
        """Set attributes when there's lack of data in any variable."""
        # Copy dataset from ModelVisibility if there is no dataset
        if self.dataset is None:
            self.dataset = self.model_visibility.input_data
        else:
            self.model_visibility.input_data = self.dataset

        if self.image is None:
            self.image = self.model_visibility.image
        else:
            self.model_visibility.image = self.image

        # Set/update primary beam cellsize
        # An alternative is self.dataset.antenna.primary_beam.cellsize = self.image.cellsize
        self.dataset.antenna.primary_beam.cellsize = self.model_visibility.cellsize

    def __setattr__(self, name, value):
        # Overrides __setattr__ to init or update model_visibility image
        if name == "image" and value is not None and "model_visibility" in self.__dict__:
            # Update ModelVisibilities image
            self.model_visibility.image = value
        # Continue normal __setattr__ behavior
        super().__setattr__(name, value)

    def function(self, *, mask: Mask = None):
        """Computes the value of applying chi2 over the dataset.

        Estimates the model visibility and then applies the chi2 function over every SubMS.

        Parameters
        ----------
        mask : pyralysis.reconstruction.mask.Mask, optional
            Mask object that is not used in this function but is a parameter of the Fi function
            method, by default None

        Returns
        -------
        da.Array
            Array of 0-dim with the result of applying chi2 over the dataset

        Notes
        -----
        Formula:

            .. math:: \\chi^2 = \\frac{1}{2}
                    \\sum_{k} \\bigg |  \\frac{V_k^m(I)-V_k^o}{\\sigma_k} \\bigg |  ^2
        """
        n_ms = self.dataset.ndatasets
        sum_ms = da.zeros(n_ms, dtype=np.float32)

        # Compute new values of model visibilities
        self.model_visibility.transform()

        for i, ms in enumerate(self.dataset.ms_list):
            pol_id = ms.polarization_id
            spw_id = ms.spw_id
            nchans = self.dataset.spws.nchans[spw_id]
            corr_names = self.dataset.polarization.corrs_string[pol_id]
            corr_idx = [x in self._corr for x in corr_names]

            w = ms.visibilities.imaging_weight.data
            flag = ms.visibilities.flag.data

            w_broadcast = da.repeat(w[:, np.newaxis, :], nchans,
                                    axis=1).rechunk(flag.chunks) * ~flag

            vis_model = ms.visibilities.model.data
            vis_observed = ms.visibilities.data.data
            vis_residual = vis_observed - vis_model

            vr2 = (
                w_broadcast *
                (vis_residual.real * vis_residual.real + vis_residual.imag * vis_residual.imag)
            ).astype(np.float32)
            vr2_corr = vr2[:, :, corr_idx]  # Filter by correlation
            sum_ms[i] = da.sum(vr2_corr)

            if self.normalize:
                sum_ms[i] /= ms.visibilities.visibility_number

        self._func_value = da.sum(sum_ms) * 0.5

        return self._func_value * self.penalization_factor

    def prox(self):
        return 0

    def gradient(self, iter=1, *, mask: Mask = None):
        """Computes the gradient of chi2 over the Dataset.

        Parameters
        ----------
        iter : int
            Number of iteration
        mask : pyralysis.reconstruction.mask.Mask
            Mask object with a boolean array that indicates which cells to consider to compute the
            gradient, by default None

        Returns
        -------
        da.Array
            Array of 2/3-dim with the result of applying the gradient of chi2 over the dataset

        Notes
        -----
        Every coordinate of the :math:`\\chi^2` gradient has a value that follows the
        equation:

            .. math:: [\\nabla \\chi^2]_i = \\sum_{k=0}^{Z-1}
                    \\frac{Re\\left(V_k^R e^{2\\pi i X_i \\cdot U_k}\\right)}{\\sigma_k^2}

        where :math:`V^R=\\{V_k^R\\}` is the residual visibility calculated from
        :math:`V_k^R=V_k^m - V_k^o`.
        """
        dchi2_1d = self._gradient(mask)

        self._grad_value = self.unflatten_gradient_image(dchi2_1d, mask)

        return self._grad_value

    @multimethod
    def unflatten_gradient_image(self, image: da.Array, mask: Mask) -> da.Array:
        # Array full with zeros to assign only valid indices with the values of the gradient
        dchi2_1d = da.zeros((np.prod(self.image.data.shape), ), dtype=np.float32)
        # Get ravel indices of the mask where its values are True
        ravel_mask_idx = da.where(mask.data.data.ravel())[0]
        ravel_mask_idx.compute_chunk_sizes()
        # In the array, assign only valid indices (ravel_mask_idx) with the gradient
        dchi2_1d[(ravel_mask_idx, )] = image
        # Reshape gradient (transposed because indices are in F order)
        image_2d = dchi2_1d.reshape(self._grad_value.shape).T
        return image_2d.rechunk(self._grad_value.chunksize)

    @unflatten_gradient_image.register
    def _(self, image: da.Array, _: None) -> da.Array:
        # Reshape gradient (transposed because indices are in F order)
        image_2d = image.reshape(self._grad_value.shape).T
        return image_2d.rechunk(self._grad_value.chunksize)

    @multimethod
    def _gradient(self, mask: Mask):

        delta_x, delta_y = self.image.cellsize.to(un.rad).value

        # Check that mask shape is compatible
        if not self.mask_shape_check(mask):
            raise ValueError(
                (
                    f"Provided mask of shape {mask.data.shape} is not compatible",
                    f" with image of shape {self.image.data.shape}"
                )
            )

        x_ind, y_ind, *z_ind = mask.indices

        dchi2_1d_masked = da.zeros_like(x_ind, dtype=np.float32, chunks=x_ind.chunks)

        l_direction = self.dataset.field.phase_direction_cosines[0].value
        m_direction = self.dataset.field.phase_direction_cosines[1].value

        for i, ms in enumerate(self.dataset.ms_list):
            dchi2_1d_masked += self.__ms_gradient(
                ms, x_ind, y_ind, l_direction, m_direction, delta_x, delta_y, mask=mask
            )

        return dchi2_1d_masked

    @_gradient.register
    def _(self, mask: None):
        delta_x, delta_y = self.image.cellsize.to(un.rad).value

        x_ind_2d, y_ind_2d, *z_ind_2d = da.indices(
            self.image.data.data.shape, dtype=np.int32, chunks=self.image.data.data.chunksize
        )

        x_ind = x_ind_2d.ravel()
        y_ind = y_ind_2d.ravel()

        dchi2_1d = da.zeros_like(x_ind, dtype=np.float32, chunks=x_ind.chunks)

        l_direction = self.dataset.field.phase_direction_cosines[0].value
        m_direction = self.dataset.field.phase_direction_cosines[1].value

        for i, ms in enumerate(self.dataset.ms_list):
            dchi2_1d += self.__ms_gradient(
                ms, x_ind, y_ind, l_direction, m_direction, delta_x, delta_y
            )

        return dchi2_1d

    def __ms_gradient(
        self,
        ms: SubMS,
        x_cell: da.Array,
        y_cell: da.Array,
        field_centers_l: np.ndarray,
        field_centers_m: np.ndarray,
        delta_x: float,
        delta_y: float,
        *,
        mask: Mask = None
    ) -> da.Array:
        """Computes the gradient of chi2 for a SubMS.

        Parameters
        ----------
        ms : pyralysis.base.SubMS
            Partitioned Measurement Set for which the gradient is calculated
        x_cell : da.Array
            Coordinate array for the first dimension of the image scaled by the corresponding
            cellsize
        y_cell : da.Array
            Coordinate array for the second dimension of the image scaled by the corresponding
            cellsize
        field_centers_x : np.ndarray
            Field center l coordinates in radians of all the fields in the dataset
        field_centers_m : np.ndarray
            Field center m coordinates in radians of all the fields in the dataset
        delta_x : float
            Size of a pixel in the first dimension or axis1 (in radians)
        delta_y : float
            Size of a pixel in the second dimension or axis2 (in radians)
        mask : pyralysis.reconstruction.mask.Mask, optional
            Mask object with a boolean array that indicates which cells to consider to compute the
            gradient

        Returns
        -------
        da.Array
            Gradient of chi2 for the SubMS
        """
        uvw = ms.visibilities.uvw.data.astype(np.float32) * un.m
        w = ms.visibilities.imaging_weight.data
        flag = ms.visibilities.flag.data
        vis_model = ms.visibilities.model.data
        vis_observed = ms.visibilities.data.data
        vis_residual = vis_observed - vis_model

        field_id = ms.field_id
        pol_id = ms.polarization_id
        spw_id = ms.spw_id
        corr_names = self.dataset.polarization.corrs_string[pol_id]

        corr_idx = [x in self._corr for x in corr_names]

        nchans = self.dataset.spws.nchans[spw_id]
        equiv = self.dataset.spws.equivalencies[spw_id]

        w_broadcast = da.repeat(w[:, np.newaxis, :], nchans, axis=1).rechunk(flag.chunks) * ~flag

        uvw_lambdas_chunks = da.core.normalize_chunks(
            flag.chunks[:-1] + uvw.chunks[-1:], shape=(uvw.shape[0], nchans, 3), dtype=uvw.dtype
        )
        uvw_lambdas = self.__uvw_lambdas(uvw, equiv, nchans, uvw_lambdas_chunks)
        uvw_lambdas = uvw_lambdas.map_blocks(
            lambda x: x.value if isinstance(x, un.Quantity) else x, dtype=np.float32
        )

        w_c = w_broadcast[:, :, corr_idx]  # Filter by correlation
        vr_c = vis_residual[:, :, corr_idx]  # Filter by correlation

        # Extracting field (l,m) phase center coordinates
        field_center_l = field_centers_l[field_id]
        field_center_m = field_centers_m[field_id]

        # Coordinates are transformed from pixels to radian units
        x = self.__cell_index_delta(
            x_cell - self.image.center_pixel[0], delta_x, np.float32
        ) - field_center_l
        y = self.__cell_index_delta(
            y_cell - self.image.center_pixel[1], delta_y, np.float32
        ) - field_center_m

        # Get the specific primary beam
        chans = self.dataset.spws.dataset[spw_id].CHAN_FREQ.data.squeeze(axis=0)

        beam = self.__primary_beam(
            chans, (self.image.center_pixel[0], self.image.center_pixel[1]),
            self.image.data.data.shape,
            x_0=field_center_l,
            y_0=field_center_m,
            chunks=mask.data.chunks if mask is not None else self.image.data.data.chunks
        )

        # We calculate the residuals as vis_observed - vis_model. Then from the derivative a factor -1.0 appears.
        gradient = -1.0 * self.__array_gradient(x, y, uvw_lambdas, vr_c, w_c, beam, mask=mask)

        if self.normalize:
            gradient /= ms.visibilities.visibility_number

        return gradient

    def __array_gradient(self, x, y, uvw, vis, w, pb, *, mask: Mask = None) -> da.Array:
        """Computes the gradient of chi2 for the full data arrays, multiplying
        the result with the primary beam.

        Parameters
        ----------
        x : da.Array
            Array with the scaled index for the first dimension of the image (1-dim) (idx, )
        y : da.Array
            Array with the scaled index for the second dimension of the image (1-dim) (idx, )
        uvw : da.Array
            Array with UVW coordinates in lambdas (3-dim) (row, chan, uvw)
        vis : da.Array
            Complex visibility array (3-dim) (row, chan, corr)
        w : da.Array
            Weight array broadcasted for every channel (3-dim) (row, chan, corr)
        pb : da.Array
            Primary beam array (chan, x, y)
        mask : pyralysis.reconstruction.mask.Mask, optional
            Mask object with a boolean array that indicates which cells to consider to compute the
            gradient

        Returns
        -------
        da.Array
            Gradient of chi2 (2-dim) (x, y)
        """
        # For this chi-square positive sign convention for the IDFT2 will be used
        data = idft2(x, y, uvw, vis, w, sign_convention="positive")

        if mask is not None:
            # Broadcast mask to match the shape of the PB, and obtain the masked primary beam values
            # via bool indexing
            broadcasted_mask = da.broadcast_to(mask.data.data, pb.shape)
            # Get the Primary Beam masked values
            pb_fitted = pb[broadcasted_mask]
            pb_fitted.compute_chunk_sizes()
        else:
            pb_fitted = pb  # If there is no mask, there is no need to filter the primary beam

        # Reshape to 2-d where the second dim is the cell idx and the first dim is the channel
        # (frequency) dim
        pb_2d = pb_fitted.reshape((-1, x.shape[0]))
        # Check length of the valid values (per channel)
        if pb_2d.shape != data.shape:
            raise ValueError(
                f'Shape error with primary beam: {pb_2d.shape} is different number of channels and '
                f'valid indices with shape {data.shape}'
            )

        data = da.einsum('in,in->n', data, pb_2d)
        return data

    @temp_irreg_local_save
    def __cell_index_delta(self, index_array, delta, dtype=np.float32):
        """Scale an array by a delta, so that each cell/pixel has the increment
        in radians specified by delta.

        Parameters
        ----------
        index_array : da.Array
            Coordinate array of the image
        delta : float
            Cellsize or cell-scale in radians (per pixel)
        dtype : data-type, optional
            Forces the calculation to use the data type specified, by default np.float32

        Returns
        -------
        da.Array
            Coordinate array of the image scaled by the corresponding cellsize
        """
        return (index_array * delta).astype(dtype)

    @temp_local_save
    def __uvw_lambdas(self, uvw, equivalencies, nchans, chunks) -> da.Array:
        """Convert the UVW coordinates to lambdas units while adding one
        dimension for the frequencies.

        Parameters
        ----------
        uvw : da.Array
            Array with UVW coordinates in m for every visibility (2-dim) (row, uvw)
        equivalencies : Equivalency
            Equivalencies of the spectral window, typically used to convert
            frequencies to wavelength units.
        nchans : int
            Number of spectral channels in the spectral window
        chunks : tuple
            Desired chunk structure for the resulting 3-dimensional array.
            The chunking structure should match the expected memory constraints
            and computation pattern. For example:
            - The first dimension (row) should align with the chunking of `uvw` for efficiency.
            - The second dimension (chan) should align with `nchans` or a subset to handle large datasets.
            - The third dimension (uvw) typically remains small and does not require fine-grained chunking.
            Providing appropriate chunks ensures efficient parallelization and memory usage.


        Returns
        -------
        da.Array
            Rechunked array with UVW coordinates in lambdas for every channel and every visibility (3-dim)
            (row, chan, uvw)
        """
        uvw_broadcast = da.repeat(uvw[:, np.newaxis, :], nchans, axis=1).rechunk(chunks)
        uvw_lambdas = array_unit_conversion(
            array=uvw_broadcast,
            unit=un.lambdas,
            equivalencies=equivalencies,
        )
        return uvw_lambdas

    @temp_local_save
    def __primary_beam(
        self,
        chans: da.Array,
        imcenter: Tuple[int, int],
        shape: Union[Tuple[int, int], List[int]],
        antenna: Union[List[int], np.ndarray, None] = None,
        x_0: Union[int, float] = 0,
        y_0: Union[int, float] = 0,
        chunks="auto"
    ):
        """Get the primary beam image for every frequency.

        Parameters
        ----------
        chans : da.Array
            Array with center frequencies (dimentionless) for each channel in the data (1-dim)
            (chan)
        imcenter : Tuple[int,int]
            Tuple of 2 integers for the center of the beam
        shape : Union[Tuple[int, int], List[int]]
            List or tuple of 2 integers giving the desired size of the image
        antenna : Union[List[int], np.ndarray, None], optional
            List of integers indicating the id of the antennas for which the beam is to be
            calculated, by default np.array([0])
        x_0 : Union[int, float], optional
            x position of the maximum of the Primary beam function, by default 0
        y_0 : Union[int, float], optional
            y position of the maximum of the Primary beam function, by default 0
        chunks : str, optional
            Desired chunk specification for primary beam image, by default 'auto'

        Returns
        -------
        da.Array
            Primary beam array (chan, x, y)
        """
        if antenna is None:
            antenna = np.array([0])

        beam = self.dataset.antenna.primary_beam.beam(
            chans, shape, imcenter=imcenter, antenna=antenna, imchunks=chunks, x_0=x_0, y_0=y_0
        )
        beam = beam[0].astype(np.float32)  # temporal indexing
        return beam
