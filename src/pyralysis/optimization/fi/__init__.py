from .chi2 import *
from .entropy import *
from .fi import *
from .l1norm import *
