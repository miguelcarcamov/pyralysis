import numbers
from dataclasses import dataclass, field
from typing import Union

import dask.array as da
import numpy as np
from multimethod import multimethod

from ...reconstruction.mask import Mask
from .fi import Fi


@dataclass(init=True, repr=True)
class Entropy(Fi):
    """Class to evaluate the entropy function and gradient over an image.

    Parameters
    ----------
    prior_image : Union[float, np.ndarray, da.Array], optional
        Prior image is an a priori model, bias or reference image, by default 1.0.

    Attributes
    ----------
    prior_image : Union[float, np.ndarray, da.Array]
        An a priori model, bias or reference image. The image I is biased towards this value, and it
        must require the value :math:`I/G` to be positive.

    See Also
    --------
    Fi, Chi2, L1Norm
    """
    prior_image: Union[float, np.ndarray, da.Array] = 1.0
    _prior_image: Union[float, np.ndarray, da.Array] = field(init=False)

    @property
    def prior_image(self) -> Union[float, np.ndarray, da.Array]:
        return self._prior_image

    @prior_image.setter
    def prior_image(self, prior_image: Union[float, np.ndarray, da.Array]) -> None:
        # Check that prior_image is of a shape that allows element-wise division
        if isinstance(prior_image, (np.ndarray, da.Array)):
            shape_I = self.image.data.shape
            shape_G = prior_image.shape
            if self.image.data.ndim < prior_image.ndim or (
                self.image.data.ndim == 2 and shape_I != shape_G
            ):
                raise AttributeError(
                    f"'prior_image' shape {shape_G} must be compatible with "
                    f"image shape {shape_I}"
                )
            elif self.image.data.ndim > 2 and prior_image.ndim == 2 and shape_I[:2] != shape_G:
                raise AttributeError(
                    f"'prior_image' shape {shape_G} must be compatible with "
                    f"image shape {shape_I}"
                )
            elif self.image.data.ndim > 2 and prior_image.ndim > 2:
                if self.image_index < 0 and shape_I != shape_G:
                    raise AttributeError(
                        f"'prior_image' shape {shape_G} must be compatible with "
                        f"image shape {shape_I}"
                    )
                elif self.image_index >= 0 and shape_I[:2] != shape_G[:2]:
                    raise AttributeError(
                        f"'prior_image' shape {shape_G} must be compatible with "
                        f"image shape {shape_I}"
                    )
        # Check that division by 0 doesn't occur
        if not isinstance(prior_image, property):
            if (isinstance(prior_image, numbers.Number) and prior_image == 0.0) or (
                isinstance(prior_image, (np.ndarray, da.Array)) and da.any(prior_image == 0.0)
            ):
                raise AttributeError("'prior_image' must not have 0 values")

        # Set default value
        self._prior_image = 1.0 if isinstance(prior_image, property) else prior_image

    def function(self, *, mask: Mask = None):
        """Computes the value of applying the entropy function to the image.

        Parameters
        ----------
        mask : Mask, optional
            Mask object with a boolean array that indicates which cells to consider to compute the
            function, by default None

        Returns
        -------
        float
            Result of applying entropy to the image

        Notes
        -----
        Entropy favors pixel-to-pixel similarity to a prior image :math:`G`, and follows the
        equation:

            .. math:: S = \\sum_{i} I_i \\log\\left(\\frac{I_i}{G}\\right)
        """
        self._func_value = self._function(mask)

        return self._func_value * self.penalization_factor

    @multimethod
    def _function(self, mask: Mask):

        if not self.mask_shape_check(mask):
            raise ValueError(
                (
                    f"Provided mask of shape {mask.data.shape} is not compatible",
                    f" with image of shape {self.image.data.shape}"
                )
            )

        im_data = self.image.data.data

        # Get the first two dimensions of the image data with image_index when there is a third
        # dimension
        if self.image_index >= 0 and im_data.ndim > 2:
            im_data = im_data[:, :, self.image_index]

        param = self._divide_by_prior_image(im_data)

        # Filter zeros or negative values to avoid undefined behaviour in log function
        masked_log = da.zeros_like(param, dtype=np.float32)

        mask_data = mask.data
        if self.image_index >= 0 and mask_data.ndim > 2:
            mask_data = mask_data[:, :, self.image_index]

        if mask_data.ndim < masked_log.ndim:
            # Broadcast mask to a 3d array to match masked_log dimensions
            masked_log = da.log(
                param, out=masked_log, where=((param > 0) * (mask_data[:, :, None]))
            )
        else:
            masked_log = da.log(param, out=masked_log, where=((param > 0) * (mask_data)))

        return da.sum(da.multiply(im_data, masked_log))

    @_function.register
    def _(self, mask: None):
        im_data = self.image.data.data
        # Get the first two dimensions of the image data with image_index when there is a third
        # dimension
        if self.image_index >= 0 and im_data.ndim > 2:
            im_data = im_data[:, :, self.image_index]

        param = self._divide_by_prior_image(im_data)

        # Filter zeros or negative values to avoid undefined behaviour in log function
        log = da.zeros_like(param, dtype=np.float32)
        # out=log is needed to not have uninitialized values
        log = da.log(param, out=log, where=param > 0)
        return da.sum(da.multiply(im_data, log))

    def gradient(self, iter=1, *, mask: Mask = None):
        """Computes the value of applying the entropy gradient to the image.

        Parameters
        ----------
        iter : int
            Number of iteration
        mask : Mask, optional
            Mask object with a boolean array that indicates which cells to consider to compute the
            gradient, by default None

        Returns
        -------
        da.Array
            Array with the result of applying the gradient of the entropy function to the image

        Notes
        -----
        Every coordinate of the entropy gradient (:math:`\\nabla S`) has a value that follows the
        equation:

            .. math:: [\\nabla S]_i =
                    \\log\\left(\\frac{I_i}{G}\\right) + 1
        """
        if iter <= 0:
            return self._grad_value

        res = self._gradient(mask)

        # Store gradient in attribute
        self._grad_value = res * self.penalization_factor
        return self._grad_value

    @multimethod
    def _gradient(self, mask: Mask):
        if not self.mask_shape_check(mask):
            raise ValueError(
                (
                    f"Provided mask of shape {mask.data.shape} is not compatible",
                    f" with image of shape {self.image.data.shape}"
                )
            )

        im_data = self.image.data.data.astype(np.float32)
        masked_res = da.full_like(im_data, -1, dtype=np.float32)

        # Get the first two dimensions of the image data with image_index when there is a third
        # dimension
        if self.image_index >= 0 and im_data.ndim > 2:
            im_data = im_data[:, :, self.image_index]

        param = self._divide_by_prior_image(im_data)

        mask_data = mask.data
        if self.image_index >= 0 and mask_data.ndim > 2:
            mask_data = mask_data[:, :, self.image_index]

        if mask_data.ndim < im_data.ndim:
            # mask is 2d but image is 3d (image_index < 0, im_data was not reduced)
            masked_res = da.log(param, out=masked_res, where=(param > 0) * (mask_data[:, :, None]))
        else:
            # Both have the same ndim
            if self.image_index >= 0 and masked_res.ndim > 2:
                masked_res[:, :, self.image_index] = da.log(
                    param, out=masked_res[:, :, self.image_index], where=(param > 0) * (mask_data)
                )
            else:
                masked_res = da.log(param, out=masked_res, where=(param > 0) * (mask_data))

        return masked_res + 1

    @_gradient.register
    def _(self, mask: None):
        im_data = self.image.data.data.astype(np.float32)
        res = da.full_like(im_data, -1, dtype=np.float32)
        # Get the first two dimensions of the image data with image_index when there is a third
        # dimension
        if self.image_index >= 0 and im_data.ndim > 2:
            im_data = im_data[:, :, self.image_index]

        param = self._divide_by_prior_image(im_data)

        # Filter zeros or negative values to avoid undefined behaviour in log function
        if self.image_index >= 0 and res.ndim > param.ndim:
            res[:, :, self.image_index] = da.log(param, where=param > 0)
        else:
            res = da.log(param, out=res, where=param > 0)

        return res + 1

    def prox(self):
        return 0

    def _divide_by_prior_image(self, data: da.Array) -> da.Array:
        """Divide data array by prior image.

        Given a data array, divide it by prior image. If prior image is an array, broadcast if it
        has less dimensions than the data array, use the indexed prior image (in the third
        dimension) if it has more dimensions than the data array, or make a direct division if they
        have the same dimensionality.

        Parameters
        ----------
        data : da.Array
            Array to divide by prior image

        Returns
        -------
        da.Array
            Result of division by prior image
        """
        if isinstance(self.prior_image, (np.ndarray, da.Array)):
            # We cast prior image to float in such that the gradient is lighter in memory
            prior_image = self.prior_image.astype(np.float32)
            if prior_image.ndim < data.ndim and self.image_index < 0:
                param = da.divide(data, prior_image[:, :, None])
            elif prior_image.ndim > data.ndim and self.image_index >= 0:
                param = da.divide(data, prior_image[:, :, self.image_index])
            else:
                param = da.divide(data, prior_image)
        else:
            param = da.divide(data, self.prior_image)

        return param
