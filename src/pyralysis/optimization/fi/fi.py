import logging
import warnings
from abc import ABCMeta, abstractmethod
from dataclasses import dataclass, field
from typing import Union

import dask.array as da
import numpy as np

from ...reconstruction import Image
from ...reconstruction.mask import Mask


@dataclass(init=True, repr=True)
class Fi(metaclass=ABCMeta):
    """Base class for applying functions and gradients of functions over an
    Image.

    Parameters
    ----------
    image_index : int, optional
        Index on the last dimension of the image when it has ndim = 3, if the index is -1 the class
        works with the whole image, by default -1
    image : Image
        Image on which the functions are applied or calculated
    penalization_factor : float, optional
        Penalization factor by which the result of the function and its gradient are multiplied, by
        default 1.0
    iteration : int, optional
        Iteration to calculate, by default 0
    normalize: bool, optional
        Either to normalize the function calculation and its gradient, by default False

    Attributes
    ----------
    image_index : int
        Index on the last dimension of the image when it has ndim = 3, if the index is -1 the class
        works with the whole image
    image : Image
        Image on which the functions are applied or calculated
    penalization_factor : float
        Penalization factor by which the result of the function and its gradient are multiplied
    iteration : int
        Iteration to calculate

    See Also
    --------
    Chi2, Entropy, L1Norm
    """
    image_index: int = -1  # equivalent to image to add
    image: Image = None
    penalization_factor: float = 1.0
    iteration: int = 0
    normalize: bool = False
    _func_value: float = field(init=False, repr=True, default=0.0)
    _grad_value: Union[np.ndarray, da.Array] = field(init=False, repr=True, default=None)

    @property
    def func_value(self):
        """float: Result of applying the function."""
        return self._func_value

    @property
    def grad_value(self):
        """da.Array: Result of applying the gradient of the function."""
        return self._grad_value

    @abstractmethod
    def function(self, *, mask: Mask = None):
        """Computes the function over the image."""
        return NotImplementedError

    @abstractmethod
    def gradient(self, iter=1, *, mask: Mask = None):
        """Computes the gradient of the function over the image."""
        return NotImplementedError

    @abstractmethod
    def prox(self, l):
        """
        Notes
        -----
        Not implemented.
        """
        return NotImplementedError

    def add_gradient(self, out: da.Array) -> None:
        """Add the result of the gradient to the indicated array.

        Given an array, replace its values by the sum of itself and grad_value. If the value of
        image_index of Fi is greater than or equal to zero and the dimensions of the image and the
        input array of this method are greater than 2, perform the addition only when the third
        dimension has the index indicated by image_index.

        Parameters
        ----------
        out : da.Array
            A location into which the result is stored
        """
        if self.image_index < 0 or self.grad_value.ndim == 2:
            da.add(out, self.grad_value, out=out)
        else:
            out[:, :, self.image_index] = da.add(
                out[:, :, self.image_index], self.grad_value[:, :, self.image_index]
            )

    def mask_shape_check(self, mask: Mask) -> bool:
        """Check that the shape of the mask can be used with the image.

        Compares the shape of the input mask with the shape of the image. The mask can be used if
        (i) the mask dimensions and shape are the same as the image, (ii) the image is 3d, and the
        mask is 2d, and the shape of the first 2 dimensions of the image are the same as the mask,
        (iii) the image and mask are 3d, the image_index is not -1, and the shape of the first two
        dimensions of both arrays are equal.

        This method just checks for shape equality, not if the index in image_index is a valid one
        for either image or mask.

        If the image attribute isn't set, the mask is considered unusable, so this method returns
        False (and a warning is raised).

        Parameters
        ----------
        mask : Mask
            Mask object whose shape will be compared to the one of the image to check compatibility

        Returns
        -------
        bool
            True if the mask can be used with the image (based on shape), False otherwise
        """
        # Case where the image hasn't been set, so the size/shape check must fail
        if self.image is None:
            logging.warning("No image has been set, cannot check mask and image size")
            warnings.warn(
                "No image has been set, cannot check mask and image size. Check fails by default"
            )
            return False

        if mask.data.ndim == 2:
            # This is the case that checks that if the mask is 2d, then its size must be equal
            # to the first two dimensions of the image.
            return mask.data.shape == self.image.data.shape[:2]
        elif self.image_index < 0:
            # If there is not an image_index, check for shape equality
            return mask.data.shape == self.image.data.shape
        else:
            # If there is an image_index, check for the shape of the first two dimensions
            return mask.data.shape[:2] == self.image.data.shape[:2]

    def __setattr__(self, name, value):
        # Overrides __setattr__ to init or update _grad_value according to the shape of the image
        if name == "image" and value is not None:
            logging.info("A new image has been set")
            im_shape = value.data.data.shape  # Get the shape of the data in an Image object
            if isinstance(value.data.data, da.Array):
                im_chunk = value.data.data.chunksize
                self.__dict__["_grad_value"] = da.zeros(im_shape, chunks=im_chunk, dtype=np.float32)
            else:
                self.__dict__["_grad_value"] = da.zeros(im_shape, dtype=np.float32)

        # Continue normal __setattr__ behavior
        super().__setattr__(name, value)
