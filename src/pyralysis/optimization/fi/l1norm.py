from dataclasses import dataclass

import dask.array as da
import numpy as np
from multimethod import multimethod

from ...reconstruction.mask import Mask
from .fi import Fi


@dataclass
class L1Norm(Fi):
    """
    Evaluate the L1 norm function, its gradient (using sign), and its proximal operator.

    This class calculates the L1 norm of an image, its (sub)gradient, and its proximal operator
    (soft-thresholding). For multi–channel images, if a specific channel is selected (i.e. when
    image_index >= 0), the operation is applied only on that channel and then embedded into an
    array having the same shape as the full image.
    """

    def function(self, *, mask: Mask = None) -> float:
        r"""
        Compute the L1 norm over the image data.

        .. math::
            \ell_1(I) = \sum_i |I_i|

        If a specific channel is selected and the image is multi–channel, only that channel is used.

        Parameters
        ----------
        mask : Mask, optional
            Mask indicating which cells to include.

        Returns
        -------
        float
            The L1 norm multiplied by the penalization factor.
        """
        self._func_value = self._compute_function(mask)
        return self._func_value * self.penalization_factor

    @multimethod
    def _compute_function(self, mask: Mask) -> da.Array:
        full_image = self.image.data.data.astype(np.float32)
        # Case: 2D image with a 3D mask and an image_index is requested.
        if full_image.ndim == 2 and mask.data.ndim == 3 and self.image_index >= 0:
            im_data = full_image  # leave as 2D
            m_data = mask.data[:, :, self.image_index]
        # Case: multi–channel image.
        elif self.image_index >= 0 and full_image.ndim > 2:
            im_data = full_image[:, :, self.image_index]
            if mask.data.ndim == full_image.ndim:
                m_data = mask.data[:, :, self.image_index]
            else:
                m_data = mask.data
        else:
            im_data = full_image
            m_data = mask.data
        # Broadcast if necessary (now im_data and m_data should be 2D)
        if m_data.ndim < im_data.ndim:
            m_data = m_data[..., None]
        im_data = da.where(m_data, im_data, 0.0)
        return da.sum(da.abs(im_data))

    @_compute_function.register
    def _(self, mask: None) -> da.Array:
        full_image = self.image.data.data.astype(np.float32)
        if self.image_index >= 0 and full_image.ndim > 2:
            im_data = full_image[:, :, self.image_index]
        else:
            im_data = full_image
        return da.sum(da.abs(im_data))

    def gradient(self, iter: int = 1, *, mask: Mask = None) -> da.Array:
        """
        Compute the gradient (subgradient) of the L1 norm using the sign function.

        .. math::
            \nabla \\ell_1(I) = \\operatorname{sign}(I)

        The result is returned with the same shape as the full image. For multi–channel images,
        if a specific channel is selected the gradient is computed on that channel and then embedded
        into a full–shaped array.

        Parameters
        ----------
        iter : int, optional
            Iteration count (unused here), default is 1.
        mask : Mask, optional
            Mask indicating which cells to include.

        Returns
        -------
        da.Array
            The gradient array multiplied by the penalization factor.
        """
        if iter <= 0:
            return self._grad_value
        full_image = self.image.data.data.astype(np.float32)
        # _apply_on_channel returns the result with full image shape.
        grad = self._apply_on_channel(full_image, da.sign)
        if mask is not None:
            grad = self._apply_mask(grad, full_image, mask)
        self._grad_value = grad * self.penalization_factor
        return self._grad_value

    @multimethod
    def _compute_gradient(self, mask: Mask) -> da.Array:
        full_image = self.image.data.data.astype(np.float32)
        grad = self._apply_on_channel(full_image, da.sign)
        return self._apply_mask(grad, full_image, mask)

    @_compute_gradient.register
    def _(self, mask: None) -> da.Array:
        full_image = self.image.data.data.astype(np.float32)
        return self._apply_on_channel(full_image, da.sign)

    def prox(self, lam: float = None) -> da.Array:
        r"""
        Compute the proximal operator of the L1 norm (soft-thresholding).

        .. math::
            \operatorname{prox}_{\lambda}(x) = \operatorname{sign}(x) \cdot \max\left(|x| - \lambda,\, 0\right)

        For multi–channel images with a selected channel, the proximal operator is applied only to that
        channel and then embedded into an array of the full image shape.

        Parameters
        ----------
        lam : float, optional
            Threshold parameter. If not provided, defaults to penalization_factor.

        Returns
        -------
        da.Array
            The proximal operator result.
        """
        if lam is None:
            lam = self.penalization_factor
        full_image = self.image.data.data.astype(np.float32)
        return self._apply_on_channel(full_image, self._prox_op, lam)

    def _apply_on_channel(self, full_image: da.Array, op, *args, **kwargs) -> da.Array:
        """
        Apply a vectorized operation to the full image. If a specific channel is selected (and the image
        is multi-channel), the operation is applied only on that channel and then embedded into a full–shaped
        array.

        For 2D images (full_image.ndim==2), the operation is applied directly.

        Parameters
        ----------
        full_image : da.Array
            The full image data.
        op : function
            A vectorized operation (e.g. da.sign or _prox_op).
        *args, **kwargs:
            Additional arguments for the operation.

        Returns
        -------
        da.Array
            The resulting array, with the same shape as full_image.
        """
        if self.image_index >= 0 and full_image.ndim > 2:
            channel_result = op(full_image[:, :, self.image_index], *args, **kwargs)
            result = da.zeros_like(full_image, dtype=full_image.dtype)
            result[:, :, self.image_index] = channel_result
            return result
        else:
            return op(full_image, *args, **kwargs)

    @staticmethod
    def _prox_op(x: da.Array, lam: float) -> da.Array:
        """
        Vectorized proximal operator for the L1 norm.
        """
        return da.sign(x) * da.maximum(da.abs(x) - lam, 0.0)

    def _apply_mask(self, arr: da.Array, full_image: da.Array, mask: Mask) -> da.Array:
        """
        Apply the mask to the given array, broadcasting the mask if necessary.

        If the mask has one extra dimension than the image (as is the case for 2D images with a 3D mask),
        and an image_index is set, the corresponding channel is selected from the mask.

        Parameters
        ----------
        arr : da.Array
            The array to be masked.
        full_image : da.Array
            The full image data.
        mask : Mask
            The mask object.

        Returns
        -------
        da.Array
            The masked array.
        """
        mask_data = mask.data
        if full_image.ndim == 2 and mask_data.ndim == 3 and self.image_index >= 0:
            mask_data = mask_data[:, :, self.image_index]
        elif mask_data.ndim == full_image.ndim - 1:
            mask_data = mask_data[..., None]
        return da.where(mask_data, arr, 0.0)
