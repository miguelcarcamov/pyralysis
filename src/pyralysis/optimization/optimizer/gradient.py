from abc import abstractmethod
from dataclasses import dataclass

import dask.array as da

from ...reconstruction.image import Image
from .optimizer import Optimizer


@dataclass(init=True, repr=True)
class GradientOptimizer(Optimizer):
    """Base class for gradient based optimizer.

    Parameters
    ----------
    gtol : float, optional
        Tolerance of gradient decrease or change to stop optimization, by default 1e-12.

    Attributes
    ----------
    gtol : float
        Tolerance of gradient decrease or change to stop optimization.
    """
    gtol: float = 1e-12

    @abstractmethod
    def optimize(self):
        raise NotImplementedError

    def _condition(self, image: Image, gradient_value: da.Array, function_value: float):
        """Compute the gradient-based stopping condition for gradient based
        algorithms.

        Parameters:
        ----------
        image : Image
            Current solution image.
        gradient_value : dask.array
            Current gradient.
        function_value : float
            Current value of the objective function.

        Returns:
        -------
        float
            Maximum value of the stopping condition.
        """
        div = max(function_value, 1.0)
        abs_im = da.abs(image.data.data)
        condition = da.abs(gradient_value) * da.maximum(abs_im, 1.0) / div
        max_value = da.max(condition)
        return max_value
