from dataclasses import dataclass

from .optimizer import Optimizer


@dataclass(init=True, repr=True)
class ProximalOptimizer(Optimizer):

    def optimize(self):
        raise NotImplementedError(
            (
                f"The class {self.__class__.__name__} will be implemented ",
                "in future versions of pyralysis."
            )
        )
