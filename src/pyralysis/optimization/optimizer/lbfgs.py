import copy
import time
from dataclasses import dataclass

import dask.array as da
import dask.distributed as dd
import numpy as np

from ...reconstruction.image import Image
from .gradient import GradientOptimizer


@dataclass(init=True, repr=True)
class LBFGS(GradientOptimizer):
    max_corrections: int = 100

    def _compute_direction(self, gradient, s_history, y_history, rho_history, current_size):
        """Compute the descent direction using vectorized two-loop recursion.

        Parameters:
        ----------
        gradient : dask.array
            Gradient.
        s_history : dask.array
            History of s vectors.
        y_history : dask.array
            History of y vectors.
        rho_history : dask.array
            History of rho scalars.
        current_size : int
            Current size of the history.

        Returns:
        -------
        dask.array
            Descent direction.
        """
        if current_size == 0:
            return -gradient  # Steepest descent if no history is available

        # Determine the shape of the images
        shape_s = s_history.shape[1:]  # Shape of a single s or y vector
        # First loop: Accumulate alpha and compute q
        s_dot_q = da.stack(
            [rho_history[i] * da.sum(s_history[i] * gradient) for i in range(current_size)]
        )
        alpha = da.expand_dims(
            s_dot_q, axis=tuple(range(1,
                                      len(shape_s) + 1))
        )  # Accumulated alphas for all history terms
        q = gradient - da.sum(alpha * y_history[:current_size], axis=0)

        # Scaling with gamma
        gamma = da.sum(
            s_history[current_size - 1] * y_history[current_size - 1]
        ) / da.maximum(da.sum(y_history[current_size - 1] * y_history[current_size - 1]), 1e-10)
        r = gamma * q

        # Second loop: Accumulate beta and update r
        y_dot_r = da.stack([rho_history[i] * da.sum(y_history[i] * r) for i in range(current_size)])
        beta = da.expand_dims(
            y_dot_r, axis=tuple(range(1,
                                      len(shape_s) + 1))
        )  # Accumulated betas for all history terms
        r += da.sum((alpha - beta) * s_history[:current_size], axis=0)

        return -r.persist()

    def _update_rho_and_histories(
        self, s, y, s_history, y_history, rho_history, current_size, start_index
    ):
        """Update histories (s, y, rho) using a circular buffer to avoid
        shifting arrays.

        Parameters:
        ----------
        s : dask.array
            Current s vector.
        y : dask.array
            Current y vector.
        s_history : dask.array
            History of s vectors.
        y_history : dask.array
            History of y vectors.
        rho_history : dask.array
            History of rho scalars.
        current_size : int
            Current size of the history.
        start_index : int
            Current start index in the circular buffer.

        Returns:
        -------
        tuple
            Updated s_history, y_history, rho_history, current_size, and start_index.
        """
        ys = da.maximum(da.sum(y * s), 1e-10).compute()  # Scalar
        rho = 1.0 / ys

        if current_size < self.max_corrections:
            index = (start_index + current_size) % self.max_corrections
            current_size += 1
        else:
            index = start_index
            start_index = (start_index + 1) % self.max_corrections

        # Update histories at the computed index
        s_history[index] = s
        y_history[index] = y
        rho_history[index] = rho

        return s_history, y_history, rho_history, current_size, start_index

    def optimize(
        self, *, verbose: bool = True, partial_image: bool = False, file_prefix: str = ""
    ) -> Image:
        """Performs the LBFGS algorithm.

        Implements the Limited-memory Broyden–Fletcher–Goldfarb–Shanno (L-BFGS) algorithm to minimize a function.
        This method approximates the inverse Hessian matrix using limited memory, leveraging historical
        gradients and position differences to iteratively update the solution.

        The stopping criteria include:
            - The norm of the current gradient is below the tolerance level (`tol`).
            - :math:`2\\left | f(x_k) - f(x_{k-1}) \\right | / (\\left | f(x_k) \\right | + \\left | f(x_{k-1}) \\right |) \\leqslant ftol`.
            - The maximum gradient component satisfies the condition:
              :math:`\\max(\\left | \\nabla f(x_k) \\right | \\odot \\mathrm{I}) \\leqslant gtol`,
              where :math:`\\mathrm{I}` is the element-wise maximum between :math:`\\left | x_k \\right |` and `1.0`.

        Parameters
        ----------
        verbose : bool, optional
            Flag to specify whether to print execution information, by default True.
        partial_image : bool, optional
            Flag to specify whether to save the partial image, by default False.

        Returns
        -------
        Image
            Optimized image.

        Notes
        -----
        - L-BFGS is particularly useful for high-dimensional optimization problems where storing the full Hessian matrix
          is computationally infeasible.
        - This implementation includes a two-loop recursion to compute the search direction efficiently, making use of
          the limited-memory Hessian approximation.
        """
        image_shape = self.image.data.shape
        image_chunks = self.image.data.chunks

        s_history = da.zeros(
            (self.max_corrections, *image_shape), chunks=(1, *image_chunks), dtype=np.float32
        )
        y_history = da.zeros(
            (self.max_corrections, *image_shape), chunks=(1, *image_chunks), dtype=np.float32
        )
        rho_history = da.zeros(
            self.max_corrections, chunks=(self.max_corrections, ), dtype=np.float32
        )
        current_size = 0
        start_index = 0  # Start of the circular buffer

        if verbose:
            dd.print(f"Starting LBFGS method")

        # Fix prefix
        if partial_image:
            file_prefix = file_prefix.strip().replace(" ", "_")
            file_prefix = file_prefix + "_" if len(file_prefix
                                                   ) > 0 and file_prefix[-1] != "_" else file_prefix

        x_prev = self.image

        # Set image in objetive function
        self.objective_function.fi_image(x_prev)
        # Compute function and gradient
        f_prev = self.objective_function.calculate_function(mask=self.mask)

        if verbose:
            dd.print(f"Starting function value = {f_prev:.04f}")

        self.objective_function.calculate_gradient(iteration=0, mask=self.mask)
        g_prev = self.objective_function.dphi

        for it in range(self.max_iter):
            start_time = time.time()

            grad_cond = self._condition(x_prev, g_prev, f_prev)
            if grad_cond < self.gtol:
                if verbose:
                    dd.print("Exit due to gradient tolerance")
                return x_prev

            r = self._compute_direction(g_prev, s_history, y_history, rho_history, current_size)

            self.objective_function.dphi = -r

            x_prev_copy = copy.deepcopy(x_prev)

            f_it, alpha_step = self.linesearch.search(
                x=x_prev, projection=self.projection, mask=self.mask
            )

            if verbose:
                alpha_val = alpha_step.compute() if isinstance(alpha_step, da.Array) else alpha_step
                dd.print(f"Alpha value = {alpha_val:e}")
            # At this point the function value has already been computed for the newest image

            # Obtain the new image and gradient
            x_next = self.objective_function.fi_list[0].image

            if verbose:
                dd.print(f"Function value = {f_it:.04f}")

            # Compute gradient with new image
            self.objective_function.calculate_gradient(iteration=it + 1, mask=self.mask)
            g_next = self.objective_function.dphi

            if partial_image:
                self.save_partial_image(x_next, f"{file_prefix}partial_{it+1}.fits")

            if 2.0 * abs(f_it - f_prev) <= self.ftol * (abs(f_it) + abs(f_prev) + self.eps):
                if verbose:
                    dd.print("Exit due to tolerance")
                return x_next

            s = (x_next.data.data - x_prev_copy.data.data).persist()
            y = (g_next - g_prev).persist()

            s_history, y_history, rho_history, current_size, start_index = self._update_rho_and_histories(
                s, y, s_history, y_history, rho_history, current_size, start_index
            )

            f_prev, g_prev, x_prev = f_it, g_next, copy.deepcopy(x_next)

            # Clear unused variables to prevent task graph growth
            del s, y, r, g_next

            end_time = time.time()
            time_elapsed = end_time - start_time

            if verbose:
                dd.print(f"Time: {time_elapsed:.04f} seconds")
        return x_next
