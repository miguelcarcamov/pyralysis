import time
from abc import abstractmethod
from dataclasses import dataclass

import dask.array as da
import dask.distributed as dd

from ...reconstruction.image import Image
from .gradient import GradientOptimizer


class GradientNormError(Exception):
    """Exception to use when the norm of the gradient is zero."""
    pass


@dataclass(init=True, repr=True)
class ConjugateGradient(GradientOptimizer):
    """Conjugate Gradient Algorithm for Optimization."""

    def optimize(
        self, *, verbose: bool = True, partial_image: bool = False, file_prefix: str = ""
    ) -> Image:
        """Performs the conjugate gradient algorithm.

        Performs an iterative optimization of an image, which requires at each iteration the current
        gradient and previous direction.

        The stopping criteria include:
            - The current gradient norm is equal to zero.
            - :math:`2\\left | f(x_k) - f(x_{k-1}) \\right | / (\\left | f(x_k) \\right | + \\left | f(x_{k-1}) \\right |) \\leqslant ftol`.
            - :math:`max(\\left | {f}'(x_k) \\right |\\mathrm{I}\\frac{1}{\\mathrm{s}})\\leqslant gtol`,
              where :math:`\\mathrm{I}` is equal to an element-wise maximum between :math:`\\left | x_k \\right |`
              and :math:`1.0`, and :math:`\\mathrm{s}` is te maximum between :math:`f(x_k)` and :math:`1.0`.

        Parameters
        ----------
        verbose : bool, optional
            Flag to specify whether to print execution information, by default True.
        partial_image : bool, optional
            Flag to specify whether to save the partial image, by default False.

        Returns
        -------
        Image
            Optimized image.
        """

        if verbose:
            dd.print(f"Starting {self.method_name()} method (Conj. Grad.)")

        # Fix prefix
        if partial_image:
            file_prefix = file_prefix.strip().replace(" ", "_")
            file_prefix = file_prefix + "_" if len(file_prefix
                                                   ) > 0 and file_prefix[-1] != "_" else file_prefix

        x = self.image

        # Set image in objetive function
        self.objective_function.fi_image(x)
        # Compute function and gradient
        f_prev = self.objective_function.calculate_function(mask=self.mask)

        if verbose:
            dd.print(f"Starting function value = {f_prev:.04f}")

        self.objective_function.calculate_gradient(iteration=0, mask=self.mask)
        g_prev = self.objective_function.dphi

        d_prev = -1.0 * g_prev  # Search direction

        for it in range(self.max_iter):

            if verbose:
                dd.print(f"Iteration {it+1}")

            start_time = time.time()

            f_it, alpha_step = self.linesearch.search(
                x=x, projection=self.projection, mask=self.mask
            )

            if verbose:
                _alpha_step = alpha_step.compute(
                ) if isinstance(alpha_step, da.Array) else alpha_step
                dd.print(f"Alpha value = {_alpha_step:e}")
            # Already has computed the function value for the new image

            # Obtain the new image
            x = self.objective_function.fi_list[0].image

            if partial_image:
                self.save_partial_image(x, f"{file_prefix}partial_{it+1}.fits")

            if 2.0 * abs(f_it - f_prev) <= self.ftol * (abs(f_it) + abs(f_prev) + self.eps):
                if verbose:
                    dd.print("Exit due to tolerance")
                return x

            if verbose:
                dd.print(f"Function value = {f_it:.04f}")

            # Compute gradient with new image
            self.objective_function.calculate_gradient(iteration=it + 1, mask=self.mask)
            g_it = self.objective_function.dphi

            gc_cond = self._condition(x, g_it, f_it).compute()
            if gc_cond < self.gtol:
                if verbose:
                    dd.print("Exit due to gradient tolerance")
                return x

            try:
                gc_param = self.conjugate_gradient_parameter(g_it, g_prev, d_prev).compute()
            except GradientNormError:
                # Catch exception and exit due to gradient norm equal to 0
                if verbose:
                    dd.print("Exit due to gg = 0")
                return x

            # Update search direction
            d_it = (-1.0 * g_it + gc_param * d_prev).persist()
            self.objective_function.dphi = -d_it

            # Update values
            f_prev, g_prev, d_prev = f_it, g_it, d_it

            # Clear unnecessary references to prevent task graph growth
            del g_it, d_it

            end_time = time.time()
            time_elapsed = end_time - start_time

            if verbose:
                dd.print(f"Time: {time_elapsed:.04f} seconds")

        if verbose:
            dd.print("Too many iterations in optimizer")

        return x

    def conjugate_gradient_parameter(
        self, grad: da.Array, grad_prev: da.Array, dir_prev: da.Array
    ) -> float:
        """Compute the conjugate gradient parameter.

        Compute the parameter to update the search direction. This parameter is often refered to as
        :math:`\\beta`, and is also called the conjugate gradient update parameter.

        Parameters
        ----------
        grad : da.Array
            Current gradient.
        grad_prev : da.Array
            Previous gradient.
        dir_prev : da.Array
            Previous search direction.

        Returns
        -------
        float
            Conjugate gradient update parameter.

        Raises
        ------
        GradientNormError
            If the current gradient norm is equal to zero.
        """
        norm2_grad_prev = da.sum(grad_prev * grad_prev)

        if norm2_grad_prev == 0.0:
            raise GradientNormError

        # Call method specialization
        gc_param = self._conjugate_gradient_parameter(
            grad, grad_prev, dir_prev=dir_prev, norm2_grad_prev=norm2_grad_prev
        )

        return gc_param

    @abstractmethod
    def method_name(self) -> str:
        """Return conjugate gradient method name.

        Returns
        -------
        str
            Name of the method.
        """
        raise NotImplementedError

    @abstractmethod
    def _conjugate_gradient_parameter(
        self, grad: da.Array, grad_prev: da.Array, **kwargs
    ) -> da.Array:
        raise NotImplementedError

    def _condition(self, image: Image, gradient_value: da.Array, function_value: float):
        div = max(function_value, 1.0)
        abs_im = da.abs(image.data.data)
        condition = da.abs(gradient_value) * da.maximum(abs_im, 1.0) / div
        max_value = da.max(condition)
        return max_value


@dataclass(init=True, repr=True)
class FletcherReeves(ConjugateGradient):
    """Fletcher-Reeves conjugate gradient method.

    Notes
    -----

    The Fletcher-Reeves conjugate gradient update parameter is defined as:

    .. math:: \\beta_k^{F R}=\\frac{g_{k+1}^T g_{k+1}}{g_k^T g_k}
    """

    def method_name(self) -> str:
        return 'Fletcher-Reeves'

    def _conjugate_gradient_parameter(
        self, grad: da.Array, grad_prev: da.Array, **kwargs
    ) -> da.Array:
        norm2_grad_prev = kwargs['norm2_grad_prev']
        norm2_grad = da.sum(grad * grad)
        return norm2_grad / norm2_grad_prev


@dataclass(init=True, repr=True)
class PolakRibiere(ConjugateGradient):
    """Polak-Ribiere-Polyak conjugate gradient method.

    Notes
    -----

    The Polak-Ribiere-Polyak conjugate gradient update parameter is defined as:

    .. math:: \\beta_k^{PRP}=\\frac{g_{k+1}^{T}(g_{k+1}-g_{k})}{\\lVert g_{k}\\rVert^2}
    """

    def method_name(self) -> str:
        return 'Polak-Ribiere-Polyak'

    def _conjugate_gradient_parameter(
        self, grad: da.Array, grad_prev: da.Array, **kwargs
    ) -> da.Array:
        norm2_grad_prev = kwargs['norm2_grad_prev']
        grad_diff = grad - grad_prev
        numer = da.sum(grad * grad_diff)
        return numer / norm2_grad_prev


@dataclass(init=True, repr=True)
class DaiYuan(ConjugateGradient):
    """Dai-Yuan conjugate gradient method.

    Notes
    -----

    The Dai-Yuan conjugate gradient update parameter is defined as:

    .. math:: \\beta_{k}^{DY}=\\frac{g_{k+1}^T g_{k+1}}{(g_{k+1} - g_{k})^T d_{k}}
    """

    def method_name(self) -> str:
        return 'Dai-Yuan'

    def _conjugate_gradient_parameter(
        self, grad: da.Array, grad_prev: da.Array, **kwargs
    ) -> da.Array:
        dir_prev = kwargs['dir_prev']
        norm2_grad = da.sum(grad * grad)
        grad_diff = grad - grad_prev
        denom = da.sum(dir_prev * grad_diff)
        return norm2_grad / denom


@dataclass(init=True, repr=True)
class HestenesStiefel(ConjugateGradient):
    """Hestenes-Stiefel conjugate gradient method.

    Notes
    -----

    The Hestenes-Stiefel conjugate gradient update parameter is defined as:

    .. math:: \\beta_{k}^{HS}=\\frac{g_{k+1}^{T}(g_{k+1}-g_{k})}{(g_{k+1}-g_{k})^Td_{k}}
    """

    def method_name(self) -> str:
        return 'Hestenes-Stiefel'

    def _conjugate_gradient_parameter(
        self, grad: da.Array, grad_prev: da.Array, **kwargs
    ) -> da.Array:
        dir_prev = kwargs['dir_prev']
        grad_diff = grad - grad_prev
        numer = da.sum(grad * grad_diff)
        denom = da.sum(dir_prev * grad_diff)
        return numer / denom


@dataclass(init=True, repr=True)
class HagerZhang(ConjugateGradient):
    """Hager-Zhang conjugate gradient method.

    Notes
    -----

    The Hager-Zhang conjugate gradient update parameter is defined as:

    .. math:: \\beta_k^{HZ}=\\frac{1}{d_{k}^T (g_{k+1} - g_{k})}((g_{k+1} - g_{k})-2 d_{k} \\frac{\\|(g_{k+1} - g_{k}) \\|^2}{d_{k}^T (g_{k+1} - g_{k})})^T{g}_{k+1}
    """

    def method_name(self) -> str:
        return 'Hager-Zhang'

    def _conjugate_gradient_parameter(
        self, grad: da.Array, grad_prev: da.Array, **kwargs
    ) -> da.Array:
        dir_prev = kwargs['dir_prev']
        grad_diff = grad - grad_prev
        denom = da.sum(dir_prev * grad_diff)
        norm2_grad_diff = da.sum(grad_diff * grad_diff)
        term = grad_diff - 2 * dir_prev * (norm2_grad_diff / denom)
        numer = da.sum(term * grad)
        return numer / denom
