from abc import ABCMeta, abstractmethod
from dataclasses import dataclass

from ...io import Io
from ...reconstruction.image import Image
from ...reconstruction.mask import Mask
from ..linesearch import LineSearch
from ..objective_function import ObjectiveFunction
from ..projection import Projection


@dataclass(init=True, repr=True)
class Optimizer(metaclass=ABCMeta):
    """Perform optimization of an objective function.

    Parameters
    ----------
    objective_function : ObjectiveFunction
        Objective function to minimize, by default None.
    linesearch : LineSearch, optional
        Line search strategy to find a local minimum of the 1-dim objective function and find the
        step size to take in the descent direction, by default None.
    image : Image
        Initial image, by default None.
    mask : Mask, optional
        Mask to limit the image values that will be evaluated with function and gradient, by default
        None.
    projection : Projection, optional
        Projection used to filter and modify values of images obtained in line search, by default
        None.
    eps : float, optional
        Small value to avoid division by zero, by default 1e-11.
    ftol : float, optional
        Tolerance of the function decrease to stop the optimization, by default 1e-12.
    max_iter : int, optional
        Maximum number of iterations executed in the optimization, by default 100.
    io_handler : Io, optional
        IO object to write images obtained in the optimization, by default None.

    Attributes
    ----------
    objective_function : ObjectiveFunction
        Objective function to minimize.
    linesearch : LineSearch
        Line search strategy to find a local minimum of the 1-dim objective function and find the
        step size to take in the descent direction.
    image : Image
        Initial image.
    mask : Mask
        Mask to limit the image values that will be evaluated with function and gradient.
    projection : Projection
        Projection used to filter and modify values of images obtained in line search.
    eps : float
        Small value to avoid division by zero.
    ftol : float
        Tolerance of the function decrease to stop the optimization.
    max_iter : int
        Maximum number of iterations executed in the optimization.
    io_handler : Io
        IO object to write images obtained in the optimization.
    """
    objective_function: ObjectiveFunction = None
    linesearch: LineSearch = None
    image: Image = None  # initial guess
    mask: Mask = None
    projection: Projection = None
    eps: float = 1e-11
    ftol: float = 1e-12
    max_iter: int = 100
    io_handler: Io = None

    @abstractmethod
    def optimize(self):
        raise NotImplementedError

    def save_partial_image(self, image: Image, filename: str) -> None:
        """Save an image content to disk.

        Parameters
        ----------
        image : Image
            Image object to write to disk.
        filename : str
            Name and path that the file will take in the system.
        """
        self.io_handler.write(data=image.data, output_name=filename)
