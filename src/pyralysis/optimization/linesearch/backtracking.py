import copy
from dataclasses import dataclass
from typing import Tuple

import dask.array as da

from ...reconstruction import Image
from ...reconstruction.mask import Mask
from ...utils.f1dim import f1dim
from ..projection import NoProjection, Projection
from .linesearch import LineSearch


@dataclass(init=True, repr=True)
class BacktrackingArmijo(LineSearch):
    """Class to apply Backtracking with Armijo rule.

    Attributes
    ----------
    contraction : float
        Value to control stopping criterion.
    decrease : float
        Value to control the decrease of the step
    """
    contraction: float = 0.5
    decrease: float = 0.5

    def search(self,
               x: Image,
               projection: Projection = None,
               mask: Mask = None,
               **kwargs) -> Tuple[float, float]:
        """Search optimum value for step using Backtracking with Armijo rule.

        Parameters
        ----------
        x : Image
            Initial image input.
        projection : Projection, optional
            Projection to modify the values of an image that meets certain conditions, by default
            None.
        mask : Mask, optional
            Mask for the image used to evaluate the function and gradient of the objective function,
            by default None.

        Returns
        -------
        Tuple[float, float]
            The first element is the value of the 1-dimensional function evaluated in the estimated
            optimum value for the step. The second element is the optimum value for the step.
        """
        self._read_kwargs(**kwargs)

        current_phi = self.objective_function.phi

        if projection is None:
            projection = NoProjection()

        f = f1dim(self.objective_function, x, projection, mask=mask)

        grad = self.objective_function.dphi
        grad_norm = da.sum(grad**2).compute()
        m = -self.contraction * grad_norm

        for _ in range(self.max_iter):
            if f(self.step) - current_phi > self.step * m:
                self.step *= self.decrease
            else:
                break

        return f(self.step), self.step

    def _read_kwargs(self, **kwargs) -> None:
        # Overwrites control and step values
        super()._read_kwargs(**kwargs)
        if 'contraction' in kwargs:
            self.contraction = kwargs['contraction']
        if 'decrease' in kwargs:
            self.decrease = kwargs['decrease']
