from abc import ABCMeta, abstractmethod
from dataclasses import dataclass
from typing import Tuple

from ...optimization.objective_function import ObjectiveFunction
from ...reconstruction import Image
from ...reconstruction.mask import Mask
from ..projection import Projection


@dataclass(init=True, repr=True)
class LineSearch(metaclass=ABCMeta):
    """Methods to iteratively search for local minima of a function using the
    gradient of the function.

    Attributes
    ----------
    objective_function : pyralysis.optimization.ObjectiveFunction
        Objective function for which the step is sought, by default None
    step : float
        Step to take, by default None
    tol : float
        Tolerance in search precision, by default 1.0e-7
    max_iter : int
        Maximum number of iterations in the search for the methods that require it, by default 100
    """
    objective_function: ObjectiveFunction = None
    step: float = None
    tol: float = 1.0e-7
    max_iter: int = 100

    @abstractmethod
    def search(self,
               x: Image,
               projection: Projection = None,
               mask: Mask = None,
               **kwargs) -> Tuple[float, float]:
        return NotImplementedError

    def _read_kwargs(self, **kwargs) -> None:
        # Overwrites step, tolerance and iteration limit values
        if 'step' in kwargs:
            self.step = kwargs['step']
        if 'tol' in kwargs:
            self.tol = kwargs['tol']
        if 'max_iter' in kwargs:
            self.max_iter = kwargs['max_iter']
