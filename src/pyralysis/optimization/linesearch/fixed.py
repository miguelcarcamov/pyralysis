from dataclasses import dataclass
from typing import Tuple

from ...reconstruction import Image
from ...reconstruction.mask import Mask
from ...utils.f1dim import f1dim
from ..projection import NoProjection, Projection
from .linesearch import LineSearch


@dataclass(init=True, repr=True)
class Fixed(LineSearch):
    """Class for a line search that returns a fixed step value."""

    def search(self,
               x: Image = None,
               projection: Projection = None,
               mask: Mask = None,
               **kwargs) -> Tuple[float, float]:
        """Apply fixed search method.

        Parameters
        ----------
        x : Image, optional
            Initial image input, by default None.
        projection : Projection, optional
            Projection to modify the values of an image that meets certain conditions, by default
            None.
        mask: Mask, optional
            Mask to apply to the image, by default None.

        Returns
        -------
        Tuple[float, float]
            The first element is the value of the 1-dimensional function evaluated in the step
            value. The second element is the step value.
        """
        super()._read_kwargs(**kwargs)

        if projection is None:
            projection = NoProjection()

        # Define a 1-dimensional function with the input image
        # This is the last time that the input image is used explicitly in the search
        f = f1dim(self.objective_function, x, projection, mask=mask)

        fstep = f(self.step)

        return fstep, self.step
