from dataclasses import dataclass
from typing import Tuple

import dask.array as da
import numpy as np

from ...reconstruction import Image
from ...reconstruction.mask import Mask
from ...utils.f1dim import f1dim
from ..projection import NoProjection, Projection
from .bracketing import mnbrak
from .linesearch import LineSearch


@dataclass(init=True, repr=True)
class Brent(LineSearch):
    """Class to perform line search using Brent's method.

    Contains an algorithm for finding minima of a scalar function, by Richard Brent.
    The search does not require the use of derivatives or gradients of the scalar function.

    Attributes
    ----------
    zeps : float
        Value that protects against trying to achieve fractional accuracy for a minimum that happens
        to be exactly zero.
    """
    zeps: float = 1e-10

    def search(self,
               x: Image,
               projection: Projection = None,
               mask: Mask = None,
               **kwargs) -> Tuple[float, float]:
        """Search a local minimum of the 1-dimensional objective function.

        The method used is a combination of golden section search and successive parabolic
        interpolation.

        To use the Brent's method with the objective function that works over an image, a function
        wrapper must be used so that the problem to be solved with Brent is 1-dimensional.

        Given the 1-dimensional function, search for a bracketing triplet of abscissas with mnbrak.
        Then isolate the minimum to a fractional precision of the class defined tolerance `tol`.

        At any stage of the search, it keeps track of six points: `a`, `b`, `u`, `v`, `w`, and`x`.
        - The minimum is bracketed between `a` and `b`.
        - The minimum function value found so far is with point `x`.
        - The second minimum function value is with point `w`.
        - The previous value of `w` is `v`.
        - The function was most recently evaluated at point `u`.

        Parameters
        ----------
        x : Image
            Image used to evaluate the function and gradient of the objective function prior to the
            linesearch.
        projection : Projection, optional
            Projection to modify the values of an image that meets certain conditions, by default
            None.
        mask : Mask, optional
            Mask for the image used to evaluate the function and gradient of the objective function,
            by default None.

        Returns
        -------
        Tuple[float, float]
            The first element is the value of the 1-dimensional function evaluated in the estimated
            minimum. The second element is the estimated value for which the function attains a
            local minimum.
        """
        from scipy.constants import golden as gold
        cgold = (1.0 / gold)**2

        self._read_kwargs(**kwargs)

        if projection is None:
            projection = NoProjection()

        # Define a 1-dimensional function with the input image
        # This is the last time that the input image is used explicitly in the search
        f = f1dim(self.objective_function, x, projection, mask=mask)

        # Bracketing a minimum of the function between [0, 1]
        a_, b_ = 0., 1.
        a_, b_, c_, _, _, _ = mnbrak(f, a_, b_)

        e = 0.0
        d = 0.0  # initial value for d (step)

        a = min(a_, c_)
        b = max(a_, c_)
        x = w = v = b_  # This replaces the value of x, so x is no longer an Image, but a float
        fx = fw = fv = f(x)

        for _ in range(self.max_iter):
            xm = 0.5 * (a + b)
            tol1 = self.tol * da.abs(x) + self.zeps
            tol2 = 2.0 * tol1

            # Test for done
            if da.abs(x - xm) <= (tol2 - 0.5 * (b - a)):
                self.step = x
                return fx, self.step

            if da.abs(e) > tol1:  # Fit a parabola
                r = (x - w) * (fx - fv)
                q = (x - v) * (fx - fw)
                p = (x - v) * q - (x - w) * r
                q = 2.0 * (q - r)

                if q > 0.0:
                    p = -p

                q = da.abs(q)
                e_ = e
                e = d

                # Determine the acceptability of the parabolic fit
                if da.abs(p) >= da.abs(0.5 * q * e_) or p <= (q * (a - x)) or p >= (q * (b - x)):
                    # Take a golden section step
                    e = a - x if x >= xm else b - x
                    d = cgold * e
                else:
                    # Take a parabolic interpolation step
                    d = p / q
                    u = x + d
                    if (u - a) < tol2 or (b - u) < tol2:
                        d = np.copysign(tol1, xm - x)

            else:  # A golden section step
                e = a - x if x >= xm else b - x
                d = cgold * e

            # Update u so f is not evaluated too close to x
            u = x + d if da.abs(d) >= tol1 else x + np.copysign(tol1, d)
            fu = f(u)

            # Update a, b, v, w and x
            if fu <= fx:
                if u >= x:
                    a = x
                else:
                    b = x
                v, w, x = w, x, u
                fv, fw, fx = fw, fx, fu
            else:
                if u < x:
                    a = u
                else:
                    b = u
                if fu <= fw or w == x:
                    v, w = w, u
                    fv, fw = fw, fu
                elif fu <= fv or v == x or v == w:
                    v = u
                    fv = fu

        self.step = x
        return fx, self.step

    def _read_kwargs(self, **kwargs) -> None:
        # Overwrites step and tolerance values
        super()._read_kwargs(**kwargs)
        if 'zeps' in kwargs:
            self.zeps = kwargs['zeps']
