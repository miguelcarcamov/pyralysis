from typing import Tuple

import numpy as np


def mnbrak(f: callable, a: float, b: float) -> Tuple[float, float, float, float, float, float]:
    """Search the points that bracked a minimum of the function.

    Given a function and two initial points a and b, search and return new points a, b, c that
    bracket a minimum of the function and the values of the function evaluated at these three points
    fa, fb, fb.

    Parameters
    ----------
    f : callable
        Function for which the minimum is sought. Receives only a float.
    a : float
        First initial point.
    b : float
        Second initial point.

    Returns
    -------
    Tuple[float, float, float, float, float, float]
        Values that bracket the function and the function values at these points.
    """
    from scipy.constants import golden as gold

    fa = f(a)
    fb = f(b)

    if fb > fa:
        a, b = b, a
        fa, fb = fb, fa

    c = b + gold * (b - a)  # New image
    fc = f(c)

    while fb > fc:
        r = (b - a) * (fb - fc)
        q = (b - c) * (fb - fa)

        denom = 2.0 * np.copysign(max(abs(q - r), 1e-12), (q - r))  # 1e-12 is the value of TINY

        u = b - ((b - c) * q - (b - a) * r) / denom
        ulim = b + 100.0 * (c - b)  # 100.0 is the value of GLIMIT

        if (b - u) * (u - c) > 0.0:
            fu = f(u)

            if fu < fc:
                a, b = b, u
                fa, fb = fb, fu
                return a, b, c, fa, fb, fc
            elif fu > fb:
                c, fc = u, fu
                return a, b, c, fa, fb, fc

            u = c + gold * (c - b)
            fu = f(u)

        elif (c - u) * (u - ulim) > 0.0:
            fu = f(u)
            if fu < fc:
                b, c = c, u
                fb, fc = fc, fu
                u = c + gold * (c - b)
                fu = f(u)
        elif (u - ulim) * (ulim - c) >= 0.0:
            u = ulim
            fu = f(u)
        else:
            u = c + gold * (c - b)
            fu = f(u)
        a, b, c = b, c, u
        fa, fb, fc = fb, fc, fu

    return a, b, c, fa, fb, fc
