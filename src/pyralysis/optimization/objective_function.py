import logging
from dataclasses import InitVar, dataclass, field
from typing import List, Union

import dask.array as da
import numpy as np
from multimethod import multimethod, overload

from ..base.dataset import Dataset
from ..optimization.fi.fi import Fi
from ..reconstruction.image import Image
from ..reconstruction.mask.mask import Mask


@dataclass(init=True, repr=True)
class ObjectiveFunction:
    """Class to evaluate the objective function and its gradient.

    Parameters
    ----------
    fi_list : List[Fi], optional
        List of functions or terms that are part of the objective function, by default []
    image : Union[Image, Tuple, List], optional
        Image or shape of the image to configure the array that stores the result of the gradient,
        by default None. If it's an instance of Image, it's also set as the image attribute of every
        Fi that is in fi_list.
    chunks : Union[Tuple, Dict], optional
        The block dimensions of the gradients final result, by default None
    persist_gradient: bool
        Either to calculate and persist the gradient calculation in memory to avoid unnecessary recalculations

    Attributes
    ----------
    fi_list : List[Fi]
        List of functions or terms that are part of the objective function
    phi : float
        Value that stores the result of the objective function
    dphi : da.Array
        Array that stores the result of the gradient of the objective function
    """
    fi_list: List[Fi] = field(default_factory=list)
    # Objective Function results
    phi: float = field(init=False, repr=True, default=0.0)
    dphi: Union[np.ndarray, da.Array] = field(init=False, repr=True, default=None)
    # Image related fields
    image: InitVar[Union[Image, tuple, list]] = None
    chunks: InitVar[Union[tuple, dict]] = None
    persist_gradient: bool = False

    def __post_init__(self, image, chunks):
        # Start dphi
        if image is not None and isinstance(image, Image):
            self.fi_image(image)  # Set Image in every Fi
        elif image is not None:
            self.configure_image_size(image) if chunks is None else self.configure_image_size(
                image, chunks
            )

    @property
    def fi_values(self):
        """np.ndarray: Fi individual (non-penalized) function results."""
        _fi_values = [fi.func_value for fi in self.fi_list]
        return np.asarray(_fi_values)

    def calculate_function(self, *, mask: Mask = None) -> float:
        """Evaluates the objective function.

        Parameters
        ----------
        mask : pyralysis.reconstruction.mask.Mask, optional
            Mask object with an array whose values determine where the function is calculated (True
            is calculated, False is ignored), by default None

        Returns
        -------
        float
            Value with the result of the objective function
        """
        # Initial value
        value = 0.0
        self.phi = 0.0
        # Compute f for every Fi
        for i, fi in enumerate(self.fi_list):
            fi_ret = fi.function(mask=mask)
            value += fi_ret
        function_value = value.compute() if isinstance(value, da.Array) else value
        self.phi = function_value
        return function_value

    def calculate_gradient(
        self,
        iteration: int,
        out: Union[np.ndarray, da.Array] = None,
        *,
        mask: Mask = None
    ) -> None:
        """Evaluates the gradient of the objective function, and stores it in
        the dphi attribute.

        Parameters
        ----------
        iteration : int
            _desc
        mask : pyralysis.reconstruction.mask.Mask, optional
            Mask object with an array whose values determine where the gradient is calculated (True
            is calculated, False is ignored), by default None.
        out : Union[np.ndarray, da.Array], optional
            A location into which the result of the gradient of the objective function is stored, by
            default None.
        """
        logging.info(f"Starting gradient calculations: iteration {iteration}")
        self.restart_dphi()
        for fi in self.fi_list:
            fi.iteration = iteration
            fi.gradient(iter=iteration, mask=mask)
            fi.add_gradient(self.dphi)

        if self.persist_gradient:
            logging.info("Persisting calculated gradient.")
            self.dphi = self.dphi.persist()

        if out is not None:
            self.copy_dphi(out)

    def restart_dphi(self) -> None:
        """Reset the dphi attribute array to zeros."""
        if self.dphi is not None:
            self.dphi = da.zeros_like(self.dphi, chunks=self.dphi.chunksize)
        else:
            logging.warning("ObjectiveFunction dphi is not set, starting with Fi Image info")
            try:
                self.configure_image_size(self.fi_list[0].image)
            except IndexError:
                logging.error("Fi list is empty")
            except:
                logging.error("Error with Image while starting dphi")

    def copy_dphi(self, out: Union[np.ndarray, da.Array]) -> None:
        """Copy the the dphi attribute array into the specified ouput.

        Parameters
        ----------
        out : Union[np.ndarray, da.Array]
            A location into which dphi is stored
        """
        if self.persist_gradient:
            logging.info("Persisting calculated gradient.")
            out = self.dphi.persist().copy()
        else:
            out = self.dphi.copy()

    def fi_dataset(self, dataset: Dataset) -> None:
        """Change or set the Dataset for every Fi in fi_list that has the
        attribute "dataset".

        Parameters
        ----------
        dataset : pyralysis.base.Dataset
            Dataset object with the MS table
        """
        # Change or set dataset for Fi in fi_list
        for fi in self.fi_list:
            if hasattr(fi, "dataset"):
                # Can also be done with "isinstance(fi, Chi2)" but will fail if other classes that
                # need dataset are added
                fi.dataset = dataset

    def fi_image(self, image: Image) -> None:
        """Change or set the Image for every Fi in fi_list.

        Parameters
        ----------
        image : pyralysis.reconstruction.Image
            Image object
        """
        self.configure_image_size(image)
        for fi in self.fi_list:
            fi.image = image

    @multimethod
    def fi_penalization(self, penalization: float) -> None:
        """Change or set the penalization_factor for every Fi in fi_list.

        Parameters
        ----------
        penalization : float | Union[list, np.ndarray]
            Value of the penalization_factor. If it's a float, each Fi is assigned the same factor.
            If it's a list or array, it has to be the same length of the fi_list, so each value is
            assigned to the Fi in the same index.
        """
        for fi in self.fi_list:
            fi.penalization_factor = penalization

    @fi_penalization.register
    def _(self, penalization: Union[list, np.ndarray]):
        assert len(penalization) == len(self.fi_list), "Lists are not the same length"
        for fi, pen in zip(self.fi_list, penalization):
            fi.penalization_factor = pen

    @multimethod
    def configure_image_size(self, image: Image):
        """Configure the shape of dchi2, array that stores the result of the
        gradient.

        Parameters
        ----------
        image : Image | Union[Tuple, List]
            Image or shape of the image to configure the shape of dchi2. If it's an instance of
            Image, it also sets the chunks from the chunk information in the image data. If not, it
            has to be a list or tuple of int.
        chunks : Union[Tuple, Dict], optional
            The block dimensions of the dchi2 array. Only can be used if the parameter image is a
            list or tuple of int.
        """
        # Shape related variables
        img_shape = image.data.shape
        # Chunks related variables
        image_chunks = image.data.data.chunksize
        # Init attribute for gradient result
        self.dphi = da.zeros(img_shape, dtype=np.float32, chunks=image_chunks)

    @configure_image_size.register
    def _(self, image: Union[tuple, list], chunks: Union[tuple, dict]):
        self.dphi = da.zeros(image, dtype=np.float32, chunks=chunks)

    @configure_image_size.register
    def _(self, image: Union[tuple, list]):
        self.dphi = da.zeros(image, dtype=np.float32)

    def calculate_prox(self, p, l):
        """
        Notes
        -----
        Not implemented.
        """
        pass
