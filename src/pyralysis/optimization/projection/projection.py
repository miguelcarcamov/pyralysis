from abc import ABCMeta, abstractmethod
from dataclasses import dataclass, field

import xarray as xr

from ...reconstruction.image import Image


@dataclass(init=True, repr=True)
class Projection(metaclass=ABCMeta):
    """Modify values of an image using boolean conditions to filter the data.

    With a projection, the data of an image is filtered. All values that meet a condition are
    replaced.

    This class is a callable.

    Parameters
    ----------
    compared_value : float
        Value to which the image data is compared in the boolean condition.
    replacement_value : float
        Value to replace the image data with where the boolean condition is true.

    Attributes
    ----------
    compared_value : float
        Value to which the image data is compared in the boolean condition.
    replacement_value : float
        Value to replace the image data with where the boolean condition is true.

    See Also
    --------
    CompositeProjection

    Examples
    --------
    >>> data = xr.DataArray(da.arange(12).reshape((3, 4)))
    >>> image = Image(data=data, cellsize=0.5)
    >>> proj = LessThan(compared_value=5, replacement_value=-1)
    >>> proj(image)
    >>> image.data.data.compute()
    array([[-1, -1, -1, -1],
           [-1,  5,  6,  7],
           [ 8,  9, 10, 11]])
    """
    compared_value: float = field()
    replacement_value: float = field()

    @abstractmethod
    def __call__(self, image: Image) -> None:
        return NotImplementedError


@dataclass(init=False, repr=True)
class NoProjection(Projection):
    """Projection that does not filter nor modify the image data.

    Because this projection is to not modify the data, this class does
    not define any attributes.
    """

    def __init__(self):
        return

    def __call__(self, image: Image) -> None:
        return


@dataclass(init=True, repr=True)
class EqualTo(Projection):
    """Projection that modifies data equal to a value."""

    def __call__(self, image: Image) -> None:
        image.data = xr.where(image.data == self.compared_value, self.replacement_value, image.data)


@dataclass(init=True, repr=True)
class NotEqualTo(Projection):
    """Projection that modifies all data different from a certain value."""

    def __call__(self, image: Image) -> None:
        image.data = xr.where(image.data != self.compared_value, self.replacement_value, image.data)


@dataclass(init=True, repr=True)
class GreaterThan(Projection):
    """Projection that modifies all data greater than a certain value."""

    def __call__(self, image: Image) -> None:
        image.data = xr.where(image.data > self.compared_value, self.replacement_value, image.data)


@dataclass(init=True, repr=True)
class GreaterThanEqualTo(Projection):
    """Projection that modifies all data greater than or equal to a certain
    value."""

    def __call__(self, image: Image) -> None:
        image.data = xr.where(image.data >= self.compared_value, self.replacement_value, image.data)


@dataclass(init=True, repr=True)
class LessThan(Projection):
    """Projection that modifies all data less than a certain value."""

    def __call__(self, image: Image) -> None:
        image.data = xr.where(image.data < self.compared_value, self.replacement_value, image.data)


@dataclass(init=True, repr=True)
class LessThanEqualTo(Projection):
    """Projection that modifies all data less than or equal to a certain
    value."""

    def __call__(self, image: Image) -> None:
        image.data = xr.where(image.data <= self.compared_value, self.replacement_value, image.data)
