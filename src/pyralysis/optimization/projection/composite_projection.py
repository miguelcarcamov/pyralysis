from collections import UserList
from dataclasses import dataclass
from typing import Any

from ...reconstruction.image import Image
from .projection import Projection


@dataclass(init=False, repr=True)
class CompositeProjection(UserList, Projection):
    """A projection that is a list of projections.

    CompositeProjection is a class that type list and type projection. The list contains a set of
    projections, which when calling the object of type CompositeProjection, are called in the order
    they have in the list.

    This class is a callable. It also follows the normal behavior of a list, being able to append,
    insert, extend, index, and so on.

    Attributes `compared_value` and `relacement_value` from Projection are not assigned or used.

    Attributes
    ----------
    data : List
        A list object used to store the contents of the UserList class.

    See Also
    --------
    Projection

    Examples
    --------
    >>> data = xr.DataArray(da.arange(12).reshape((3, 4)))
    >>> image = Image(data=data, cellsize=0.5)
    >>> a = LessThan(compared_value=5, replacement_value=-1)
    >>> b = GreaterThan(compared_value=10, replacement_value=-2)
    >>> comp_proj = CompositeProjection(a, b)
    >>> comp_proj(image)
    >>> image.data.data.compute()
    array([[-1, -1, -1, -1],
           [-1,  5,  6,  7],
           [ 8,  9, 10, -2]])
    >>> comp_proj += GreaterThan(compared_value=7, replacement_value=-3)
    >>> comp_proj(image)
    >>> image.data.data.compute()
    array([[-1, -1, -1, -1],
           [-1,  5,  6,  7],
           [-3, -3, -3, -1]])
    """

    def __init__(self, *projections):
        super().__init__(self._validate_projection(proj) for proj in projections)

    def __call__(self, image: Image) -> None:
        for projection in self.data:
            projection(image)

    def _validate_projection(self, value: Any) -> Projection:
        if isinstance(value, Projection):
            return value
        raise TypeError(f"Projection object expected, got {type(value).__name__}")

    ## Override UserList Behavior ##

    def insert(self, index, item):
        self.data.insert(index, self._validate_projection(item))

    def append(self, item):
        self.data.append(self._validate_projection(item))

    def extend(self, other):
        if isinstance(other, type(self)):
            self.data.extend(other)
        else:
            self.data.extend(self._validate_projection(item) for item in other)

    def __setitem__(self, index, item):
        self.data[index] = self._validate_projection(item)

    def __add__(self, other):
        if isinstance(other, UserList):
            data = self.data + other.data
            return self.__class__(*data)
        elif isinstance(other, type(self.data)):
            data = self.data + other
            return self.__class__(*data)
        elif isinstance(other, Projection):
            data = self.data + [other]
            return self.__class__(*data)
        data = self.data + list(other)
        return self.__class__(*data)

    def __radd__(self, other):
        if isinstance(other, UserList):
            data = other.data + self.data
            return self.__class__(*data)
        elif isinstance(other, type(self.data)):
            data = other + self.data
            return self.__class__(*data)
        elif isinstance(other, Projection):
            data = [other] + self.data
            return self.__class__(*data)
        data = list(other) + self.data
        return self.__class__(*data)

    def __iadd__(self, other):
        if isinstance(other, UserList):
            self.data += [self._validate_projection(o) for o in other.data]
        elif isinstance(other, type(self.data)):
            self.data += [self._validate_projection(o) for o in other]
        elif isinstance(other, Projection):
            self.data += [other]
        else:
            other_list = list(other)
            self.data += [self._validate_projection(o) for o in other_list]
        return self
