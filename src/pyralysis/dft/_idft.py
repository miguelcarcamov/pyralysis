from typing import Union

import dask.array as da
import numpy as np
from numba import njit, prange

__all__ = ["idft2"]


def idft2(
    x: da.Array,
    y: da.Array,
    uvw: da.Array,
    vis: da.Array,
    wt: Union[da.Array, None] = None,
    sign_convention: str = "negative",
    numba: bool = False,
) -> da.Array:
    """Compute inverse discrete fourier transform for a 2-d array.

    Follows the equation:

        .. math:: \\Large \\sum_k e^{ 2 \\pi i (u_k l + v_k m + w_k (n - 1))} \\cdot V_k \\cdot \\omega_k

    Parameters
    ----------
    x : da.Array
        1-d array with the indices of the 2-d array for the x coordinate.
    y : da.Array
        1-d array with the indices of the 2-d array for the y coordinate.
    uvw : da.Array
        UVW coordinates. 3-dim with coordinates ('row', 'chan', 'uvw').
    vis : da.Array
        Complex visibility matrix. 3-dim with coordinates ('row', 'chan', 'corr').
    wt : Union[da.Array, None], optional
        Weight for the data, by default None. 3-dim with coordinates
        ('row', 'chan', 'corr').
        If not provided, a matrix of ones with the same dimensions as `vis`.
    sign_convention : str, optional
        Fourier transform sign convention that can take values such as "positive"
        or "negative", by default "negative".
    numba : bool, optional
        Indicates whether numba is used in the inner function, by default False.

    Returns
    -------
    da.Array
        2-dim array with the idft2, with coordinates ('chan', 'idx').
    """
    # The sign convention is related to the sign of the DFT. Therefore, if the
    # DFT convention is positive, the IDFT sign will be negative. Otherwise it will be positive.

    # Check for valid sign convention
    if sign_convention not in ["positive", "negative"]:
        raise ValueError(
            "The parameter sign_convention can only take 'positive' or 'negative'",
            f" as its value. '{sign_convention}' was passed."
        )

    sign = np.float32(1)
    if sign_convention == "positive":
        sign = np.float32(-1)

    # If wt is None, create an array of ones the same shape as vis but not complex type
    if wt is None:
        wt = da.ones(vis.shape, dtype=vis.real.dtype)

    func = _idft2_numba if numba else _idft2
    # Equivalent to Re(Vr * exp(2j*pi*uv))
    u = uvw[..., 0]
    v = uvw[..., 1]
    w = uvw[..., 2]
    dft_chan = da.blockwise(
        func,
        ("row", "chan", "corr", "idx"),
        x,
        ("idx", ),
        y,
        ("idx", ),
        u,
        ("row", "chan"),
        v,
        ("row", "chan"),
        w,
        ("row", "chan"),
        vis,
        ("row", "chan", "corr"),
        wt,
        ("row", "chan", "corr"),
        sign,
        None,
        adjust_chunks={
            "row": 1,
            "corr": 1
        },
        dtype=np.float32,
    )
    return da.sum(dft_chan, (0, 2), dft_chan.dtype)


def _idft2(
    x: np.ndarray,
    y: np.ndarray,
    u: np.ndarray,
    v: np.ndarray,
    w: np.ndarray,
    vis: np.ndarray,
    wt: np.ndarray,
    sign_convention: np.float32,
) -> np.ndarray:
    """Block idft2 function."""
    z = np.sqrt(1 - x**2 - y**2) - 1
    xu = np.einsum("i,rc->irc", x, u)
    yv = np.einsum("i,rc->irc", y, v)
    wz = np.einsum("i,rc->irc", z, w)

    phase = xu + yv + wz
    cosuvw = np.cos(sign_convention * 2 * np.pi * phase)
    sinuvw = np.sin(sign_convention * 2 * np.pi * phase)

    vdif = np.einsum("rco,irc->irco", vis.real,
                     cosuvw) - np.einsum("rco,irc->irco", vis.imag, sinuvw)
    res = np.einsum("rco, irco->ic", wt, vdif)
    res_t = np.transpose(res, (1, 0))

    return res_t[np.newaxis, :, np.newaxis, :]


@njit(parallel=True)
def _idft2_numba(x, y, u, v, w, vis, wt, sign_convention):
    """Block idft2 function.

    Numba version.
    """
    nidx = x.shape[0]
    _ = u.shape[0]
    nchan = u.shape[1]
    ncorr = vis.shape[2]
    vis_r = vis.real
    vis_i = vis.imag

    res = np.zeros((1, nchan, 1, nidx), dtype=np.float32)
    z = np.sqrt(1 - x**2 - y**2) - 1

    for idx in prange(nidx):
        for chan in prange(nchan):

            temp = np.zeros((ncorr, ))

            uvw2pi = (
                sign_convention * 2 * np.pi *
                (u[:, chan] * x[idx] + v[:, chan] * y[idx] + w[:, chan] * z[idx])
            )
            cosuvw = np.cos(uvw2pi)
            sinuvw = np.sin(uvw2pi)

            for corr in range(ncorr):
                temp[corr] = np.sum(
                    wt[:, chan, corr] *
                    (vis_r[:, chan, corr] * cosuvw - vis_i[:, chan, corr] * sinuvw)
                )

            res[0, chan, 0, idx] += np.sum(temp)

    return res
