from abc import ABCMeta, abstractmethod
from dataclasses import dataclass, field
from typing import List, Union

import astropy.units as un
import numpy as np
import xarray as xr
from astropy.units import Quantity

from ..units.units_functions import check_units
from ..utils.cellsize import cellsize as cellsize_check_set


@dataclass(init=True, repr=True)
class Parameter(metaclass=ABCMeta):
    """Class that represents a parameter. This parameter can be passed to
    Optimizers in order to reconstruct them.

    Parameters
    ----------
    data : xr.DataArray
        Array with the data
    cellsize : Union[List[Quantity], Quantity, float]
        Cell-size space between data points.
    name : str
        Name of the parameter
    noise : Quantity
        Noise of the parameter
    chunks : tuple
        Change the chunks of the dask array
    data_gpu : bool
        Whether to convert data to a cupy array.
    """
    data: xr.DataArray = None
    cellsize: Union[List[Quantity], Quantity, float] = None
    name: str = None
    noise: Quantity = None
    chunks: Union[tuple, str] = None
    data_gpu: bool = None

    def __post_init__(self):
        if self.data is not None:
            chunks = "auto"
            if isinstance(self.chunks, tuple):
                # Chunks as a dict with data dimension names and chunks size as
                chunks = {dim: chunk for dim, chunk in zip(self.data.dims, self.chunks)}
            elif isinstance(self.chunks, dict):
                chunks = self.chunks
            self.data = self.data.chunk(chunks)

    @property
    def cellsize(self):
        return self.__cellsize

    @cellsize.setter
    def cellsize(self, cellsize):
        # Check cellsize and assign value per type
        self.__cellsize = cellsize_check_set(cellsize)
        # Assign pixel area value
        if self.__cellsize is not None:
            area = np.abs(self.__cellsize[0] * self.__cellsize[1])
            if check_units(area, un.steradian):
                self.__pixel_area = area.to(un.steradian)
            else:
                self.__pixel_area = 0. * un.steradian
        else:
            self.__pixel_area = None

    @property
    def pixel_area(self):
        return self.__pixel_area

    @abstractmethod
    def calculate_noise(self):
        pass
