from radio_beam import Beams


class PSF(Beams):
    """Class that represents the Point Spread Function. Inherits from Beam.

    Parameters
    ----------
    kwargs :
        Beam general arguments
    """

    def __new__(cls, **kwargs):
        return super().__new__(cls, **kwargs)
