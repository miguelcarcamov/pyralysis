from __future__ import annotations

import warnings
from dataclasses import dataclass, field
from typing import TYPE_CHECKING, List

import astropy.units as un
import numpy as np
from astropy.coordinates import SkyCoord
from astropy.io import fits
from astropy.units import Quantity, Unit

from ..units import beam_equivalencies, check_units
from .parameter import Parameter

if TYPE_CHECKING:
    from ..base import Dataset


@dataclass(init=True, repr=True)
class Image(Parameter):
    """Class that represents an image. Inherits from parameter.

    Parameters
    ----------
    kwargs :
        Parameter general arguments
    """
    imsize: list[int] = field(init=False, repr=True, default=None)
    phase_center: SkyCoord = field(init=False, repr=True, default=None)
    center_pixel: list[int] | np.ndarray = field(init=False, repr=True, default=None)

    def __post_init__(self):
        # Call Parameter post_init
        super().__post_init__()

        if self.data is not None:
            # Image size takes 2 last shapes
            self.imsize = [self.data.shape[-2], self.data.shape[-1]]
            self.center_pixel = np.array(
                [self.imsize[0] // 2, self.imsize[1] // 2], dtype=np.float32
            )
            # Set cellsize
            if self.data.attrs:

                if "CDELT1" in self.data.attrs and "CDELT2" in self.data.attrs:  # Normal header
                    self.cellsize = [self.data.attrs["CDELT1"], self.data.attrs["CDELT2"]] * un.deg
                elif "CD1_1" in self.data.attrs and "CD2_2" in self.data.attrs:  # IRAC header
                    self.cellsize = [self.data.attrs["CD1_1"], self.data.attrs["CD2_2"]] * un.deg

                if "CRVAL1" in self.data.attrs and "CRVAL2" in self.data.attrs:
                    if "RADESYS" in self.data.attrs:
                        self.phase_center = SkyCoord(
                            ra=self.data.attrs["CRVAL1"] * un.deg,
                            dec=self.data.attrs["CRVAL2"] * un.deg,
                            frame=self.data.attrs["RADESYS"].lower()
                        )
                    else:
                        # ICRS by default
                        warnings.warn(
                            "The image provided does not contain RADESYS keyword in header. Phase center coordinates will be assumed to be in ICRS."
                        )
                        self.phase_center = SkyCoord(
                            ra=self.data.attrs["CRVAL1"] * un.deg,
                            dec=self.data.attrs["CRVAL2"] * un.deg
                        )
                if "CRPIX1" in self.data.attrs and "CRPIX2" in self.data.attrs:
                    center_pixel_x = self.data.attrs["CRPIX1"] - 1.0
                    center_pixel_y = self.data.attrs["CRPIX2"] - 1.0

                    if center_pixel_x >= 0 and center_pixel_x < self.imsize[
                        1] and center_pixel_y >= 0 and center_pixel_y < self.imsize[0]:
                        self.center_pixel[0] = self.data.attrs["CRPIX1"] - 1.0
                        self.center_pixel[1] = self.data.attrs["CRPIX2"] - 1.0
                    else:
                        warnings.warn(
                            "The image provided has invalid CRPIX keywords in header. Default values will be used instead.",
                            UserWarning
                        )

        # If there is a cellsize, check that is in angle units
        if self.cellsize is not None:
            if check_units(self.cellsize, un.rad):
                self.cellsize = self.cellsize.to(un.rad)
            else:
                raise ValueError("Units for cellsize should be in units of Angle")
        else:
            raise ValueError("For an Image instance, the attribute cellsize should not be None")

    def create_header(
        self,
        projection: str = "SIN",
        bunit: str = "JY/BEAM",
        dataset: Dataset = None,
        add_beam: bool = True,
    ):
        """Public function that creates a header from Image attributes.

        Parameters
        ----------
        projection : str
            Projection string. Default: "SIN"
        bunit : str
            Beam unit. Default: "JY/BEAM"
        dataset : Dataset
            Pyralysis dataset instance
        add_beam : bool
            Whether to add the dirty beam major, minor and angle to the header.
        """
        # Get main header from data
        hdu = fits.PrimaryHDU(self.data.data)
        main_header = hdu.header
        header_dict = {
            "EQUINOX": 2000.0,
            "CTYPE1": "RA---" + projection,
            "CUNIT1": "deg",
            "CDELT1": self.cellsize[0].to(un.deg).value,
            "CRPIX1": self.center_pixel[0] + 1,
            "CRVAL1": dataset.field.center_phase_dir.ra.to(un.deg).value,
            "CTYPE2": "DEC--" + projection,
            "CUNIT2": "deg",
            "CDELT2": self.cellsize[1].to(un.deg).value,
            "CRPIX2": self.center_pixel[1] + 1,
            "CRVAL2": dataset.field.center_phase_dir.dec.to(un.deg).value,
            "RADESYS": dataset.field.center_phase_dir.frame.name.upper(),
            "BUNIT": bunit,
        }
        # We combine the headers
        main_header.update(header_dict)

        # We update attributes of our xarray Array
        self.data.attrs.update(main_header)
        # If add_beam is True and dataset PSFs have been calculated then we added them to the header
        # TODO: Find a way to edit attrs for each one of the xarrays of the cube
        if add_beam is True and dataset.psf is not None:
            beam_header = {
                "BMAJ": dataset.psf[0].major.to(un.deg).value,
                "BMIN": dataset.psf[0].minor.to(un.deg).value,
                "BPA": dataset.psf[0].pa.to(un.deg).value,
            }
            # We update the attributes of the xarray data
            self.data.attrs.update(beam_header)
            main_header.update(beam_header)

    def transform_intensity_units(
        self, intensity: Quantity = None, beam_area: Quantity = None, unit: Unit = None
    ):
        if intensity is not None and beam_area is not None and unit is not None:
            if beam_area.unit == self.pixel_area.unit:
                beam_equiv = beam_equivalencies(beam_area=beam_area / self.pixel_area, unit=unit)
            else:
                raise ValueError("Beam and image areas need to have same units")
            return intensity.to((un.Jy / unit), equivalencies=beam_equiv)
        else:
            raise ValueError("Intensity, beam_area and unit need to be different from None")

    def calculate_noise(self, measurements: Dataset = None) -> None:
        if measurements is None:
            raise ValueError("Measurement cannot be Nonetype")
        else:
            if measurements.polarization.feed_kind is None:
                measurements.polarization.check_feed()

            sum_weight_per_stokes = measurements.sum_weights_stokes()
            weights_per_selected_stokes = np.array(
                list(map(sum_weight_per_stokes.get, ["I", "Q", "U", "V"]))
            )
            noise = 1.0 / np.sqrt(weights_per_selected_stokes)

            if self.name == "I":
                self.noise = noise[0]
            elif self.name == "Q":
                self.noise = noise[1]
            elif self.name == "U":
                self.noise = noise[2]
            elif self.name == "V":
                self.noise = noise[3]
            else:
                self.noise = None
