import warnings
from dataclasses import InitVar, dataclass, field
from typing import List, Tuple, Union

import astropy.units as un
import dask.array as da
import numpy as np
import xarray as xr

from ...base.dataset import Dataset
from ...reconstruction.parameter import Parameter
from ...utils.decorators import temp_irreg_local_save


@dataclass(init=True, repr=True)
class Mask(Parameter):
    """A Mask represents an array that provides bool values to filter an image,
    where a True value indicates that the corresponding element is valid.

    Parameters
    ----------
    data : xr.DataArray, optional
        Array with the mask data, by default None
    cellsize : Union[List[Quantity], Quantity, float], optional
        Cell-size space between data points, by default None
    dataset : pyralysis.base.Dataset, optional
        Dataset object that contains the PrimaryBeam object to compute the default mask
    imsize : Union[Tuple[int, int], List[int]], optional
        List or tuple of 2 integers giving the desired size of the image for the default mask
    threshold : float, optional
        Threshold above which the values used to compute the default mask are invalid/False in the
        mask data

    Attributes
    ----------
    data : xr.DataArray
        Bool array with the mask data
    cellsize : Quantity
        Array-like Quantity with the cell-size space between data points
    indices: Tuple[da.Array, da.Array]
        Tuple with arrays that contains the indices of the valid/True values in the mask data

    See Also
    --------
    Parameter
    """
    dataset: InitVar[Dataset] = None
    imsize: InitVar[Union[Tuple[int, int], List[int]]] = None
    imcenter: InitVar[Union[Tuple[int, int], List[int]]] = None
    threshold: InitVar[float] = None
    #TODO for 3d mask change to array of tuples, one array per frequency
    indices: Tuple[da.Array, da.Array,
                   Union[None, da.Array]] = field(init=False, repr=True, default=None)

    def __post_init__(self, dataset, imsize, imcenter, threshold):
        # Call Parameter post_init
        super().__post_init__()

        if self.data is None and (dataset and imsize and threshold):
            self.data = self._default_mask(dataset, imsize, imcenter, threshold)

        # Get indices if data is not None
        if self.data is not None:
            indices = da.nonzero(self.data)
            self.indices = tuple(map(self._compute_identity, indices))

    @temp_irreg_local_save
    def _compute_identity(self, arr: da.Array) -> da.Array:
        arr.compute_chunk_sizes()
        return arr

    def _default_mask(
        self, dataset: Dataset, imsize: Union[Tuple[int, int], List[int]],
        imcenter: Union[Tuple[int, int], List[int]], threshold: float
    ) -> xr.DataArray:
        """Computes a default mask from the dataset, imsize and threshold
        value.

        From a Dataset that has a PrimaryBeam attribute (as an attribute of Antenna), computes a
        primary beam with the reference frequency (in the Dataset) for the first antenna in the
        Dataset. The imsize parameter determines the size of the beam.
        The mask valid values are all those cells in an `1/beam` array that are lower than the
        threshold value.

        Parameters
        ----------
        dataset : pyralysis.base.Dataset
            Dataset object that contains the PrimaryBeam object to compute the default mask
        imsize : Union[Tuple[int, int], List[int]]
            List or tuple of 2 integers giving the desired size of the image for the default mask
        threshold : float
            Threshold above which the values used to compute the default mask are invalid/False in
            the mask data

        Returns
        -------
        xr.DataArray
            2-dim bool array with the mask data
        """
        # Default 2D Mask (Mask can be 3D [x, y, freq])
        freq = [dataset.spws.min_nu.value]
        pb = dataset.antenna.primary_beam
        pointings = dataset.field.phase_direction_cosines[0:2].value
        cellsize = np.array([-1.0, 1.0]) * un.rad

        if self.cellsize is not None:
            cellsize = self.cellsize

        beams = da.array(
            [
                pb.beam(
                    frequency=freq,
                    imsize=imsize,
                    cellsize=cellsize,
                    antenna=np.array([0]),  # Token antenna, for default mask
                    x_0=pointings[0][i],
                    y_0=pointings[1][i],
                    imcenter=imcenter,
                    imchunks="auto" if self.chunks is None else self.chunks
                ) for i in range(pointings.shape[-1])
            ]
        )

        beam = da.sum(beams, axis=(0, 1, 2))
        normalized_beam = beam / da.max(beam)

        dims = ["x", "y"]
        inverse_beam = 1 / normalized_beam
        # masked_beam to xr.DataArray
        masked_beam = xr.DataArray(inverse_beam < threshold, dims=dims)
        return masked_beam

    def calculate_noise(self) -> None:
        """Mask object does not have noise, so nothing is calculated.

        Warns
        -----
        UserWarning
            Always, due to mask not having noise.
        """
        warnings.warn("Bool mask does not have noise: calculate_noise will return None")
        return
