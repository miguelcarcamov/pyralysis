from dataclasses import dataclass
from typing import Union

import dask.array as da
import numpy as np
import xarray as xr
import zarr

from ...utils.image_utils import get_coordinates
from ..io import Io


@dataclass(init=True, repr=True)
class ZarrArray(Io):
    """I/O class to handle Zarr image files.

    Parameters
    ----------
    use_dask : bool
        Whether to use dask arrays or not
    chunks : Union[int, tuple, str]
        How to chunk the array. Must be one of the following forms:
            - A blocksize like 1000.
            - A blockshape like (1000, 1000).
            - Explicit sizes of all blocks along all dimensions like
            ((1000, 1000, 500), (400, 400)).
            - A size in bytes, like "100 MiB" which will choose a uniform
            block-like shape
            - The word "auto" which acts like the above, but uses a configuration
            value ``array.chunk-size`` for the chunk size
            -1 or None as a blocksize indicate the size of the corresponding
            dimension.
    kwargs :
        IO arguments
    """
    use_dask: bool = True
    chunks: Union[int, tuple, str] = "auto"

    def read(self):
        zarr_array = zarr.load(self.input_name)
        attrs = zarr_array.attrs.asdict()
        ndims = zarr_array.squeeze().ndim
        coords = get_coordinates(attrs)
        dims = []
        if ndims == 3:
            dims = ["nu", "x", "y"]
        elif ndims == 2:
            dims = ["x", "y"]
        if self.use_dask:
            data = da.from_array(zarr_array, chunks=self.chunks).squeeze()
        else:
            data = zarr_array
        return xr.DataArray(data, dims=dims, coords=coords, attrs=attrs)

    def write(
        self,
        data: Union[xr.DataArray, zarr.core.Array, np.ndarray] = None,
        output_name: str = None
    ):
        if output_name is None:
            output_name = self.output_name

        if data is None:
            raise ValueError("Parameter data cannot be Nonetype")

        if not isinstance(data, (xr.DataArray, da.Array, zarr.core.Array, np.ndarray)):
            raise TypeError(
                "Data parameter must be either ",
                "xarray.DataArray, dask.Array, zarr.Array or np.ndarray"
            )

        store = zarr.storage.FSStore(output_name, check=True, create=True)
        kwargs_ = {
            'shape': data.shape,
            'dtype': data.dtype,
            'store': store,
            'overwrite': True,
            'path': None
        }

        # Get data array and attributes
        attrs = {}
        if isinstance(data, xr.DataArray):
            attrs = data.attrs
            data = data.data

        # Set chunks argument for zarr array
        if isinstance(data, da.Array):
            kwargs_['chunks'] = data.chunksize
        elif isinstance(data, zarr.core.Array):
            kwargs_['chunks'] = data.chunks

        # Create zarr array with defined store
        zarr_array = zarr.create(**kwargs_)
        zarr_array.attrs.update(attrs)

        # Fill zarr array with data and write
        if isinstance(data, da.Array):
            da.store(data, zarr_array, lock=False)
        else:
            zarr_array[...] = data
        store.close()
