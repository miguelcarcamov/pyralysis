import logging
from dataclasses import dataclass, field
from logging import Logger

from .io import Io


@dataclass(init=True, repr=True)
class HDF5(Io):
    """I/O class to handle HDF5 image files.

    Parameters
    ----------
    kwargs :
        IO arguments
    """

    logger: Logger = field(init=False, repr=False)

    def __post_init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(logging.INFO)

    def read(self):
        pass

    def write(self):
        pass
