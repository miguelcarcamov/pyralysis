import logging
from dataclasses import dataclass, field
from typing import Union

import dafits
import dask.array as da
import numpy as np
import xarray as xr
from astropy.io import fits
from astropy.io.fits import Header

from ..reconstruction import Image
from ..utils.image_utils import get_coordinates
from .io import Io


@dataclass(init=True, repr=True)
class FITS(Io):
    """I/O class to handle FITS files.

    Parameters
    ----------
    hdu : int
        HDU index of the FITS file
    memmap : bool
        Whether to use memory map or to load the whole image in memory
    lazy_load_hdus : bool
        Whether to load HDUs on a lazy manner
    use_dask : bool
        Whether to use dask arrays or not
    chunks : Union[int, tuple, str]
        How to chunk the array. Must be one of the following forms:
            - A blocksize like 1000.
            - A blockshape like (1000, 1000).
            - Explicit sizes of all blocks along all dimensions like
            ((1000, 1000, 500), (400, 400)).
            - A size in bytes, like "100 MiB" which will choose a uniform
            block-like shape
            - The word "auto" which acts like the above, but uses a configuration
            value ``array.chunk-size`` for the chunk size
            -1 or None as a blocksize indicate the size of the corresponding
            dimension.
    kwargs : General IO arguments
    """
    hdu: int = 0
    memmap: bool = True
    lazy_load_hdus: bool = False
    use_dask: bool = True
    chunks: Union[int, tuple, str] = "auto"

    __header: Header = field(init=False, repr=False, default=None)

    def __post_init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(logging.INFO)

    def read(self):
        with fits.open(
            self.input_name, memmap=self.memmap, lazy_load_hdus=self.lazy_load_hdus
        ) as hdul:
            self.__header = hdul[self.hdu].header
            ndims = hdul[self.hdu].data.squeeze().ndim
            coords = get_coordinates(hdul)
            dims = []
            if ndims == 3:
                dims = ["nu", "x", "y"]
            elif ndims == 2:
                dims = ["x", "y"]
            if self.use_dask:
                data = da.from_array(hdul[self.hdu].data, chunks=self.chunks).squeeze()
            else:
                data = hdul[self.hdu].data.squeeze()
            image_xarray = xr.DataArray(
                data, dims=dims, coords=coords, attrs=dict(hdul[self.hdu].header)
            )
            return Image(data=image_xarray)

    def write(
        self,
        data: Union[Image, xr.DataArray, da.core.Array, np.ndarray] = None,
        header: Union[dict, fits.Header] = None,
        output_name: str = None,
        overwrite: bool = True,
    ):
        if output_name is None:
            output_name = self.output_name

        if data is None:
            raise ValueError("Parameter data cannot be Nonetype")

        # Get array with data
        if isinstance(data, Image):
            data_array = data.data.data
        elif isinstance(data, xr.DataArray):
            data_array = data.data
        elif isinstance(data, (da.core.Array, np.ndarray)):
            data_array = data
        else:
            raise TypeError(
                "Parameter data must be either xarray.DataArray, da.core.Array or np.ndarray"
            )

        # Set header
        if header is None:
            if self.__header is not None:
                header = self.__header
            elif isinstance(data, xr.DataArray):
                attrs = data.attrs
                header = Header(attrs) if attrs else None
        else:
            if isinstance(header, dict):
                header = Header(header)

        # Write to disk
        if isinstance(data_array, da.core.Array):
            dafits.write(
                file=output_name,
                data=data_array,
                header=header,
                overwrite=overwrite,
                verbose=False,
            )
        else:
            fits.writeto(
                filename=output_name,
                data=data_array,
                header=header,
                overwrite=overwrite,
            )
