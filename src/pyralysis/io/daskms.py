import copy
import logging
import os
import shutil
import warnings
from collections.abc import KeysView
from dataclasses import dataclass
from dataclasses import field as _field
from logging import Logger
from typing import List, Union

import dask
import dask.array as da
import numpy as np
import xarray
from daskms import xds_from_ms, xds_from_table, xds_to_table

from ..base.antenna import Antenna
from ..base.baseline import Baseline
from ..base.dataset import Dataset
from ..base.field import Field
from ..base.observation import Observation
from ..base.polarization import Polarization
from ..base.spectral_window import SpectralWindow
from ..base.subms import SubMS
from ..base.visibility_set import VisibilitySet
from .io import Io


@dataclass(init=True, repr=True)
class DaskMS(Io):
    """I/O class to handle Measurements Sets v2 using Dask.

    Parameters
    ----------
    chunks:
        A {dim: chunk} dictionary, specifying the chunking strategy of each dimension in the schema.
        Defaults to {'row': 100000 } which will partition the row dimension into chunks of 100000.
        - If a dict, the chunking strategy is applied to each group.
        - If a list of dicts, each element is applied to the associated group.
        The last element is extended over the remaining groups if there are insufficient elements.
        It’s also possible to specify the individual chunks for multiple dimensions:
        {'row': (40000, 60000, 40000, 60000),
         'chan': (16, 16, 16, 16),
         'corr': (1, 2, 1)}
        The above chunks a 200,000 row, 64 channel and 4 correlation space into 4 x 4 x 3 = 48 chunks,
        but requires prior knowledge of dimensionality, probably obtained with an initial call to
        xds_from_table().
    kwargs :
        IO general arguments

    Returns
    -------
    None
    """
    chunks: Union[dict, List[dict]] = None

    logger: Logger = _field(init=False, repr=False)
    ms_name_dask: str = _field(init=False, repr=True, default="")
    _DEFAULT_ROW_CHUNKS: int = _field(init=False, repr=False, default=10000)

    def __post_init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(logging.INFO)
        self.ms_name_dask = self.input_name + "::"

    # This should look for CORRECTED_DATA and if does not exist then look for DATA
    def read(
        self,
        read_flagged_data: bool = False,
        filter_flag_column: bool = False,
        calculate_psf: bool = True,
        taql_query: str = None,
        chunks: Union[dict, List[dict]] = None
    ) -> Dataset:
        """Function that reads a Measurement Set v2 lazily using Dask.

        Parameters
        ----------
        read_flagged_data : bool
            Whether to read flagged_data or not.
        filter_flag_column: bool
            Whether to check flag column in order to filter flagged data
        calculate_psf: bool
            Whether to calculate the PSF or not.
        taql_query: str
            TAQL where clause
        chunks:  Union[dict, List[dict]]
            A {dim: chunk} dictionary, specifying the chunking strategy of each dimension in the schema.
            Defaults to {'row': 100000 } which will partition the row dimension into chunks of 100000.
            - If a dict, the chunking strategy is applied to each group.
            - If a list of dicts, each element is applied to the associated group.
            The last element is extended over the remaining groups if there are insufficient elements.
            It’s also possible to specify the individual chunks for multiple dimensions:
            {'row': (40000, 60000, 40000, 60000),
             'chan': (16, 16, 16, 16),
             'corr': (1, 2, 1)}
            The above chunks a 200,000 row, 64 channel and 4 correlation space into 4 x 4 x 3 = 48 chunks,
            but requires prior knowledge of dimensionality, probably obtained with an initial call to
            xds_from_table().


        Returns
        -------
        Returns a Pyralysis Dataset object
        """

        chunks_read = [{'row': self._DEFAULT_ROW_CHUNKS}]
        taql_query_flag_row = ""
        taql_query_read = ""

        if chunks is not None:
            chunks_read = chunks
        elif self.chunks is not None:
            chunks_read = self.chunks

        if taql_query is not None:
            taql_query_read = taql_query

        if not read_flagged_data:
            taql_query_flag_row = "!FLAG_ROW"
            if taql_query_read == "":
                taql_query_read += taql_query_flag_row
            else:
                taql_query_read += " and " + taql_query_flag_row

        # Reading observation dataset
        obs = xds_from_table(self.ms_name_dask + "OBSERVATION", taql_where=taql_query_flag_row)[0]
        obs_obj = Observation(dataset=obs)
        antenna_obs_id = None  # variable for array with id of observation per antenna

        # Creating antenna object
        antennas = xds_from_table(self.ms_name_dask + "ANTENNA", taql_where=taql_query_flag_row)[0]
        antenna_obj = Antenna(dataset=antennas)

        if obs_obj.ntelescope > 1:
            # if there is more than one telescope in the dataset, allocate space for
            # one observation id per antenna
            antenna_obs_id = da.zeros_like(antenna_obj.dataset.ROWID, dtype=np.int32)

        # Creating baseline object
        baseline_obj = Baseline(antenna_obj)

        # Reading spectral window datasets (separated by different spectral windows)
        spws = xds_from_table(
            self.ms_name_dask + "SPECTRAL_WINDOW",
            taql_where=taql_query_flag_row,
            group_cols="__row__",
        )

        # Reading field datasets (separated by different fields)
        fields, fields_tabkw, fields_colkw = xds_from_table(
            self.ms_name_dask + "FIELD",
            taql_where=taql_query_flag_row,
            table_keywords=True,
            column_keywords=True
        )

        # Reading data description dataset
        ddids = xds_from_table(
            self.ms_name_dask + "DATA_DESCRIPTION", taql_where=taql_query_flag_row
        )[0]

        # Reading polarization datasets (separated by different polarization rows)
        pols = xds_from_table(
            self.ms_name_dask + "POLARIZATION",
            taql_where=taql_query_flag_row,
            group_cols="__row__",
        )

        field_obj = Field(
            dataset=fields[0], table_keywords=fields_tabkw, column_keywords=fields_colkw
        )
        spw_obj = SpectralWindow(dataset=spws)
        pol_obj = Polarization(dataset=pols)

        # Reading MS main table indexing columns such as ANTENNA1, ANTENNA2, SCAN_NUMBER and TIME
        # This returns a list of measurement sets partitioned by FIELDS and SPWS
        ms = xds_from_ms(
            self.input_name,
            taql_where=taql_query_read,
            group_cols=["FIELD_ID", "DATA_DESC_ID"],
            index_cols=["SCAN_NUMBER", "TIME", "ANTENNA1", "ANTENNA2"],
            chunks=chunks_read
        )

        ms_list = []
        for ms_id, msds in enumerate(ms):
            ms_keys = msds.data_vars.keys()
            fdid = msds.attrs["FIELD_ID"]
            ddid = msds.attrs["DATA_DESC_ID"]
            spw_id = ddids["SPECTRAL_WINDOW_ID"][ddid].data.compute()
            pol_id = ddids["POLARIZATION_ID"][ddid].data.compute()

            base_dataset = msds[[
                "UVW", "DATA", "WEIGHT", "FLAG", "TIME", "SCAN_NUMBER", "ANTENNA1", "ANTENNA2",
                "OBSERVATION_ID"
            ]]

            if self.__key_is_on_dataset("MODEL_DATA", ms_keys):
                # Adds MODEL_DATA column to base dataset
                base_dataset = base_dataset.assign(
                    MODEL_DATA=(["row", "chan", "corr"], msds["MODEL_DATA"].data)
                )

            if self.__key_is_on_dataset("CORRECTED_DATA", ms_keys):
                # Adds CORRECTED_DATA column to base dataset
                base_dataset = base_dataset.assign(
                    CORRECTED_DATA=(["row", "chan", "corr"], msds["CORRECTED_DATA"].data)
                )

            if self.__key_is_on_dataset("IMAGING_WEIGHTS", ms_keys):
                # Adds IMAGING_WEIGHTS column to base dataset
                base_dataset = base_dataset.assign(
                    IMAGING_WEIGHTS=(["row", "corr"], msds["IMAGING_WEIGHTS"].data)
                )
            else:
                base_dataset = base_dataset.assign(
                    IMAGING_WEIGHTS=(["row", "corr"], copy.deepcopy(msds["WEIGHT"].data))
                )

            if filter_flag_column:
                valid_indexes = da.flatnonzero(da.any(~base_dataset["FLAG"].data,
                                                      axis=(1, 2))).compute()

                if not valid_indexes.size:
                    continue

                ds = base_dataset.isel(row=valid_indexes)
            else:
                ds = base_dataset

            baseline_id = da.argmax(
                (baseline_obj.dataset.ANTENNA1.data[None, :] == ds["ANTENNA1"].data[:, None]) &
                (baseline_obj.dataset.ANTENNA2.data[None, :] == ds["ANTENNA2"].data[:, None]),
                axis=1,
            ).astype(np.int32)

            ds = ds.assign(BASELINE_ID=(["row"], baseline_id))

            visibility_obj = VisibilitySet(dataset=ds)
            ms_obj = SubMS(
                _id=ms_id,
                field_id=fdid,
                polarization_id=pol_id,
                spw_id=spw_id,
                visibilities=visibility_obj,
            )
            ms_list.append(ms_obj)

            # Observation id for every antenna
            if antenna_obs_id is not None:
                antenna1_id, indices1 = da.unique(ds["ANTENNA1"].data, return_index=True)
                antenna2_id, indices2 = da.unique(ds["ANTENNA2"].data, return_index=True)
                antenna_obs_id[(antenna1_id, )] = ds["OBSERVATION_ID"].data[indices1]
                antenna_obs_id[(antenna2_id, )] = ds["OBSERVATION_ID"].data[indices2]

        antenna_obj.create_primary_beam(observation=obs_obj, antenna_obs_id=antenna_obs_id)

        dataset = Dataset(
            antenna=antenna_obj,
            baseline=baseline_obj,
            field=field_obj,
            spws=spw_obj,
            polarization=pol_obj,
            observation=obs_obj,
            ms_list=ms_list,
            do_psf_calculation=calculate_psf
        )

        ms_keys_first = ms[0].data_vars.keys()
        if self.__key_is_on_dataset("CORRECTED_DATA", ms_keys_first):
            dataset.corrected_column_present = True

        return dataset

    @staticmethod
    def assign_zero_column(
        datasets: List[xarray.Dataset] = None,
        original_column: str = None,
        new_column: str = None,
    ):
        for i, ds in enumerate(datasets):
            dims = ds[original_column].dims
            datasets[i] = ds.assign({new_column: (dims, da.zeros_like(ds[original_column].data))})

    @staticmethod
    def __key_is_on_dataset(key: str = None, keys: KeysView = None):
        if key is not None or keys is not None:
            return key in keys
        else:
            raise ValueError("Cannot check Nonetype keys on Nonetype KeysView object")

    @staticmethod
    def copy_column(
        datasets: List[xarray.Dataset] = None,
        original_column: str = None,
        new_column: str = None,
    ):
        for i, ds in enumerate(datasets):
            dims = ds[original_column].dims
            datasets[i] = ds.assign({new_column: (dims, ds[original_column].data)})

    @staticmethod
    def rename_column(
        datasets: List[xarray.Dataset] = None,
        original_column: str = None,
        new_column: str = None,
    ):
        for i, ds in enumerate(datasets):
            datasets[i] = ds.rename_vars({original_column: new_column})

    def write_xarray_ds(
        self,
        dataset: Union[xarray.Dataset, List[xarray.Dataset]] = None,
        ms_name: str = None,
        table_name: str = None,
        columns: Union[List[str], str] = None,
        dask_compute: bool = True,
    ) -> Union[List[xarray.Dataset], None]:
        """Function that writes a xarray Dataset to a Measurement Set File.

        Parameters
        ----------
        dataset : xarray Dataset to write
        ms_name : Name of the output Measurement Set
        table_name : Name of the table you might write. Default: None
        columns : Name of the columns to write to the table
        dask_compute: Whether to compute the write operations or not
        """
        if dataset is None:
            raise ValueError("Cannot write into Measurement Set without an xarray Dataset object")

        if ms_name is None:
            ms_name = self.input_name
        else:
            if not os.path.exists(ms_name):
                raise FileNotFoundError("Not such file or directory {}".format(ms_name))

        if table_name is not None:
            ms_name = ms_name + "::" + table_name

        if columns is None:
            columns = "ALL"
        else:
            if isinstance(columns, str):
                columns = columns.split(",")

        writes = xds_to_table(dataset, table_name=ms_name, columns=columns)
        # TODO: This could be returned and then the compute can be done by the user
        if dask_compute:
            dask.compute(writes)
            return None
        else:
            return writes

    def write(
        self,
        dataset: Dataset = None,
        ms_name: str = None,
        columns: Union[List[str], str] = "ALL",
        dask_compute: bool = True,
    ) -> Union[List[xarray.Dataset], None]:
        """Function that writes a Pyralysis Dataset (only a ms_list) to a
        Measurement Set File.

        Parameters
        ----------
        dataset : Dataset
            Pyralysis Dataset to write
        ms_name : str
            Name of the output Measurement Set
        columns : Union[List[str], str]
            Name of the column to write the table
        dask_compute: bool
            Whether to compute the write operations or not
        """
        if ms_name is None:
            ms_name = self.input_name
        else:
            if self.input_name != ms_name:
                if not os.path.exists(ms_name):
                    warnings.warn(
                        "The Measurement Set file does not exist, it will be copied from the input MS",
                        UserWarning,
                    )
                    shutil.copytree(self.input_name, ms_name)

        ms_datasets = [ms.visibilities.dataset for ms in dataset.ms_list]

        return self.write_xarray_ds(
            dataset=ms_datasets,
            ms_name=ms_name,
            columns=columns,
            dask_compute=dask_compute,
        )
