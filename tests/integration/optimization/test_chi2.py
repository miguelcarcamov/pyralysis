import dask.array as da
import numpy as np
import pytest
import xarray as xr

from pyralysis.estimators import NearestNeighbor
from pyralysis.optimization.fi import Chi2
from pyralysis.reconstruction.image import Image
from pyralysis.reconstruction.mask import Mask


class TestChi2:

    @pytest.fixture
    def chi2(self, request, dataset):
        imsize = request.param[0]
        chunks = request.param[1] if len(request.param) > 1 else "auto"

        X, Y = da.indices((imsize[0], imsize[1]))
        image = Image(
            data=xr.DataArray(
                data=da.zeros((imsize[0], imsize[1]), chunks=chunks),
                dims=["x", "y"],
                coords=dict(X=(["x", "y"], X), Y=(["x", "y"], Y))
            ),
            cellsize=1.0,
            chunks=chunks
        )

        model_vis = NearestNeighbor(
            input_data=dataset,
            image=image,
            cellsize=1.0,
            hermitian_symmetry=False,
            padding_factor=1.2
        )

        _chi2 = Chi2(model_visibility=model_vis)

        return _chi2

    @pytest.fixture
    def chi2_daskms(self, request, dataset_daskms):
        imsize = request.param[0]
        chunks = request.param[1] if len(request.param) > 1 else "auto"

        X, Y = da.indices((imsize[0], imsize[1]))
        image = Image(
            data=xr.DataArray(
                data=da.zeros((imsize[0], imsize[1]), chunks=chunks),
                dims=["x", "y"],
                coords=dict(X=(["x", "y"], X), Y=(["x", "y"], Y))
            ),
            cellsize=1.0,
            chunks=chunks
        )

        model_vis = NearestNeighbor(
            input_data=dataset_daskms,
            image=image,
            cellsize=1.0,
            hermitian_symmetry=False,
            padding_factor=1.2
        )

        _chi2 = Chi2(model_visibility=model_vis)

        return _chi2

    @pytest.mark.parametrize(
        "chi2, chunks", [
            (
                ((20, 20), (10, 10)),
                (10, 10),
            ),
            (
                ((20, 20), (10, 5)),
                (10, 5),
            ),
        ],
        indirect=["chi2"]
    )
    def test_init_image_chunks(self, chi2, chunks):
        assert chi2.image.data.data.chunksize == chunks

    def test_init_image_supersede_modelvis(self, dataset):
        imsize = (256, 256)
        image = Image(
            data=xr.DataArray(
                data=da.zeros((imsize[0], imsize[1])),
                dims=["x", "y"],
            ),
            cellsize=1.0,
        )
        model_vis = NearestNeighbor(
            input_data=dataset, image=image, hermitian_symmetry=False, padding_factor=1.2
        )

        imsize_chi2 = (1024, 1024)
        image_chi2 = Image(
            data=xr.DataArray(
                data=da.zeros((imsize_chi2[0], imsize_chi2[1])),
                dims=["x", "y"],
            ),
            cellsize=1.0,
        )
        chi2 = Chi2(image=image_chi2, model_visibility=model_vis)

        assert chi2.image == image_chi2
        assert chi2.model_visibility.image == image_chi2

    @pytest.mark.parametrize(
        "chi2, shape, chunks", [
            (
                ((20, 20), (10, 10)),
                (20, 20),
                (10, 10),
            ),
            (
                ((20, 20), (10, 5)),
                (20, 20),
                (10, 5),
            ),
        ],
        indirect=["chi2"]
    )
    def test_gradient_chunks(self, chi2, shape, chunks):
        # Get shape chunks of grad_value
        assert chi2.grad_value.shape == shape
        assert chi2.grad_value.chunksize == chunks
        # Get result
        res = chi2.gradient()
        assert res.shape == shape
        assert chi2.grad_value.shape == shape
        # Chunks of gradient as the image
        assert res.chunksize == chunks
        assert chi2.grad_value.chunksize == chunks

    @pytest.mark.parametrize(
        "chi2, shape, chunks, mask", [
            (
                ((20, 20), (10, 10)), (20, 20), (10, 10),
                da.array(
                    [
                        [1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0],
                        [1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0],
                        [0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0],
                        [1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0],
                        [0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0],
                        [1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0],
                        [0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0],
                        [0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1],
                        [1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0],
                        [0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0],
                        [0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
                        [1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1],
                        [1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0],
                        [0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0],
                        [0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1],
                        [1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0],
                        [0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
                        [0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0],
                        [0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0],
                        [1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1]
                    ],
                    dtype=bool
                )
            ),
            (
                ((20, 20), (10, 5)), (20, 20), (10, 5),
                da.array(
                    [
                        [1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0],
                        [1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0],
                        [0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0],
                        [1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0],
                        [0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0],
                        [1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0],
                        [0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0],
                        [0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1],
                        [1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0],
                        [0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0],
                        [0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
                        [1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1],
                        [1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0],
                        [0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0],
                        [0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1],
                        [1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0],
                        [0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
                        [0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0],
                        [0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0],
                        [1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1]
                    ],
                    dtype=bool
                )
            ),
        ],
        indirect=["chi2"]
    )
    def test_gradient_mask_chunks(self, chi2, shape, chunks, mask):
        # Make mask
        mask = Mask(data=xr.DataArray(mask), chunks=chi2.image.data.data.chunksize)
        assert mask.data.data.chunksize == chunks
        # Get shape chunks of grad_value
        assert chi2.grad_value.shape == shape
        assert chi2.grad_value.chunksize == chunks
        # Get result
        res = chi2.gradient(mask=mask)
        assert res.shape == shape
        assert chi2.grad_value.shape == shape
        # Chunks of gradient as the image
        assert res.chunksize == chunks
        assert chi2.grad_value.chunksize == chunks

    @pytest.mark.parametrize(
        "chi2, new_imsize", [(((20, 20), (10, 10)), (256, 256))], indirect=["chi2"]
    )
    def test_update_image(self, chi2, new_imsize):
        X, Y = da.indices((new_imsize[0], new_imsize[1]))
        new_image = Image(
            data=xr.DataArray(
                data=da.zeros((new_imsize[0], new_imsize[1])),
                dims=["x", "y"],
                coords=dict(X=(["x", "y"], X), Y=(["x", "y"], Y))
            ),
            cellsize=1.0,
        )

        assert chi2.image == chi2.model_visibility.image

        chi2.image = new_image

        assert chi2.grad_value.shape == new_image.data.shape
        assert chi2.image == new_image
        assert chi2.model_visibility.image == new_image
        assert chi2.image == chi2.model_visibility.image
