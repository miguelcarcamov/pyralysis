import astropy.units as un
import dask.array as da
import numpy as np
import pytest
import xarray as xr

from pyralysis.convolution.primary_beam import PrimaryBeam
from pyralysis.reconstruction.mask import Mask


class TestMaskPrimaryBeam:

    @pytest.mark.parametrize(
        "threshold, center", [(0.5, False), (1.0, False), (1.1, True), (1.5, True)]
    )
    def test_init_default_mask_threshold(self, dataset, threshold, center):
        imsize = (2048, 2048)
        imcenter = (1024, 1024)

        mask = Mask(dataset=dataset, imsize=imsize, threshold=threshold, cellsize=1.0)

        assert isinstance(mask.data, xr.DataArray)
        assert mask.data.data.shape == imsize
        assert mask.data.data.chunksize == imsize
        assert mask.data.data[imcenter[0], imcenter[1]].compute() == center

    @pytest.mark.parametrize("chunks", [(2048, 2048), (1024, 1024), (256, 256), (1024, 2048)])
    def test_init_default_mask_chunks(self, dataset, chunks):
        imsize = (2048, 2048)
        threshold = 1.0
        imcenter = (1024, 1024)

        mask = Mask(
            dataset=dataset, imsize=imsize, threshold=threshold, cellsize=1.0, chunks=chunks
        )

        assert isinstance(mask.data, xr.DataArray)
        assert mask.data.data.shape == imsize
        assert mask.data.data.chunksize == chunks
        assert mask.data.data[imcenter[0], imcenter[1]] == False
        assert (mask.cellsize == [-1.0, 1.0] * un.rad).all()
