import json
import os
import pathlib
from types import SimpleNamespace as Namespace

import dask.array as da
import numpy as np
import pytest
import xarray as xr
from daskms import xds_from_ms, xds_from_table
from daskms.example_data import example_ms

from pyralysis.base import (
    Antenna,
    Dataset,
    Field,
    Observation,
    Polarization,
    SpectralWindow,
    SubMS,
    VisibilitySet,
)
from pyralysis.reconstruction import Image
from pyralysis.utils.image_utils import get_coordinates

SCRIPT_PATH = os.path.dirname(__file__)


def namespace_to_dict(namespace):
    dic = vars(namespace)
    for key in dic:
        if isinstance(dic[key], Namespace):
            dic[key] = namespace_to_dict(dic[key])
    return dic


@pytest.fixture
def image_2dim():
    filename = pathlib.Path(SCRIPT_PATH).joinpath("fixtures/test_image.json")
    with filename.open() as f:
        # Load json as object
        content = json.load(f, object_hook=lambda d: Namespace(**d))
        hdul = content.image
        # Get header data
        header = vars(hdul[0].header)  # as dict
        coords = get_coordinates(header)
        dims = ["x", "y"]  # because ndims = 2
        # Get data array
        data = da.from_array(hdul[0].data, chunks="auto").squeeze()
        image_xarray = xr.DataArray(data, dims=dims, coords=coords, attrs=header)
        return Image(data=image_xarray)


@pytest.fixture
def dataset_daskms():
    ms_filename = example_ms()

    # SPWS
    spws = xds_from_table(
        ms_filename + "::SPECTRAL_WINDOW",
        taql_where="!FLAG_ROW",
        group_cols="__row__",
    )
    spws_obj = SpectralWindow(dataset=spws)

    # POLARIZATION
    pols = xds_from_table(
        ms_filename + "::POLARIZATION",
        taql_where="!FLAG_ROW",
        group_cols="__row__",
    )
    pol_obj = Polarization(dataset=pols)

    # FIELD
    fields, fields_tabkw, fields_colkw = xds_from_table(
        ms_filename + "::FIELD",
        taql_where="!FLAG_ROW",
        group_cols="__row__",
        table_keywords=True,
        column_keywords=True
    )
    # Fill missing values
    fields_colkw["PHASE_DIR"]["MEASINFO"]["TabRefTypes"] = ["J2000", "ICRS"]
    fields_colkw["PHASE_DIR"]["MEASINFO"]["TabRefCodes"] = [0, 21]
    fields_colkw["REFERENCE_DIR"]["MEASINFO"]["TabRefTypes"] = ["J2000", "ICRS"]
    fields_colkw["REFERENCE_DIR"]["MEASINFO"]["TabRefCodes"] = [0, 21]
    for i, f in enumerate(fields):
        f = f.assign(RefDir_Ref=(["row"], da.from_array([0])))
        fields[i] = f.assign(PhaseDir_Ref=(["row"], da.from_array([0])))

    field_obj = Field(dataset=fields, table_keywords=fields_tabkw, column_keywords=fields_colkw)

    # MS LIST
    ddids = xds_from_table(ms_filename + "::DATA_DESCRIPTION", taql_where="!FLAG_ROW")[0]
    dataset = xds_from_ms(
        ms_filename,
        taql_where="!FLAG_ROW",
        group_cols=["FIELD_ID", "DATA_DESC_ID"],
        index_cols=["SCAN_NUMBER", "TIME", "ANTENNA1", "ANTENNA2"]
    )
    ms_list = []
    for i, ds in enumerate(dataset):
        ms_keys = ds.data_vars.keys()
        f_did = ds.attrs["FIELD_ID"]
        ddid = ds.attrs["DATA_DESC_ID"]
        spw_id = ddids["SPECTRAL_WINDOW_ID"][ddid].data.compute()
        pol_id = ddids["POLARIZATION_ID"][ddid].data.compute()

        base_dataset = ds[[
            "UVW", "DATA", "TIME", "SCAN_NUMBER", "ANTENNA1", "ANTENNA2", "OBSERVATION_ID"
        ]]

        # Create missing variables
        base_dataset = base_dataset.assign(
            WEIGHT=(["row", "corr"], da.ones(ds.DATA.data[:, 0, :].shape, dtype=np.float32))
        )
        base_dataset = base_dataset.assign(
            IMAGING_WEIGHTS=(
                ["row", "corr"], da.zeros(ds.DATA.data[:, 0, :].shape, dtype=np.float32)
            )
        )
        base_dataset = base_dataset.assign(
            FLAG=(
                ["row", "chan", "corr"],
                da.from_array(np.resize(ds.FLAG_ROW.data.compute(), ds.DATA.data.shape))
            )
        )
        base_dataset = base_dataset.assign(BASELINE_ID=(["row"], da.zeros(ds.ANTENNA1.data.shape)))

        visibility_obj = VisibilitySet(dataset=base_dataset)

        ms_obj = SubMS(
            _id=i,
            field_id=f_did,
            polarization_id=pol_id,
            spw_id=spw_id,
            visibilities=visibility_obj,
        )
        ms_list.append(ms_obj)

    # DATASET
    ds_obj = Dataset(field=field_obj, spws=spws_obj, polarization=pol_obj, ms_list=ms_list)

    return ds_obj


@pytest.fixture
def dataset():
    filename = pathlib.Path(SCRIPT_PATH).joinpath("fixtures/test_dataset.json")
    with filename.open() as f:
        content = json.load(f, object_hook=lambda d: Namespace(**d))

        # OBSERVATION
        obs = content.observation
        data_vars = {"TELESCOPE_NAME": (["row"], da.from_array(obs.TELESCOPE_NAME))}
        coords = {"ROWID": (["row"], np.arange(len(obs.TELESCOPE_NAME)))}

        obs_obj = Observation(dataset=xr.Dataset(data_vars=data_vars, coords=coords))
        antenna_obs_id = None

        # ANTENNA
        antenna = content.antenna
        data_vars = {"DISH_DIAMETER": (["row"], da.from_array(antenna.DISH_DIAMETER))}
        coords = {"ROWID": (["row"], np.arange(len(antenna.DISH_DIAMETER)))}

        antenna_obj = Antenna(dataset=xr.Dataset(data_vars=data_vars, coords=coords))

        if obs_obj.ntelescope > 1:
            antenna_obs_id = da.zeros_like(antenna_obj.dataset.ROWID, dtype=np.int32)

        # SPECTRAL WINDOW
        spws = content.spws
        spws_dataset = []  # 3 spws
        for i, sw in enumerate(spws):
            data_vars = {
                "CHAN_FREQ": (["row", "chan"], da.from_array(sw.CHAN_FREQ)),
                "REF_FREQUENCY": (["row"], da.from_array(sw.REF_FREQUENCY)),
                "NUM_CHAN": (["row"], da.from_array(sw.NUM_CHAN))
            }
            coords = {"ROWID": (["row"], np.array([i]))}
            spws_dataset.append(xr.Dataset(data_vars=data_vars, coords=coords))
        spws_obj = SpectralWindow(dataset=spws_dataset)

        # FIELD
        field = content.field
        field_dataset = []  # 1 field
        for i, fd in enumerate(field):
            data_vars = {
                "REFERENCE_DIR": (
                    ["row", "field-poly", "field-dir"], da.from_array(fd.REFERENCE_DIR)
                ),
                "RefDir_Ref": (["row"], da.from_array(fd.RefDir_Ref)),
                "PHASE_DIR": (["row", "field-poly", "field-dir"], da.from_array(fd.PHASE_DIR)),
                "PhaseDir_Ref": (["row"], da.from_array(fd.PhaseDir_Ref)),
                "TIME": (["row"], da.from_array(fd.TIME))
            }
            coords = {"ROWID": (["row"], np.array([i]))}
            field_dataset.append(xr.Dataset(data_vars=data_vars, coords=coords))
        field_column_keyword = content.field_column_keyword
        field_column_keyword = namespace_to_dict(field_column_keyword)

        field_obj = Field(dataset=field_dataset[0], column_keywords=field_column_keyword)

        # POLARIZATION
        pol = content.polarization
        pol_dataset = []  # 1 pol
        for i, p in enumerate(pol):
            data_vars = {
                "CORR_TYPE": (["row", "corr"], da.from_array(p.CORR_TYPE)),
                "NUM_CORR": (["row"], da.from_array(p.NUM_CORR))
            }
            coords = {"ROWID": (["row"], np.array([i]))}
            pol_dataset.append(xr.Dataset(data_vars=data_vars, coords=coords))
        pol_obj = Polarization(dataset=pol_dataset)

        # MSLIST
        ms_ds = content.dataset
        ms_list = []
        for i, ms in enumerate(ms_ds):
            field_id = ms.field_id
            spw_id = ms.spw_id
            pol_id = ms.polarization_id

            nrow = len(ms.visibilities.UVW)
            nchan = spws_obj.nchans[spw_id]
            ncorr = pol_obj.ncorrs[pol_id]

            zero_1d = da.zeros(nrow)
            zero_2d = da.zeros((nrow, ncorr))
            zero_3d = da.zeros((nrow, nchan, ncorr))
            bool_3d = zero_3d.astype(bool)

            data = ms.visibilities.DATA
            model_data = ms.visibilities.MODEL_DATA
            data_da = da.from_array(np.array(data)[..., 0] + 1.0j * np.array(data)[..., 1])
            model_data_da = da.from_array(
                np.array(model_data)[..., 0] + 1.0j * np.array(model_data)[..., 1]
            )

            data_vars = {
                "UVW": (["row", "uvw"], da.from_array(ms.visibilities.UVW)),
                "WEIGHT": (["row", "corr"], da.from_array(ms.visibilities.WEIGHT)),
                "IMAGING_WEIGHTS": (["row", "corr"], zero_2d),
                "FLAG": (["row", "chan", "corr"], bool_3d),
                "DATA": (["row", "chan", "corr"], data_da),
                "MODEL_DATA": (["row", "chan", "corr"], model_data_da),
                "CORRECTED_DATA": (["row", "chan", "corr"], zero_3d),
                "ANTENNA1": (["row"], zero_1d),
                "ANTENNA2": (["row"], zero_1d),
                "OBSERVATION_ID": (["row"], zero_1d),
                "BASELINE_ID": (["row"], zero_1d),
                "TIME": (["row"], zero_1d),
                "SCAN_NUMBER": (["row"], zero_1d)
            }
            coords = {"ROWID": (["row"], da.arange(nrow))}
            ds = xr.Dataset(data_vars=data_vars, coords=coords)

            vis_obj = VisibilitySet(dataset=ds)
            ms_obj = SubMS(
                _id=i,
                field_id=field_id,
                polarization_id=pol_id,
                spw_id=spw_id,
                visibilities=vis_obj,
            )
            ms_list.append(ms_obj)

        antenna_obj.create_primary_beam(observation=obs_obj, antenna_obs_id=antenna_obs_id)
        ds_obj = Dataset(
            observation=obs_obj,
            antenna=antenna_obj,
            field=field_obj,
            spws=spws_obj,
            polarization=pol_obj,
            ms_list=ms_list
        )
        return ds_obj
