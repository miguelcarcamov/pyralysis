import dask.array as da
import pytest
import xarray as xr

from pyralysis.optimization.fi import L1Norm
from pyralysis.optimization.objective_function import ObjectiveFunction
from pyralysis.optimization.projection import (
    CompositeProjection,
    EqualTo,
    GreaterThan,
    GreaterThanEqualTo,
    LessThan,
    NoProjection,
    NotEqualTo,
)
from pyralysis.reconstruction.image import Image
from pyralysis.reconstruction.mask import Mask
from pyralysis.utils.f1dim import f1dim


class TestF1dim:

    @pytest.fixture
    def f1dimparams(self, request):
        # Request order : image data, fi list, mask data
        # Image definition
        image = Image(data=xr.DataArray(request.param[0]), cellsize=1.0)
        # Create objective function and calculate function and gradient
        obj_func = ObjectiveFunction(fi_list=request.param[1], image=image)
        if len(request.param) > 2:
            mask = Mask(data=xr.DataArray(request.param[2]))
            obj_func.calculate_function(mask=mask)
            obj_func.calculate_gradient(iteration=1, mask=mask)
        else:
            mask = None
            obj_func.calculate_function()
            obj_func.calculate_gradient(iteration=1)
        return obj_func, image, mask

    @pytest.mark.parametrize(
        "f1dimparams, projection, eval, expected", [
            (
                (da.arange(16).reshape((4, 4)), [L1Norm()]),
                NoProjection(),
                [0.0, 0.5, 1.0],
                [120, 112.5, 105],
            ),
            (
                (da.arange(16).reshape((4, 4)), [L1Norm()]),
                GreaterThan(compared_value=7, replacement_value=0),
                [0.0, 0.5, 1.0],
                [28, 24.5, 28],
            ),
            (
                (da.arange(16).reshape((4, 4)), [L1Norm()]),
                LessThan(compared_value=8, replacement_value=0),
                [0.0, 0.5, 1.0],
                [92, 80.5, 77],
            ),
            (
                (da.arange(16).reshape((4, 4)), [L1Norm()]),
                CompositeProjection(
                    GreaterThan(compared_value=6, replacement_value=0),
                    LessThan(compared_value=5, replacement_value=0)
                ),
                [0.0, 0.5, 1.0],
                [11, 5.5, 11],
            )
        ],
        indirect=["f1dimparams"]
    )
    def test_f1dim(self, f1dimparams, projection, eval, expected):
        obj_func, image, _ = f1dimparams
        f = f1dim(obj_func, image, projection)
        for ev, ex in zip(eval, expected):
            assert f(ev) == ex

    @pytest.mark.parametrize(
        "f1dimparams, projection, eval, expected", [
            (
                (da.arange(16).reshape((4, 4)), [L1Norm()], da.ones((4, 4)).astype(bool)),
                NoProjection(),
                [0.0, 0.5, 1.0],
                [120, 112.5, 105],
            ),
            (
                (
                    da.arange(16).reshape((4, 4)), [L1Norm()],
                    da.array([[0, 1, 1, 0], [1, 0, 0, 1], [1, 1, 1, 1], [1, 0, 0, 1]]).astype(bool)
                ),
                NoProjection(),
                [0.0, 0.5, 1.0],
                [79, 74, 69],
            ),
            (
                (
                    da.arange(16).reshape((4, 4)), [L1Norm()],
                    da.array([[0, 1, 1, 0], [1, 0, 0, 1], [1, 1, 1, 1], [1, 0, 0, 1]]).astype(bool)
                ),
                EqualTo(compared_value=9, replacement_value=0),
                [0.0, 0.5, 1.0],
                [70, 74, 60],
            ),
            (
                (
                    da.arange(16).reshape((4, 4)), [L1Norm()],
                    da.array([[0, 1, 1, 0], [1, 0, 0, 1], [1, 1, 1, 1], [1, 0, 0, 1]]).astype(bool)
                ),
                NotEqualTo(compared_value=9, replacement_value=0),
                [0.0, 0.5, 1.0],
                [9, 0, 9],
            ),
            (
                (
                    da.arange(16).reshape((4, 4)), [L1Norm()],
                    da.array([[0, 1, 1, 0], [1, 0, 0, 1], [1, 1, 1, 1], [1, 0, 0, 1]]).astype(bool)
                ),
                CompositeProjection(
                    EqualTo(compared_value=1, replacement_value=0),
                    GreaterThanEqualTo(compared_value=9.5, replacement_value=0)
                ),
                [0.0, 0.5, 1.0],
                [30, 28, 33],
            )
        ],
        indirect=["f1dimparams"]
    )
    def test_f1dim_mask(self, f1dimparams, projection, eval, expected):
        obj_func, image, mask = f1dimparams
        f = f1dim(obj_func, image, projection, mask)
        for ev, ex in zip(eval, expected):
            assert f(ev) == ex
