from daskms import xds_from_table
from daskms.example_data import example_ms
from pytest import fixture


@fixture()
def sample_dataset(request):
    sample_dataset = xds_from_table(example_ms() + f"::{request.param}")[0]
    return sample_dataset
