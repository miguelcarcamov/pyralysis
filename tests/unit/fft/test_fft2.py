import dask.array as da
import numpy as np
import pytest
from numpy.testing import assert_almost_equal

from pyralysis.fft import fft2, ifft2
from pyralysis.fft._fft2 import _infer_chunks


class TestFFT2:

    @pytest.mark.parametrize(
        "imsize", [
            (30, 30), (32, 32), (60, 60), (64, 64), (250, 250), (256, 256), (500, 500), (512, 512),
            (513, 513)
        ]
    )
    @pytest.mark.parametrize("hermitian_symmetry", [False, True])
    @pytest.mark.parametrize("sign_convention", ["negative", "positive"])
    @pytest.mark.parametrize("padding_factor", [1.0, 1.2, 1.5, 1.8, 2.0])
    @pytest.mark.parametrize(
        "dtype", ["float", np.float32, np.float64, "complex", np.complex64, np.complex128]
    )
    def test_imsize_dims_dtypes(
        self, imsize, hermitian_symmetry, sign_convention, padding_factor, dtype
    ):
        image = da.zeros(imsize, dtype=dtype)

        if hermitian_symmetry:
            image = image.real

        padded_imsize = tuple(int(padding_factor * x) for x in imsize[-2:])

        fourier = fft2(
            image,
            sign_convention=sign_convention,
            padding_factor=padding_factor,
            hermitian_symmetry=hermitian_symmetry
        )

        assert da.sum(da.abs(fourier)) == 0.0

        assert (fourier.dtype.type == np.complex64 or fourier.dtype.type == np.complex128)

        if not hermitian_symmetry:
            assert fourier.shape == padded_imsize
        else:
            assert fourier.shape == (padded_imsize[0], padded_imsize[1] // 2 + 1)

        assert image.ndim == fourier.ndim

        if dtype == np.float32 or dtype == np.complex64:
            assert fourier.dtype == np.complex64

        if dtype == np.float64 or dtype == np.complex128:
            assert fourier.dtype == np.complex128

    @pytest.mark.parametrize(
        "imsize", [
            (30, 30), (32, 32), (60, 60), (64, 64), (250, 250), (256, 256), (500, 500), (512, 512),
            (513, 513)
        ]
    )
    @pytest.mark.parametrize("dtype", ["float", np.float32, np.float64])
    @pytest.mark.parametrize("hermitian_symmetry", [False, True])
    @pytest.mark.parametrize("sign_convention", ["negative", "positive"])
    @pytest.mark.parametrize("padding_factor", [1.0, 1.2, 1.5, 1.8, 2.0])
    @pytest.mark.parametrize("apply_fftshift", [False, True])
    def test_values(
        self, imsize, dtype, hermitian_symmetry, sign_convention, padding_factor, apply_fftshift
    ):
        state = da.random.RandomState(666)
        image = state.random(size=imsize).astype(dtype)
        sum_image_pixels = da.sum(image)
        padded_imsize = tuple(int(padding_factor * x) for x in imsize[-2:])

        fourier = fft2(
            image,
            sign_convention=sign_convention,
            padding_factor=padding_factor,
            hermitian_symmetry=hermitian_symmetry,
            apply_fftshift=apply_fftshift
        )

        if not apply_fftshift:
            assert_almost_equal(fourier[0, 0].real.compute(), sum_image_pixels.compute(), decimal=1)
        else:
            if hermitian_symmetry:
                assert_almost_equal(
                    fourier[padded_imsize[0] // 2, 0].real.compute(),
                    sum_image_pixels.compute(),
                    decimal=1
                )
            else:
                assert_almost_equal(
                    fourier[padded_imsize[0] // 2, padded_imsize[1] // 2].real.compute(),
                    sum_image_pixels.compute(),
                    decimal=1
                )


class TestIFFT2:

    @pytest.mark.parametrize(
        "imsize", [
            (30, 30), (32, 32), (60, 60), (64, 64), (250, 250), (256, 256), (500, 500), (512, 512),
            (513, 513)
        ]
    )
    @pytest.mark.parametrize("hermitian_symmetry", [False, True])
    @pytest.mark.parametrize("sign_convention", ["negative", "positive"])
    @pytest.mark.parametrize("padding_factor", [1.0])
    @pytest.mark.parametrize("dtype", ["complex", np.complex64, np.complex128])
    def test_imsize_dims_dtypes(
        self, imsize, hermitian_symmetry, sign_convention, padding_factor, dtype
    ):
        fourier = da.zeros(imsize, dtype=dtype)

        if hermitian_symmetry:
            fourier = fourier[:, 0:imsize[1] // 2 + 1]
            fourier = da.flip(fourier, axis=(-2, -1)).conj()

        image = ifft2(
            fourier,
            s=imsize,
            sign_convention=sign_convention,
            hermitian_symmetry=hermitian_symmetry
        )

        assert da.sum(da.abs(image)) == 0.0

        assert (image.dtype.type == np.float32 or image.dtype.type == np.float64)

        assert image.shape == imsize

        assert image.ndim == fourier.ndim

        if dtype == np.complex64:
            assert image.dtype == np.float32

        if dtype == np.complex128:
            assert image.dtype == np.float64


class TestFFT2RestoreChunks:

    @pytest.mark.parametrize("imsize", [(30, 30), (60, 60), (250, 250), (512, 512)])
    @pytest.mark.parametrize("hermitian_symmetry", [False, True])
    @pytest.mark.parametrize("restore_chunks", [True, False])
    def test_restore_chunks(self, imsize, hermitian_symmetry, restore_chunks):
        # Create a random image with specific chunks
        image = da.random.random(imsize, chunks=(imsize[0] // 2, imsize[1] // 2))
        original_chunks = image.chunks

        # Perform FFT2 without padding
        fourier = fft2(
            image,
            padding_factor=1.0,
            hermitian_symmetry=hermitian_symmetry,
            restore_chunks=restore_chunks
        )

        if restore_chunks:
            if hermitian_symmetry:
                # Infer expected chunks for Hermitian symmetry
                expected_chunks = list(original_chunks)
                expected_chunks[-1] = _infer_chunks(original_chunks[-1], imsize[-1] // 2 + 1)
                assert fourier.chunks == tuple(expected_chunks)
            else:
                assert fourier.chunks == original_chunks
        else:
            assert fourier.chunks != original_chunks

        # Verify shape
        if hermitian_symmetry:
            assert fourier.shape == (imsize[0], imsize[1] // 2 + 1)
        else:
            assert fourier.shape == imsize


class TestIFFT2RestoreChunks:

    @pytest.mark.parametrize("imsize", [(30, 30), (60, 60), (250, 250), (512, 512)])
    @pytest.mark.parametrize("hermitian_symmetry", [False, True])
    @pytest.mark.parametrize("restore_chunks", [True, False])
    def test_restore_chunks(self, imsize, hermitian_symmetry, restore_chunks):
        # Create a Fourier array with specific chunks
        reduced_shape = (imsize[0], imsize[1] // 2 + 1) if hermitian_symmetry else imsize
        fourier = da.random.random(reduced_shape, chunks=(imsize[0] // 2, reduced_shape[1] // 2))
        original_chunks = fourier.chunks

        # Perform IFFT2
        image = ifft2(
            fourier, s=imsize, hermitian_symmetry=hermitian_symmetry, restore_chunks=restore_chunks
        )

        if restore_chunks:
            if hermitian_symmetry:
                # Infer expected chunks for Hermitian symmetry
                inferred_chunks = list(original_chunks)
                inferred_chunks[-1] = _infer_chunks(original_chunks[-1], imsize[-1])
                assert image.chunks == tuple(inferred_chunks)
            else:
                assert image.chunks == original_chunks
        else:
            assert image.chunks != original_chunks

        # Verify shape
        assert image.shape == imsize
