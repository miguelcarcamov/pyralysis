import dask.array as da
import numpy as np
import pytest

from pyralysis.utils.fourier import phase_shift_grid


class TestPhaseShift:

    @pytest.mark.parametrize(
        "imsize", [
            (30, 30),
            (32, 32),
            (60, 60),
            (64, 64),
            (250, 250),
            (256, 256),
            (500, 500),
            (512, 512),
        ]
    )
    def test_fft2(self, imsize):
        image = da.zeros(imsize, dtype=float)
        image[imsize[0] // 2, imsize[1] // 2] = 1.0
        fourier_image = da.fft.ifftshift(da.fft.fft2(da.fft.fftshift(image)))

        x0_shift = np.random.randint(low=-imsize[0] // 2, high=imsize[0] // 2, size=1, dtype=int)[0]
        y0_shift = np.random.randint(low=-imsize[1] // 2, high=imsize[1] // 2, size=1, dtype=int)[0]

        shifted_fourier_image = phase_shift_grid(fourier_image, x0_shift, y0_shift)
        shifted_image = da.fft.ifftshift(da.fft.ifft2(da.fft.fftshift(shifted_fourier_image)))

        shifted_should_be = da.zeros(imsize, dtype=float)
        shifted_should_be[imsize[0] // 2 + y0_shift, imsize[1] // 2 + x0_shift] = 1.0

        final_y, final_x = da.unravel_index(
            da.argmax(shifted_image.real), shifted_image.real.shape
        )[0].compute(), da.unravel_index(da.argmax(shifted_image.real),
                                         shifted_image.real.shape)[1].compute()

        assert final_x == imsize[0] // 2 + x0_shift
        assert final_y == imsize[1] // 2 + y0_shift

    @pytest.mark.parametrize(
        "imsize", [
            (30, 30),
            (32, 32),
            (60, 60),
            (64, 64),
            (250, 250),
            (256, 256),
            (500, 500),
            (512, 512),
        ]
    )
    def test_rfft2(self, imsize):
        image = da.zeros(imsize, dtype=float)
        image[imsize[0] // 2, imsize[1] // 2] = 1.0
        fourier_image = da.fft.ifftshift(
            da.fft.rfft2(da.fft.fftshift(image, axes=(-2, -1))), axes=-2
        )

        x0_shift = np.random.randint(low=-imsize[0] // 2, high=imsize[0] // 2, size=1, dtype=int)[0]
        y0_shift = np.random.randint(low=-imsize[1] // 2, high=imsize[1] // 2, size=1, dtype=int)[0]

        shifted_fourier_image = phase_shift_grid(fourier_image, x0_shift, y0_shift)
        shifted_image = da.fft.ifftshift(
            da.fft.irfft2(da.fft.fftshift(shifted_fourier_image, axes=-2), s=imsize), axes=(-2, -1)
        )

        shifted_should_be = da.zeros(imsize, dtype=float)
        shifted_should_be[imsize[0] // 2 + y0_shift, imsize[1] // 2 + x0_shift] = 1.0

        final_y, final_x = da.unravel_index(
            da.argmax(shifted_image.real), shifted_image.real.shape
        )[0].compute(), da.unravel_index(da.argmax(shifted_image.real),
                                         shifted_image.real.shape)[1].compute()

        assert final_x == imsize[0] // 2 + x0_shift
        assert final_y == imsize[1] // 2 + y0_shift
        np.testing.assert_array_almost_equal(shifted_image, shifted_should_be)

    @pytest.mark.parametrize(
        "imsize", [
            (30, 30),
            (32, 32),
            (60, 60),
            (64, 64),
            (2, 60, 60),
            (2, 64, 64),
            (2, 3, 60, 60),
            (2, 3, 64, 64),
        ]
    )
    def test_fft2_phase_shift_nd(self, imsize):
        image = da.zeros(imsize, dtype=float)
        image[..., imsize[-2] // 2, imsize[-1] // 2] = 1.0
        fourier_image = da.fft.ifftshift(da.fft.fft2(da.fft.fftshift(image)))

        x0_shift = np.random.randint(low=-imsize[-2] // 2, high=imsize[-2] // 2, size=1,
                                     dtype=int)[0]
        y0_shift = np.random.randint(low=-imsize[-1] // 2, high=imsize[-1] // 2, size=1,
                                     dtype=int)[0]

        shifted_fourier_image = phase_shift_grid(fourier_image, x0_shift, y0_shift)
        shifted_image = da.fft.ifftshift(
            da.fft.ifft2(da.fft.fftshift(shifted_fourier_image, axes=(-2, -1)), axes=(-2, -1)),
            axes=(-2, -1)
        )

        shifted_should_be = da.zeros(imsize, dtype=float)
        shifted_should_be[..., imsize[-2] // 2 + y0_shift, imsize[-1] // 2 + x0_shift] = 1.0

        unravel_index = da.unravel_index(da.argmax(shifted_image.real), shifted_image.real.shape)
        final_y, final_x = unravel_index[-2].compute(), unravel_index[-1].compute()

        assert final_x == imsize[-2] // 2 + x0_shift
        assert final_y == imsize[-1] // 2 + y0_shift
        np.testing.assert_array_almost_equal(shifted_image, shifted_should_be)

    @pytest.mark.parametrize(
        "imsize", [
            (30, 30),
            (32, 32),
            (60, 60),
            (64, 64),
            (2, 60, 60),
            (2, 64, 64),
            (2, 3, 60, 60),
            (2, 3, 64, 64),
        ]
    )
    def test_rfft2_nd(self, imsize):
        image = da.zeros(imsize, dtype=float)
        image[..., imsize[-2] // 2, imsize[-1] // 2] = 1.0
        fourier_image = da.fft.ifftshift(
            da.fft.rfftn(da.fft.fftshift(image, axes=(-2, -1))), axes=-2
        )

        x0_shift = np.random.randint(low=-imsize[-2] // 2, high=imsize[-2] // 2, size=1,
                                     dtype=int)[0]
        y0_shift = np.random.randint(low=-imsize[-1] // 2, high=imsize[-1] // 2, size=1,
                                     dtype=int)[0]

        shifted_fourier_image = phase_shift_grid(fourier_image, x0_shift, y0_shift)
        shifted_image = da.fft.ifftshift(
            da.fft.irfftn(da.fft.fftshift(shifted_fourier_image, axes=-2), s=imsize), axes=(-2, -1)
        )

        shifted_should_be = da.zeros(imsize, dtype=float)
        shifted_should_be[..., imsize[-2] // 2 + y0_shift, imsize[-1] // 2 + x0_shift] = 1.0

        unravel_index = da.unravel_index(da.argmax(shifted_image.real), shifted_image.real.shape)
        final_y, final_x = unravel_index[-2].compute(), unravel_index[-1].compute()

        assert final_x == imsize[-2] // 2 + x0_shift
        assert final_y == imsize[-1] // 2 + y0_shift
        np.testing.assert_array_almost_equal(shifted_image, shifted_should_be)
