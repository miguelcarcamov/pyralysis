import astropy.units as un
import numpy as np
import pytest
from astropy.coordinates import CartesianRepresentation, SkyCoord

from pyralysis.utils import (
    calculate_lm,
    calculate_lmn,
    direction_cosines_to_radec,
    radec_to_direction_cosines,
)


def radec_to_direction_cosines_astropy(radec: SkyCoord, phase_centre: SkyCoord):
    """Astropy radec_to_direction_cosines conversion, only useful for testing
    results.

    Parameters
    ----------
    radec : :class:`astropy.coordinates.SkyCoord`
        Sky coordinates
    phase_centre : :class:`astropy.coordinates.SkyCoord`
        Phase Centre

    Returns
    -------
    lmn : :class:`numpy.ndarray`
        lmn coordinates of shape :code:`(3, n_sources)`
    """
    # Transform radec relative to phase centre
    relative = radec.transform_to(phase_centre.skyoffset_frame())
    ret = relative.represent_as(CartesianRepresentation)

    # Rearrange astropy's coordinates into lmn convention
    result = np.empty((3, ret.x.value.shape[0]), dtype=ret.x.value.dtype)
    l = ret.y.value
    m = ret.z.value
    n = np.sqrt(1 - l**2 - m**2)
    result[0, :] = l
    result[1, :] = m
    result[2, :] = n
    return result


class TestCoordinateTransforms:

    def test_transform_radec_zero(self):
        n_sources = np.random.randint(low=1, high=100, size=1, dtype=np.int32)[0]
        ra = np.zeros(n_sources, dtype=np.float32)
        dec = np.zeros(n_sources, dtype=np.float32)

        lm = calculate_lm(ra, dec)
        lmn = calculate_lmn(ra, dec)

        lm_expected = np.zeros((2, n_sources), dtype=np.float32)
        lmn_expected = np.zeros((3, n_sources), dtype=np.float32)
        lmn_expected[2, :] = 1.0

        assert np.array_equal(lm, lmn[0:2, :])
        assert np.array_equal(lm.value, lm_expected)
        assert np.array_equal(lmn.value, lmn_expected)
        assert len(lm_expected) == len(lm)
        assert lm_expected.shape == lm.shape
        assert len(lmn_expected) == len(lmn)
        assert lmn_expected.shape == lmn.shape

    def test_transform_radec_zero_skycoord(self):
        n_sources = np.random.randint(low=1, high=100, size=1, dtype=np.int32)[0]
        ra = np.zeros(n_sources, dtype=np.float32)
        dec = np.zeros(n_sources, dtype=np.float32)

        skycoord = SkyCoord(ra=ra * un.rad, dec=dec * un.rad, frame="icrs")
        phase_center = SkyCoord(ra=0.0 * un.deg, dec=0.0 * un.deg, frame="icrs")

        lmn = radec_to_direction_cosines(skycoord, phase_center)
        lmn_expected = np.zeros((3, n_sources), dtype=np.float32)
        lmn_expected[2, :] = 1.0

        assert np.array_equal(lmn.value, lmn_expected)
        assert len(lmn_expected) == len(lmn)
        assert lmn.shape == lmn_expected.shape

    def test_transform_radec_skycoord_zero_phase_centre(self):
        n_sources = np.random.randint(low=1, high=100, size=1, dtype=np.int32)[0]
        ra = np.random.uniform(size=n_sources, low=0.0, high=2 * np.pi)
        dec = np.random.uniform(size=n_sources, low=-0.5 * np.pi, high=0.5 * np.pi)

        skycoord = SkyCoord(ra=ra * un.rad, dec=dec * un.rad, frame="icrs")
        phase_center = SkyCoord(ra=0.0 * un.rad, dec=0.0 * un.rad, frame="icrs")

        lmn = radec_to_direction_cosines(skycoord, phase_center)
        lmn_expected = radec_to_direction_cosines_astropy(skycoord, phase_center)

        assert np.testing.assert_allclose(lmn.value, lmn_expected, rtol=1e-7) is None
        assert lmn_expected.shape == lmn.shape

    def test_transform_radec_skycoord_random_phase_centre(self):
        n_sources = np.random.randint(low=1, high=100, size=1, dtype=np.int32)[0]
        ra = np.random.uniform(size=n_sources, low=0.0, high=2 * np.pi)
        dec = np.random.uniform(size=n_sources, low=-0.5 * np.pi, high=0.5 * np.pi)

        ra_phase = np.random.uniform(size=1, low=0.0, high=2 * np.pi)
        dec_phase = np.random.uniform(size=1, low=-0.5 * np.pi, high=0.5 * np.pi)

        skycoord = SkyCoord(ra=ra * un.rad, dec=dec * un.rad, frame="icrs")
        phase_center = SkyCoord(ra=ra_phase[0] * un.rad, dec=dec_phase[0] * un.rad, frame="icrs")

        lmn = radec_to_direction_cosines(skycoord, phase_center)
        lmn_expected = radec_to_direction_cosines_astropy(skycoord, phase_center)

        assert np.testing.assert_allclose(lmn.value, lmn_expected, rtol=1e-7) is None
        assert lmn_expected.shape == lmn.shape

    def test_transform_direction_cosines_to_radec_zero_phase_center(self):
        n_sources = np.random.randint(low=1, high=100, size=1, dtype=np.int32)[0]
        lm = np.zeros((2, n_sources), dtype="float")
        n_direction = np.sqrt(1.0 - lm[0]**2 - lm[1]**2)

        phase_center = SkyCoord(ra=0.0 * un.rad, dec=0.0 * un.rad, frame="icrs")
        radec_skycoord = direction_cosines_to_radec(lm, phase_center=phase_center)

        ra_directions = phase_center.ra.rad + np.arctan(
            lm[0] /
            (n_direction * np.cos(phase_center.dec.rad) - lm[1] * np.sin(phase_center.dec.rad))
        )

        dec_directions = np.arcsin(
            lm[1] * np.cos(phase_center.dec.rad) + n_direction * np.sin(phase_center.dec.rad)
        )

        resulting_skycoord = SkyCoord(
            ra=ra_directions * un.rad, dec=dec_directions * un.rad, frame=phase_center.frame
        )

        assert (radec_skycoord == resulting_skycoord).all()
        assert (radec_skycoord.shape == resulting_skycoord.shape)
        assert np.testing.assert_array_equal(
            radec_skycoord.ra.rad, resulting_skycoord.ra.rad
        ) is None
        assert np.testing.assert_array_equal(
            radec_skycoord.dec.rad, resulting_skycoord.dec.rad
        ) is None

    @pytest.mark.filterwarnings("ignore")
    def test_transform_direction_cosines_to_radec_phase_center(self):
        n_sources = np.random.randint(low=1, high=100, size=1, dtype=np.int32)[0]

        lm = np.cos(np.random.uniform(size=(2, n_sources), low=-np.pi, high=np.pi))
        n_direction = np.sqrt(1.0 - lm[0]**2 - lm[1]**2)

        ra_phase = np.random.uniform(size=1, low=0.0, high=2 * np.pi)
        dec_phase = np.random.uniform(size=1, low=-0.5 * np.pi, high=0.5 * np.pi)

        phase_center = SkyCoord(ra=ra_phase[0] * un.rad, dec=dec_phase[0] * un.rad, frame="icrs")
        radec_skycoord = direction_cosines_to_radec(lm, phase_center=phase_center)

        ra_directions = phase_center.ra.rad + np.arctan(
            lm[0] /
            (n_direction * np.cos(phase_center.dec.rad) - lm[1] * np.sin(phase_center.dec.rad))
        )

        dec_directions = np.arcsin(
            lm[1] * np.cos(phase_center.dec.rad) + n_direction * np.sin(phase_center.dec.rad)
        )

        resulting_skycoord = SkyCoord(
            ra=ra_directions * un.rad, dec=dec_directions * un.rad, frame=phase_center.frame
        )

        assert (radec_skycoord.shape == resulting_skycoord.shape)
        assert np.testing.assert_array_equal(
            radec_skycoord.ra.rad, resulting_skycoord.ra.rad
        ) is None
        assert np.testing.assert_array_equal(
            radec_skycoord.dec.rad, resulting_skycoord.dec.rad
        ) is None
