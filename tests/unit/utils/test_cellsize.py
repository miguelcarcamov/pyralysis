import astropy.units as un
import numpy as np
import pytest
from astropy.units import Quantity

from pyralysis.utils.cellsize import cellsize


class TestCellsize:

    @pytest.mark.parametrize(
        "value, r",
        [
            # dimentionless
            (Quantity(1.0), [-1.0, 1.0]),
            (Quantity(-1.0), [-1.0, 1.0]),
            # degree
            (Quantity(1.0, un.deg), [-0.01745329, 0.01745329]),
            (Quantity(-1.0, un.deg), [-0.01745329, 0.01745329]),
            # radians
            (Quantity(1.0, un.rad), [-1.0, 1.0]),
            (Quantity(-1.0, un.rad), [-1.0, 1.0]),
        ]
    )
    def test_quantity_scalar(self, value, r):
        cs = cellsize(value)
        assert type(cs) == Quantity
        assert len(cs) == 2
        assert pytest.approx(cs.value) == r
        assert cs.unit == un.rad

    @pytest.mark.parametrize(
        "value, r",
        [
            # dimentionless
            (Quantity([1.0, 1.0]), [-1.0, 1.0]),
            (Quantity([-1.0, 1.0]), [-1.0, 1.0]),
            (Quantity([1.0, -1.0]), [-1.0, 1.0]),
            (Quantity([-1.0, -1.0]), [-1.0, 1.0]),
            # degree
            (Quantity([1.0, 1.0], un.deg), [-0.01745329, 0.01745329]),
            (Quantity([-1.0, 1.0], un.deg), [-0.01745329, 0.01745329]),
            (Quantity([1.0, -1.0], un.deg), [-0.01745329, 0.01745329]),
            (Quantity([-1.0, -1.0], un.deg), [-0.01745329, 0.01745329]),
            # radians
            (Quantity([1.0, 1.0], un.rad), [-1.0, 1.0]),
            (Quantity([-1.0, 1.0], un.rad), [-1.0, 1.0]),
            (Quantity([1.0, -1.0], un.rad), [-1.0, 1.0]),
            (Quantity([-1.0, -1.0], un.rad), [-1.0, 1.0]),
        ]
    )
    def test_quantity_list(self, value, r):
        cs = cellsize(value)
        assert type(cs) == Quantity
        assert len(cs) == 2
        assert pytest.approx(cs.value) == r
        assert cs.unit == un.rad

    def test_quantity_list_too_long(self):
        value = Quantity([1.0, 1.0, 1.0])
        with pytest.raises(ValueError) as e_info:
            cs = cellsize(value)

    @pytest.mark.parametrize("v", [([1.0]), ([])])
    def test_quantity_list_short(self, v):
        value = Quantity(v)
        with pytest.raises(ValueError) as e_info:
            cs = cellsize(value)

    @pytest.mark.parametrize(
        "value",
        [
            #scalar
            (Quantity(1.0, un.hour)),
            (Quantity(-1.0, un.km)),
            # list
            (Quantity([1.0, 1.0], un.parsec)),
            (Quantity([-1.0, 1.0], un.Hz)),
            (Quantity([1.0, -1.0], un.s)),
            (Quantity([-1.0, -1.0], un.Pa))
        ]
    )
    def test_invalid_quantity(self, value):
        with pytest.raises(TypeError) as e_info:
            cs = cellsize(value)

    @pytest.mark.parametrize(
        "value, r", [
            ([1.0, 1.0], [-1.0, 1.0]),
            ([-1.0, 1.0], [-1.0, 1.0]),
            ([1.0, -1.0], [-1.0, 1.0]),
            ([-1.0, -1.0], [-1.0, 1.0]),
        ]
    )
    def test_list(self, value, r):
        cs = cellsize(value)
        assert type(cs) == Quantity
        assert len(cs) == 2
        assert (cs.value == r).all()
        assert cs.unit == un.rad

    def test_list_too_long(self):
        value = [1.0, 1.0, 1.0]
        with pytest.raises(ValueError) as e_info:
            cs = cellsize(value)

    @pytest.mark.parametrize("value", [([1.0]), ([])])
    def test_list_short(self, value):
        with pytest.raises(Exception) as e_info:
            cs = cellsize(value)

    @pytest.mark.parametrize(
        "v, r", [
            ([1.0, 1.0], [-0.01745329, 0.01745329]),
            ([-1.0, 1.0], [-0.01745329, 0.01745329]),
            ([1.0, -1.0], [-0.01745329, 0.01745329]),
            ([-1.0, -1.0], [-0.01745329, 0.01745329]),
        ]
    )
    def test_list_quantity(self, v, r):
        value_0 = Quantity(v[0]) * un.deg
        value_1 = Quantity(v[1]) * un.deg
        value = [value_0, value_1]
        cs = cellsize(value)
        assert type(cs) == Quantity
        assert len(cs) == 2
        assert pytest.approx(cs.value) == r
        assert cs.unit == un.rad

    @pytest.mark.parametrize(
        "value, r", [
            ((1.0, 1.0), [-1.0, 1.0]),
            ((-1.0, 1.0), [-1.0, 1.0]),
            ((1.0, -1.0), [-1.0, 1.0]),
            ((-1.0, -1.0), [-1.0, 1.0]),
        ]
    )
    def test_tuple(self, value, r):
        cs = cellsize(value)
        assert type(cs) == Quantity
        assert len(cs) == 2
        assert (cs.value == r).all()
        assert cs.unit == un.rad

    def test_tuple_too_long(self):
        value = (1.0, 1.0, 1.0)
        with pytest.raises(ValueError) as e_info:
            cs = cellsize(value)

    @pytest.mark.parametrize("value", [(tuple([1.0])), (tuple([])), (tuple())])
    def test_tuple_short(self, value):
        with pytest.raises(Exception) as e_info:
            cs = cellsize(value)

    @pytest.mark.parametrize(
        "value, r", [
            (np.array([1.0, 1.0]), [-1.0, 1.0]),
            (np.array([-1.0, 1.0]), [-1.0, 1.0]),
            (np.array([1.0, -1.0]), [-1.0, 1.0]),
            (np.array([-1.0, -1.0]), [-1.0, 1.0]),
        ]
    )
    def test_array(self, value, r):
        cs = cellsize(value)
        assert type(cs) == Quantity
        assert len(cs) == 2
        assert (cs.value == r).all()
        assert cs.unit == un.rad

    def test_array_too_long(self):
        value = np.array([1.0, 1.0, 1.0])
        with pytest.raises(ValueError) as e_info:
            cs = cellsize(value)

    @pytest.mark.parametrize("value", [(np.array([1.0])), (np.array([]))])
    def test_array_short(self, value):
        with pytest.raises(Exception) as e_info:
            cs = cellsize(value)

    @pytest.mark.parametrize(
        "v, r", [
            ([1.0, 1.0], [-1.0, 1.0]),
            ([-1.0, 1.0], [-1.0, 1.0]),
            ([1.0, -1.0], [-1.0, 1.0]),
            ([-1.0, -1.0], [-1.0, 1.0]),
        ]
    )
    def test_array_quantity(self, v, r):
        value_0 = Quantity(v[0]) * un.deg
        value_1 = Quantity(v[1]) * un.deg
        list_qty = [value_0, value_1]
        with pytest.raises(TypeError) as e_info:
            # np array can not have quantity as value
            value = np.array(list_qty)

    @pytest.mark.parametrize(
        "v, r", [
            ([1.0, 1.0], [-1.0, 1.0]),
            ([-1.0, 1.0], [-1.0, 1.0]),
            ([1.0, -1.0], [-1.0, 1.0]),
            ([-1.0, -1.0], [-1.0, 1.0]),
        ]
    )
    def test_array_quantity_dimentionless(self, v, r):
        value_0 = Quantity(v[0])
        value_1 = Quantity(v[1])
        list_qty = [value_0, value_1]
        value = np.array(list_qty)
        cs = cellsize(value)
        assert type(cs) == Quantity
        assert len(cs) == 2
        assert (cs.value == r).all()
        # Must be radians because numpy arrays can't contain information with Quantity units
        assert cs.unit == un.rad

    @pytest.mark.parametrize(
        "v, r", [
            ([1.0, 1.0], [-0.01745329, 0.01745329]),
            ([-1.0, 1.0], [-0.01745329, 0.01745329]),
            ([1.0, -1.0], [-0.01745329, 0.01745329]),
            ([-1.0, -1.0], [-0.01745329, 0.01745329]),
        ]
    )
    def test_tuple_quantity(self, v, r):
        value_0 = Quantity(v[0]) * un.deg
        value_1 = Quantity(v[1]) * un.deg
        value = (value_0, value_1)
        cs = cellsize(value)
        assert type(cs) == Quantity
        assert len(cs) == 2
        assert pytest.approx(cs.value) == r
        assert cs.unit == un.rad

    @pytest.mark.parametrize("value, r", [(1.0, [-1.0, 1.0]), (-1.0, [-1.0, 1.0])])
    def test_float(self, value, r):
        cs = cellsize(value)
        assert type(cs) == Quantity
        assert len(cs) == 2
        assert (cs.value == r).all()
        assert cs.unit == un.rad

    def test_property(self):
        value = property()
        cs = cellsize(value)
        assert cs is None

    def test_none(self):
        cs = cellsize(None)
        assert cs is None

    @pytest.mark.parametrize("value", [("test"), ({"0": "0"}), (10 + 10j)])
    def test_invalid(self, value):
        # Invalid input type
        with pytest.raises(TypeError) as e_info:
            cs = cellsize(value)
