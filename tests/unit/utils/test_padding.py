import dask.array as da
import numpy as np
import pytest

from pyralysis.utils.padding import apply_padding, calculate_padding, remove_padding


class TestPaddingFunctions:

    @pytest.mark.parametrize("backend", ["numpy", "dask"])
    @pytest.mark.parametrize(
        "padding, expected_size, input_size",
        [
            # Symmetric padding cases
            (((2, 2), (2, 2)), (10, 12), (14, 16)),
            (((3, 3), (4, 4)), (12, 14), (18, 22)),

            # Asymmetric padding cases
            (((2, 3), (1, 2)), (10, 12), (15, 15)),
            (((4, 2), (3, 1)), (8, 10), (14, 14)),
        ],
    )
    def test_remove_padding(self, backend, padding, expected_size, input_size):
        """Test removing symmetric/asymmetric padding."""
        # Create input data
        data = (
            np.random.random(input_size) if backend == "numpy" else
            da.random.random(input_size, chunks=tuple(x // 2 for x in input_size))
        )

        # Apply padding
        padded_data = (
            np.pad(data, padding, mode="constant")
            if backend == "numpy" else da.pad(data, padding, mode="constant")
        )

        # Remove padding
        result = remove_padding(padded_data, expected_size)

        # Assertions
        assert result.shape == expected_size

    @pytest.mark.parametrize("backend", ["numpy", "dask"])
    def test_remove_padding_without_padding(self, backend):
        """Test removing padding for both NumPy and Dask arrays."""
        if backend == "numpy":
            data = np.random.random((12, 14))
        else:
            data = da.random.random((12, 14), chunks=(6, 7))

        expected_size = (10, 12)
        cropped_data = remove_padding(data, expected_size)

        assert cropped_data.shape[-2:] == expected_size

    @pytest.mark.parametrize("backend", ["numpy", "dask"])
    @pytest.mark.parametrize(
        "padding, input_size",
        [
            # Symmetric padding cases
            (((2, 2), (2, 2)), (10, 12)),  # Proper tuple of tuples
            (((3, 3), (4, 4)), (8, 10)),

            # Asymmetric padding cases
            (((2, 3), (1, 2)), (12, 14)),
            (((4, 1), (3, 1)), (14, 16)),
        ],
    )
    def test_padding_and_removal(self, backend, padding, input_size):
        """Test the end-to-end process of padding and removing padding."""
        # Ensure padding is a tuple of tuples
        if not (
            isinstance(padding, tuple) and len(padding) == 2 and isinstance(padding[0], tuple)
            and isinstance(padding[1], tuple)
        ):
            raise ValueError("Padding must be a tuple of two tuples (y-axis, x-axis).")

        # Create original data
        original_data = (
            np.random.random(input_size) if backend == "numpy" else
            da.random.random(input_size, chunks=tuple(x // 2 for x in input_size))
        )

        # Calculate the padded size
        padded_height = input_size[0] + padding[0][0] + padding[0][1]
        padded_width = input_size[1] + padding[1][0] + padding[1][1]

        # Apply padding
        padded_data = (
            np.pad(original_data, padding, mode="constant")
            if backend == "numpy" else da.pad(original_data, padding, mode="constant")
        )

        # Ensure the padded shape is correct
        assert padded_data.shape == (padded_height, padded_width)

        # Verify the non-padded region matches the original
        start_y, end_y = padding[0][0], padding[0][0] + input_size[0]
        start_x, end_x = padding[1][0], padding[1][0] + input_size[1]

        if backend == "dask":
            np.testing.assert_array_equal(
                padded_data[start_y:end_y, start_x:end_x].compute(), original_data.compute()
            )
        else:
            np.testing.assert_array_equal(padded_data[start_y:end_y, start_x:end_x], original_data)

        # Remove padding
        depadded_data = remove_padding(padded_data, input_size, padding)

        # Validate the resulting shape matches the original
        assert depadded_data.shape == input_size

        # Compare depadded data with the original
        if backend == "dask":
            np.testing.assert_array_equal(depadded_data.compute(), original_data.compute())
        else:
            np.testing.assert_array_equal(depadded_data, original_data)

    def test_remove_padding_error(self):
        """Test removing padding when expected size is larger than the
        input."""
        data = np.random.random((10, 12))
        with pytest.raises(
            ValueError, match="Expected size must be smaller or equal to the input size."
        ):
            remove_padding(data, (15, 18))

    @pytest.mark.parametrize("backend", ["numpy", "dask"])
    def test_calculate_padding(self, backend):
        """Test calculating symmetric/asymmetric padding."""
        data = (
            np.random.random((10, 12))
            if backend == "numpy" else da.random.random((10, 12), chunks=(5, 6))
        )
        expected_size = (16, 18)

        vertical_pad, horizontal_pad, new_chunks = calculate_padding(data, expected_size)

        assert vertical_pad == (3, 3)  # Padding is symmetric on height
        assert horizontal_pad == (3, 3)  # Padding is symmetric on width
        if backend == "dask":
            assert new_chunks is not None
            assert len(new_chunks) == 2

    @pytest.mark.parametrize("backend", ["numpy", "dask"])
    def test_apply_padding(self, backend):
        """Test dynamically applying padding."""
        data = (
            np.random.random((10, 12))
            if backend == "numpy" else da.random.random((10, 12), chunks=(5, 6))
        )
        padding_factor = 1.5
        expected_size = tuple(int(dim * padding_factor) for dim in data.shape)

        padded_data, pad_width, new_chunks = apply_padding(data, padding_factor)

        # Verify the padded size
        assert padded_data.shape[-2:] == expected_size

        # Verify the padding applied
        vertical_pad, horizontal_pad = calculate_padding(data, expected_size)[:2]
        assert pad_width[-2] == vertical_pad
        assert pad_width[-1] == horizontal_pad

        if backend == "dask":
            # Verify that chunks are updated
            assert new_chunks is not None
            assert padded_data.chunks == new_chunks

    @pytest.mark.parametrize("backend", ["numpy", "dask"])
    def test_asymmetric_padding(self, backend):
        """Test cases with asymmetric padding."""
        data = (
            np.random.random((10, 11))
            if backend == "numpy" else da.random.random((10, 11), chunks=(5, 6))
        )
        expected_size = (15, 14)

        vertical_pad, horizontal_pad, _ = calculate_padding(data, expected_size)

        assert vertical_pad == (2, 3)  # Asymmetric padding on height
        assert horizontal_pad == (1, 2)  # Asymmetric padding on width

    def test_apply_padding_numpy(self):
        """Test padding with NumPy array and verify correctness."""
        data = np.random.random((8, 8))
        padding_factor = 2.0

        padded_data, pad_width, _ = apply_padding(data, padding_factor)
        expected_shape = (16, 16)

        assert padded_data.shape[-2:] == expected_shape
        assert pad_width[-2:] == [(4, 4), (4, 4)]  # Validate spatial padding only

    def test_apply_padding_dask(self):
        """Test padding with Dask array and verify correctness."""
        data = da.random.random((8, 8), chunks=(4, 4))
        padding_factor = 2.0

        padded_data, pad_width, new_chunks = apply_padding(data, padding_factor)
        expected_shape = (16, 16)

        assert padded_data.shape[-2:] == expected_shape
        assert pad_width[-2:] == [(4, 4), (4, 4)]  # Validate spatial padding only
        assert new_chunks is not None
        assert padded_data.chunks == new_chunks
