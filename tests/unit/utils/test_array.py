import dask.array as da
import numpy as np
import pytest

from pyralysis.utils.array import ravel_chunks


class TestArrayUtils:

    @pytest.mark.skip(reason="implementation not yet completed")
    @pytest.mark.parametrize(
        "array, chunks", [
            (da.zeros((10, 10), chunks=(5, 5)), ((25, 25, 25, 25), )),
            (da.zeros((10, 10), chunks=(6, 6)), ((36, 24, 24, 16), )),
            (da.zeros((10, 10), chunks=(6, 4)), ((24, 24, 12, 16, 16, 8), )),
        ]
    )
    def test_ravel_chunks(self, array, chunks):
        res = ravel_chunks(array)
        assert res.shape == (array.size, )
        assert res.chunks == chunks
        assert res.compute().shape == (array.size, )
