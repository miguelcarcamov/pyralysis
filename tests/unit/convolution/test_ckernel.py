# tests/unit/convolution/test_ckernel.py

import astropy.units as un
import dask.array as da
import numpy as np
import pytest
from astropy.units import Quantity

# Import the kernel classes; adjust the import path as necessary.
from pyralysis.convolution import (
    PSWF1,
    Bicubic,
    CKernel,
    Gaussian,
    GaussianSinc,
    KaiserBessel,
    Pillbox,
    Spline,
)

############################################
# Dummy subclass for testing the base CKernel functionality.
############################################


class DummyKernel(CKernel):
    """
    A dummy subclass that implements the minimal raw methods so that the base‐class
    methods kernel() and gcf() can be tested.

    For this dummy version:
      - _compute_raw_kernel(imsize) returns an array of ones with shape imsize.
      - _compute_raw_gcf(imsize) returns an array of ones with shape imsize.
    """

    def _compute_raw_kernel(self, imsize):
        return da.from_array(np.ones(imsize, dtype=np.float32), chunks=imsize)

    def _compute_raw_gcf(self, imsize):
        return da.from_array(np.ones(imsize, dtype=np.float32), chunks=imsize)

    def kernel(self, imsize, half=False):
        # Use the base-class implementation.
        return super().kernel(imsize, half=half)

    def gcf(self, imsize):
        raw = self._compute_raw_gcf(imsize)
        return raw / raw.max()


############################################
# Tests for the base CKernel functionality.
############################################


class TestCKernelBase:

    def test_missing_cellsize_raises(self):
        # Missing cellsize now causes a TypeError because it's a required keyword-only argument.
        with pytest.raises(TypeError):
            _ = DummyKernel()  # cellsize is required

    @pytest.mark.parametrize(
        "cell_input, expected",
        [
            (Quantity((1.0, 1.0), un.rad), [-1.0, 1.0]),
            (Quantity((-1.0, 1.0), un.rad), [-1.0, 1.0]),
            (2.0 * un.rad, [-2.0, 2.0]),
            (Quantity((1.0, 2.0), un.rad), [-1.0, 2.0]),
        ],
    )
    def test_init_cellsize(self, cell_input, expected):
        ck = DummyKernel(cellsize=cell_input)
        assert isinstance(ck.cellsize, Quantity)
        np.testing.assert_allclose(ck.cellsize.value, expected)
        assert ck.size == 3
        assert ck.w is None
        assert ck.support == 3 // 2
        uv0 = 1 / (3 * expected[0])
        uv1 = 1 / (3 * expected[1])
        np.testing.assert_allclose(ck.uvcellsize[0].value, uv0)
        np.testing.assert_allclose(ck.uvcellsize[1].value, uv1)
        ri = ck.relative_indexes
        expected_shape = (3, 3)
        np.testing.assert_array_equal(ri[0].shape, expected_shape)
        np.testing.assert_array_equal(ri[1].shape, expected_shape)

    @pytest.mark.parametrize("size, expected_support", [(5, 2), (10, 5), (7, 3)])
    def test_init_size_cellsize(self, size, expected_support):
        ck = DummyKernel(size=size, cellsize=Quantity((1.0, 1.0), un.rad))
        np.testing.assert_allclose(ck.cellsize.value, [-1.0, 1.0])
        assert ck.size == size
        assert ck.w is None
        assert ck.support == expected_support
        uv0 = 1 / (size * -1.0)
        uv1 = 1 / (size * 1.0)
        np.testing.assert_allclose(ck.uvcellsize[0].value, uv0)
        np.testing.assert_allclose(ck.uvcellsize[1].value, uv1)
        ri = ck.relative_indexes
        expected_shape = (expected_support * 2 + 1, expected_support * 2 + 1)
        np.testing.assert_array_equal(ri[0].shape, expected_shape)
        np.testing.assert_array_equal(ri[1].shape, expected_shape)

    def test_kernel_and_gcf_dummy(self):
        imsize = (64, 64)
        ck = DummyKernel(cellsize=Quantity((1.0, 1.0), un.rad))
        kernel = ck.kernel(imsize).compute()
        np.testing.assert_allclose(np.abs(kernel.sum()), 1.0, rtol=1e-5)
        gcf = ck.gcf(imsize).compute()
        np.testing.assert_allclose(np.abs(gcf.max()), 1.0, rtol=1e-5)

    def test_relative_indexes_oversampling(self):
        oversample = 2
        ck = DummyKernel(
            cellsize=Quantity((1.0, 1.0), un.rad), oversampling_factor=oversample, size=3
        )
        ri = ck.relative_indexes
        expected = np.array([-2, 0, 2], dtype=np.int32)
        np.testing.assert_array_equal(ri[0][:, 0], expected)


############################################
# Additional tests for oversampling behavior
############################################


# Test both the convolution kernel and the gridding correction function
# for several oversampling factors.
@pytest.mark.parametrize("os_factor", [1, 2, 3, 4])
@pytest.mark.parametrize(
    "kernel_cls",
    [PSWF1, Bicubic, GaussianSinc, Gaussian, Pillbox, Spline, KaiserBessel, DummyKernel]
)
def test_kernel_normalization_oversampling(kernel_cls, os_factor):
    # Instantiate a kernel with a given oversampling factor and native size 3.
    kernel = kernel_cls(
        cellsize=Quantity((1.0, 1.0), un.rad), oversampling_factor=os_factor, size=3
    )
    # Define an image size corresponding to the oversampled grid.
    imsize = (kernel.size * os_factor, kernel.size * os_factor)
    result = kernel.kernel(imsize).compute()
    if os_factor == 1:
        total_sum = np.abs(result.sum())
        np.testing.assert_allclose(total_sum, 1.0, rtol=1e-4)
    else:
        full_sum = np.abs(result.sum())
        # For oversampled kernels the full grid sum is not necessarily 1.
        # Instead, extract the native grid via downsampling.
        native_kernel = result[os_factor // 2::os_factor, os_factor // 2::os_factor]
        native_sum = np.abs(native_kernel.sum())
        np.testing.assert_allclose(native_sum, 1.0, rtol=1e-4)
        # Also, check that the full oversampled kernel is not normalized.
        assert not np.isclose(full_sum, 1.0, rtol=1e-4)


@pytest.mark.parametrize("os_factor", [1, 2, 3, 4])
@pytest.mark.parametrize(
    "kernel_cls",
    [PSWF1, Bicubic, GaussianSinc, Gaussian, Pillbox, Spline, KaiserBessel, DummyKernel]
)
def test_gcf_normalization_oversampling(kernel_cls, os_factor):
    kernel = kernel_cls(
        cellsize=Quantity((1.0, 1.0), un.rad), oversampling_factor=os_factor, size=3
    )
    imsize = (kernel.size * os_factor, kernel.size * os_factor)
    result = kernel.gcf(imsize).compute()
    max_val = np.abs(result.max())
    np.testing.assert_allclose(max_val, 1.0, rtol=1e-4)


############################################
# Test extraction of the half kernel (only the quadrant with nonnegative offsets)
############################################


@pytest.mark.parametrize("os_factor", [1, 2, 3])
@pytest.mark.parametrize(
    "kernel_cls",
    [PSWF1, Bicubic, GaussianSinc, Gaussian, Pillbox, Spline, KaiserBessel, DummyKernel]
)
def test_kernel_half_extraction(kernel_cls, os_factor):
    kernel = kernel_cls(
        cellsize=Quantity((1.0, 1.0), un.rad), oversampling_factor=os_factor, size=3
    )
    imsize = (kernel.size * os_factor, kernel.size * os_factor)
    full_kernel = kernel.kernel(imsize, half=False).compute()
    half_kernel = kernel.kernel(imsize, half=True).compute()
    center = full_kernel.shape[0] // 2
    expected_half = full_kernel[center:, center:]
    np.testing.assert_allclose(half_kernel, expected_half, rtol=1e-4)


############################################
# Additional tests for delta_values with even and odd sizes
############################################


@pytest.mark.parametrize(
    "imsize, expected",
    [
        ((3, 3), np.array([-1.0, 0.0, 1.0], dtype=np.float32)),
        ((4, 4), np.array([-2.0, -1.0, 0.0, 1.0], dtype=np.float32)),
        ((5, 7), np.arange(-2, 3,
                           dtype=np.float32)),  # for height=5, expected y-values: [-2, -1, 0, 1, 2]
    ]
)
def test_delta_values_even_odd(imsize, expected):
    cell = Quantity((1.0, 1.0), un.rad)
    x_vals, y_vals = CKernel.delta_values(imsize, deltas=cell)
    # Check x-values if the width equals the expected length.
    if imsize[1] == len(expected):
        np.testing.assert_allclose(x_vals, expected, atol=1e-5)
    # Check y-values if the height equals the expected length.
    if imsize[0] == len(expected):
        np.testing.assert_allclose(y_vals, expected, atol=1e-5)


############################################
# Additional test for the scaling parameter w affecting kernel output
############################################


@pytest.mark.parametrize("w_value", [5, 10])
@pytest.mark.parametrize("kernel_cls", [Bicubic, Gaussian, Pillbox, Spline, KaiserBessel])
def test_kernel_w_parameter(kernel_cls, w_value):
    # Skip this test for GaussianSinc because it does not use the w parameter.
    if kernel_cls.__name__ == "GaussianSinc":
        pytest.skip("GaussianSinc does not use the w parameter")
    # Create two kernels with different w values.
    kernel1 = kernel_cls(
        cellsize=Quantity((1.0, 1.0), un.rad), oversampling_factor=2, size=3, w=w_value
    )
    kernel2 = kernel_cls(
        cellsize=Quantity((1.0, 1.0), un.rad), oversampling_factor=2, size=3, w=w_value * 2
    )
    imsize = (
        kernel1.size * kernel1.oversampling_factor, kernel1.size * kernel1.oversampling_factor
    )
    result1 = kernel1.kernel(imsize).compute()
    result2 = kernel2.kernel(imsize).compute()
    # Assert that the two results are not close.
    assert not np.allclose(result1, result2, rtol=1e-4)


if __name__ == "__main__":
    pytest.main([__file__])
