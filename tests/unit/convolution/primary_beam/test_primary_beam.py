import dask.array as da
import numpy as np
import pytest
from astropy import units as u
from astropy.constants import c

from pyralysis.convolution.primary_beam import PrimaryBeam


class TestPrimaryBeam:

    @pytest.fixture
    def primary_beam(self):
        diameter = np.asarray([12., 15., 23., 12.])
        telescope = np.asarray(['ALMA'] * 4)
        beam = PrimaryBeam(dish_diameter=diameter, telescope=telescope)
        return beam

    def test_init(self, primary_beam):
        assert pytest.approx(primary_beam.amplitude) == 1.0
        assert pytest.approx(primary_beam.dish_diameter) == [12., 15., 23., 12.]
        assert (primary_beam.telescope == np.array(['ALMA', 'ALMA', 'ALMA', 'ALMA'])).all()
        assert pytest.approx(primary_beam.cellsize.value) == [-1.0, 1.0]
        assert primary_beam.cellsize.unit == u.rad

    def test_beam_range(self, primary_beam):
        beam = primary_beam.beam(frequency=[c.value], imsize=[512, 512])
        assert 0 <= beam.min()
        assert 1 >= beam.max()

    @pytest.mark.parametrize("imsize", [([128, 128])])
    def test_image_size_shape(self, primary_beam, imsize):
        beam = primary_beam.beam(frequency=[c.value], imsize=imsize)
        assert beam.shape == (4, 1, imsize[0], imsize[1])
        assert beam.dtype == np.float32

    @pytest.mark.parametrize(
        "imsize, imcenter, chunks, expected0", [
            (
                [3, 3], [1, 1], "auto",
                np.array(
                    [
                        [2.1006019e-05, 9.2684222e-06, 2.1006019e-05],
                        [9.2684222e-06, 1.0000000e+00, 9.2684222e-06],
                        [2.1006019e-05, 9.2684222e-06, 2.1006019e-05]
                    ]
                )
            ),
            (
                [3, 3], [1, 1], (2, 2),
                np.array(
                    [
                        [2.1006019e-05, 9.2684222e-06, 2.1006019e-05],
                        [9.2684222e-06, 1.0000000e+00, 9.2684222e-06],
                        [2.1006019e-05, 9.2684222e-06, 2.1006019e-05]
                    ]
                )
            ),
            (
                [3, 3], [0, 0], "auto",
                np.array(
                    [
                        [1.0000000e+00, 9.2684222e-06, 1.2011693e-08],
                        [9.2684222e-06, 2.1006019e-05, 4.7151370e-06],
                        [1.2011693e-08, 4.7151370e-06, 8.8886247e-07]
                    ]
                )
            ),
            (
                [3, 3], [0, 0], (2, 2),
                np.array(
                    [
                        [1.0000000e+00, 9.2684222e-06, 1.2011693e-08],
                        [9.2684222e-06, 2.1006019e-05, 4.7151370e-06],
                        [1.2011693e-08, 4.7151370e-06, 8.8886247e-07]
                    ]
                )
            )
        ]
    )
    def test_image_center(self, primary_beam, imsize, imcenter, chunks, expected0):
        # WITH ALMA THE PB IS AN AIRYDISK
        beam = primary_beam.beam(
            frequency=[c.value], imsize=imsize, imcenter=imcenter, imchunks=chunks
        )
        assert beam.shape == (4, 1, imsize[0], imsize[1])
        assert (beam[:, :, imcenter[0], imcenter[1]][:, :, None, None] >= beam).all()
        assert pytest.approx(beam[:, :, imcenter[0], imcenter[1]]) == primary_beam.amplitude
        assert pytest.approx(beam[0, 0, :, :], rel=1e-2) == expected0  #TODO FIX
        assert beam.dtype == np.float32

    @pytest.mark.parametrize(
        "frequency, chunks, expected0", [
            (
                np.array([c.value]), "auto",
                np.array(
                    [
                        [
                            [2.1006019e-05, 9.2684222e-06, 2.1006019e-05],
                            [9.2684222e-06, 1.0000000e+00, 9.2684222e-06],
                            [2.1006019e-05, 9.2684222e-06, 2.1006019e-05]
                        ]
                    ]
                )
            ),
            (
                np.array([c.value]), (2, 2),
                np.array(
                    [
                        [
                            [2.1006019e-05, 9.2684222e-06, 2.1006019e-05],
                            [9.2684222e-06, 1.0000000e+00, 9.2684222e-06],
                            [2.1006019e-05, 9.2684222e-06, 2.1006019e-05]
                        ]
                    ]
                )
            ),
            (
                np.array([1e9, 1e11]), "auto",
                np.array(
                    [
                        [
                            [1.9100406e-07, 3.9144044e-07, 1.9100406e-07],
                            [3.9144044e-07, 1.0000000e+00, 3.9144044e-07],
                            [1.9100406e-07, 3.9144044e-07, 1.9100406e-07]
                        ],
                        [
                            [4.3972334e-13, 7.3617029e-13, 4.3972334e-13],
                            [7.3617029e-13, 1.0000000e+00, 7.3617029e-13],
                            [4.3972334e-13, 7.3617029e-13, 4.3972334e-13]
                        ],
                    ]
                )
            ),
            (
                np.array([1e9, 1e11]), (1, 1),
                np.array(
                    [
                        [
                            [1.9100406e-07, 3.9144044e-07, 1.9100406e-07],
                            [3.9144044e-07, 1.0000000e+00, 3.9144044e-07],
                            [1.9100406e-07, 3.9144044e-07, 1.9100406e-07]
                        ],
                        [
                            [4.3972334e-13, 7.3617029e-13, 4.3972334e-13],
                            [7.3617029e-13, 1.0000000e+00, 7.3617029e-13],
                            [4.3972334e-13, 7.3617029e-13, 4.3972334e-13]
                        ],
                    ]
                )
            )
        ]
    )
    def test_frequency(self, primary_beam, frequency, chunks, expected0):
        # WITH ALMA THE PB IS AN AIRYDISK
        beam = primary_beam.beam(
            frequency=frequency, imsize=[3, 3], imcenter=[1, 1], imchunks=chunks
        )
        assert beam.shape == (4, len(frequency), 3, 3)
        assert beam.dtype == np.float32
        assert pytest.approx(beam[0, :, :, :], rel=1e-2) == expected0  #TODO FIX

    @pytest.mark.parametrize(
        "new_cellsize, cellsize_expected", [
            (1e-4, [-1e-4, 1e-4]),
            ([1e-4, 1e-4], [-1e-4, 1e-4]),
            ((1e-4, 1e-4), [-1e-4, 1e-4]),
            (1e-4 * u.arcsec, [-4.84813681e-10, 4.84813681e-10]),
            ([1e-4 * u.arcsec, 1e-4 * u.arcsec], [-4.84813681e-10, 4.84813681e-10]),
            ([1e-4, 1e-4] * u.arcsec, [-4.84813681e-10, 4.84813681e-10]),
        ]
    )
    def test_update_cellsize(self, primary_beam, new_cellsize, cellsize_expected):
        assert pytest.approx(primary_beam.cellsize.value) == [-1.0, 1.0]
        beam = primary_beam.beam(frequency=[1e8, 1.5e8, 1e9], imsize=[10, 10], imcenter=[5, 5])
        primary_beam.cellsize = new_cellsize
        assert pytest.approx(primary_beam.cellsize.value) == cellsize_expected
        newbeam = primary_beam.beam(frequency=[1e8, 1.5e8, 1e9], imsize=[10, 10], imcenter=[5, 5])
        assert (newbeam >= beam).compute().all()  # Comparison handled by dask

    @pytest.mark.parametrize("amplitude", [(1.0), (2.0), (1.333), (7.0)])
    def test_amplitude(self, amplitude):
        primary_beam = PrimaryBeam(
            amplitude=amplitude, dish_diameter=np.array([10., 12.]), telescope=['ALMA', 'ALMA']
        )
        beam = primary_beam.beam(frequency=[1e9, 1e8], imsize=[3, 3], imcenter=[1, 1])
        assert beam.dtype == np.float32
        assert beam.shape == (2, 2, 3, 3)
        assert beam.min() >= 0
        assert beam.max() == pytest.approx(amplitude)
        assert (beam[:, :, 1, 1] == amplitude).compute().all()

    def test_telescope(self):
        primary_beam = PrimaryBeam(
            dish_diameter=np.array([10., 12., 10., 12.]), telescope=['ALMA', 'ALMA', 'VLA', 'VLA']
        )
        beam = primary_beam.beam(frequency=[c.value], imsize=[5, 5])
        assert beam.shape == (4, 1, 5, 5)
        assert beam.dtype == np.float32
        assert (beam[0, 0, :, :] >= beam[2, 0, :, :]).compute().all()
        assert (beam[1, 0, :, :] >= beam[3, 0, :, :]).compute().all()
        # Beam for dish 2 and 3 is gaussian and is equal to 0 away from the center
        assert pytest.approx(beam[2:, 0, 0, :]) == 0.0
        assert pytest.approx(beam[2:, 0, -1, :]) == 0.0
        assert pytest.approx(beam[2:, 0, :, 0]) == 0.0
        assert pytest.approx(beam[2:, 0, :, -1]) == 0.0

    def test_telescope_str(self):
        primary_beam = PrimaryBeam(dish_diameter=np.array([10., 12., 10., 12.]), telescope='ALMA')
        beam = primary_beam.beam(frequency=[c.value], imsize=[5, 5])
        assert beam.shape == (4, 1, 5, 5)
        assert beam.dtype == np.float32
        assert (beam[0, 0, :, :] == beam[2, 0, :, :]).compute().all()
        assert (beam[1, 0, :, :] == beam[3, 0, :, :]).compute().all()

    @pytest.mark.parametrize("antenna", [np.array([0, 0, 1]), da.array([0, 0, 1])])
    def test_antenna_array(self, primary_beam, antenna):
        beam = primary_beam.beam(frequency=[c.value], imsize=[512, 512], antenna=antenna)
        assert beam.shape == (3, 1, 512, 512)
        assert beam.dtype == np.float32
        assert (beam[0] == beam[1]).all()
        assert (beam[0] != beam[2]).any()

    def test_antenna_none(self, primary_beam):
        beam = primary_beam.beam(frequency=[c.value], imsize=[512, 512])
        assert beam.shape == (4, 1, 512, 512)
        assert beam.dtype == np.float32
        assert (beam[0] != beam[1]).any()
        assert (beam[0] != beam[2]).any()
        assert (beam[0] == beam[3]).all()

    @pytest.mark.parametrize(
        "chunks, cs", [
            (32, [32, 32]),
            (tuple([32, 32]), [32, 32]),
            (tuple([32, 16]), [32, 16]),
            (tuple([16, 32]), [16, 32]),
            ({
                0: 16,
                1: 16
            }, [16, 16]),
            ({
                0: 16,
                1: 32
            }, [16, 32]),
        ]
    )
    def test_chunksize(self, primary_beam, chunks, cs):
        beam = primary_beam.beam(frequency=[c.value], imsize=[64, 64], imchunks=chunks)
        assert beam.shape == (4, 1, 64, 64)
        assert beam.dtype == np.float32
        assert beam.chunksize == (4, 1, cs[0], cs[1])
