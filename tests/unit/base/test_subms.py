import logging

from pyralysis.base.subms import SubMS
from pyralysis.base.visibility_set import VisibilitySet


class TestSubMS:
    empty_subms = SubMS()

    def test_logger(self):
        assert self.empty_subms.logger is not None

    def test_logger_info_level(self):
        assert self.empty_subms.logger.level is logging.INFO

    def check_subms_parameters(self):
        test_id = 1
        test_field_id = 2
        test_polarization_id = 3
        test_spw_id = 4
        test_vis = VisibilitySet(dataset=None)

        initialized_subms = SubMS(
            _id=test_id,
            field_id=test_field_id,
            polarization_id=test_polarization_id,
            spw_id=test_spw_id,
            visibilities=test_vis
        )

        assert self.empty_subms._id != initialized_subms._id
        assert self.empty_subms.field_id != initialized_subms.field_id
        assert self.empty_subms.polarization_id != initialized_subms.polarization_id
        assert self.empty_subms.spw_id != initialized_subms.spw_id
        assert self.empty_subms.visibilities != initialized_subms.visibilities
