import logging

import pytest

from pyralysis.base.antenna import Antenna


class TestAntenna:
    antenna = Antenna(dataset=None)

    def test_init_with_empty_dataset(self):
        assert self.antenna.dataset is None

    @pytest.mark.parametrize('sample_dataset', ['ANTENNA'], indirect=True)
    def test_init_antenna_with_dataset(self, sample_dataset):
        self.antenna.dataset = sample_dataset
        assert self.antenna.dataset is not None

    def test_max_diameter_empty_dataset(self):
        assert self.antenna.max_diameter == 0.0

    @pytest.mark.parametrize('sample_dataset', ['ANTENNA'], indirect=True)
    def test_max_diameter_with_dataset(self, sample_dataset):
        antenna2 = Antenna(dataset=sample_dataset)

        if antenna2.dataset.DISH_DIAMETER.values.sum() > 0:
            assert self.antenna.max_diameter != antenna2.max_diameter
        else:
            assert self.antenna.max_diameter == antenna2.max_diameter

    def test_logger(self):
        assert self.antenna.logger is not None

    def test_logger_info_level(self):
        assert self.antenna.logger.level is logging.INFO
