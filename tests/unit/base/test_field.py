from unittest.mock import MagicMock

import astropy.units as u
import dask.array as da
import numpy as np
import pytest
import xarray as xr
from astropy.coordinates import SkyCoord
from astropy.time import Time

from pyralysis.base import Field


def create_mock_dataset(n_rows=10):
    """
    Create a mock xarray.Dataset to simulate a Measurement Set field table.
    The dataset will include the variables TIME, REFERENCE_DIR, PHASE_DIR,
    RefDir_Ref, and PhaseDir_Ref.
    """
    # Create a dask array for TIME with values between 59000 and 59100 (MJD).
    time_data = da.linspace(59000, 59100, n_rows, chunks=n_rows)
    # Create random direction arrays for REFERENCE_DIR and PHASE_DIR with shape (n_rows, 1, 2)
    ref_dir = da.random.uniform(-1, 1, (n_rows, 1, 2), chunks=(n_rows, 1, 2))
    phase_dir = da.random.uniform(-1, 1, (n_rows, 1, 2), chunks=(n_rows, 1, 2))
    # For MS v3, include RefDir_Ref and PhaseDir_Ref as integer arrays.
    ref_dir_ref = da.random.randint(0, 3, (n_rows, ), chunks=n_rows)
    phase_dir_ref = da.random.randint(0, 3, (n_rows, ), chunks=n_rows)
    ds = xr.Dataset(
        {
            "TIME": ("row", time_data),
            "REFERENCE_DIR": (["row", "field-poly", "field-dir"], ref_dir),
            "PHASE_DIR": (["row", "field-poly", "field-dir"], phase_dir),
            "RefDir_Ref": ("row", ref_dir_ref),
            "PhaseDir_Ref": ("row", phase_dir_ref),
        }
    )
    return ds


@pytest.fixture
def ms_v3_field():
    """
    Fixture for a Field instance with MS v3 keywords.
    The dataset is a single xarray.Dataset.
    """
    ds = create_mock_dataset(n_rows=10)
    # For MS v3, the column keywords include TabRefTypes and TabRefCodes.
    col_keywords = {
        "TIME": {
            "QuantumUnits": ["d"],
            "MEASINFO": {
                "Ref": "UTC"
            },
        },
        "REFERENCE_DIR": {
            "QuantumUnits": ["rad", "rad"],
            "MEASINFO": {
                "Ref": "J2000",
                # Cycle through 0, 1, 2 for each of the 10 rows.
                "TabRefTypes": ["J2000"] * 10,
                "TabRefCodes": list(np.arange(10) % 3),
            },
        },
        "PHASE_DIR": {
            "QuantumUnits": ["rad", "rad"],
            "MEASINFO": {
                "Ref": "J2000",
                "TabRefTypes": ["J2000"] * 10,
                "TabRefCodes": list(np.arange(10) % 3),
            },
        },
    }
    return Field(dataset=ds, table_keywords={}, column_keywords=col_keywords)


@pytest.fixture
def non_ms_v3_field():
    """
    Fixture for a Field instance with non-MS v3 keywords.
    In this case the column keywords do not include TabRefTypes/TabRefCodes.
    """
    ds = create_mock_dataset(n_rows=10)
    col_keywords = {
        "TIME": {
            "QuantumUnits": ["d"],
            "MEASINFO": {
                "Ref": "UTC"
            },
        },
        "REFERENCE_DIR": {
            "QuantumUnits": ["rad", "rad"],
            "MEASINFO": {
                "Ref": "J2000"
            },
        },
        "PHASE_DIR": {
            "QuantumUnits": ["rad", "rad"],
            "MEASINFO": {
                "Ref": "J2000"
            },
        },
    }
    return Field(dataset=ds, table_keywords={}, column_keywords=col_keywords)


class TestFieldInitialization:

    def test_ms_v3_field_initialization(self, ms_v3_field):
        """
        Test that a Field with MS v3 keywords is initialized properly.
        Verify that is_ms_v3 is True and that coordinate properties are set.
        """
        assert ms_v3_field.is_ms_v3 is True
        assert isinstance(ms_v3_field.ref_dirs, SkyCoord)
        assert isinstance(ms_v3_field.phase_dirs, SkyCoord)
        assert isinstance(ms_v3_field.center_ref_dir, SkyCoord)
        assert isinstance(ms_v3_field.center_phase_dir, SkyCoord)

    def test_non_ms_v3_field_initialization(self, non_ms_v3_field):
        """
        Test that a Field with non-MS v3 keywords is initialized properly.
        Verify that is_ms_v3 is False and that coordinate properties are set.
        """
        assert non_ms_v3_field.is_ms_v3 is False
        assert isinstance(non_ms_v3_field.ref_dirs, SkyCoord)
        assert isinstance(non_ms_v3_field.phase_dirs, SkyCoord)
        assert isinstance(non_ms_v3_field.center_ref_dir, SkyCoord)
        assert isinstance(non_ms_v3_field.center_phase_dir, SkyCoord)


class TestFieldProperties:

    def test_set_ref_dirs(self, ms_v3_field):
        """
        Test that setting ref_dirs updates center_ref_dir correctly.
        """
        coords = SkyCoord(
            ra=np.linspace(10, 20, 10) * u.deg,
            dec=np.linspace(0, 10, 10) * u.deg,
            frame="icrs",
            obstime=Time(59000, format="mjd", scale="utc"),
        )
        ms_v3_field.ref_dirs = coords
        center_ref = ms_v3_field.center_ref_dir
        np.testing.assert_allclose(center_ref.ra.deg, np.mean(coords.ra.deg))
        np.testing.assert_allclose(center_ref.dec.deg, np.mean(coords.dec.deg))

    def test_set_phase_dirs(self, ms_v3_field):
        """
        Test that setting phase_dirs updates center_phase_dir correctly.
        """
        coords = SkyCoord(
            ra=np.linspace(30, 40, 10) * u.deg,
            dec=np.linspace(10, 20, 10) * u.deg,
            frame="icrs",
            obstime=Time(59000, format="mjd", scale="utc"),
        )
        ms_v3_field.phase_dirs = coords
        center_phase = ms_v3_field.center_phase_dir
        np.testing.assert_allclose(center_phase.ra.deg, np.mean(coords.ra.deg))
        np.testing.assert_allclose(center_phase.dec.deg, np.mean(coords.dec.deg))


class TestFieldMethods:

    def test_is_measurement_set_v3(self, ms_v3_field, non_ms_v3_field):
        """
        Test that __is_measurement_set_v3 returns True for MS v3 and False for non-MS v3.
        """
        assert ms_v3_field._Field__is_measurement_set_v3() is True
        assert non_ms_v3_field._Field__is_measurement_set_v3() is False

    def test_get_mean_direction(self):
        """
        Test the __get_mean_direction static method.
        We now provide an obstime so that the input SkyCoord has an obstime attribute.
        """
        coords = SkyCoord(
            ra=np.array([10, 20, 30]) * u.deg,
            dec=np.array([0, 10, 20]) * u.deg,
            frame="icrs",
            obstime=Time([59000, 59000, 59000], format="mjd", scale="utc"),
        )
        mean_dir = Field._Field__get_mean_direction(coords)
        np.testing.assert_allclose(mean_dir.ra.deg, 20.0)
        np.testing.assert_allclose(mean_dir.dec.deg, 10.0)

    def test_get_sky_coords_from_coordinates(self):
        """
        Test the __get_sky_coords_from_coordinates static method.
        We supply a simple dask array and a frame string.
        """
        n = 5
        # Create an array with linearly increasing values.
        arr = np.linspace(0, 1, n * 2).reshape(n, 2)
        darr = da.from_array(arr, chunks=n)
        # Use a frame string (for non-MS v3, the method expects a string).
        d_type = "J2000"
        times = Time(np.linspace(59000, 59010, n), format="mjd", scale="utc")
        coords = Field._Field__get_sky_coords_from_coordinates(darr, d_type, ["rad", "rad"], times)
        # Since __get_sky_coords_from_coordinates calls .icrs at the end, the result should be ICRS.
        assert coords.frame.name.lower() == "icrs"

    def test_get_sky_coords_from_coordinates_v3(self):
        """
        Test the __get_sky_coords_from_coordinates_v3 static method.
        Provide dummy dask arrays for directions and directions_type.
        """
        n = 5
        # Create dummy direction values.
        arr = np.linspace(0, 1, n * 2).reshape(n, 2)
        darr = da.from_array(arr, chunks=n)
        # Create dummy direction type array that cycles through 0, 1, 2.
        d_type = da.from_array(np.array([0, 1, 2, 0, 1]), chunks=n)
        # Provide type_strings and type_ids (make sure type_ids cover the values in d_type).
        type_strings = ["J2000", "ICRS", "ICRS"]
        type_ids = [0, 1, 2]
        times = Time(np.linspace(59000, 59010, n), format="mjd", scale="utc")
        coords = Field._Field__get_sky_coords_from_coordinates_v3(
            darr, d_type, ["rad", "rad"], type_strings, type_ids, times
        )
        # Check that the returned SkyCoord is in ICRS.
        assert coords.frame.name.lower() == "icrs"
