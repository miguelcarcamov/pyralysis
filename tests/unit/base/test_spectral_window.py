import logging

import astropy.constants as co
import astropy.units as u
import dask.array as da
from astropy.constants import c
from daskms import xds_from_table

from pyralysis.base.spectral_window import SpectralWindow


class TestSpectralWindow:
    co65_spw = xds_from_table("./datasets/co65/co65.ms" + "::SPECTRAL_WINDOW")

    spec_win = SpectralWindow(dataset=None)

    def test_init_with_empty_dataset(self):
        assert self.spec_win is not None

    def test_logger(self):
        assert self.spec_win.logger is not None

    def test_logger_info_level(self):
        assert self.spec_win.logger.level is logging.INFO

    def test_ndataset(self):
        assert self.spec_win.ndatasets == 0

        sw = SpectralWindow(dataset=self.co65_spw)
        if sw.dataset is not None:
            assert sw.ndatasets == len(self.co65_spw)

    def test_max_nu(self):
        sw = SpectralWindow(dataset=self.co65_spw)

        test_max_freqs = da.max(self.co65_spw[0].CHAN_FREQ.data)
        test_max_nu = da.max(da.array(test_max_freqs)).compute() * u.Hz

        assert sw.max_nu == test_max_nu

    def test_lambda_min(self):
        sw = SpectralWindow(dataset=self.co65_spw)

        test_max_freqs = da.max(self.co65_spw[0].CHAN_FREQ.data)
        test_max_nu = da.max(da.array(test_max_freqs)).compute() * u.Hz

        test_lambda_min = c / test_max_nu
        test_lambda_min.to(u.m)
        assert sw.lambda_min == test_lambda_min

    def test_equivalencies(self):

        sw = SpectralWindow(dataset=self.co65_spw)

        assert sw.equivalencies is not None
        assert len(sw.equivalencies) == 1

        cf = self.co65_spw[0].CHAN_FREQ.data.squeeze(axis=0)[None, :, None]
        test_cf = cf.compute() * u.Hz
        test_cf_m = test_cf.to(u.m, u.spectral())

        s = 1 * u.s
        m = 1 * u.m
        r = 2 * u.rad
        o = 1 * u.one
        l = 3 * u.lambdas

        assert l.to(u.s, equivalencies=sw.equivalencies[0]) == (3 / test_cf.value) * u.s
        assert s.to(u.lambdas, equivalencies=sw.equivalencies[0]) == (1 * test_cf.value) * u.lambdas
        assert l.to(u.m, equivalencies=sw.equivalencies[0]) == (3 * test_cf_m.value) * u.m
        assert m.to(u.lambdas,
                    equivalencies=sw.equivalencies[0]) == (1 / test_cf_m.value) * u.lambdas
        assert (o / l).to(u.rad, equivalencies=sw.equivalencies[0]) == (1 / 3) * u.rad
        assert r.to(u.one / u.lambdas, equivalencies=sw.equivalencies[0]) == 2 * u.one / u.lambdas
        assert l.to(u.one / u.rad, equivalencies=sw.equivalencies[0]) == 3 * u.one / u.rad
        assert (o / r).to(u.lambdas, equivalencies=sw.equivalencies[0]) == (1 / 2) * u.lambdas
        assert l.to(u.rad, equivalencies=sw.equivalencies[0]) == (1 / 3) * u.rad
        assert r.to(u.lambdas, equivalencies=sw.equivalencies[0]) == (1 / 2) * u.lambdas
