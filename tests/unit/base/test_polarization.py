import logging

from daskms import xds_from_table

from pyralysis.base.polarization import Polarization


class TestPolarization:
    co65_pol = xds_from_table("./datasets/co65/co65.ms" + "::POLARIZATION")
    polarization = Polarization(dataset=None)

    def test_init_with_empty_dataset(self):
        assert self.polarization is not None

    def test_logger(self):
        assert self.polarization.logger is not None

    def test_logger_info_level(self):
        assert self.polarization.logger.level is logging.INFO

    def test_ndataset(self):
        pol = Polarization(dataset=None)
        assert pol.ndatasets == 0

        pol = Polarization(dataset=self.co65_pol)
        if pol.dataset is not None:
            assert pol.ndatasets == len(self.co65_pol)
