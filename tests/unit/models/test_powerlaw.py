import dask.array as da
import numpy as np
import pytest

from pyralysis.models.power_law import PowerLawIntensityModel


# Mock frequencies for testing (as dask.linspace)
def create_mock_frequencies(start=1e9, stop=2e9, num=100):
    return da.linspace(start, stop, num, chunks=num // 10)


# Mock images for different dimensions
def create_mock_image(dim, shape):
    if dim == 1:
        return da.random.uniform(size=shape, chunks=shape)
    elif dim in [2, 3, 4]:
        return da.random.uniform(size=shape, chunks=tuple(s // 2 for s in shape))
    else:
        raise ValueError("Unsupported dimension.")


# Fixtures
@pytest.fixture
def frequencies():
    """Create a mock dask array of frequencies."""
    return create_mock_frequencies()


@pytest.fixture(params=[(1, (3, )), (2, (256, 256)), (3, (3, 256, 256)), (4, (4, 3, 256, 256))])
def image_and_dim(request):
    """Create mock images with varying dimensions."""
    dim, shape = request.param
    return dim, create_mock_image(dim, shape)


@pytest.fixture
def powerlaw_model():
    """Create a PowerLawIntensityModel instance."""
    return PowerLawIntensityModel(reference_frequency=1.5e9)


# Tests
class TestPowerLawIntensityModel:

    def test_apply_model(self, powerlaw_model, image_and_dim, frequencies):
        """Test the apply_model method with varying image dimensions."""
        dim, image = image_and_dim

        result = powerlaw_model.apply_model(image, frequencies)

        # Check that the result has the same shape as the input frequencies
        if dim < 4:
            assert result.shape[0] == frequencies.shape[0]
        else:
            assert result.shape[1] == frequencies.shape[0]

        # For 2D, the result should match the input image's dimensions after the frequency axis
        if dim == 2:
            assert result.shape[1:] == image.shape

        # For 3D or 4D, verify the shape includes image dimensions
        elif dim == 3:
            assert result.shape[1:] == image.shape[1:]

        elif dim == 4:
            assert result.shape[2:] == image.shape[2:]

    def test_invalid_image_type(self, powerlaw_model, frequencies):
        """Test apply_model raises an error for invalid image type."""
        with pytest.raises(TypeError):
            powerlaw_model.apply_model("invalid_image", frequencies)

    def test_extract_1d_parameters(self, powerlaw_model):
        """Test 1D parameter extraction."""
        image = da.array([1.0, -0.7, 0.2])
        I_nu_0, alpha, beta = powerlaw_model._extract_1d_parameters(image)

        assert I_nu_0 == 1.0
        assert alpha == -0.7
        assert beta == 0.2

    def test_extract_2d_parameters(self, powerlaw_model):
        """Test 2D parameter extraction."""
        image = da.random.uniform(size=(256, 256))
        I_nu_0, alpha, beta = powerlaw_model._extract_2d_parameters(image)

        assert I_nu_0.shape == image.shape
        assert alpha == 0
        assert beta == 0

    def test_extract_3d_parameters(self, powerlaw_model):
        """Test 3D parameter extraction."""
        image = da.random.uniform(size=(3, 256, 256))
        I_nu_0, alpha, beta = powerlaw_model._extract_3d_parameters(image)

        assert I_nu_0.shape == image.shape[1:]
        assert alpha.shape == image.shape[1:]
        assert beta.shape == image.shape[1:]

    def test_1d_image(self, powerlaw_model, frequencies):
        """Test power-law model with 1D image input."""
        I_nu_0 = 10.0
        alpha = -0.7
        beta = 0.1
        image = da.array([I_nu_0, alpha, beta])

        expected_result = I_nu_0 * (frequencies / powerlaw_model.reference_frequency)**(
            alpha + beta * da.log10(frequencies / powerlaw_model.reference_frequency)
        )
        result = powerlaw_model.apply_model(image, frequencies)
        da.assert_eq(result, expected_result)

    def test_2d_image(self, powerlaw_model, frequencies):
        """Test power-law model with 2D image input."""
        I_nu_0 = da.ones((5, 5), chunks=(5, 5)) * 10.0
        alpha = -0.7
        beta = 0.1
        image = I_nu_0  # For 2D, only I_nu_0 is present, alpha and beta are scalar
        powerlaw_model.alpha = alpha
        powerlaw_model.beta = beta

        expected_result = I_nu_0 * (
            frequencies[:, None, None] / powerlaw_model.reference_frequency
        )**(
            alpha +
            beta * da.log10(frequencies[:, None, None] / powerlaw_model.reference_frequency)
        )
        result = powerlaw_model.apply_model(image, frequencies)
        da.assert_eq(result, expected_result)

    def test_3d_image(self, powerlaw_model, frequencies):
        """Test power-law model with 3D image input."""
        I_nu_0 = da.ones((3, 5, 5), chunks=(3, 5, 5))
        I_nu_0[0, ...] = 10.0
        I_nu_0[1, ...] = -0.7
        I_nu_0[2, ...] = 0.1
        image = I_nu_0

        expected_result = I_nu_0[
            0, ...] * (frequencies[:, None, None] / powerlaw_model.reference_frequency)**(
                I_nu_0[1, ...] + I_nu_0[2, ...] *
                da.log10(frequencies[:, None, None] / powerlaw_model.reference_frequency)
            )
        result = powerlaw_model.apply_model(image, frequencies)
        da.assert_eq(result, expected_result)

    #def test_extract_4d_parameters(self, powerlaw_model):
    #    """Test 4D parameter extraction."""
    #    image = da.random.uniform(size=(4, 3, 256, 256))
    #    I_nu_0, alpha, beta = powerlaw_model._extract_4d_parameters(image)
    #
    #    assert I_nu_0.shape == image.shape[2:]
    #    assert alpha.shape == image.shape[2:]
    #    assert beta.shape == image.shape[2:]
