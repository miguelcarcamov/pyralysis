import astropy.units as un
import numpy as np
import pytest
from astropy.units import Quantity

from pyralysis.transformers import Gridder


class TestGridder:

    @pytest.fixture
    def gridder(self):
        _gridder = Gridder(imsize=256, cellsize=1.0)
        return _gridder

    def test_init(self, gridder):
        assert isinstance(gridder, Gridder)
        assert gridder.image == None
        assert gridder.imsize == [256, 256]
        assert (gridder.cellsize == Quantity([-1., 1.], un.rad)).all()
        assert gridder.padding_factor == 1.2
        assert gridder.ckernel_object == None
        assert not gridder.hermitian_symmetry
        assert gridder._grid_size == [256, 256]
        assert gridder._padded_grid_size == [307, 307]
        assert gridder._padded_imsize == [307, 307]
        assert (
            gridder._uvcellsize ==
            Quantity([-0.003257328990228013, 0.003257328990228013], un.lambdas)
        ).all()

    def test_update_padding_factor(self, gridder):
        gridder.padding_factor = 1.4
        assert gridder.padding_factor == 1.4
        assert gridder._grid_size == [256, 256]
        assert gridder._padded_grid_size == [358, 358]
        assert gridder._padded_imsize == [358, 358]
        assert gridder._uvcellsize is not None

    @pytest.mark.parametrize(
        "imsize, hermitian_symmetry, expected, grid_size, padded_grid_size, padded_imsize", [
            (200, False, [200, 200], [200, 200], [240, 240], [240, 240]),
            ([200, 200], False, [200, 200], [200, 200], [240, 240], [240, 240]),
            ([200, 210], False, [200, 210], [200, 210], [240, 252], [240, 252]),
            (200, True, [200, 200], [200, 101], [240, 121], [240, 240]),
            ([200, 200], True, [200, 200], [200, 101], [240, 121], [240, 240]),
            ([200, 210], True, [200, 210], [200, 106], [240, 127], [240, 252]),
        ]
    )
    def test_init_imsize(
        self, imsize, hermitian_symmetry, expected, grid_size, padded_grid_size, padded_imsize
    ):
        gridder = Gridder(imsize=imsize, cellsize=1.0, hermitian_symmetry=hermitian_symmetry)
        assert gridder.imsize == expected
        assert gridder._grid_size == grid_size
        assert gridder._padded_grid_size == padded_grid_size
        assert gridder._padded_imsize == padded_imsize
        assert gridder._uvcellsize is not None

    @pytest.mark.parametrize(
        "imsize, expected, grid_size, padded_grid_size, padded_imsize", [
            (200, [200, 200], [200, 200], [240, 240], [240, 240]),
            ([200, 200], [200, 200], [200, 200], [240, 240], [240, 240]),
            ([200, 210], [200, 210], [200, 210], [240, 252], [240, 252]),
        ]
    )
    def test_update_imsize(
        self, gridder, imsize, expected, grid_size, padded_grid_size, padded_imsize
    ):
        gridder.imsize = imsize
        assert gridder.imsize == expected
        assert gridder._grid_size == grid_size
        assert gridder._padded_grid_size == padded_grid_size
        assert gridder._padded_imsize == padded_imsize
        assert gridder._uvcellsize is not None

    @pytest.mark.parametrize(
        "imsize, expected, grid_size, padded_grid_size, padded_imsize", [
            (200, [200, 200], [200, 101], [240, 121], [240, 240]),
            ([200, 200], [200, 200], [200, 101], [240, 121], [240, 240]),
            ([200, 210], [200, 210], [200, 106], [240, 127], [240, 252]),
        ]
    )
    def test_init_hermitian_symmetry(
        self, imsize, expected, grid_size, padded_grid_size, padded_imsize
    ):
        gridder = Gridder(imsize=imsize, cellsize=1.0, hermitian_symmetry=True)
        assert gridder.hermitian_symmetry
        assert gridder.imsize == expected
        assert gridder._grid_size == grid_size
        assert gridder._padded_grid_size == padded_grid_size
        assert gridder._padded_imsize == padded_imsize
        assert gridder._uvcellsize is not None

    @pytest.mark.parametrize(
        "imsize, expected, grid_size0, padded_grid_size0, padded_imsize0, grid_size1, padded_grid_size1, padded_imsize1",
        [
            (
                200, [200, 200], [200, 200], [240, 240], [240, 240], [200, 101], [240, 121
                                                                                  ], [240, 240]
            ),
            (
                [200, 200], [200, 200], [200, 200], [240, 240], [240, 240], [200, 101], [240, 121],
                [240, 240]
            ),
            (
                [200, 210], [200, 210], [200, 210], [240, 252], [240, 252], [200, 106], [240, 127],
                [240, 252]
            ),
        ]
    )
    def test_update_hermitian_symmetry(
        self, imsize, expected, grid_size0, padded_grid_size0, padded_imsize0, grid_size1,
        padded_grid_size1, padded_imsize1
    ):
        gridder = Gridder(imsize=imsize, cellsize=1.0)
        assert not gridder.hermitian_symmetry
        assert gridder.imsize == expected
        assert gridder._grid_size == grid_size0
        assert gridder._padded_grid_size == padded_grid_size0
        assert gridder._padded_imsize == padded_imsize0
        assert gridder._uvcellsize is not None
        gridder.hermitian_symmetry = True
        assert gridder.hermitian_symmetry
        assert gridder.imsize == expected
        assert gridder._grid_size == grid_size1
        assert gridder._padded_grid_size == padded_grid_size1
        assert gridder._padded_imsize == padded_imsize1
        assert gridder._uvcellsize is not None

    @pytest.mark.parametrize(
        "imsize, expected, grid_size0, padded_grid_size0, padded_imsize0, grid_size1, padded_grid_size1, padded_imsize1",
        [
            (
                200, [200, 200], [200, 200], [240, 240], [240, 240], [200, 101], [240, 121
                                                                                  ], [240, 240]
            ),
            (
                [200, 200], [200, 200], [200, 200], [240, 240], [240, 240], [200, 101], [240, 121],
                [240, 240]
            ),
            (
                [200, 210], [200, 210], [200, 210], [240, 252], [240, 252], [200, 106], [240, 127],
                [240, 252]
            ),
        ]
    )
    def test_update_hermitian_symmetry_twice(
        self, imsize, expected, grid_size0, padded_grid_size0, padded_imsize0, grid_size1,
        padded_grid_size1, padded_imsize1
    ):
        gridder = Gridder(imsize=imsize, cellsize=1.0)
        assert not gridder.hermitian_symmetry
        assert gridder.imsize == expected
        assert gridder._grid_size == grid_size0
        assert gridder._padded_grid_size == padded_grid_size0
        assert gridder._padded_imsize == padded_imsize0
        assert gridder._uvcellsize is not None
        gridder.hermitian_symmetry = True
        assert gridder.hermitian_symmetry
        assert gridder.imsize == expected
        assert gridder._grid_size == grid_size1
        assert gridder._padded_grid_size == padded_grid_size1
        assert gridder._padded_imsize == padded_imsize1
        assert gridder._uvcellsize is not None
        gridder.hermitian_symmetry = True
        assert gridder.hermitian_symmetry
        assert gridder.imsize == expected
        assert gridder._grid_size == grid_size1
        assert gridder._padded_grid_size == padded_grid_size1
        assert gridder._padded_imsize == padded_imsize1
        assert gridder._uvcellsize is not None

    @pytest.mark.parametrize(
        "cellsize, expected, uvcellsize", [
            (
                2., Quantity([-2., 2.], un.rad),
                Quantity([-0.0016286644951140066, 0.0016286644951140066], un.lambdas)
            ),
            (
                Quantity(2., un.rad), Quantity([-2., 2.], un.rad),
                Quantity([-0.0016286644951140066, 0.0016286644951140066], un.lambdas)
            ),
            (
                [1., 2.], Quantity([-1., 2.], un.rad),
                Quantity([-0.003257328990228013, 0.0016286644951140066], un.lambdas)
            ),
            (
                Quantity([1., 2.], un.rad), Quantity([-1., 2.], un.rad),
                Quantity([-0.003257328990228013, 0.0016286644951140066], un.lambdas)
            ),
            (
                Quantity([-1., 2.], un.rad), Quantity([-1., 2.], un.rad),
                Quantity([-0.003257328990228013, 0.0016286644951140066], un.lambdas)
            ),
        ]
    )
    def test_update_cellsize(self, gridder, cellsize, expected, uvcellsize):
        gridder.cellsize = cellsize
        assert (gridder.cellsize == expected).all()
        assert (gridder._uvcellsize == uvcellsize).all()

    def test_no_cellsize(self):
        with pytest.raises(AttributeError, match="The cellsize attribute must be set"):
            gridder = Gridder(imsize=256)
