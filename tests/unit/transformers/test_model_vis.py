from unittest.mock import Mock

import astropy.units as un
import dask.array as da
import numpy as np
import pytest
import xarray as xr

from pyralysis.reconstruction import Image
from pyralysis.transformers import ModelVisibilities


class TestModelVisibilities:

    class ModelVisibilitiesMock(ModelVisibilities):

        def estimate_visibility(self, grid_fft: da.Array, uv: tuple) -> da.Array:
            raise NotImplementedError

    @pytest.fixture
    def image(self, request):
        shape = request.param
        X, Y = np.mgrid[0:shape[-2], 0:shape[-1]]
        image = Image(
            data=xr.DataArray(
                data=da.zeros(shape, chunks=shape),
                dims=["x", "y"],
                coords=dict(X=(["x", "y"], Y), Y=(["x", "y"], X))
            ),
            cellsize=0.2 * un.arcsec,
            chunks=shape
        )
        return image

    @pytest.mark.parametrize("image", [(64, 64)], indirect=["image"])
    def test_init(self, image):
        model_visibilities = self.ModelVisibilitiesMock(
            input_data=Mock(),
            image=image,
            cellsize=2.0 * un.arcsec,
            hermitian_symmetry=False,
            padding_factor=1.0
        )
        # Public attributes
        assert model_visibilities.image.data is not None
        assert model_visibilities.padding_factor == 1.0
        assert model_visibilities.hermitian_symmetry is False
        assert model_visibilities.imsize == list(image.data.shape[-2:])
        assert model_visibilities.cellsize is not None
        assert model_visibilities.ckernel_object is None
        # Private attributes
        assert model_visibilities._grid_size is not None
        assert model_visibilities._padded_grid_size is not None
        assert model_visibilities._padded_imsize is not None
        assert model_visibilities._uvcellsize is not None
