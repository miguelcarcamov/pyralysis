import astropy.units as un
import dask.array as da
import numpy as np
import pytest
import xarray as xr
from astropy.coordinates import SkyCoord

from pyralysis.reconstruction.image import Image


class TestImage:

    def test_init_empty(self):
        with pytest.raises(ValueError):
            image = Image()

    def test_init_super(self):
        data = xr.DataArray(da.zeros((2048, 2048), chunks=(1024, 1024)))
        assert data.chunks == ((1024, 1024), (1024, 1024))
        image = Image(data=data, chunks=((2048, 2048)), cellsize=1.0)
        assert image.imsize == [2048, 2048]
        assert image.data.chunks == ((2048, ), (2048, ))
        assert image.noise is None
        assert image.name is None
        assert image.phase_center is None
        assert image.data_gpu is None
        assert (image.cellsize.value == [-1.0, 1.0]).all()
        assert image.cellsize.unit == un.rad
        assert image.pixel_area.value == 1.0
        assert image.pixel_area.unit == un.steradian

    def test_init_no_chunks(self):
        data = xr.DataArray(da.zeros((2048, 2048), chunks=(1024, 1024)))
        assert data.chunks == ((1024, 1024), (1024, 1024))
        image = Image(data=data, cellsize=1.0)
        assert image.data.chunks == ((2048, ), (2048, ))
        assert image.imsize == [2048, 2048]

    def test_init_no_cellsize(self):
        data = xr.DataArray(da.zeros((2048, 2048), chunks=(1024, 1024)))
        with pytest.raises(ValueError):
            image = Image(data=data, chunks=((2048, 2048)))

    def test_init_data_attrs(self):
        # test with data attrs
        data = xr.DataArray(
            da.zeros((2048, 2048), chunks=(1024, 1024)),
            attrs={
                "CDELT1": 180 / np.pi,
                "CDELT2": 360 / np.pi,
                "CRVAL1": 0.0,
                "CRVAL2": 0.0,
                "RADESYS": "FK5"
            }
        )
        image = Image(data=data)
        assert (image.cellsize.value == [-1.0, 2.0]).all()
        assert image.cellsize.unit == un.rad
        assert image.pixel_area.value == 2.0
        assert image.pixel_area.unit == un.steradian
        assert image.phase_center == SkyCoord(ra=0.0 * un.deg, dec=0.0 * un.deg, frame="fk5")

    def test_init_data_attrs_icrs(self):
        # test with data attrs
        data = xr.DataArray(
            da.zeros((2048, 2048), chunks=(1024, 1024)),
            attrs={
                "CDELT1": 180 / np.pi,
                "CDELT2": 360 / np.pi,
                "CRVAL1": 0.0,
                "CRVAL2": 0.0,
                "RADESYS": "ICRS"
            }
        )
        image = Image(data=data)
        assert (image.cellsize.value == [-1.0, 2.0]).all()
        assert image.cellsize.unit == un.rad
        assert image.pixel_area.value == 2.0
        assert image.pixel_area.unit == un.steradian
        assert image.phase_center == SkyCoord(ra=0.0 * un.deg, dec=0.0 * un.deg, frame="icrs")

    @pytest.mark.skip(reason='not yet implemented')
    def test_addition_images(self):
        data_a = xr.DataArray(da.ones((5, 5)))
        a = Image(data=data_a, cellsize=1.0)
        data_b = xr.DataArray(da.ones((5, 5)))
        b = Image(data=data_b, cellsize=1.0)
        assert a + b is not None

    @pytest.mark.skip(reason='not yet implemented')
    def test_subtraction_images(self):
        data_a = xr.DataArray(da.ones((5, 5)))
        a = Image(data=data_a, cellsize=1.0)
        data_b = xr.DataArray(da.ones((5, 5)))
        b = Image(data=data_b, cellsize=1.0)
        assert a - b is not None

    @pytest.mark.skip(reason='not yet implemented')
    def test_multiplication_images(self):
        data_a = xr.DataArray(da.ones((5, 5)))
        a = Image(data=data_a, cellsize=1.0)
        data_b = xr.DataArray(da.ones((5, 5)))
        b = Image(data=data_b, cellsize=1.0)
        assert a * b is not None

    @pytest.mark.skip(reason='not yet implemented')
    def test_division_images(self):
        data_a = xr.DataArray(da.ones((5, 5)))
        a = Image(data=data_a, cellsize=1.0)
        data_b = xr.DataArray(da.ones((5, 5)))
        b = Image(data=data_b, cellsize=1.0)
        assert a / b is not None

    @pytest.mark.skip(reason='not yet implemented')
    def test_power_images(self):
        data_a = xr.DataArray(da.ones((5, 5)))
        a = Image(data=data_a, cellsize=1.0)
        data_b = xr.DataArray(da.ones((5, 5)))
        b = Image(data=data_b, cellsize=1.0)
        assert a**b is not None

    @pytest.mark.skip(reason='not yet implemented')
    def test_floor_division_images(self):
        data_a = xr.DataArray(da.ones((5, 5)))
        a = Image(data=data_a, cellsize=1.0)
        data_b = xr.DataArray(da.ones((5, 5)))
        b = Image(data=data_b, cellsize=1.0)
        assert a // b is not None

    @pytest.mark.skip(reason='not yet implemented')
    def test_remainder_images(self):
        data_a = xr.DataArray(da.ones((5, 5)))
        a = Image(data=data_a, cellsize=1.0)
        data_b = xr.DataArray(da.ones((5, 5)))
        b = Image(data=data_b, cellsize=1.0)
        assert a % b is not None

    @pytest.mark.skip(reason='not yet implemented')
    def test_abs_images(self):
        pass

    @pytest.mark.skip(reason='not yet implemented')
    def test_less_than_images(self):
        pass

    @pytest.mark.skip(reason='not yet implemented')
    def test_less_equal_images(self):
        pass

    @pytest.mark.skip(reason='not yet implemented')
    def test_greater_than_images(self):
        pass

    @pytest.mark.skip(reason='not yet implemented')
    def test_greater_equal_images(self):
        pass

    @pytest.mark.skip(reason='not yet implemented')
    def test_equal_images(self):
        pass
