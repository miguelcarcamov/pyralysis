from unittest.mock import Mock

import astropy.units as un
import dask.array as da
import numpy as np
import pytest
import xarray as xr

from pyralysis.reconstruction.mask import Mask


class TestMask:

    def test_init_empty(self):
        mask = Mask()
        assert mask.data is None
        assert mask.cellsize is None
        assert mask.pixel_area is None

    def test_init_default_mask_cellsize(self):
        imsize = 256
        threshold = 0.5 * 1e-3
        phase_direction_cosines = np.zeros((2, 3)) * un.rad

        dataset = Mock()
        dataset.spws.min_nu.value = 1.0
        dataset.antenna.primary_beam.beam.return_value = da.ones(
            (1, 1, imsize, imsize), dtype=np.float32
        )
        dataset.field.phase_direction_cosines = phase_direction_cosines

        mask = Mask(dataset=dataset, imsize=(imsize, imsize), threshold=threshold, cellsize=1.)

        assert dataset.antenna.primary_beam.beam.call_count == phase_direction_cosines.shape[-1]
        assert len(dataset.antenna.primary_beam.beam.call_args) == 2
        assert len(dataset.antenna.primary_beam.beam.call_args[1]) == 8
        assert dataset.antenna.primary_beam.beam.call_args[1]['frequency'] == [1.0]
        assert dataset.antenna.primary_beam.beam.call_args[1]['imsize'] == (256, 256)
        assert (dataset.antenna.primary_beam.beam.call_args[1]['cellsize'] == [-1.0, 1.0] *
                un.rad).all()
        assert (dataset.antenna.primary_beam.beam.call_args[1]['antenna'] == np.array([0])).all()
        assert dataset.antenna.primary_beam.beam.call_args[1]['imchunks'] == "auto"
        assert mask.data is not None
        assert mask.pixel_area is not None

    def test_init_default_mask_no_cellsize(self):
        imsize = 256
        threshold = 0.5 * 1e-3
        phase_direction_cosines = np.zeros((2, 3)) * un.rad

        dataset = Mock()
        dataset.spws.min_nu.value = 1.0
        dataset.antenna.primary_beam.beam.return_value = da.ones(
            (1, 1, imsize, imsize), dtype=np.float32
        )
        dataset.field.phase_direction_cosines = phase_direction_cosines

        mask = Mask(dataset=dataset, imsize=(imsize, imsize), threshold=threshold)

        assert dataset.antenna.primary_beam.beam.call_count == phase_direction_cosines.shape[-1]
        assert len(dataset.antenna.primary_beam.beam.call_args) == 2
        assert len(dataset.antenna.primary_beam.beam.call_args[1]) == 8
        assert dataset.antenna.primary_beam.beam.call_args[1]['frequency'] == [1.0]
        assert dataset.antenna.primary_beam.beam.call_args[1]['imsize'] == (256, 256)
        assert (
            dataset.antenna.primary_beam.beam.call_args[1]['cellsize'] == np.array([-1, 1]) * un.rad
        ).all()
        assert (dataset.antenna.primary_beam.beam.call_args[1]['antenna'] == np.array([0])).all()
        assert dataset.antenna.primary_beam.beam.call_args[1]['imchunks'] == "auto"
        assert mask.data is not None
        assert mask.pixel_area is None

    def test_init_parameter_init(self):
        data = xr.DataArray(da.zeros((2048, 2048), chunks=(1024, 1024)))
        assert data.chunks == ((1024, 1024), (1024, 1024))
        mask = Mask(data=data, chunks=(2048, 2048))
        assert mask.data.chunks == ((2048, ), (2048, ))

    def test_calculate_noise_warn(self):
        imsize = 256
        threshold = 0.5 * 1e-3
        phase_direction_cosines = np.zeros((2, 3)) * un.rad

        dataset = Mock()
        dataset.spws.ref_nu.value = 1.0
        dataset.antenna.primary_beam.beam.return_value = da.ones(
            (1, 1, imsize, imsize), dtype=np.float32
        )
        dataset.field.phase_direction_cosines = phase_direction_cosines

        mask = Mask(dataset=dataset, imsize=(imsize, imsize), threshold=threshold, cellsize=1.)

        with pytest.warns(UserWarning) as record:
            res = mask.calculate_noise()
            assert res is None

        assert len(record) == 1
        assert "mask does not have noise" in record[0].message.args[0]

    def test_init_default_mask_chunks(self):
        imsize = 256
        chunks = (32, 32)
        threshold = 0.5 * 1e-3
        phase_direction_cosines = np.zeros((2, 3)) * un.rad

        dataset = Mock()
        dataset.spws.min_nu.value = 1.0
        dataset.antenna.primary_beam.beam.return_value = da.ones(
            (1, 1, imsize, imsize), dtype=np.float32
        )
        dataset.field.phase_direction_cosines = phase_direction_cosines

        mask = Mask(dataset=dataset, imsize=(imsize, imsize), threshold=threshold, chunks=chunks)

        assert dataset.antenna.primary_beam.beam.call_count == phase_direction_cosines.shape[-1]
        assert len(dataset.antenna.primary_beam.beam.call_args) == 2
        assert len(dataset.antenna.primary_beam.beam.call_args[1]) == 8
        assert dataset.antenna.primary_beam.beam.call_args[1]['frequency'] == [1.0]
        assert dataset.antenna.primary_beam.beam.call_args[1]['imsize'] == (imsize, imsize)
        assert (
            dataset.antenna.primary_beam.beam.call_args[1]['cellsize'] == np.array([-1, 1]) * un.rad
        ).all()
        assert (dataset.antenna.primary_beam.beam.call_args[1]['antenna'] == np.array([0])).all()
        assert dataset.antenna.primary_beam.beam.call_args[1]['imchunks'] == chunks
        assert mask.data is not None
        assert mask.data.shape == (imsize, imsize)
        assert mask.pixel_area is None

    def test_init_indices_chunks(self):
        # No indices
        data1 = xr.DataArray(da.zeros((4, 4), chunks=(2, 2)))
        assert data1.chunks == ((2, 2), (2, 2))
        mask1 = Mask(data=data1, chunks=data1.data.chunksize)
        assert mask1.data.chunks == ((2, 2), (2, 2))
        assert len(mask1.indices[0]) == 0
        assert len(mask1.indices[1]) == 0

        # Full indices
        data2 = xr.DataArray(da.ones((4, 4), chunks=(2, 2)))
        assert data2.chunks == ((2, 2), (2, 2))
        mask2 = Mask(data=data2, chunks=data2.data.chunksize)
        assert mask2.data.chunks == ((2, 2), (2, 2))
        assert len(mask2.indices[0]) == 16
        assert len(mask2.indices[1]) == 16
        assert mask2.indices[0].chunks == ((4, 4, 4, 4), )
        assert mask2.indices[1].chunks == ((4, 4, 4, 4), )

        # Ragged
        data3_values = [
            [True, False, False, True],
            [False, False, False, False],
            [True, True, True, False],
            [False, True, False, True],
        ]
        data3 = xr.DataArray(da.from_array(data3_values, chunks=(2, 2)))
        assert data3.chunks == ((2, 2), (2, 2))
        mask3 = Mask(data=data3, chunks=data3.data.chunksize)
        assert mask3.data.chunks == ((2, 2), (2, 2))
        assert len(mask3.indices[0]) == 7
        assert len(mask3.indices[1]) == 7
        assert mask3.indices[0].chunks == ((2, 3, 2), )
        assert mask3.indices[1].chunks == ((2, 3, 2), )
