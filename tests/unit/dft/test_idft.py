import itertools
import numbers

import dask.array as da
import numpy as np
import pytest

from pyralysis.dft import idft2


class TestIDFT:

    @pytest.fixture
    def uvw_vis(self):
        freq = 233e9
        wavelength = 3e8 / freq
        a = 1e4
        b = 2e4
        x = np.arange(-4, 3, 2).reshape((1, -1)).repeat(4, axis=0).ravel(order="F")
        y = np.arange(4, -3, -2).repeat(4)
        z = np.zeros(16)
        xyz_antenna = np.vstack([x, y, z]).T
        id_antenna = np.arange(0, len(x))
        combi = np.fromiter(itertools.chain(*itertools.combinations(id_antenna, 2)),
                            dtype=int).reshape(-1, 2)
        baselines = np.vstack(
            [
                xyz_antenna[combi[:, 0]] - xyz_antenna[combi[:, 1]],
                xyz_antenna[combi[:, 1]] - xyz_antenna[combi[:, 0]]
            ]
        )
        R = np.eye(3)
        uvw = np.sum(R * baselines[..., np.newaxis], axis=1)
        uvw_chan = np.repeat(uvw[:, np.newaxis, :], repeats=3, axis=1) / wavelength  # row chan uvw
        vis = np.exp(-((uvw_chan[..., 0] / a)**2 +
                       (uvw_chan[..., 1] / b)**2)) / np.abs(a * b)  # row chan
        vis_corr = vis[:, :, np.newaxis] + 0j
        return uvw_chan, vis_corr

    @pytest.mark.parametrize(("use_numba"), [False, True])
    def test_dask(self, uvw_vis, use_numba):
        uvw, vis = uvw_vis
        assert uvw.ndim == 3
        assert vis.ndim == 3
        assert uvw.shape[:2] == vis.shape[:2]

        n = 64
        deltax = 1e-7
        xx = (np.arange(0, n, 1) - n // 2) * deltax
        yy = (np.arange(0, n, 1) - n // 2) * deltax
        x_, y_ = np.meshgrid(xx, yy, indexing='xy')
        x = np.ravel(x_)
        y = np.ravel(y_)
        wt = np.ones(vis.shape)

        # Chunk data to test change of result by using blockwise
        x_da = da.from_array(x, chunks=1024)
        y_da = da.from_array(y, chunks=1024)
        uvw_da = da.from_array(uvw, chunks=(40, 1, 3))
        vis_da = da.from_array(vis, chunks=(40, 1, 1))
        wt_da = da.from_array(wt, chunks=(40, 1, 1))

        res = idft2(x_da, y_da, uvw_da, vis_da, wt_da, numba=use_numba)
        assert res.shape == (vis.shape[1], x.shape[0])
        # Check that after compute the dim stay the same
        res_mem = res.compute()
        assert res_mem.shape == (vis.shape[1], x.shape[0])

    @pytest.mark.parametrize(("use_numba"), [False, True])
    def test_numpy(self, uvw_vis, use_numba):
        uvw, vis = uvw_vis
        assert uvw.ndim == 3
        assert vis.ndim == 3
        assert uvw.shape[:2] == vis.shape[:2]

        n = 64
        deltax = 1e-7
        xx = (np.arange(0, n, 1) - n // 2) * deltax
        yy = (np.arange(0, n, 1) - n // 2) * deltax
        x_, y_ = np.meshgrid(xx, yy, indexing='xy')
        x = np.ravel(x_)
        y = np.ravel(y_)
        wt = np.ones(vis.shape)

        res = idft2(x, y, uvw, vis, wt, numba=use_numba)
        assert res.shape == (vis.shape[1], x.shape[0])
        # Check that after compute the dim stay the same
        res_mem = res.compute()
        assert res_mem.shape == (vis.shape[1], x.shape[0])

    @pytest.mark.parametrize(("use_numba"), [False, True])
    def test_no_weight(self, uvw_vis, use_numba):
        uvw, vis = uvw_vis
        assert uvw.ndim == 3
        assert vis.ndim == 3
        assert uvw.shape[:2] == vis.shape[:2]

        n = 64
        deltax = 1e-7
        xx = (np.arange(0, n, 1) - n // 2) * deltax
        yy = (np.arange(0, n, 1) - n // 2) * deltax
        x_, y_ = np.meshgrid(xx, yy, indexing='xy')
        x = np.ravel(x_)
        y = np.ravel(y_)

        # Chunk data to test change of result by using blockwise
        x_da = da.from_array(x, chunks=1024)
        y_da = da.from_array(y, chunks=1024)
        uvw_da = da.from_array(uvw, chunks=(40, 1, 3))
        vis_da = da.from_array(vis, chunks=(40, 1, 1))

        res = idft2(x_da, y_da, uvw_da, vis_da, numba=use_numba)
        assert res.shape == (vis.shape[1], x.shape[0])
        # Check that after compute the dim stay the same
        res_mem = res.compute()
        assert res_mem.shape == (vis.shape[1], x.shape[0])

    @pytest.mark.parametrize(("sign_convention"), ["negative", "positive"])
    @pytest.mark.parametrize(("use_numba"), [False, True])
    def test_sign_convention(self, uvw_vis, sign_convention, use_numba):
        uvw_, vis_ = uvw_vis

        n = 64
        deltax = 1e-7
        xx = (np.arange(0, n, 1) - n // 2) * deltax
        yy = (np.arange(0, n, 1) - n // 2) * deltax
        x_, y_ = np.meshgrid(xx, yy, indexing='xy')
        x = da.from_array(np.ravel(x_), chunks=1024)
        y = da.from_array(np.ravel(y_), chunks=1024)

        uvw = da.from_array(uvw_, chunks=(40, 1, 3))
        vis = da.from_array(vis_, chunks=(40, 1, 1))

        res = idft2(x, y, uvw, vis, sign_convention=sign_convention, numba=use_numba)
        assert res.shape == (vis_.shape[1], n * n)
        # Test that the array is Real
        assert issubclass(res.dtype.type, numbers.Real)

    def test_sign_convention_error(self):
        mock_input = [None] * 5
        with pytest.raises(ValueError, match="The parameter sign_convention can only take"):
            idft2(*mock_input, "positiv")
