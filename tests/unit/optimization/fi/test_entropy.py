from unittest.mock import Mock

import dask.array as da
import numpy as np
import pytest
import xarray as xr

from pyralysis.optimization.fi import Entropy


class TestEntropy:

    @pytest.fixture
    def e(self):
        image = Mock(data=xr.DataArray(np.ones((3, 3, 2))))
        _e = Entropy(image=image)
        return _e

    def test_init(self, e):
        assert e.prior_image == 1.0
        assert np.all(e.image.data == np.ones((3, 3, 2)))
        assert e.penalization_factor == 1.0
        assert e.image_index == -1
        assert e.func_value == 0.0
        assert np.all(e.grad_value == np.zeros((3, 3, 2)))
        assert e.normalize == False

    @pytest.mark.parametrize(
        "prior_image", [
            0,
            np.zeros((3, 3)),
            np.array([[1., 1., 1.], [1., 0., 1.], [1., 1., 1.]]),
        ]
    )
    def test_prior_image_value_error(self, prior_image):
        image = Mock(data=xr.DataArray(np.ones((3, 3))))
        with pytest.raises(Exception) as e_info:
            e = Entropy(prior_image=prior_image, image_index=0, image=image)

    @pytest.mark.parametrize(
        "image, prior_image, image_index",
        [  # image 2d
            (np.ones((16, 16)), np.ones((8, 8)), -1),
            (np.ones((16, 16)), np.ones((16, 16, 1)), -1),
            (np.ones((16, 16)), np.ones((8, 8)), 0),
            (np.ones((16, 16)), np.ones((16, 16, 1)), 0),
            # image 3d
            (np.ones((16, 16, 2)), np.ones((8, 8)), -1),
            (np.ones((16, 16, 2)), np.ones((16, 16, 1)), -1),
            (np.ones((16, 16, 2)), np.ones((8, 8)), 0),
            (np.ones((16, 16, 2)), np.ones((8, 8, 1)), 0),
        ]
    )
    def test_prior_image_shape_error(self, image, prior_image, image_index):
        image_ = Mock(data=xr.DataArray(image))
        with pytest.raises(AttributeError) as e_info:
            e = Entropy(prior_image=prior_image, image=image_, image_index=image_index)

    @pytest.mark.parametrize(
        "image, prior_image, image_index",
        [
            # 2d image
            (np.ones((3, 3)), np.ones((3, 3)), -1),
            (np.ones((3, 3)), np.ones((3, 3)), 0),
            # 3d image
            (np.ones((3, 3, 2)), np.ones((3, 3)), -1),
            (np.ones((3, 3, 2)), np.ones((3, 3)), 0),
            (np.ones((3, 3, 2)), np.ones((3, 3, 2)), -1),
            (np.ones((3, 3, 2)), np.ones((3, 3, 2)), 0),
            (np.ones((3, 3, 2)), np.ones((3, 3, 1)), 0),
        ]
    )
    def test_prior_image_array(self, image, prior_image, image_index):
        image_ = Mock(data=xr.DataArray(image))
        e = Entropy(prior_image=prior_image, image=image_, image_index=image_index)
        assert e.image is not None
        assert e.prior_image is not None
        assert (e.prior_image == prior_image).all

    def test_normalize(self, e):
        e.normalize = True
        assert e.normalize == True

    @pytest.mark.parametrize(
        "p, prior_image, expected",
        [
            # 2d image
            (np.array([[np.e]]), 1.0, np.e),
            (np.array([[0.0]]), 1.0, 0.0),
            (np.array([[np.e, np.e]]), 1.0, 2 * np.e),
            (np.array([[2.6, 9.8, 3.9]]), 1.0, 30.1594),
            (np.array([[np.e]]), np.array([[1.0]]), np.e),
            (np.array([[0.0]]), np.array([[1.0]]), 0.0),
            (np.array([[np.e, np.e]]), np.array([[1.0, 1.0]]), 2 * np.e),
            (np.array([[2.6, 9.8, 3.9]]), np.array([[1.0, 1.0, 1.0]]), 30.1594),
            # 3d image
            (np.array([[[np.e]]]), 1.0, np.e),
            (np.array([[[0.0]]]), 1.0, 0.0),
            (np.array([[[-1.7]]]), 1.0, 0.0),
            (np.array([[[1.0]]]), 1.0, 0.0),
            (np.array([[[0.5]]]), 1.0, -0.3465),
            (np.array([[[np.e, np.e]]]), 1.0, 2 * np.e),
            (np.array([[[2.6, 9.8, 3.9]]]), 1.0, 30.1594),
            (np.array([[[-2.6, 9.8, -3.9]]]), 1.0, 22.3673),
            (np.array([[[-2.6, -9.8, -3.9]]]), 1.0, 0.0),
            # 3d prior image
            (np.array([[[np.e]]]), np.array([[[1.0]]]), np.e),
            (np.array([[[1.0]]]), np.array([[[1.0]]]), 0.0),
            (np.array([[[0.5]]]), np.array([[[1.0]]]), -0.3465),
            (np.array([[[np.e, np.e]]]), np.array([[[1.0, 1.0]]]), 2 * np.e),
            (np.array([[[-2.6, 9.8, -3.9]]]), np.array([[[1.0, 1.0, 1.0]]]), 22.3673),
            (
                np.array([[[2.6, -2.6]], [[9.8, 9.8]], [[3.9, -3.9]]]),
                np.array([[[1., 1.]], [[1., 1.]], [[1., 1.]]]),
                52.5268,
            ),
            # 2d prior image
            (np.array([[[np.e]]]), np.array([[1.0]]), np.e),
            (np.array([[[1.0]]]), np.array([[1.0]]), 0.0),
            (np.array([[[0.5]]]), np.array([[1.0]]), -0.3465),
            (np.array([[[np.e, np.e]]]), np.array([[1.0]]), 2 * np.e),
            (np.array([[[-2.6, 9.8, -3.9]]]), np.array([[1.0]]), 22.3673),
            (
                np.array([[[2.6, -2.6]], [[9.8, 9.8]], [[3.9, -3.9]]]),
                np.array([[1.], [1.], [1.]]),
                52.5268,
            ),
        ]
    )
    def test_function(self, e, p, prior_image, expected):
        # Set image
        e.image = Mock(data=xr.DataArray(p))
        # Set prior image
        e.prior_image = prior_image
        # Get result
        res = e.function()
        assert pytest.approx(res, rel=1e-3) == expected

    @pytest.mark.parametrize(
        "p, image_index, prior_image, expected",
        [
            # 2d image
            (np.array([[np.e, -17.3]]), 0, 1.0, np.e),
            (np.array([[np.e, -17.3]]), 1, 1.0, np.e),
            (np.array([[0.5, 1.0]]), 0, 1.0, -0.3465),
            (np.array([[0.5, 1.0]]), 1, 1.0, -0.3465),
            (np.array([[2.6, -2.6], [9.8, 9.8], [3.9, -3.9]]), 0, 1.0, 52.5268),
            (np.array([[2.6, -2.6], [9.8, 9.8], [3.9, -3.9]]), 1, 1.0, 52.5268),
            (np.array([[np.e, -17.3]]), 0, np.array([[1.0, 1.0]]), np.e),
            (np.array([[np.e, -17.3]]), 1, np.array([[1.0, 1.0]]), np.e),
            (np.array([[0.5, 1.0]]), 0, np.array([[1.0, 1.0]]), -0.3465),
            (np.array([[0.5, 1.0]]), 1, np.array([[1.0, 1.0]]), -0.3465),
            (
                np.array([[2.6, -2.6], [9.8, 9.8], [3.9, -3.9]]),
                0,
                np.array([[1., 1.], [1., 1.], [1., 1.]]),
                52.5268,
            ),
            (
                np.array([[2.6, -2.6], [9.8, 9.8], [3.9, -3.9]]),
                1,
                np.array([[1., 1.], [1., 1.], [1., 1.]]),
                52.5268,
            ),
            # 3d image
            (np.array([[[np.e, -17.3]]]), 0, 1.0, np.e),
            (np.array([[[np.e, -17.3]]]), 1, 1.0, 0.0),
            (np.array([[[0.5, 1.0]]]), 0, 1.0, -0.3465),
            (np.array([[[0.5, 1.0]]]), 1, 1.0, 0.0),
            (np.array([[[2.6, -2.6]], [[9.8, 9.8]], [[3.9, -3.9]]]), 0, 1.0, 30.1594),
            (np.array([[[2.6, -2.6]], [[9.8, 9.8]], [[3.9, -3.9]]]), 1, 1.0, 22.3673),
            # 3d prior image
            (np.array([[[np.e, -17.3]]]), 0, np.array([[[1., 1., 1.]]]), np.e),
            (np.array([[[np.e, -17.3]]]), 1, np.array([[[1., 1., 1.]]]), 0.0),
            (np.array([[[0.5, 1.0]]]), 0, np.array([[[1., 1., 1.]]]), -0.3465),
            (np.array([[[0.5, 1.0]]]), 1, np.array([[[1., 1., 1.]]]), 0.0),
            (
                np.array([[[2.6, -2.6]], [[9.8, 9.8]], [[3.9, -3.9]]]),
                0,
                np.array([[[1., 1.]], [[1., 1.]], [[1., 1.]]]),
                30.1594,
            ),
            (
                np.array([[[2.6, -2.6]], [[9.8, 9.8]], [[3.9, -3.9]]]),
                1,
                np.array([[[1., 1.]], [[1., 1.]], [[1., 1.]]]),
                22.3673,
            ),
            # 2d prior image
            (np.array([[[np.e, -17.3]]]), 0, np.array([[1.]]), np.e),
            (np.array([[[np.e, -17.3]]]), 1, np.array([[1.]]), 0.0),
            (np.array([[[0.5, 1.0]]]), 0, np.array([[1.]]), -0.3465),
            (np.array([[[0.5, 1.0]]]), 1, np.array([[1.]]), 0.0),
            (
                np.array([[[2.6, -2.6]], [[9.8, 9.8]], [[3.9, -3.9]]]),
                0,
                np.array([[1.], [1.], [1.]]),
                30.1594,
            ),
            (
                np.array([[[2.6, -2.6]], [[9.8, 9.8]], [[3.9, -3.9]]]),
                1,
                np.array([[1.], [1.], [1.]]),
                22.3673,
            ),
        ]
    )
    def test_function_one_index(self, e, p, image_index, prior_image, expected):
        # Set image data
        e.image = Mock(data=xr.DataArray(p))
        # Set image index in Entropy
        e.image_index = image_index
        # Set prior image
        e.prior_image = prior_image
        # Get result
        res = e.function()
        assert pytest.approx(res, rel=1e-3) == expected
        assert pytest.approx(e.func_value, rel=1e-3) == expected

    @pytest.mark.parametrize(
        "p, prior_image, mask_data, expected",
        [
            # 2d image
            (np.array([[np.e]]), 1., np.array([[True]]), np.e),
            (np.array([[np.e]]), 1., np.array([[False]]), 0.0),
            (np.array([[np.e, np.e]]), 1., np.array([[True, True]]), 2 * np.e),
            (np.array([[np.e, np.e]]), 1., np.array([[True, False]]), np.e),
            (np.array([[np.e, np.e]]), 1., np.array([[False, True]]), np.e),
            (np.array([[np.e, np.e]]), 1., np.array([[False, False]]), 0.0),
            (
                np.array([[1., 3., 5.], [7., 9., 11.], [13., 15., 17.]]),
                1.,
                np.array([[False, True, False], [True, True, True], [False, True, False]]),
                103.6898,
            ),
            (np.array([[np.e]]), np.array([[1.]]), np.array([[True]]), np.e),
            (np.array([[np.e, np.e]]), np.array([[1., 1.]]), np.array([[True, False]]), np.e),
            (
                np.array([[1., 3., 5.], [7., 9., 11.], [13., 15., 17.]]),
                np.array([[1., 1., 1.], [1., 1., 1.], [1., 1., 1.]]),
                np.array([[False, True, False], [True, True, True], [False, True, False]]),
                103.6898,
            ),
            # 3d image
            # Same dimensions
            (np.array([[[np.e]]]), 1., np.array([[[True]]]), np.e),
            (np.array([[[np.e]]]), 1., np.array([[[False]]]), 0.0),
            (np.array([[[0.0]]]), 1., np.array([[[True]]]), 0.0),
            (np.array([[[-1.7]]]), 1., np.array([[[True]]]), 0.0),
            (np.array([[[1.0]]]), 1., np.array([[[True]]]), 0.0),
            (np.array([[[0.5]]]), 1., np.array([[[True]]]), -0.3465),
            (np.array([[[np.e, np.e]]]), 1., np.array([[[True, True]]]), 2 * np.e),
            (np.array([[[np.e, np.e]]]), 1., np.array([[[True, False]]]), np.e),
            (np.array([[[np.e, np.e]]]), 1., np.array([[[False, True]]]), np.e),
            (np.array([[[np.e, np.e]]]), 1., np.array([[[False, False]]]), 0.0),
            (np.array([[[2.6, 9.8, 3.9]]]), 1., np.array([[[True, True, True]]]), 30.1594),
            (np.array([[[2.6, 9.8, 3.9]]]), 1., np.array([[[False, True, False]]]), 22.3673),
            (np.array([[[-2.6, 9.8, -3.9]]]), 1., np.array([[[True, True, True]]]), 22.3673),
            (np.array([[[-2.6, 9.8, -3.9]]]), 1., np.array([[[True, False, True]]]), 0.0),
            (np.array([[[-2.6, -9.8, -3.9]]]), 1., np.array([[[True, True, True]]]), 0.0),
            # Different dimensions
            (np.array([[[np.e]]]), 1., np.array([[True]]), np.e),
            (np.array([[[np.e]]]), 1., np.array([[False]]), 0.0),
            (np.array([[[np.e, np.e]]]), 1., np.array([[True]]), 2 * np.e),
            (np.array([[[np.e, np.e]]]), 1., np.array([[False]]), 0.0),
            (
                np.array(
                    [
                        [[1., 2.], [3., 4.], [5., 6.]], [[7., 8.], [9., 10.], [11., 12.]],
                        [[13., 14.], [15., 16.], [17., 18.]]
                    ]
                ),
                1.,
                np.array([[False, True, False], [True, True, True], [False, True, False]]),
                223.0766,
            ),
            # 3d prior image
            (np.array([[[np.e]]]), np.array([[[1.]]]), np.array([[[True]]]), np.e),  # 30
            (np.array([[[np.e, np.e]]]), np.array([[[1., 1.]]]), np.array([[[True, False]]]), np.e),
            (
                np.array([[[-2.6, 9.8, -3.9]]]
                         ), np.array([[[1., 1., 1.]]]), np.array([[[True, True, True]]]), 22.3673
            ),
            (np.array([[[np.e]]]), np.array([[[1.]]]), np.array([[True]]), np.e),
            (
                np.array(
                    [
                        [[1., 2.], [3., 4.], [5., 6.]], [[7., 8.], [9., 10.], [11., 12.]],
                        [[13., 14.], [15., 16.], [17., 18.]]
                    ]
                ),
                np.array(
                    [
                        [[1., 1.], [1., 1.], [1., 1.]], [[1., 1.], [1., 1.], [1., 1.]],
                        [[1., 1.], [1., 1.], [1., 1.]]
                    ]
                ),
                np.array([[False, True, False], [True, True, True], [False, True, False]]),
                223.0766,
            ),
            # 2d prior image
            (np.array([[[np.e]]]), np.array([[1.]]), np.array([[[True]]]), np.e),  # 35
            (np.array([[[np.e, np.e]]]), np.array([[1.]]), np.array([[[True, False]]]), np.e),
            (
                np.array([[[-2.6, 9.8, -3.9]]]), np.array([[1.]]), np.array([[[True, True, True]]]
                                                                            ), 22.3673
            ),
            (np.array([[[np.e]]]), np.array([[1.]]), np.array([[True]]), np.e),
            (
                np.array(
                    [
                        [[1., 2.], [3., 4.], [5., 6.]], [[7., 8.], [9., 10.], [11., 12.]],
                        [[13., 14.], [15., 16.], [17., 18.]]
                    ]
                ),
                np.array([[1., 1., 1.], [1., 1., 1.], [1., 1., 1.]]),
                np.array([[False, True, False], [True, True, True], [False, True, False]]),
                223.0766,
            ),
        ]
    )
    def test_function_mask(self, e, p, prior_image, mock_mask, mask_data, expected):
        # Mock mask
        mask = mock_mask()
        mask.data = mask_data
        # Set image
        e.image = Mock(data=xr.DataArray(p))
        # Set prior image
        e.prior_image = prior_image
        # Get result with mask
        res = e.function(mask=mask)
        assert pytest.approx(res, rel=1e-3) == expected
        assert pytest.approx(e.func_value, rel=1e-3) == expected

    @pytest.mark.parametrize(
        "p, image_index, prior_image, mask_data, expected",
        [
            # 2d image
            (
                np.array([[1., 2.], [3., 4.]]),
                0,
                1.,
                np.array([[True, True], [False, True]]),
                6.9315,
            ),
            (
                np.array([[1., 2.], [3., 4.]]),
                1,
                1.,
                np.array([[True, True], [False, True]]),
                6.9315,
            ),
            (
                np.array([[1., 2.], [3., 4.]]),
                0,
                1.,
                np.array([[[True, True], [False, True]], [[True, False], [True, False]]]),
                8.8410,
            ),
            (
                np.array([[1., 2.], [3., 4.]]),
                1,
                1.,
                np.array([[[True, True], [False, True]], [[True, False], [True, False]]]),
                1.3863,
            ),
            (
                np.array([[1., 2.], [3., 4.]]),
                0,
                np.array([[1., 1.], [1., 1.]]),
                np.array([[True, True], [False, True]]),
                6.9315,
            ),
            (
                np.array([[1., 2.], [3., 4.]]),
                1,
                np.array([[1., 1.], [1., 1.]]),
                np.array([[True, True], [False, True]]),
                6.9315,
            ),
            (
                np.array([[1., 2.], [3., 4.]]),
                0,
                np.array([[1., 1.], [1., 1.]]),
                np.array([[[True, True], [False, True]], [[True, False], [True, False]]]),
                8.8410,
            ),
            (
                np.array([[1., 2.], [3., 4.]]),
                1,
                np.array([[1., 1.], [1., 1.]]),
                np.array([[[True, True], [False, True]], [[True, False], [True, False]]]),
                1.3863,
            ),
            # 3d image
            (
                np.array([[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]]),
                0,
                1.,
                np.array([[True, True], [False, True]]),
                16.9172,
            ),
            (
                np.array([[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]]),
                1,
                1.,
                np.array([[True, True], [False, True]]),
                23.5670,
            ),
            (
                np.array([[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]]),
                0,
                1.,
                np.array([[[True, True], [False, True]], [[True, False], [True, False]]]),
                21.6685,
            ),
            (
                np.array([[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]]),
                1,
                1.,
                np.array([[[True, True], [False, True]], [[True, False], [True, False]]]),
                6.9314,
            ),
            # 3d prior image
            (
                np.array([[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]]),
                0,
                np.array([[[1., 1.], [1., 1.]], [[1., 1.], [1., 1.]]]),
                np.array([[True, True], [False, True]]),
                16.9172,
            ),
            (
                np.array([[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]]),
                1,
                np.array([[[1., 1.], [1., 1.]], [[1., 1.], [1., 1.]]]),
                np.array([[True, True], [False, True]]),
                23.5670,
            ),
            (
                np.array([[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]]),
                0,
                np.array([[[1., 1.], [1., 1.]], [[1., 1.], [1., 1.]]]),
                np.array([[[True, True], [False, True]], [[True, False], [True, False]]]),
                21.6685,
            ),
            (
                np.array([[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]]),
                1,
                np.array([[[1., 1.], [1., 1.]], [[1., 1.], [1., 1.]]]),
                np.array([[[True, True], [False, True]], [[True, False], [True, False]]]),
                6.9314,
            ),
            # 2d prior image
            (
                np.array([[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]]),
                0,
                np.array([[1., 1.], [1., 1.]]),
                np.array([[True, True], [False, True]]),
                16.9172,
            ),
            (
                np.array([[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]]),
                1,
                np.array([[1., 1.], [1., 1.]]),
                np.array([[True, True], [False, True]]),
                23.5670,
            ),
            (
                np.array([[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]]),
                0,
                np.array([[1., 1.], [1., 1.]]),
                np.array([[[True, True], [False, True]], [[True, False], [True, False]]]),
                21.6685,
            ),
            (
                np.array([[[1., 2.], [3., 4.]], [[5., 6.], [7., 8.]]]),
                1,
                np.array([[1., 1.], [1., 1.]]),
                np.array([[[True, True], [False, True]], [[True, False], [True, False]]]),
                6.9314,
            ),
            (
                np.array([[[1., 2.]], [[3., 4.]], [[5., 6.]]]),
                0,
                np.array([[1.], [1.], [1.]]),
                np.array([[True], [True], [True]]),
                11.3430,
            ),
            (
                np.array([[[1., 2.]], [[3., 4.]], [[5., 6.]]]),
                1,
                np.array([[1.], [1.], [1.]]),
                np.array([[True], [True], [True]]),
                17.6820,
            ),
        ]
    )
    def test_function_one_index_mask(
        self, e, p, image_index, prior_image, mock_mask, mask_data, expected
    ):
        # Mock mask
        mask = mock_mask()
        mask.data = mask_data
        # Set image
        e.image = Mock(data=xr.DataArray(p))
        # Set image index
        e.image_index = image_index
        # Set prior image
        e.prior_image = prior_image
        # Get result with mask
        res = e.function(mask=mask)
        assert pytest.approx(res, rel=1e-3) == expected
        assert pytest.approx(e.func_value, rel=1e-3) == expected

    @pytest.mark.parametrize(
        "p, prior_image, expected",
        [
            # 2d image
            (
                np.array([[1.], [2.], [3.], [4.]]),
                1.,
                np.array([[1.], [1.6931], [2.0986], [2.3863]]),
            ),
            (
                np.array([[1., 2., 3., 4.]]),
                1.,
                np.array([[1., 1.6931, 2.0986, 2.3863]]),
            ),
            (
                np.array([[1, 2], [3, 4]]),
                1.,
                np.array([[1., 1.6931], [2.0986, 2.3863]]),
            ),
            (
                np.array([[1.], [2.], [3.], [4.]]),
                np.array([[1.], [1.], [1.], [1.]]),
                np.array([[1.], [1.6931], [2.0986], [2.3863]]),
            ),
            (
                np.array([[1., 2., 3., 4.]]),
                np.array([[1., 1., 1., 1.]]),
                np.array([[1., 1.6931, 2.0986, 2.3863]]),
            ),
            (
                np.array([[1, 2], [3, 4]]),
                np.array([[1., 1.], [1., 1.]]),
                np.array([[1., 1.6931], [2.0986, 2.3863]]),
            ),
            # 3d image
            (
                np.array([[[1., 2., 3.], [4., 5., 6.]]]),
                1.,
                np.array([[[1., 1.6931, 2.0986], [2.3863, 2.6094, 2.7917]]]),
            ),
            (
                np.array([[[1.], [2.], [3.]], [[4.], [5.], [6.]]]),
                1.,
                np.array([[[1.], [1.6931], [2.0986]], [[2.3863], [2.6094], [2.7917]]]),
            ),
            (
                np.array([[[1., 2.]], [[3., 4.]], [[5., 6.]]]),
                1.,
                np.array([[[1., 1.6931]], [[2.0986, 2.3863]], [[2.6094, 2.7917]]]),
            ),
            # 3d prior image
            (
                np.array([[[1., 2., 3.], [4., 5., 6.]]]),
                np.array([[[1., 1., 1.], [1., 1., 1.]]]),
                np.array([[[1., 1.6931, 2.0986], [2.3863, 2.6094, 2.7917]]]),
            ),
            (
                np.array([[[1.], [2.], [3.]], [[4.], [5.], [6.]]]),
                np.array([[[1.], [1.], [1.]], [[1.], [1.], [1.]]]),
                np.array([[[1.], [1.6931], [2.0986]], [[2.3863], [2.6094], [2.7917]]]),
            ),
            (
                np.array([[[1., 2.]], [[3., 4.]], [[5., 6.]]]),
                np.array([[[1., 1.]], [[1., 1.]], [[1., 1.]]]),
                np.array([[[1., 1.6931]], [[2.0986, 2.3863]], [[2.6094, 2.7917]]]),
            ),
            # 2d prior image
            (
                np.array([[[1., 2., 3.], [4., 5., 6.]]]),
                np.array([[1., 1.]]),
                np.array([[[1., 1.6931, 2.0986], [2.3863, 2.6094, 2.7917]]]),
            ),
            (
                np.array([[[1.], [2.], [3.]], [[4.], [5.], [6.]]]),
                np.array([[1., 1., 1.], [1., 1., 1.]]),
                np.array([[[1.], [1.6931], [2.0986]], [[2.3863], [2.6094], [2.7917]]]),
            ),
            (
                np.array([[[1., 2.]], [[3., 4.]], [[5., 6.]]]),
                np.array([[1.], [1.], [1.]]),
                np.array([[[1., 1.6931]], [[2.0986, 2.3863]], [[2.6094, 2.7917]]]),
            ),
        ]
    )
    def test_gradient(self, e, p, prior_image, expected):
        # Set image
        e.image = Mock(data=xr.DataArray(p))
        # Set prior image
        e.prior_image = prior_image
        # Get result
        res = e.gradient()
        assert e.grad_value.dtype == np.float32
        assert e.grad_value.shape == e.image.data.shape
        assert pytest.approx(res, rel=1e-3) == expected
        assert pytest.approx(e.grad_value, rel=1e-3) == expected

    @pytest.mark.parametrize(
        "p, image_index, prior_image, expected",
        [
            # 2d image
            (
                np.array([[1.], [2.], [3.], [4.]]),
                0,
                1.,
                np.array([[1.], [1.6931], [2.0986], [2.3863]]),
            ),
            (
                np.array([[1., 2., 3., 4.]]),
                0,
                1.,
                np.array([[1., 1.6931, 2.0986, 2.3863]]),
            ),
            (
                np.array([[1, 2], [3, 4]]),
                0,
                1.,
                np.array([[1., 1.6931], [2.0986, 2.3863]]),
            ),
            (
                np.array([[1.], [2.], [3.], [4.]]),
                0,
                np.array([[1.], [1.], [1.], [1.]]),
                np.array([[1.], [1.6931], [2.0986], [2.3863]]),
            ),
            (
                np.array([[1., 2., 3., 4.]]),
                0,
                np.array([[1., 1., 1., 1.]]),
                np.array([[1., 1.6931, 2.0986, 2.3863]]),
            ),
            (
                np.array([[1, 2], [3, 4]]),
                0,
                np.array([[1., 1.], [1., 1.]]),
                np.array([[1., 1.6931], [2.0986, 2.3863]]),
            ),
            # 3d image
            (
                np.array([[[1., 2., 3.], [4., 5., 6.]]]),
                0,
                1.,
                np.array([[[1., 0., 0.], [2.3863, 0., 0.]]]),
            ),
            (
                np.array([[[1.], [2.], [3.]], [[4.], [5.], [6.]]]),
                0,
                1.,
                np.array([[[1.], [1.6931], [2.0986]], [[2.3863], [2.6094], [2.7917]]]),
            ),
            (
                np.array([[[1., 2.]], [[3., 4.]], [[5., 6.]]]),
                0,
                1.,
                np.array([[[1., 0.]], [[2.0986, 0.]], [[2.6094, 0.]]]),
            ),
            # 3d prior image
            (
                np.array([[[1., 2., 3.], [4., 5., 6.]]]),
                0,
                np.array([[[1., 1., 1.], [1., 1., 1.]]]),
                np.array([[[1., 0., 0.], [2.3863, 0., 0.]]]),
            ),
            (
                np.array([[[1.], [2.], [3.]], [[4.], [5.], [6.]]]),
                0,
                np.array([[[1.], [1.], [1.]], [[1.], [1.], [1.]]]),
                np.array([[[1.], [1.6931], [2.0986]], [[2.3863], [2.6094], [2.7917]]]),
            ),
            (
                np.array([[[1., 2.]], [[3., 4.]], [[5., 6.]]]),
                0,
                np.array([[[1., 1.]], [[1., 1.]], [[1., 1.]]]),
                np.array([[[1., 0.]], [[2.0986, 0.]], [[2.6094, 0.]]]),
            ),
            # 2d prior image
            (
                np.array([[[1., 2., 3.], [4., 5., 6.]]]),
                0,
                np.array([[1., 1.]]),
                np.array([[[1., 0., 0.], [2.3863, 0., 0.]]]),
            ),
            (
                np.array([[[1.], [2.], [3.]], [[4.], [5.], [6.]]]),
                0,
                np.array([[1., 1., 1.], [1., 1., 1.]]),
                np.array([[[1.], [1.6931], [2.0986]], [[2.3863], [2.6094], [2.7917]]]),
            ),
            (
                np.array([[[1., 2.]], [[3., 4.]], [[5., 6.]]]),
                0,
                np.array([[1.], [1.], [1.]]),
                np.array([[[1., 0.]], [[2.0986, 0.]], [[2.6094, 0.]]]),
            ),
        ]
    )
    def test_gradient_one_index(self, e, p, image_index, prior_image, expected):
        # Set image
        e.image = Mock(data=xr.DataArray(p))
        # Set image index
        e.image_index = image_index
        # Set prior image
        e.prior_image = prior_image
        # Get result
        res = e.gradient()
        assert e.grad_value.dtype == np.float32
        assert e.grad_value.shape == e.image.data.shape
        assert pytest.approx(res, rel=1e-3) == expected
        assert pytest.approx(e.grad_value, rel=1e-3) == expected

    @pytest.mark.parametrize(
        "p, prior_image, mask_data, expected",
        [
            # 2d image
            (
                np.array([[1.], [2.], [3.], [4.]]),
                1.,
                np.array([[True], [False], [False], [True]]),
                np.array([[1.], [0.], [0.], [2.3863]]),
            ),
            (
                np.array([[1., 2., 3., 4.]]),
                1.,
                np.array([[True, False, False, True]]),
                np.array([[1., 0., 0., 2.3863]]),
            ),
            (
                np.array([[1, 2], [3, 4]]),
                1.,
                np.array([[True, False], [False, True]]),
                np.array([[1., 0.], [0., 2.3863]]),
            ),
            (
                np.array([[1.], [2.], [3.], [4.]]),
                np.array([[1.], [1.], [1.], [1.]]),
                np.array([[True], [False], [False], [True]]),
                np.array([[1.], [0.], [0.], [2.3863]]),
            ),
            (
                np.array([[1., 2., 3., 4.]]),
                np.array([[1., 1., 1., 1.]]),
                np.array([[True, False, True, True]]),
                np.array([[1., 0., 2.0986, 2.3863]]),
            ),
            (
                np.array([[1, 2], [3, 4]]),
                np.array([[1., 1.], [1., 1.]]),
                np.array([[True, True], [False, True]]),
                np.array([[1., 1.6931], [0., 2.3863]]),
            ),
            # 3d image
            (
                np.array([[[1., 2., 3.], [4., 5., 6.]]]),
                1.,
                np.array([[[True, False, True], [False, True, False]]]),
                np.array([[[1., 0., 2.0986], [0., 2.6094, 0.]]]),
            ),
            (
                np.array([[[1.], [2.], [3.]], [[4.], [5.], [6.]]]),
                1.,
                np.array([[[True], [False], [True]], [[False], [True], [False]]]),
                np.array([[[1.], [0.], [2.0986]], [[0.], [2.6094], [0.]]]),
            ),
            (
                np.array([[[1., 2.]], [[3., 4.]], [[5., 6.]]]),
                1.,
                np.array([[[True, True]], [[False, True]], [[True, False]]]),
                np.array([[[1., 1.6931]], [[0., 2.3863]], [[2.6094, 0.]]]),
            ),
            # Different dimensions
            (
                np.array([[[1., 2., 3.], [4., 5., 6.]]]),
                1.,
                np.array([[True, False]]),
                np.array([[[1., 1.6931, 2.0986], [0., 0., 0.]]]),
            ),
            (
                np.array([[[1.], [2.], [3.]], [[4.], [5.], [6.]]]),
                1.,
                np.array([[True, False, True], [True, True, False]]),
                np.array([[[1.], [0.], [2.0986]], [[2.3863], [2.6094], [0.]]]),
            ),
            (
                np.array([[[1., 2.]], [[3., 4.]], [[5., 6.]]]),
                1.,
                np.array([[True], [False], [True]]),
                np.array([[[1., 1.6931]], [[0., 0.]], [[2.6094, 2.7917]]]),
            ),
            # 3d prior image
            (
                np.array([[[1., 2., 3.], [4., 5., 6.]]]),
                np.array([[[1., 1., 1.], [1., 1., 1.]]]),
                np.array([[[True, False, True], [False, True, False]]]),
                np.array([[[1., 0., 2.0986], [0., 2.6094, 0.]]]),
            ),
            (
                np.array([[[1.], [2.], [3.]], [[4.], [5.], [6.]]]),
                np.array([[[1.], [1.], [1.]], [[1.], [1.], [1.]]]),
                np.array([[[True], [False], [True]], [[False], [True], [False]]]),
                np.array([[[1.], [0.], [2.0986]], [[0.], [2.6094], [0.]]]),
            ),
            (
                np.array([[[1., 2.]], [[3., 4.]], [[5., 6.]]]),
                np.array([[[1., 1.]], [[1., 1.]], [[1., 1.]]]),
                np.array([[[True, True]], [[False, True]], [[True, False]]]),
                np.array([[[1., 1.6931]], [[0., 2.3863]], [[2.6094, 0.]]]),
            ),
            # 2d prior image
            (
                np.array([[[1., 2., 3.], [4., 5., 6.]]]),
                np.array([[1., 1.]]),
                np.array([[[True, False, True], [False, True, False]]]),
                np.array([[[1., 0., 2.0986], [0., 2.6094, 0.]]]),
            ),
            (
                np.array([[[1.], [2.], [3.]], [[4.], [5.], [6.]]]),
                np.array([[1., 1., 1.], [1., 1., 1.]]),
                np.array([[[True], [False], [True]], [[False], [True], [False]]]),
                np.array([[[1.], [0.], [2.0986]], [[0.], [2.6094], [0.]]]),
            ),
            (
                np.array([[[1., 2.]], [[3., 4.]], [[5., 6.]]]),
                np.array([[1.], [1.], [1.]]),
                np.array([[[True, True]], [[False, True]], [[True, False]]]),
                np.array([[[1., 1.6931]], [[0., 2.3863]], [[2.6094, 0.]]]),
            ),
        ]
    )
    def test_gradient_mask(self, e, p, prior_image, mock_mask, mask_data, expected):
        # Mock mask
        mask = mock_mask()
        mask.data = mask_data
        # Set image
        e.image = Mock(data=xr.DataArray(p))
        # Set prior image
        e.prior_image = prior_image
        # Get result with mask
        res = e.gradient(mask=mask)
        assert e.grad_value.dtype == np.float32
        assert e.grad_value.shape == e.image.data.shape
        assert pytest.approx(res, rel=1e-3) == expected
        assert pytest.approx(e.grad_value, rel=1e-3) == expected

    @pytest.mark.parametrize(
        "p, image_index, prior_image, mask_data, expected",
        [
            # 2d image
            (
                np.array([[1.], [2.], [3.], [4.]]),
                0,
                1.,
                np.array([[True], [False], [False], [True]]),
                np.array([[1.], [0.], [0.], [2.3863]]),
            ),
            (
                np.array([[1., 2., 3., 4.]]),
                0,
                1.,
                np.array([[True, False, False, True]]),
                np.array([[1., 0., 0., 2.3863]]),
            ),
            (
                np.array([[1, 2], [3, 4]]),
                0,
                1.,
                np.array([[True, False], [False, True]]),
                np.array([[1., 0.], [0., 2.3863]]),
            ),
            (
                np.array([[1.], [2.], [3.], [4.]]),
                0,
                np.array([[1.], [1.], [1.], [1.]]),
                np.array([[True], [False], [False], [True]]),
                np.array([[1.], [0.], [0.], [2.3863]]),
            ),
            (
                np.array([[1., 2., 3., 4.]]),
                0,
                np.array([[1., 1., 1., 1.]]),
                np.array([[True, False, True, True]]),
                np.array([[1., 0., 2.0986, 2.3863]]),
            ),
            (
                np.array([[1, 2], [3, 4]]),
                0,
                np.array([[1., 1.], [1., 1.]]),
                np.array([[True, True], [False, True]]),
                np.array([[1., 1.6931], [0., 2.3863]]),
            ),
            # 3d image
            (
                np.array([[[1., 2., 3.], [4., 5., 6.]]]),
                0,
                1.,
                np.array([[[True, False, True], [False, True, True]]]),
                np.array([[[1., 0., 0.], [0., 0., 0.]]]),
            ),
            (
                np.array([[[1.], [2.], [3.]], [[4.], [5.], [6.]]]),
                0,
                1.,
                np.array([[[True], [False], [True]], [[False], [True], [False]]]),
                np.array([[[1.], [0.], [2.0986]], [[0.], [2.6094], [0.]]]),
            ),
            (
                np.array([[[1., 2.]], [[3., 4.]], [[5., 6.]]]),
                0,
                1.,
                np.array([[[True, True]], [[False, True]], [[True, False]]]),
                np.array([[[1., 0.]], [[0., 0.]], [[2.6094, 0.]]]),
            ),
            # Different dimensions
            (
                np.array([[[1., 2., 3.], [4., 5., 6.]]]),
                0,
                1.,
                np.array([[True, False]]),
                np.array([[[1., 0., 0.], [0., 0., 0.]]]),
            ),
            (
                np.array([[[1.], [2.], [3.]], [[4.], [5.], [6.]]]),
                0,
                1.,
                np.array([[True, False, True], [True, True, False]]),
                np.array([[[1.], [0.], [2.0986]], [[2.3863], [2.6094], [0.]]]),
            ),
            (
                np.array([[[1., 2.]], [[3., 4.]], [[5., 6.]]]),
                0,
                1.,
                np.array([[True], [False], [True]]),
                np.array([[[1., 0.]], [[0., 0.]], [[2.6094, 0.]]]),
            ),
            # 3d prior image
            (
                np.array([[[1., 2., 3.], [4., 5., 6.]]]),
                0,
                np.array([[[1., 1., 1.], [1., 1., 1.]]]),
                np.array([[[True, False, True], [False, True, False]]]),
                np.array([[[1., 0., 0.], [0., 0., 0.]]]),
            ),
            (
                np.array([[[1.], [2.], [3.]], [[4.], [5.], [6.]]]),
                0,
                np.array([[[1.], [1.], [1.]], [[1.], [1.], [1.]]]),
                np.array([[[True], [False], [True]], [[False], [True], [False]]]),
                np.array([[[1.], [0.], [2.0986]], [[0.], [2.6094], [0.]]]),
            ),
            (
                np.array([[[1., 2.]], [[3., 4.]], [[5., 6.]]]),
                0,
                np.array([[[1., 1.]], [[1., 1.]], [[1., 1.]]]),
                np.array([[[True, True]], [[False, True]], [[True, False]]]),
                np.array([[[1., 0.]], [[0., 0.]], [[2.6094, 0.]]]),
            ),
            # 2d prior image
            (
                np.array([[[1., 2., 3.], [4., 5., 6.]]]),
                0,
                np.array([[1., 1.]]),
                np.array([[[True, False, True], [False, True, False]]]),
                np.array([[[1., 0., 0.], [0., 0., 0.]]]),
            ),
            (
                np.array([[[1.], [2.], [3.]], [[4.], [5.], [6.]]]),
                0,
                np.array([[1., 1., 1.], [1., 1., 1.]]),
                np.array([[[True], [False], [True]], [[False], [True], [False]]]),
                np.array([[[1.], [0.], [2.0986]], [[0.], [2.6094], [0.]]]),
            ),
            (
                np.array([[[1., 2.]], [[3., 4.]], [[5., 6.]]]),
                0,
                np.array([[1.], [1.], [1.]]),
                np.array([[[True, True]], [[False, True]], [[True, False]]]),
                np.array([[[1., 0.]], [[0., 0.]], [[2.6094, 0.]]]),
            ),
        ]
    )
    def test_gradient_one_index_mask(
        self, e, p, image_index, prior_image, mock_mask, mask_data, expected
    ):
        # Mock mask
        mask = mock_mask()
        mask.data = mask_data
        # Set image
        e.image = Mock(data=xr.DataArray(p))
        # Set image index
        e.image_index = image_index
        # Set prior image
        e.prior_image = prior_image
        # Get result with mask
        res = e.gradient(mask=mask)
        assert e.grad_value.dtype == np.float32
        assert e.grad_value.shape == e.image.data.shape
        assert pytest.approx(res, rel=1e-3) == expected
        assert pytest.approx(e.grad_value, rel=1e-3) == expected

    @pytest.mark.parametrize(
        "p, shape, chunks", [
            (
                da.from_array(
                    [
                        [[4.1], [9.3], [1.2], [4.8], [9.3], [3.7], [0.6], [2.3]],
                        [[4.3], [6.4], [1.5], [4.7], [2.9], [0.9], [4.5], [6.9]],
                        [[8.8], [3.5], [4.4], [3.5], [7.1], [7.5], [5.7], [7.4]],
                        [[4.8], [9.8], [4.5], [3.9], [9.2], [2.4], [3.7], [4.9]],
                        [[9.8], [8.6], [8.4], [8.3], [7.4], [6.9], [6.2], [2.1]],
                        [[2.9], [3.7], [1.2], [9.5], [6.6], [1.5], [9.8], [4.6]]
                    ],
                    chunks=(3, 4, 1)
                ),
                (6, 8, 1),
                (3, 4, 1),
            ),
            (
                da.from_array(
                    [
                        [[7.5], [2.4], [6.8], [6.1]], [[9.6], [5.8], [8.9], [3.2]],
                        [[7.5], [6.5], [9.6], [2.4]], [[1.3], [2.1], [3.7], [3.5]]
                    ],
                    chunks=(2, 2, 1)
                ),
                (4, 4, 1),
                (2, 2, 1),
            )
        ]
    )
    def test_gradient_chunks(self, e, p, shape, chunks):
        # Set image
        e.image = Mock(data=xr.DataArray(p))
        # Get shape chunks of grad_value
        assert e.grad_value.shape == shape
        assert e.grad_value.chunksize == chunks
        # Get result
        res = e.gradient()
        assert res.dtype == np.float32
        assert res.shape == shape
        assert e.grad_value.shape == shape
        # Chunks of gradient as the image
        assert res.chunksize == chunks
        assert e.grad_value.chunksize == chunks

    @pytest.mark.parametrize(
        "p, shape, chunks, mask_data", [
            (
                da.from_array(
                    [
                        [[4.1], [9.3], [1.2], [4.8], [9.3], [3.7], [0.6], [2.3]],
                        [[4.3], [6.4], [1.5], [4.7], [2.9], [0.9], [4.5], [6.9]],
                        [[8.8], [3.5], [4.4], [3.5], [7.1], [7.5], [5.7], [7.4]],
                        [[4.8], [9.8], [4.5], [3.9], [9.2], [2.4], [3.7], [4.9]],
                        [[9.8], [8.6], [8.4], [8.3], [7.4], [6.9], [6.2], [2.1]],
                        [[2.9], [3.7], [1.2], [9.5], [6.6], [1.5], [9.8], [4.6]]
                    ],
                    chunks=(3, 4, 1)
                ),
                (6, 8, 1),
                (3, 4, 1),
                da.from_array(
                    [
                        [[False], [False], [True], [True], [True], [True], [False], [False]],
                        [[False], [False], [True], [True], [True], [True], [False], [False]],
                        [[True], [True], [True], [True], [True], [True], [True], [True]],
                        [[True], [True], [True], [True], [True], [True], [True], [True]],
                        [[False], [False], [True], [True], [True], [True], [False], [False]],
                        [[False], [False], [True], [True], [True], [True], [False], [False]]
                    ],
                    chunks=(3, 4, 1)
                ),
            ),
            (
                da.from_array(
                    [
                        [[7.5], [2.4], [6.8], [6.1]], [[9.6], [5.8], [8.9], [3.2]],
                        [[7.5], [6.5], [9.6], [2.4]], [[1.3], [2.1], [3.7], [3.5]]
                    ],
                    chunks=(2, 2, 1)
                ),
                (4, 4, 1),
                (2, 2, 1),
                da.from_array(
                    [
                        [[False], [True], [True], [False]], [[True], [True], [True], [True]],
                        [[True], [True], [True], [True]], [[False], [True], [True], [False]]
                    ],
                    chunks=(2, 2, 1)
                ),
            )
        ]
    )
    def test_gradient_mask_chunks(self, e, p, shape, chunks, mock_mask, mask_data):
        # Mock mask
        mask = mock_mask()
        mask.data = mask_data
        # Set image
        e.image = Mock(data=xr.DataArray(p))
        # Get shape chunks of grad_value
        assert e.grad_value.shape == shape
        assert e.grad_value.chunksize == chunks
        # Get result with mask
        res = e.gradient(mask=mask)
        assert res.shape == shape
        assert res.dtype == np.float32
        assert e.grad_value.dtype == np.float32
        assert e.grad_value.shape == shape
        # Chunks of gradient as the image
        assert res.chunksize == chunks
        assert e.grad_value.chunksize == chunks

    @pytest.mark.parametrize(
        "p, image_index, mask_data",
        [
            # 2d image
            (np.ones((8, 9)), -1, np.ones((8, 8))),
            (np.ones((8, 9)), 0, np.ones((8, 8))),
            (np.ones((8, 8)), -1, np.ones((8, 8, 2))),
            (np.ones((8, 9)), 0, np.ones((8, 8, 2))),
            # 3d image
            (np.ones((8, 9, 3)), -1, np.ones((8, 8))),
            (np.ones((8, 9, 3)), 0, np.ones((8, 8))),
            (np.ones((8, 8, 3)), -1, np.ones((8, 8, 2))),
            (np.ones((8, 9, 2)), 0, np.ones((8, 8, 2))),
        ]
    )
    def test_function_incompatible_mask(self, e, p, image_index, mock_mask, mask_data):
        # Mock mask
        mask = mock_mask()
        mask.data = mask_data
        # Set image
        e.image = Mock(data=xr.DataArray(p))
        # Set image index
        e.image_index = image_index
        with pytest.raises(ValueError):
            e.function(mask=mask)

    @pytest.mark.parametrize(
        "p, image_index, mask_data",
        [
            # 2d image
            (np.ones((8, 9)), -1, np.ones((8, 8))),
            (np.ones((8, 9)), 0, np.ones((8, 8))),
            (np.ones((8, 8)), -1, np.ones((8, 8, 2))),
            (np.ones((8, 9)), 0, np.ones((8, 8, 2))),
            # 3d image
            (np.ones((8, 9, 3)), -1, np.ones((8, 8))),
            (np.ones((8, 9, 3)), 0, np.ones((8, 8))),
            (np.ones((8, 8, 3)), -1, np.ones((8, 8, 2))),
            (np.ones((8, 9, 2)), 0, np.ones((8, 8, 2))),
        ]
    )
    def test_gradient_incompatible_mask(self, e, p, image_index, mock_mask, mask_data):
        # Mock mask
        mask = mock_mask()
        mask.data = mask_data
        # Set image
        e.image = Mock(data=xr.DataArray(p))
        # Set image index
        e.image_index = image_index
        with pytest.raises(ValueError):
            e.gradient(mask=mask)
