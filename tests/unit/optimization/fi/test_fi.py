import warnings
from unittest.mock import Mock

import astropy.units as un
import dask.array as da
import numpy as np
import pytest
import xarray as xr
from astropy.units import Quantity

from pyralysis.optimization.fi import Fi


class TestFi:

    class FiMock(Fi):

        def function(self):
            raise NotImplementedError

        def gradient(self):
            raise NotImplementedError

        def prox(self):
            raise NotImplementedError

    @pytest.fixture
    def image(self, request):
        shape = request.param
        data = xr.DataArray(da.ones(shape))
        cs = Quantity([-1.0, 1.0] * un.rad)
        image = Mock(data=data, cellsize=cs)
        return image

    @pytest.mark.parametrize(
        "image, image_index, mask, expected",
        [
            # 2d image, 2d mask
            (((64, 64)), None, Mock(data=np.zeros((64, 64))), True),
            (((64, 64)), None, Mock(data=np.zeros((64, 63))), False),
            (((64, 64)), 0, Mock(data=np.zeros((64, 64))), True),
            (((64, 64)), 0, Mock(data=np.zeros((64, 63))), False),
            # 2d image, 3d mask
            (((64, 64)), None, Mock(data=np.zeros((64, 64, 3))), False),
            (((64, 64)), None, Mock(data=np.zeros((64, 63, 3))), False),
            (((64, 64)), 0, Mock(data=np.zeros((64, 64, 3))), True),
            (((64, 64)), 0, Mock(data=np.zeros((64, 63, 3))), False),
            # 3d image, 2d mask
            (((32, 32, 2)), None, Mock(data=np.zeros((32, 32))), True),
            (((32, 32, 2)), None, Mock(data=np.zeros((32, 63))), False),
            (((32, 32, 2)), 0, Mock(data=np.zeros((32, 32))), True),
            (((32, 32, 2)), 0, Mock(data=np.zeros((32, 63))), False),
            # 3d image, 3d mask
            (((32, 32, 2)), None, Mock(data=np.zeros((32, 32, 3))), False),
            (((32, 32, 2)), None, Mock(data=np.zeros((32, 63, 3))), False),
            (((32, 32, 2)), 0, Mock(data=np.zeros((32, 32, 3))), True),
            (((32, 32, 2)), 0, Mock(data=np.zeros((32, 63, 3))), False),
        ],
        indirect=["image"]
    )
    def test_mask_shape_check(self, image, image_index, mask, expected):
        if image_index is None:
            fi = self.FiMock(image=image)
        else:
            fi = self.FiMock(image=image, image_index=image_index)
        check = fi.mask_shape_check(mask)
        assert check == expected

    def test_mask_shape_check_no_image(self):
        fi = self.FiMock()
        mask = Mock(data=np.zeros((3, 3)))
        with pytest.warns(UserWarning, match=r"No image.*"):
            check = fi.mask_shape_check(mask)
            assert check == False

    @pytest.mark.parametrize(
        "image, expected_shape", [
            (((64, 64)), (64, 64)),
            (((8, 8, 2)), (8, 8, 2)),
            (((16, 16, 2, 3)), (16, 16, 2, 3)),
        ],
        indirect=["image"]
    )
    def test_set_image(self, image, expected_shape):
        # Check image shape
        assert image.data.shape == expected_shape
        # Test that the image and gradient preserve the input image shape
        fi = self.FiMock(image=image)
        assert fi.image.data.shape == expected_shape
        assert fi.grad_value.shape == expected_shape
        # New image to update fi
        new_shape = (32, 32, 2)
        data = xr.DataArray(da.ones(new_shape))
        _image = Mock(data=data)
        # Update image
        fi.image = _image
        # Test that the shape of the shape of the image and gradient updates
        assert fi.image.data.shape == new_shape
        assert fi.grad_value.shape == new_shape

    @pytest.mark.parametrize(
        "image, image_index, expected_shape", [
            (((16, 16)), -1, (16, 16)),
            (((16, 16)), 1, (16, 16)),
            (((16, 16, 2)), -1, (16, 16, 2)),
            (((16, 16, 2)), 1, (16, 16, 2)),
        ],
        indirect=["image"]
    )
    def test_set_image_index(self, image, image_index, expected_shape):
        # Check image shape
        assert image.data.shape == expected_shape
        # Test that the image does not change shape even when the index is different of -1
        fi = self.FiMock(image=image)
        assert fi.image_index == -1
        assert fi.grad_value.shape == expected_shape
        # Change image_index
        fi.image_index = image_index
        assert fi.image.data.shape == expected_shape
        # Test that the gradient has the same shape as the image
        assert fi.grad_value.shape == expected_shape

    @pytest.mark.parametrize(
        "image, image_index, out_shape, expected", [
            (((16, 16)), -1, (16, 16), da.full((16, 16), 2.0)),
            (((16, 16)), 0, (16, 16), da.full((16, 16), 2.0)),
            (((16, 16, 2)), -1, (16, 16, 2), da.full((16, 16, 2), 2.0)),
            (
                ((16, 16, 2)), 1, (16, 16, 2), da.tile(np.array([[[0.0, 2.0]]]),
                                                       (16, 16, 1)).rechunk("auto")
            ),
        ],
        indirect=["image"]
    )
    def test_add_gradient(self, image, image_index, out_shape, expected):
        # Test that only adds in certain image index
        fi = self.FiMock(image=image, image_index=image_index)
        fi._grad_value = da.full_like(fi.grad_value, 2.0)

        out = da.zeros(out_shape, dtype=np.float32)
        fi.add_gradient(out)

        assert (out == expected).compute().all()

    @pytest.mark.parametrize(
        "image", [
            ((64, 64)),
            ((8, 8, 2)),
            ((16, 16, 2, 3)),
        ], indirect=["image"]
    )
    def test_normalize(self, image):
        fi = self.FiMock(image=image, normalize=True)
        assert fi.normalize == True

    @pytest.mark.parametrize(
        "image", [
            ((64, 64)),
            ((8, 8, 2)),
            ((16, 16, 2, 3)),
        ], indirect=["image"]
    )
    def test_set_normalize(self, image):
        fi = self.FiMock(image=image)
        fi.normalize = True
        assert fi.normalize == True
