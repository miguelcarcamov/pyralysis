from unittest.mock import Mock

import dask.array as da
import numpy as np
import pytest
import xarray as xr

from pyralysis.optimization.fi import L1Norm


class TestL1Norm:

    @pytest.fixture
    def l1(self):
        _l1 = L1Norm()
        return _l1

    def test_init(self, l1):
        assert l1.image is None
        assert l1.penalization_factor == 1.0
        assert l1.image_index == -1
        assert l1.func_value == 0.0
        assert l1.grad_value == None
        assert l1.normalize == False

    @pytest.mark.parametrize(
        "p, expected",
        [
            # 2d image
            (np.array([[10]]), 10),
            (np.array([[0, -11, 2]]), 13),
            (np.array([[0, 11, 2]]), 13),
            (np.array([[13.3]], dtype=np.float32), 13.3),
            (np.array([[-13.3]], dtype=np.float32), 13.3),
            (np.array([[0, 13.3, 2.2, 7.6]], dtype=np.float32), 23.1),
            (np.array([[0, -13.3, 2.2, 7.6]], dtype=np.float32), 23.1),
            # 3d image
            (np.array([[[10]]]), 10),
            (np.array([[[0, -11, 2]]]), 13),
            (np.array([[[0, 11, 2]]]), 13),
            (np.array([[[13.3]]], dtype=np.float32), 13.3),
            (np.array([[[-13.3]]], dtype=np.float32), 13.3),
            (np.array([[[0, 13.3, 2.2, 7.6]]], dtype=np.float32), 23.1),
            (np.array([[[0, -13.3, 2.2, 7.6]]], dtype=np.float32), 23.1),
        ]
    )
    def test_function(self, l1, p, expected):
        # Set image data
        l1.image = Mock(data=xr.DataArray(p))
        # Get result
        res = l1.function()
        assert pytest.approx(res) == expected

    @pytest.mark.parametrize(
        "p, image_index, expected",
        [
            # 2d image
            (np.array([[10, 20]]), 0, 30),
            (np.array([[10, 20]]), 1, 30),
            (np.array([[0, 1], [-11, 12], [2, 3]]), 0, 29),
            (np.array([[0, 1], [-11, 12], [2, 3]]), 1, 29),
            (np.array([[0, 1], [11, 12], [2, 3]]), 0, 29),
            (np.array([[0., 1.], [13.3, 17.], [2.2, 3.1], [7.6, 8.2]]), 0, 52.4),
            # 3d image
            (np.array([[[10, 20]]]), 0, 10),
            (np.array([[[10, 20]]]), 1, 20),
            (np.array([[[0, 1]], [[-11, 12]], [[2, 3]]]), 0, 13),
            (np.array([[[0, 1]], [[-11, 12]], [[2, 3]]]), 1, 16),
            (np.array([[[0, 1]], [[11, 12]], [[2, 3]]]), 0, 13),
            (np.array([[[0., 1.]], [[13.3, 17.]], [[2.2, 3.1]], [[7.6, 8.2]]]), 0, 23.1),
        ]
    )
    def test_function_one_index(self, l1, p, image_index, expected):
        # Set image data
        l1.image = Mock(data=xr.DataArray(p))
        # Set image index in L1Norm
        l1.image_index = image_index
        # Get result
        res = l1.function()
        assert pytest.approx(res) == expected
        assert pytest.approx(l1.func_value) == expected

    def test_normalization(self, l1):
        l1.normalize = True
        assert l1.normalize == True

    @pytest.mark.parametrize(
        "p, mask_data, expected",
        [
            # 2d image
            (np.array([[10]]), np.array([[True]]), 10),
            (np.array([[10]]), np.array([[False]]), 0),
            (np.array([[0, -11, 2]]), np.array([[True, True, True]]), 13),
            (np.array([[0, -11, 2]]), np.array([[False, True, False]]), 11),
            (np.array([[0, 11, 2]]), np.array([[True, True, True]]), 13),
            (np.array([[0, 11, 2]]), np.array([[False, False, True]]), 2),
            (np.array([[13.3]], dtype=np.float32), np.array([[True]], dtype=np.float32), 13.3),
            (np.array([[13.3]], dtype=np.float32), np.array([[False]], dtype=np.float32), 0.0),
            (np.array([[-13.3]], dtype=np.float32), np.array([[True]], dtype=np.float32), 13.3),
            (
                np.array([[0, 13.3, 2.2, 7.6]],
                         dtype=np.float32), np.array([[True, True, True, True]]), 23.1
            ),
            (
                np.array([[0, 13.3, 2.2, 7.6]],
                         dtype=np.float32), np.array([[False, False, True, True]]), 9.8
            ),
            (
                np.array([[0, 13.3, 2.2, 7.6]],
                         dtype=np.float32), np.array([[False, False, True, False]]), 2.2
            ),
            (
                np.array([[0, -13.3, 2.2, 7.6]],
                         dtype=np.float32), np.array([[True, True, True, True]]), 23.1
            ),
            (
                np.array([[0, -13.3, 2.2, 7.6]],
                         dtype=np.float32), np.array([[False, True, False, False]]), 13.3
            ),
            (
                np.array([[1., 3., 5.], [7., 9., 11.], [13., 15., 17.]]),
                np.array([[False, True, False], [True, True, True], [False, True, False]]), 45.0
            ),
            # 3d image
            (np.array([[[10]]]), np.array([[[True]]]), 10),
            (np.array([[[10]]]), np.array([[[False]]]), 0),
            (np.array([[[0, -11, 2]]]), np.array([[[True, True, True]]]), 13),
            (np.array([[[0, -11, 2]]]), np.array([[[False, True, False]]]), 11),
            (np.array([[[0, 11, 2]]]), np.array([[[True, True, True]]]), 13),
            (np.array([[[0, 11, 2]]]), np.array([[[False, False, True]]]), 2),
            (np.array([[[13.3]]], dtype=np.float32), np.array([[[True]]], dtype=np.float32), 13.3),
            (np.array([[[13.3]]], dtype=np.float32), np.array([[[False]]], dtype=np.float32), 0.0),
            (np.array([[[-13.3]]], dtype=np.float32), np.array([[[True]]], dtype=np.float32), 13.3),
            (
                np.array([[[0, 13.3, 2.2, 7.6]]],
                         dtype=np.float32), np.array([[[True, True, True, True]]]), 23.1
            ),
            (
                np.array([[[0, 13.3, 2.2, 7.6]]],
                         dtype=np.float32), np.array([[[False, False, True, True]]]), 9.8
            ),
            (
                np.array([[[0, 13.3, 2.2, 7.6]]],
                         dtype=np.float32), np.array([[[False, False, True, False]]]), 2.2
            ),
            (
                np.array([[[0, -13.3, 2.2, 7.6]]],
                         dtype=np.float32), np.array([[[True, True, True, True]]]), 23.1
            ),
            (
                np.array([[[0, -13.3, 2.2, 7.6]]],
                         dtype=np.float32), np.array([[[False, True, False, False]]]), 13.3
            ),
            (
                np.array(
                    [
                        [[1., 2.], [3., 4.], [5., 6.]], [[7., 8.], [9., 10.], [11., 12.]],
                        [[13., 14.], [15., 16.], [17., 18.]]
                    ]
                ), np.array([[False, True, False], [True, True, True], [False, True, False]]), 95.0
            ),
        ]
    )
    def test_function_mask(self, l1, p, mock_mask, mask_data, expected):
        # Mock mask
        mask = mock_mask()
        mask.data = mask_data
        # Set image
        l1.image = Mock(data=xr.DataArray(p))
        # Get result with mask
        res = l1.function(mask=mask)
        assert pytest.approx(res) == expected
        assert pytest.approx(l1.func_value) == expected

    @pytest.mark.parametrize(
        "p, image_index, mask_data, expected",
        [
            # 2d image
            (np.array([[0., 1.], [2., 3.]]), 0, np.array([[True, True], [False, True]]), 4.),
            (np.array([[0., 1.], [2., 3.]]), 1, np.array([[True, True], [False, True]]), 4.),
            (
                np.array([[0., 1.], [2., 3.]]), 0,
                np.array([[[True, True], [False, True]], [[True, False], [True, False]]]), 5.
            ),
            (
                np.array([[0., 1.], [2., 3.]]), 1,
                np.array([[[True, True], [False, True]], [[True, False], [True, False]]]), 1.
            ),
            # 3d image
            (
                np.array([[[0., 1.], [2., 3.]], [[4., 5.], [6., 7.]]]
                         ), 0, np.array([[True, True], [False, True]]), 8.
            ),
            (
                np.array([[[0., 1.], [2., 3.]], [[4., 5.], [6., 7.]]]
                         ), 1, np.array([[True, True], [False, True]]), 11.
            ),
            (
                np.array([[[0., 1.], [2., 3.]], [[4., 5.], [6., 7.]]]), 0,
                np.array([[[True, True], [False, True]], [[True, False], [True, False]]]), 10.
            ),
            (
                np.array([[[0., 1.], [2., 3.]], [[4., 5.], [6., 7.]]]), 1,
                np.array([[[True, True], [False, True]], [[True, False], [True, False]]]), 4.
            ),
        ]
    )
    def test_function_one_index_mask(self, l1, p, image_index, mock_mask, mask_data, expected):
        # Mock mask
        mask = mock_mask()
        mask.data = mask_data
        # Set image
        l1.image = Mock(data=xr.DataArray(p))
        # Set index
        l1.image_index = image_index
        # Get result with mask
        res = l1.function(mask=mask)
        assert pytest.approx(res) == expected
        assert pytest.approx(l1.func_value) == expected

    @pytest.mark.parametrize(
        "p, expected",
        [
            # 2d image
            ([[10]], [[1]]),
            (np.array([[0, 11, 2]]), np.array([[0, 1, 1]])),
            (np.array([[0, -11, 2]]), np.array([[0, -1, 1]])),
            (np.array([[13.3]], dtype=np.float32), np.array([[1]])),
            (np.array([[-13.3]], dtype=np.float32), np.array([[-1]])),
            (np.array([[0, 13.3, 2.2, 7.6]], dtype=np.float32), np.array([[0, 1, 1, 1]])),
            (np.array([[0, -13.3, 2.2, 7.6]], dtype=np.float32), np.array([[0, -1, 1, 1]])),
            # 3d image
            ([[[10]]], [[[1]]]),
            (np.array([[[0, 11, 2]]]), np.array([[[0, 1, 1]]])),
            (np.array([[[0, -11, 2]]]), np.array([[[0, -1, 1]]])),
            (np.array([[[13.3]]], dtype=np.float32), np.array([[[1]]])),
            (np.array([[[-13.3]]], dtype=np.float32), np.array([[[-1]]])),
            (np.array([[[0, 13.3, 2.2, 7.6]]], dtype=np.float32), np.array([[[0, 1, 1, 1]]])),
            (np.array([[[0, -13.3, 2.2, 7.6]]], dtype=np.float32), np.array([[[0, -1, 1, 1]]])),
        ]
    )
    def test_gradient(self, l1, p, expected):
        # Set image data
        l1.image = Mock(data=xr.DataArray(p))
        # Get result
        res = l1.gradient()
        assert l1.grad_value.shape == l1.image.data.shape
        assert l1.grad_value.dtype == np.float32
        assert pytest.approx(res) == expected
        assert pytest.approx(l1.grad_value) == expected

    @pytest.mark.parametrize(
        "p, image_index, expected",
        [
            # 2d image
            (
                np.array([[10.0, 0.0, 11.3], [-13.3, 7.6, 0.0], [0.0, -11.7, -4.9]]),
                0,
                np.array([[1.0, 0.0, 1.0], [-1.0, 1.0, 0.0], [0.0, -1.0, -1.0]]),
            ),
            (
                np.array([[10.0, 0.0, 11.3], [-13.3, 7.6, 0.0], [0.0, -11.7, -4.9]]),
                1,
                np.array([[1.0, 0.0, 1.0], [-1.0, 1.0, 0.0], [0.0, -1.0, -1.0]]),
            ),
            (
                np.array([[10.0, 0.0, 11.3], [-13.3, 7.6, 0.0], [0.0, -11.7, -4.9]]),
                2,
                np.array([[1.0, 0.0, 1.0], [-1.0, 1.0, 0.0], [0.0, -1.0, -1.0]]),
            ),
            # 3d image
            (
                np.array([[[10.0, 0.0, 11.3]], [[-13.3, 7.6, 0.0]], [[0.0, -11.7, -4.9]]]),
                0,
                np.array([[[1., 0., 0.]], [[-1., 0., 0.]], [[0., 0., 0.]]]),
            ),
            (
                np.array([[[10.0, 0.0, 11.3]], [[-13.3, 7.6, 0.0]], [[0.0, -11.7, -4.9]]]),
                1,
                np.array([[[0., 0., 0.]], [[0., 1., 0.]], [[0., -1., 0.]]]),
            ),
            (
                np.array([[[10.0, 0.0, 11.3]], [[-13.3, 7.6, 0.0]], [[0.0, -11.7, -4.9]]]),
                2,
                np.array([[[0., 0., 1.]], [[0., 0., 0.]], [[0., 0., -1.]]]),
            ),
        ]
    )
    def test_gradient_one_index(self, l1, p, image_index, expected):
        # Set image
        l1.image = Mock(data=xr.DataArray(p))
        # Set image index
        l1.image_index = image_index
        # Get result
        res = l1.gradient()
        assert res.ndim == l1.image.data.ndim
        assert res.shape == l1.image.data.shape
        assert l1.grad_value.shape == l1.image.data.shape
        assert l1.grad_value.dtype == np.float32
        assert pytest.approx(res) == expected
        assert pytest.approx(l1.grad_value) == expected

    @pytest.mark.parametrize(
        "p, mask_data, expected",
        [
            # 2d image
            (np.array([[10]]), np.array([[True]]), np.array([[1]])),
            (np.array([[10]]), np.array([[False]]), np.array([[0]])),
            (
                np.array([[1., 2.], [3., 4.], [5., 6.]]
                         ), np.array([[True, False], [False, True], [True, True]]
                                     ), np.array([[1, 0], [0, 1], [1, 1]])
            ),
            # 3d image
            (np.array([[[10]]]), np.array([[[True]]]), [[[1]]]),
            (np.array([[[10]]]), np.array([[[False]]]), [[[0]]]),
            (np.array([[[0, 11, 2]]]), np.array([[[True, True, True]]]), np.array([[[0, 1, 1]]])),
            (np.array([[[0, -11, 2]]]), np.array([[[True, True, True]]]), np.array([[[0, -1, 1]]])),
            (
                np.array([[[0, -11, 2]]]), np.array([[[False, False, True]]]
                                                    ), np.array([[[0, 0, 1]]])
            ),
            (
                np.array([[[0, -11, 2]]]), np.array([[[False, True, False]]]
                                                    ), np.array([[[0, -1, 0]]])
            ),
            (np.array([[[13.3]]], dtype=np.float32), np.array([[[True]]]), np.array([[[1]]])),
            (np.array([[[-13.3]]], dtype=np.float32), np.array([[[True]]]), np.array([[[-1]]])),
            (np.array([[[-13.3]]], dtype=np.float32), np.array([[[False]]]), np.array([[[0]]])),
            (
                np.array([[[0, 13.3, 2.2, 7.6]]], dtype=np.float32),
                np.array([[[True, True, True, True]]]), np.array([[[0, 1, 1, 1]]])
            ),
            (
                np.array([[[0, -13.3, 2.2, 7.6]]], dtype=np.float32),
                np.array([[[True, True, True, True]]]), np.array([[[0, -1, 1, 1]]])
            ),
            (
                np.array([[[0, -13.3, 2.2, 7.6]]], dtype=np.float32),
                np.array([[[False, False, True, True]]]), np.array([[[0, 0, 1, 1]]])
            ),
            (
                np.array([[[0, -13.3, 2.2, 7.6]]], dtype=np.float32),
                np.array([[[False, True, True, False]]]), np.array([[[0, -1, 1, 0]]])
            ),
            (
                np.array([[[0, -13.3, 2.2, 7.6]]], dtype=np.float32),
                np.array([[[False, True, False, False]]]), np.array([[[0, -1, 0, 0]]])
            ),
            (
                np.array(
                    [
                        [[1., 2.], [3., 4.], [5., 6.]], [[7., 8.], [9., 10.], [11., 12.]],
                        [[13., 14.], [15., 16.], [17., 18.]]
                    ]
                ),
                np.array([[False, True, False], [True, True, True], [False, True, False]]),
                np.array(
                    [
                        [[0., 0.], [1., 1.], [0., 0.]], [[1., 1.], [1., 1.], [1., 1.]],
                        [[0., 0.], [1., 1.], [0., 0.]]
                    ]
                ),
            ),
        ]
    )
    def test_gradient_mask(self, l1, p, mock_mask, mask_data, expected):
        # Mock mask
        mask = mock_mask()
        mask.data = mask_data
        # Set image
        l1.image = Mock(data=xr.DataArray(p))
        # Get result with mask
        res = l1.gradient(mask=mask)
        assert l1.grad_value.shape == l1.image.data.shape
        assert l1.grad_value.dtype == np.float32
        assert pytest.approx(res) == expected
        assert pytest.approx(l1.grad_value) == expected

    @pytest.mark.parametrize(
        "p, image_index, mask_data, expected",
        [
            # 2d image
            (np.array([[10]]), 0, np.array([[True]]), np.array([[1]])),
            (np.array([[10]]), 1, np.array([[True]]), np.array([[1]])),
            (np.array([[10]]), 0, np.array([[False]]), np.array([[0]])),
            (np.array([[10]]), 1, np.array([[False]]), np.array([[0]])),
            (
                np.array([[1., 2.], [3., 4.], [5., 6.]]),
                0,
                np.array([[True, False], [False, True], [True, True]]),
                np.array([[1, 0], [0, 1], [1, 1]]),
            ),
            (
                np.array([[1., 2.], [3., 4.], [5., 6.]]),
                1,
                np.array([[True, False], [False, True], [True, True]]),
                np.array([[1, 0], [0, 1], [1, 1]]),
            ),
            (
                np.array([[0., 1.], [2., 3.]]),
                0,
                np.array([[[True, True], [False, True]], [[True, False], [True, False]]]),
                np.array([[0., 0.], [1., 1.]]),
            ),
            (
                np.array([[0., 1.], [2., 3.]]),
                1,
                np.array([[[True, True], [False, True]], [[True, False], [True, False]]]),
                np.array([[0., 1.], [0., 0.]]),
            ),
            # 3d image
            (np.array([[[10]]]), 0, np.array([[[True]]]), [[[1]]]),
            (np.array([[[10]]]), 0, np.array([[[False]]]), [[[0]]]),
            (
                np.array([[[0, 11, 2]]]),
                0,
                np.array([[[True, True, True]]]),
                np.array([[[0, 0, 0]]]),
            ),
            (
                np.array([[[0, -11, 2]]]),
                1,
                np.array([[[True, True, True]]]),
                np.array([[[0, -1, 0]]]),
            ),
            (
                np.array([[[0, -11, 2]]]),
                2,
                np.array([[[True, True, True]]]),
                np.array([[[0, 0, 1]]]),
            ),
            (
                np.array(
                    [
                        [[1., 2.], [3., 4.], [5., 6.]], [[7., 8.], [9., 10.], [11., 12.]],
                        [[13., 14.], [15., 16.], [17., 18.]]
                    ]
                ),
                0,
                np.array([[False, True, False], [True, True, True], [False, True, False]]),
                np.array(
                    [
                        [[0., 0.], [1., 0.], [0., 0.]], [[1., 0.], [1., 0.], [1., 0.]],
                        [[0., 0.], [1., 0.], [0., 0.]]
                    ]
                ),
            ),
            (
                np.array(
                    [
                        [[1., 2.], [3., 4.], [5., 6.]], [[7., 8.], [9., 10.], [11., 12.]],
                        [[13., 14.], [15., 16.], [17., 18.]]
                    ]
                ),
                1,
                np.array([[False, True, False], [True, True, True], [False, True, False]]),
                np.array(
                    [
                        [[0., 0.], [0., 1.], [0., 0.]], [[0., 1.], [0., 1.], [0., 1.]],
                        [[0., 0.], [0., 1.], [0., 0.]]
                    ]
                ),
            ),
            (
                np.array([[[10.0, 0.0, 11.3]], [[-13.3, 7.6, 0.0]], [[0.0, -11.7, -4.9]]]),
                0,
                np.array([[[False, True, True]], [[True, False, False]], [[True, False, True]]]),
                np.array([[[0., 0., 0.]], [[-1., 0., 0.]], [[0., 0., 0.]]]),
            ),
            (
                np.array([[[10.0, 0.0, 11.3]], [[-13.3, 7.6, 0.0]], [[0.0, -11.7, -4.9]]]),
                1,
                np.array([[[False, True, True]], [[True, False, False]], [[True, False, True]]]),
                np.array([[[0., 0., 0.]], [[0., 0., 0.]], [[0., 0., 0.]]]),
            ),
            (
                np.array([[[10.0, 0.0, 11.3]], [[-13.3, 7.6, 0.0]], [[0.0, -11.7, -4.9]]]),
                2,
                np.array([[[False, True, True]], [[True, False, False]], [[True, False, True]]]),
                np.array([[[0., 0., 1.]], [[0., 0., 0.]], [[0., 0., -1.]]]),
            ),
        ]
    )
    def test_gradient_one_index_mask(self, l1, p, image_index, mock_mask, mask_data, expected):
        # Mock mask
        mask = mock_mask()
        mask.data = mask_data
        # Set image
        l1.image = Mock(data=xr.DataArray(p))
        # Set image_index
        l1.image_index = image_index
        # Get result with mask
        res = l1.gradient(mask=mask)
        assert l1.grad_value.shape == l1.image.data.shape
        assert l1.grad_value.dtype == np.float32
        assert pytest.approx(res) == expected
        assert pytest.approx(l1.grad_value) == expected

    @pytest.mark.parametrize(
        "p, shape, chunks", [
            (
                da.from_array(
                    [
                        [[4.1], [9.3], [1.2], [4.8], [9.3], [3.7], [0.6], [2.3]],
                        [[4.3], [6.4], [1.5], [4.7], [2.9], [0.9], [4.5], [6.9]],
                        [[8.8], [3.5], [4.4], [3.5], [7.1], [7.5], [5.7], [7.4]],
                        [[4.8], [9.8], [4.5], [3.9], [9.2], [2.4], [3.7], [4.9]],
                        [[9.8], [8.6], [8.4], [8.3], [7.4], [6.9], [6.2], [2.1]],
                        [[2.9], [3.7], [1.2], [9.5], [6.6], [1.5], [9.8], [4.6]]
                    ],
                    chunks=(3, 4, 1)
                ),
                (6, 8, 1),
                (3, 4, 1),
            ),
            (
                da.from_array(
                    [
                        [[7.5], [2.4], [6.8], [6.1]], [[9.6], [5.8], [8.9], [3.2]],
                        [[7.5], [6.5], [9.6], [2.4]], [[1.3], [2.1], [3.7], [3.5]]
                    ],
                    chunks=(2, 2, 1)
                ),
                (4, 4, 1),
                (2, 2, 1),
            )
        ]
    )
    def test_gradient_chunks(self, l1, p, shape, chunks):
        # Set image
        l1.image = Mock(data=xr.DataArray(p))
        # Get shape chunks of grad_value
        assert l1.grad_value.shape == shape
        assert l1.grad_value.dtype == np.float32
        assert l1.grad_value.chunksize == chunks
        # Get result
        res = l1.gradient()
        assert res.shape == shape
        assert l1.grad_value.shape == shape
        # Chunks of gradient as the image
        assert res.chunksize == chunks
        assert l1.grad_value.chunksize == chunks

    @pytest.mark.parametrize(
        "p, shape, chunks, mask_data", [
            (
                da.from_array(
                    [
                        [[4.1], [9.3], [1.2], [4.8], [9.3], [3.7], [0.6], [2.3]],
                        [[4.3], [6.4], [1.5], [4.7], [2.9], [0.9], [4.5], [6.9]],
                        [[8.8], [3.5], [4.4], [3.5], [7.1], [7.5], [5.7], [7.4]],
                        [[4.8], [9.8], [4.5], [3.9], [9.2], [2.4], [3.7], [4.9]],
                        [[9.8], [8.6], [8.4], [8.3], [7.4], [6.9], [6.2], [2.1]],
                        [[2.9], [3.7], [1.2], [9.5], [6.6], [1.5], [9.8], [4.6]]
                    ],
                    chunks=(3, 4, 1)
                ),
                (6, 8, 1),
                (3, 4, 1),
                da.from_array(
                    [
                        [[False], [False], [True], [True], [True], [True], [False], [False]],
                        [[False], [False], [True], [True], [True], [True], [False], [False]],
                        [[True], [True], [True], [True], [True], [True], [True], [True]],
                        [[True], [True], [True], [True], [True], [True], [True], [True]],
                        [[False], [False], [True], [True], [True], [True], [False], [False]],
                        [[False], [False], [True], [True], [True], [True], [False], [False]]
                    ],
                    chunks=(3, 4, 1)
                ),
            ),
            (
                da.from_array(
                    [
                        [[7.5], [2.4], [6.8], [6.1]], [[9.6], [5.8], [8.9], [3.2]],
                        [[7.5], [6.5], [9.6], [2.4]], [[1.3], [2.1], [3.7], [3.5]]
                    ],
                    chunks=(2, 2, 1)
                ),
                (4, 4, 1),
                (2, 2, 1),
                da.from_array(
                    [
                        [[False], [True], [True], [False]], [[True], [True], [True], [True]],
                        [[True], [True], [True], [True]], [[False], [True], [True], [False]]
                    ],
                    chunks=(2, 2, 1)
                ),
            )
        ]
    )
    def test_gradient_mask_chunks(self, l1, p, shape, chunks, mock_mask, mask_data):
        # Mock mask
        mask = mock_mask()
        mask.data = mask_data
        # Set image
        l1.image = Mock(data=xr.DataArray(p))
        assert l1.grad_value.dtype == np.float32
        # Get shape chunks of grad_value
        assert l1.grad_value.shape == shape
        assert l1.grad_value.chunksize == chunks
        # Get result with mask
        res = l1.gradient(mask=mask)
        assert res.shape == shape
        assert l1.grad_value.shape == shape
        # Chunks of gradient as the image
        assert res.chunksize == chunks
        assert l1.grad_value.chunksize == chunks

    @pytest.mark.parametrize(
        "p, image_index, mask_data",
        [
            # 2d image
            (np.ones((8, 9)), -1, np.ones((8, 8))),
            (np.ones((8, 9)), 0, np.ones((8, 8))),
            (np.ones((8, 8)), -1, np.ones((8, 8, 2))),
            (np.ones((8, 9)), 0, np.ones((8, 8, 2))),
            # 3d image
            (np.ones((8, 9, 3)), -1, np.ones((8, 8))),
            (np.ones((8, 9, 3)), 0, np.ones((8, 8))),
            (np.ones((8, 8, 3)), -1, np.ones((8, 8, 2))),
            (np.ones((8, 9, 2)), 0, np.ones((8, 8, 2))),
        ]
    )
    def test_function_incompatible_mask(self, l1, p, image_index, mock_mask, mask_data):
        # Mock mask
        mask = mock_mask()
        mask.data = mask_data
        # Set image
        l1.image = Mock(data=xr.DataArray(p))
        # Set image index
        l1.image_index = image_index
        with pytest.raises(ValueError):
            l1.function(mask=mask)

    @pytest.mark.parametrize(
        "p, image_index, mask_data",
        [
            # 2d image
            (np.ones((8, 9)), -1, np.ones((8, 8))),
            (np.ones((8, 9)), 0, np.ones((8, 8))),
            (np.ones((8, 8)), -1, np.ones((8, 8, 2))),
            (np.ones((8, 9)), 0, np.ones((8, 8, 2))),
            # 3d image
            (np.ones((8, 9, 3)), -1, np.ones((8, 8))),
            (np.ones((8, 9, 3)), 0, np.ones((8, 8))),
            (np.ones((8, 8, 3)), -1, np.ones((8, 8, 2))),
            (np.ones((8, 9, 2)), 0, np.ones((8, 8, 2))),
        ]
    )
    def test_gradient_incompatible_mask(self, l1, p, image_index, mock_mask, mask_data):
        # Mock mask
        mask = mock_mask()
        mask.data = mask_data
        # Set image
        l1.image = Mock(data=xr.DataArray(p))
        # Set image index
        l1.image_index = image_index
        with pytest.raises(ValueError):
            l1.gradient(mask=mask)

    # ---- New tests for the prox operator using Dask arrays ----

    @pytest.mark.parametrize(
        "p, lam, expected",
        [
            # 2D image tests:
            (
                da.from_array(np.array([[10, -2, 0]]), chunks=np.array([[10, -2, 0]]).shape), 1,
                da.from_array(np.array([[9, -1, 0]]), chunks=np.array([[9, -1, 0]]).shape)
            ),
            (
                da.from_array(np.array([[0, 5, -3]]), chunks=np.array([[0, 5, -3]]).shape), 2,
                da.from_array(np.array([[0, 3, -1]]), chunks=np.array([[0, 3, -1]]).shape)
            ),
            # When lam equals the absolute value, the result should be zero.
            (
                da.from_array(np.array([[10, -2, 0]]), chunks=np.array([[10, -2, 0]]).shape), 10,
                da.from_array(np.array([[0, 0, 0]]), chunks=np.array([[0, 0, 0]]).shape)
            ),
        ]
    )
    def test_prox_2d(self, l1, p, lam, expected):
        l1.image = Mock(data=xr.DataArray(p))
        res = l1.prox(lam)
        np.testing.assert_allclose(res.compute(), expected.compute(), rtol=1e-5)

    @pytest.mark.parametrize(
        "p, image_index, lam, expected",
        [
            # 3D image tests with channel selection:
            (
                da.from_array(np.array([[[10, -2, 5]]]), chunks=np.array([[[10, -2, 5]]]).shape), 0,
                2, da.from_array(np.array([[[8, 0, 0]]]), chunks=np.array([[[8, 0, 0]]]).shape)
            ),
            (
                da.from_array(np.array([[[10, -2, 5]]]), chunks=np.array([[[10, -2, 5]]]).shape), 1,
                2, da.from_array(np.array([[[0, 0, 0]]]), chunks=np.array([[[0, 0, 0]]]).shape)
            ),
            (
                da.from_array(np.array([[[10, -2, 5]]]), chunks=np.array([[[10, -2, 5]]]).shape), 2,
                3, da.from_array(np.array([[[0, 0, 2]]]), chunks=np.array([[[0, 0, 2]]]).shape)
            ),
            # Additional 3D case: multiple rows.
            (
                da.from_array(
                    np.array([[[5, -6, 10], [0, 3, -2]]]),
                    chunks=np.array([[[5, -6, 10], [0, 3, -2]]]).shape
                ), 0, 4,
                da.from_array(
                    np.array([[[1, 0, 0], [0, 0, 0]]]),
                    chunks=np.array([[[1, 0, 0], [0, 0, 0]]]).shape
                )
            ),
            (
                da.from_array(
                    np.array([[[5, -6, 10], [0, 3, -2]]]),
                    chunks=np.array([[[5, -6, 10], [0, 3, -2]]]).shape
                ), 1, 2,
                da.from_array(
                    np.array([[[0, -4, 0], [0, 1, 0]]]),
                    chunks=np.array([[[0, -4, 0], [0, 1, 0]]]).shape
                )
            ),
            (
                da.from_array(
                    np.array([[[5, -6, 10], [0, 3, -2]]]),
                    chunks=np.array([[[5, -6, 10], [0, 3, -2]]]).shape
                ), 2, 5,
                da.from_array(
                    np.array([[[0, 0, 5], [0, 0, 0]]]),
                    chunks=np.array([[[0, 0, 5], [0, 0, 0]]]).shape
                )
            ),
        ]
    )
    def test_prox_3d(self, l1, p, image_index, lam, expected):
        l1.image = Mock(data=xr.DataArray(p))
        l1.image_index = image_index
        res = l1.prox(lam)
        np.testing.assert_allclose(res.compute(), expected.compute(), rtol=1e-5)
