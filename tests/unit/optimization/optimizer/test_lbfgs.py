from unittest.mock import Mock

import astropy.units as un
import dask.array as da
import numpy as np
import pytest
import xarray as xr

from pyralysis.optimization.optimizer import LBFGS
from pyralysis.reconstruction.image import Image


@pytest.fixture
def mock_image():
    """Fixture to create a mock image."""
    data = xr.DataArray(data=da.random.random((128, 128), chunks=(64, 64)))
    return Image(data=data, cellsize=0.01 * un.arcsec)


@pytest.fixture
def mock_objective_function(mock_image):
    """Fixture to create a mock objective function."""
    objective_function = Mock()
    objective_function.fi_image = Mock()
    objective_function.calculate_function = Mock(return_value=da.from_array(0.5))
    objective_function.calculate_gradient = Mock()
    objective_function.dphi = da.random.random((128, 128), chunks=(64, 64))
    objective_function.fi_list = [Mock(image=mock_image)]
    return objective_function


@pytest.fixture
def mock_lbfgs(mock_image, mock_objective_function):
    """Fixture to create an LBFGS optimizer instance."""
    return LBFGS(
        image=mock_image,
        objective_function=mock_objective_function,
        max_iter=5,
        max_corrections=3,
        ftol=1e-6,
        gtol=1e-6,
        linesearch=Mock(),
    )


def test_compute_direction(mock_lbfgs):
    """Test the _compute_direction method of LBFGS."""
    gradient = da.random.random((128, 128), chunks=(64, 64))
    s_history = da.zeros((3, 128, 128), chunks=(1, 64, 64))
    y_history = da.zeros((3, 128, 128), chunks=(1, 64, 64))
    rho_history = da.ones(3)

    direction = mock_lbfgs._compute_direction(gradient, s_history, y_history, rho_history, 2)
    assert isinstance(direction, da.Array)
    assert direction.shape == (128, 128)


def test_update_rho_and_histories(mock_lbfgs):
    """Test the _update_rho_and_histories method of LBFGS."""
    s = da.random.random((128, 128), chunks=(64, 64))
    y = da.random.random((128, 128), chunks=(64, 64))
    s_history = da.zeros((3, 128, 128), chunks=(1, 64, 64))
    y_history = da.zeros((3, 128, 128), chunks=(1, 64, 64))
    rho_history = da.ones(3)

    s_history, y_history, rho_history, current_size, start_index = mock_lbfgs._update_rho_and_histories(
        s, y, s_history, y_history, rho_history, 0, 0
    )

    assert isinstance(s_history, da.Array)
    assert isinstance(y_history, da.Array)
    assert isinstance(rho_history, da.Array)
    assert current_size == 1
    assert start_index == 0


def test_optimize(mock_lbfgs, mock_objective_function):
    """Test the optimize method of LBFGS."""
    mock_lbfgs.linesearch.search = Mock(return_value=(0.4, 1e-2))
    result = mock_lbfgs.optimize(verbose=False)

    assert isinstance(result, Image)
    assert result.data.shape == mock_lbfgs.image.data.shape

    # Ensure function and gradient were calculated
    mock_objective_function.calculate_function.assert_called()
    mock_objective_function.calculate_gradient.assert_called()

    # Ensure at least one line search was performed
    mock_lbfgs.linesearch.search.assert_called()


if __name__ == "__main__":
    pytest.main()
