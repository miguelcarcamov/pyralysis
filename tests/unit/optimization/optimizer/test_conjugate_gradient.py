import dask.array as da
import pytest

from pyralysis.optimization.optimizer import (
    DaiYuan,
    FletcherReeves,
    GradientNormError,
    HagerZhang,
    HestenesStiefel,
    PolakRibiere,
)


class TestConjugateGradient:

    @pytest.mark.parametrize("shape", [(16, 16), (16, 16, 3)])
    def test_conjugate_gradient_parameter_error(self, shape):
        cg = DaiYuan()
        grad = da.ones(shape)
        grad_prev = da.zeros(shape)
        dir_prev = da.ones(shape)
        with pytest.raises(GradientNormError):
            cg.conjugate_gradient_parameter(grad, grad_prev, dir_prev)

    @pytest.mark.parametrize(
        "grad, grad_prev, dir_prev, expected", [
            (da.ones((16, 16)), da.ones((16, 16)), None, 1.0),
            (da.ones((16, 16, 3)), da.ones((16, 16, 3)), None, 1.0)
        ]
    )
    def test_conjugate_gradient_parameter_fletcher_reeves(
        self, grad, grad_prev, dir_prev, expected
    ):
        cg = FletcherReeves()
        param = cg.conjugate_gradient_parameter(grad, grad_prev, dir_prev)
        assert isinstance(param, da.Array)
        assert param.ndim == 0
        assert param == expected

    @pytest.mark.parametrize(
        "grad, grad_prev, dir_prev, expected", [
            (da.ones((16, 16)), da.ones((16, 16)), None, 0.0),
            (da.ones((16, 16, 3)), da.ones((16, 16, 3)), None, 0.0)
        ]
    )
    def test_conjugate_gradient_parameter_polak_ribiere(self, grad, grad_prev, dir_prev, expected):
        cg = PolakRibiere()
        param = cg.conjugate_gradient_parameter(grad, grad_prev, dir_prev)
        assert isinstance(param, da.Array)
        assert param.ndim == 0
        assert param == expected

    @pytest.mark.parametrize(
        "grad, grad_prev, dir_prev, expected", [
            (da.full((16, 16), 2), da.ones((16, 16)), da.ones((16, 16)), 4.0),
            (da.full((16, 16, 3), 2), da.ones((16, 16, 3)), da.ones((16, 16, 3)), 4.0),
        ]
    )
    def test_conjugate_gradient_parameter_dai_yuan(self, grad, grad_prev, dir_prev, expected):
        cg = DaiYuan()
        param = cg.conjugate_gradient_parameter(grad, grad_prev, dir_prev)
        assert isinstance(param, da.Array)
        assert param.ndim == 0
        assert param == expected

    @pytest.mark.parametrize(
        "grad, grad_prev, dir_prev, expected", [
            (da.full((16, 16), 2), da.ones((16, 16)), da.ones((16, 16)), 2.0),
            (da.full((16, 16, 3), 2), da.ones((16, 16, 3)), da.ones((16, 16, 3)), 2.0),
        ]
    )
    def test_conjugate_gradient_parameter_hesteness_stiefel(
        self, grad, grad_prev, dir_prev, expected
    ):
        cg = HestenesStiefel()
        param = cg.conjugate_gradient_parameter(grad, grad_prev, dir_prev)
        assert isinstance(param, da.Array)
        assert param.ndim == 0
        assert param == expected

    @pytest.mark.parametrize(
        "grad, grad_prev, dir_prev, expected", [
            (da.full((16, 16), 2), da.ones((16, 16)), da.ones((16, 16)), -2.0),
            (da.full((16, 16, 3), 2), da.ones((16, 16, 3)), da.ones((16, 16, 3)), -2.0),
        ]
    )
    def test_conjugate_gradient_parameter_hager_zhang(self, grad, grad_prev, dir_prev, expected):
        cg = HagerZhang()
        param = cg.conjugate_gradient_parameter(grad, grad_prev, dir_prev)
        assert isinstance(param, da.Array)
        assert param.ndim == 0
        assert param == expected
