from collections import UserList

import numpy as np
import pytest
import xarray as xr

from pyralysis.optimization.projection import (
    CompositeProjection,
    EqualTo,
    GreaterThan,
    GreaterThanEqualTo,
    LessThan,
    LessThanEqualTo,
    NoProjection,
    NotEqualTo,
)
from pyralysis.reconstruction.image import Image


class TestProjection:

    @pytest.fixture
    def image(self):
        data = np.arange(16).reshape((4, 4))
        image = Image(data=xr.DataArray(data=data), cellsize=1.0)
        return image

    @pytest.fixture
    def cproj(self):
        cproj = CompositeProjection(
            LessThan(compared_value=4, replacement_value=-1),
            GreaterThan(compared_value=10, replacement_value=-2)
        )
        return cproj

    def test_init_error(self):
        with pytest.raises(TypeError):
            EqualTo(compared_value=10)

    def test_no_projection(self, image):
        proj = NoProjection()
        assert 'compared_value' not in vars(proj)
        assert 'replacement_value' not in vars(proj)
        # Prepare data
        expected = np.arange(16).reshape((4, 4))
        # Call projection
        proj(image)
        assert (image.data.data == expected).all()

    def test_equal_to(self, image):
        proj = EqualTo(compared_value=5, replacement_value=-1)
        # Prepare data
        array = np.arange(16).reshape((4, 4))
        expected = np.where(array == 5, -1, array)
        # Call projection
        proj(image)
        assert (image.data.data == expected).all()

    def test_not_equal_to(self, image):
        proj = NotEqualTo(compared_value=5, replacement_value=-1)
        # Prepare data
        array = np.arange(16).reshape((4, 4))
        expected = np.where(array != 5, -1, array)
        # Call projection
        proj(image)
        assert (image.data.data == expected).all()

    def test_greater_than(self, image):
        proj = GreaterThan(compared_value=5, replacement_value=-1)
        # Prepare data
        array = np.arange(16).reshape((4, 4))
        expected = np.where(array > 5, -1, array)
        # Call projection
        proj(image)
        assert (image.data.data == expected).all()

    def test_greater_than_equal_to(self, image):
        proj = GreaterThanEqualTo(compared_value=5, replacement_value=-1)
        # Prepare data
        array = np.arange(16).reshape((4, 4))
        expected = np.where(array >= 5, -1, array)
        # Call projection
        proj(image)
        assert (image.data.data == expected).all()

    def test_less_than(self, image):
        proj = LessThan(compared_value=5, replacement_value=-1)
        # Prepare data
        array = np.arange(16).reshape((4, 4))
        expected = np.where(array < 5, -1, array)
        # Call projection
        proj(image)
        assert (image.data.data == expected).all()

    def test_less_than_equal_to(self, image):
        proj = LessThanEqualTo(compared_value=5, replacement_value=-1)
        # Prepare data
        array = np.arange(16).reshape((4, 4))
        expected = np.where(array <= 5, -1, array)
        # Call projection
        proj(image)
        assert (image.data.data == expected).all()

    @pytest.mark.parametrize(
        "projections, expected", [
            (
                [
                    LessThan(compared_value=4, replacement_value=-1),
                    GreaterThan(compared_value=10, replacement_value=-2)
                ], np.array([[-1, -1, -1, -1], [4, 5, 6, 7], [8, 9, 10, -2], [-2, -2, -2, -2]])
            ),
            (
                [
                    GreaterThan(compared_value=4, replacement_value=5),
                    NotEqualTo(compared_value=5, replacement_value=6)
                ], np.array([[6, 6, 6, 6], [6, 5, 5, 5], [5, 5, 5, 5], [5, 5, 5, 5]])
            ),
        ]
    )
    def test_composite_projection(self, image, projections, expected):
        proj = CompositeProjection(*projections)
        # Call projection
        proj(image)
        assert (image.data.data == expected).all()

    def test_cp_init_error(self):
        with pytest.raises(TypeError):
            CompositeProjection(LessThan(compared_value=10, replacement_value=1), "bad value")

    def test_cp_set(self, cproj):
        last_cv = cproj[1].compared_value
        last_rv = cproj[1].replacement_value
        cproj[1] = NotEqualTo(compared_value=last_cv + 1, replacement_value=last_rv + 1)
        assert cproj[1].compared_value == last_cv + 1
        assert cproj[1].replacement_value == last_rv + 1

    def test_cp_set_error(self, cproj):
        with pytest.raises(TypeError):
            cproj[1] = 10

    def test_cp_insert(self, cproj):
        assert len(cproj) == 2
        cproj.insert(1, EqualTo(compared_value=5, replacement_value=3))
        assert len(cproj) == 3
        assert cproj[1].compared_value == 5
        assert cproj[1].replacement_value == 3

    def test_cp_insert_error(self, cproj):
        assert len(cproj) == 2
        with pytest.raises(TypeError):
            cproj.insert(1, 10.5)

    def test_cp_append(self, cproj):
        assert len(cproj) == 2
        cproj.append(EqualTo(compared_value=5, replacement_value=3))
        assert len(cproj) == 3
        assert cproj[2].compared_value == 5
        assert cproj[2].replacement_value == 3

    def test_cp_append_error(self, cproj):
        assert len(cproj) == 2
        with pytest.raises(TypeError):
            cproj.append(10.5)

    def test_cp_extend(self, cproj):
        other_cproj = CompositeProjection(
            NoProjection(), EqualTo(compared_value=5, replacement_value=3)
        )
        other_list = [
            GreaterThan(compared_value=10, replacement_value=0),
            LessThan(compared_value=3, replacement_value=0)
        ]

        assert len(cproj) == 2
        cproj.extend(other_cproj)
        assert len(cproj) == 4
        cproj.extend(other_list)
        assert len(cproj) == 6

    @pytest.mark.parametrize("other", [[1, 2, 3], ["value", "str"], UserList([10, 20])])
    def test_cp_extend_error(self, cproj, other):
        with pytest.raises(TypeError):
            cproj.extend(other)

    def test_cp_add(self, cproj):
        other_cproj = CompositeProjection(
            NoProjection(), EqualTo(compared_value=5, replacement_value=3)
        )
        other_list = [
            GreaterThan(compared_value=10, replacement_value=0),
            LessThan(compared_value=3, replacement_value=0)
        ]
        proj = NotEqualTo(compared_value=0, replacement_value=-1)
        # With other Composite Projection
        cproj = cproj + other_cproj
        assert len(cproj) == 4
        cproj = other_cproj + cproj
        assert len(cproj) == 6
        cproj += other_cproj
        assert len(cproj) == 8
        # With other list with Projections
        cproj = cproj + other_list
        assert len(cproj) == 10
        cproj = other_list + cproj
        assert len(cproj) == 12
        cproj += other_list
        assert len(cproj) == 14
        # With other Projections
        cproj = cproj + proj
        assert len(cproj) == 15
        cproj = proj + cproj
        assert len(cproj) == 16
        cproj += proj
        assert len(cproj) == 17

    @pytest.mark.parametrize("other", [[1, 2, 3], ["value", "str"], UserList([10, 20]), 10, "str"])
    def test_cp_add_error(self, cproj, other):
        with pytest.raises(TypeError):
            cproj = cproj + other

    @pytest.mark.parametrize("other", [[1, 2, 3], ["value", "str"], UserList([10, 20]), 10, "str"])
    def test_cp_radd_error(self, cproj, other):
        with pytest.raises(TypeError):
            cproj = other + cproj

    @pytest.mark.parametrize("other", [[1, 2, 3], ["value", "str"], UserList([10, 20]), 10, "str"])
    def test_cp_iadd_error(self, cproj, other):
        with pytest.raises(TypeError):
            cproj += other
