import unittest
from dataclasses import dataclass

import numpy as np
import pytest

from pyralysis.optimization.fi import Fi
from pyralysis.reconstruction.mask import Mask


class MockMask(Mask):
    """Class to use Mask object in unit tests.

    Mock Class that inherits from Mask, where each and every attribute is `None`. The goal is to
    have objects that can be dispatched to the same methods/functions that Mask objects, specially
    for the use of the multimethod library.
    """

    def __init__(self):
        pass


@pytest.fixture
def mock_mask():
    """Fixture that returns a class to create an instance of MockMask.

    Returns
    -------
    MockMask
        Class definition, to create an instance of MockMask.
    """
    return MockMask


@dataclass
class FunctionFi(Fi):
    """Class to test personalized functions."""
    function_definition: callable = None
    gradient_definition: callable = None

    def function(self, *, mask: Mask = None):
        """Computes the function over the image."""
        image_func = self.function_definition(self.image.data.data)
        if mask is not None:
            image_func *= mask.data
        self._func_value = np.sum(image_func)
        return self._func_value

    def gradient(self, iter=1, *, mask: Mask = None):
        """Computes the gradient of the function over the image."""
        image_grad = self.gradient_definition(self.image.data.data)
        if mask is not None:
            image_grad *= mask.data
        self._grad_value = image_grad
        return self._grad_value

    def prox(self):
        pass


@pytest.fixture
def function_fi():
    """Fixture that returns a class to create an instance of FunctionFi.

    Returns a class to create a Fi whose function is the callable `function_definition` applied to
    every image element and then added. The gradient of the newly defined Fi is the derivative of
    the function applied to every image element.

    Returns
    -------
    FunctionFi
        Class definition, to create an instance of FunctionFi.
    """
    return FunctionFi
