from unittest.mock import Mock

import pytest

from pyralysis.optimization.linesearch import LineSearch


class TestLineSearch:

    class LineSearchMock(LineSearch):

        def search(self):
            raise NotImplementedError

    def test_init(self):
        ls = self.LineSearchMock()
        assert ls.objective_function is None
        assert ls.step is None
        assert ls.tol == 1.0e-7
        assert ls.max_iter == 100

    def test_read_kwargs(self):
        ls = self.LineSearchMock()
        assert ls.step is None
        assert ls.tol == 1.0e-7
        assert ls.max_iter == 100
        ls._read_kwargs(step=1.0, tol=1.0e-2, max_iter=20)
        assert ls.step == 1.0
        assert ls.tol == 1.0e-2
        assert ls.max_iter == 20
