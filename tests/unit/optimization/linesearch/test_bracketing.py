from unittest.mock import Mock

import dask.array as da
import numpy as np
import pytest
import xarray as xr
from scipy.optimize import bracket

from pyralysis.optimization.linesearch.bracketing import mnbrak


class TestBracketing:

    @pytest.mark.parametrize(
        "f, initial, expected",
        [
            (
                lambda x: (x**3) - 2.0 * (x**2) - 3.0 * x + 2, (0.2, 0.4),
                (0.723607, 1.24721, 6.32493, -0.839149, -2.91263, 156.042)
            ),  # 1 8 1 5
            (
                lambda x:
                (x**2) - 10.0 * x + 12, (-1.5, -0.5), (1.118, 5, 11.281, 2.0696, -13, 26.453)
            ),  # 1 5 6
            (
                lambda x:
                (x**2) - 10.0 * x + 12, (8.8, 8.9), (8.638, 4.999, -0.886, 0.2365, -13, 21.654)
            ),  # 1 5 6
            (
                lambda x:
                (x**3) - 10.0 * x + 12, (8.8, 8.9), (4.579, 1.0365, -1.9881, 62.235, 2.748, 24.023)
            ),  # 1 5 6 1 2 3
            (
                lambda x: (x**3) - 10.0 * x + 12, (-0.01, 0),
                (0.0161803, 1.61803, 3.06757, 11.8382, 0.0557281, 10.1901)
            ),  # 1 7 1 5
        ]
    )
    def test_polynomial(self, f, initial, expected):
        res = mnbrak(f, initial[0], initial[1])
        assert res == pytest.approx(expected, rel=1e-3)

    @pytest.mark.parametrize(
        "a, initial, expected", [
            (4.0, (3.7, 5.5), (5.5, 7.05579, 8.41246, -0.150073, -0.193237, -0.162595)),
            (5.0, (4, 6), (6, 8.69183, 9.23607, -0.0830726, -0.173085, -0.169887))
        ]
    )
    def test_morlet(self, a, initial, expected):
        A = 2 / (np.sqrt(3 * a) * (np.pi**0.25))
        f = lambda x: A * (1 - (x / a)**2) * np.exp(-0.5 * (x / a)**2)
        res = mnbrak(f, initial[0], initial[1])
        assert res == pytest.approx(expected, rel=1e-3)

    @pytest.mark.parametrize(
        "b, initial, expected", [
            (2.0, (0.01, 0.11), (0.11, 0.271803, 0.533607, -0.350287, -0.510818, -0.483528)),
            (2.0, (0.14, 0.25), (0.25, 0.36326, 0.427984, -0.5, -0.530696, -0.524011))
        ]
    )
    def test_entropy(self, b, initial, expected):
        f = lambda x: x * np.log(x) / np.log(b)
        res = mnbrak(f, initial[0], initial[1])
        assert res == pytest.approx(expected, rel=1e-3)

    @pytest.mark.parametrize(
        "f, initial", [
            (lambda x: (x**2) / 7 + x + 32, None),
            (lambda x: (x**2) / 7 + x + 32, [-2, 0]),
            (lambda x: (x**2) - x - 7, None),
            (lambda x: (x**2) - x - 7, [-1, 5]),
            (lambda x: -(x**3) + 3 * (x**2) + x - 1, None),
            (lambda x: -(x**3) + 3 * (x**2) + x - 1, [-0.2, 1.2]),
            (lambda x: -(x**3) + 3 * (x**2) + x - 1, [-0.2, 0]),
            (lambda x: -(x**3) + 3 * (x**2) + x - 1, [0, -0.2]),
        ]
    )
    def test_compare_scipy(self, f, initial):
        if initial is None:
            initial = [0., 1.]
        sres = bracket(f, initial[0], initial[1])
        res = mnbrak(f, initial[0], initial[1])
        np.testing.assert_almost_equal(res, sres[:-1], decimal=5)
