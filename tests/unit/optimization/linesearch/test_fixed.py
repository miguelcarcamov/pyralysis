from unittest.mock import Mock

import dask.array as da
import pytest
import xarray as xr

from pyralysis.optimization.linesearch import Fixed
from pyralysis.optimization.objective_function import ObjectiveFunction
from pyralysis.reconstruction.image import Image


class TestFixed:

    @pytest.fixture
    def setup_fixed(self, request, function_fi, mock_mask):
        # Fi list definition
        fi_list = [
            function_fi(function_definition=request.param[0], gradient_definition=request.param[1])
        ]
        # Image definition
        image = Image(data=xr.DataArray(request.param[2]), cellsize=1.0)
        # Create objective function and calculate function and gradient
        obj_func = ObjectiveFunction(fi_list=fi_list, image=image)
        if len(request.param) > 3:
            mask = mock_mask()
            mask.data = request.param[3]
            obj_func.calculate_function(mask=mask)
            obj_func.calculate_gradient(iteration=1, mask=mask)
        else:
            obj_func.calculate_function()
            obj_func.calculate_gradient(iteration=1)
        # Init fixed
        fixed = Fixed(objective_function=obj_func)
        return fixed

    def test_init_empty(self):
        f = Fixed()
        assert f.objective_function is None
        assert f.step is None
        assert f.tol == 1.0e-7
        assert f.max_iter == 100

    def test_init_step(self):
        f = Fixed(step=0.3)
        assert f.objective_function is None
        assert f.step == 0.3
        assert f.tol == 1.0e-7
        assert f.max_iter == 100

    @pytest.mark.parametrize(
        "setup_fixed, step, expected", [
            ((lambda x: x**2, lambda x: 2 * x, da.ones((16, 16))), 0.3, 0.3),
            ((lambda x: x**2, lambda x: 2 * x, da.ones((16, 16))), 0.5, 0.5)
        ],
        indirect=["setup_fixed"]
    )
    def test_search(self, setup_fixed, step, expected):
        # Set step
        setup_fixed.step = step
        # Get image from Fi in ObjectiveFunction list
        x = setup_fixed.objective_function.fi_list[0].image
        res = setup_fixed.search(x)
        assert len(res) == 2
        assert res[1] == expected

    @pytest.mark.parametrize(
        "setup_fixed, step_0, step_1", [
            ((lambda x: x**2, lambda x: 2 * x, da.ones((16, 16))), 0.01, 0.02),
            ((lambda x: x**2, lambda x: 2 * x, da.ones((16, 16))), 0.2, 0.01)
        ],
        indirect=["setup_fixed"]
    )
    def test_search_kwargs(self, setup_fixed, step_0, step_1):
        # Get image from Fi in ObjectiveFunction list
        x = setup_fixed.objective_function.fi_list[0].image
        # Set step
        setup_fixed.step = step_0
        assert setup_fixed.step == step_0
        assert setup_fixed.tol == 1.0e-7
        assert setup_fixed.max_iter == 100
        step = setup_fixed.search(x, step=step_1, max_iter=0, tol=1e-2)
        assert len(step) == 2
        assert step[1] == step_1
        assert setup_fixed.step == step_1
        assert setup_fixed.tol == 1e-2
        assert setup_fixed.max_iter == 0

    @pytest.mark.parametrize(
        "setup_fixed, step, mask_data, expected", [
            (
                (lambda x: x**2, lambda x: 2 * x, da.ones((3, 3)), da.ones(
                    (3, 3)
                )), 0.3, da.ones((3, 3)), 0.3
            ),
            (
                (lambda x: x**2, lambda x: 2 * x, da.ones((5, 5)), da.ones(
                    (5, 5)
                )), 0.5, da.ones((5, 5)), 0.5
            )
        ],
        indirect=["setup_fixed"]
    )
    def test_search_mask(self, setup_fixed, step, mock_mask, mask_data, expected):
        # Mock mask
        mask = mock_mask()
        mask.data = mask_data
        # Get image from Fi in ObjectiveFunction list
        x = setup_fixed.objective_function.fi_list[0].image
        setup_fixed.step = step
        res = setup_fixed.search(x, mask=mask)
        assert len(res) == 2
        assert res[1] == expected
