from unittest.mock import Mock

import dask.array as da
import numpy as np
import pytest
import xarray as xr
from scipy.optimize import bracket, brent

import pyralysis.optimization.linesearch as linesearch
from pyralysis.optimization.linesearch import Brent
from pyralysis.optimization.objective_function import ObjectiveFunction
from pyralysis.reconstruction.image import Image


class TestBrent:

    @pytest.fixture
    def setup_brent(self, request, function_fi, mock_mask):
        # Fi list definition
        fi_list = [
            function_fi(function_definition=request.param[0], gradient_definition=request.param[1])
        ]
        # Image definition
        image = Image(data=xr.DataArray(request.param[2]), cellsize=1.0)
        # Create objective function and calculate function and gradient
        obj_func = ObjectiveFunction(fi_list=fi_list, image=image)
        if len(request.param) > 3:
            mask = mock_mask()
            mask.data = request.param[3]
            obj_func.calculate_function(mask=mask)
            obj_func.calculate_gradient(iteration=1, mask=mask)
        else:
            obj_func.calculate_function()
            obj_func.calculate_gradient(iteration=1)
        # Init brent
        brent = Brent(objective_function=obj_func)
        return brent

    def test_init_empty(self):
        brent = Brent()
        assert brent.objective_function is None
        assert brent.step is None
        assert pytest.approx(brent.tol) == 1e-7
        assert pytest.approx(brent.zeps) == 1e-10
        assert brent.max_iter == 100

    def test_init_zeps(self):
        z = 5e-11
        brent = Brent(zeps=z)
        assert brent.objective_function is None
        assert brent.step is None
        assert pytest.approx(brent.tol) == 1e-7
        assert pytest.approx(brent.zeps) == z
        assert brent.max_iter == 100

    @pytest.mark.parametrize(
        "setup_brent, gt, lt", [
            ((lambda x: x**2, lambda x: 2 * x, da.ones((16, 16))), 0.55, 0.45),
            ((lambda x: x**2, lambda x: 2 * x, da.ones((16, 16, 2))), 0.55, 0.45),
            (
                (lambda x: (x - 2) * (x - 2) + 1, lambda x: 2 * x - 4, da.ones((16, 16))),
                0.55,
                0.45,
            ),
            (
                (lambda x: (x - 2) * (x - 2) + 1, lambda x: 2 * x - 4, da.ones((16, 16, 2))),
                0.55,
                0.45,
            ),
            (
                (lambda x: x**2 + np.exp(-x), lambda x: 2 * x - np.exp(-x), da.ones((16, 16))),
                1.0,
                0.0,
            ),
            (
                (lambda x: x**2 + np.exp(-x), lambda x: 2 * x - np.exp(-x), da.ones((16, 16, 2))),
                1.0,
                0.0,
            ),
            (
                (
                    lambda x: ((x * x + 2) * x + 1) * x + 3,
                    lambda x: 4 * x**3 + 4 * x + 1,
                    da.ones((16, 16)),
                ),
                0.15,
                0.12,
            ),
            (
                (
                    lambda x: ((x * x + 2) * x + 1) * x + 3,
                    lambda x: 4 * x**3 + 4 * x + 1,
                    da.ones((16, 16, 2)),
                ),
                0.15,
                0.12,
            ),
        ],
        indirect=["setup_brent"]
    )
    def test_search(self, setup_brent, gt, lt):
        # Check that gradient and function value are not zero
        assert setup_brent.objective_function.phi != 0.0
        assert np.any(setup_brent.objective_function.dphi != 0.0)
        # Get image from Fi in ObjectiveFunction list
        x = setup_brent.objective_function.fi_list[0].image
        # Search
        step = setup_brent.search(x)
        assert len(step) == 2
        assert setup_brent.step == step[1]
        assert gt > step[1]
        assert lt < step[1]

    @pytest.mark.parametrize(
        "setup_brent, gt, lt", [
            (
                (
                    lambda x: ((x * x + 2) * x + 1) * x + 3,
                    lambda x: 4 * x**3 + 4 * x + 1,
                    da.ones((16, 16)),
                ),
                0.2,
                0.1,
            ),
            (
                (
                    lambda x: ((x * x + 2) * x + 1) * x + 3,
                    lambda x: 4 * x**3 + 4 * x + 1,
                    da.ones((16, 16, 2)),
                ),
                0.2,
                0.1,
            ),
        ],
        indirect=["setup_brent"]
    )
    def test_search_max_iter(self, setup_brent, gt, lt):
        # Check that gradient and function value are not zero
        assert setup_brent.objective_function.phi != 0.0
        assert np.any(setup_brent.objective_function.dphi != 0.0)
        # Get image from Fi in ObjectiveFunction list
        x = setup_brent.objective_function.fi_list[0].image
        # Change max_iter
        setup_brent.max_iter = 3
        # Search
        step = setup_brent.search(x)
        assert len(step) == 2
        assert setup_brent.step == step[1]
        assert gt > step[1]
        assert lt < step[1]

    @pytest.mark.parametrize(
        "setup_brent", [
            ((lambda x: x**2 + 1, lambda x: 2 * x, da.ones((16, 16)))),
            ((lambda x: x**2 + 1, lambda x: 2 * x, da.ones((16, 16, 2))))
        ],
        indirect=["setup_brent"]
    )
    def test_search_kwargs(self, setup_brent):
        assert not isinstance(setup_brent.step, (int, float, da.Array))
        assert setup_brent.tol == 1.0e-7
        assert setup_brent.max_iter == 100
        assert setup_brent.zeps == 1e-10
        # Get image from Fi in ObjectiveFunction list
        x = setup_brent.objective_function.fi_list[0].image
        # Search with kwargs
        # Step initial value is not considered in this linesearch
        step = setup_brent.search(x, tol=1.0e-6, max_iter=50, zeps=1e-11)
        assert len(step) == 2
        assert isinstance(setup_brent.step, (int, float, da.Array))
        assert setup_brent.step == step[1]
        assert step[1] >= 0.0 and step[1] <= 1.0
        # Check update in attributes
        assert setup_brent.tol == 1.0e-6
        assert setup_brent.max_iter == 50
        assert setup_brent.zeps == 1e-11

    @pytest.mark.parametrize(
        "setup_brent, mask_data, gt, lt", [
            (
                (
                    lambda x: x**2 + 1, lambda x: 2 * x, da.ones(
                        (3, 3)
                    ), da.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]])
                ), da.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]]), 0.55, 0.45
            ),
            (
                (
                    lambda x: x**2 + 1, lambda x: 2 * x, da.ones((3, 3, 2)),
                    da.array(
                        [
                            [[0, 0], [1, 1], [0, 0]], [[0, 0], [1, 1], [0, 0]],
                            [[0, 0], [1, 1], [0, 0]]
                        ]
                    )
                ),
                da.array(
                    [[[0, 0], [1, 1], [0, 0]], [[0, 0], [1, 1], [0, 0]], [[0, 0], [1, 1], [0, 0]]]
                ), 0.55, 0.45
            ),
        ],
        indirect=["setup_brent"]
    )
    def test_search_mask(self, setup_brent, mock_mask, mask_data, gt, lt):
        # Mock mask
        mask = mock_mask()
        mask.data = mask_data
        # Check that gradient and function value are not zero
        assert setup_brent.objective_function.phi != 0.0
        assert np.any(setup_brent.objective_function.dphi != 0.0)
        # Get image from Fi in ObjectiveFunction list
        x = setup_brent.objective_function.fi_list[0].image
        # Search
        step = setup_brent.search(x, mask=mask)
        assert len(step) == 2
        assert setup_brent.step == step[1]
        assert gt > step[1]
        assert lt < step[1]

    @pytest.mark.parametrize(
        "polynomial", ["(x**2) - 2*x - 7", "-(x**3) + 3 * (x**2) + x - 1", "(x**2) - 2*x - 7"]
    )
    def test_search_polynomial(self, monkeypatch, polynomial):
        # Mock function definition
        f = lambda x: eval(polynomial.replace("x", str(x)))

        def mock_f1dim(*args, **kwargs):
            return f

        # Expected values from scipy
        a, b, c, *_ = bracket(f)
        xmin, fval, *_ = brent(f, brack=(a, b, c), tol=1.48e-08, full_output=True, maxiter=100)
        # Pyralysis objects and execution
        of = Mock(dphi=0, function=lambda x: x)
        x = Mock(data=0)
        pyralysis_brent = Brent(objective_function=of)
        monkeypatch.setattr(linesearch.brent, "f1dim", mock_f1dim)
        fstep, step = pyralysis_brent.search(x, tol=1.48e-08, max_iter=100)
        # Test values
        assert step == pytest.approx(xmin)
        assert fstep == pytest.approx(fval)
