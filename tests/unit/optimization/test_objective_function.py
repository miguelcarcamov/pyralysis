from unittest.mock import Mock

import dask.array as da
import numpy as np
import pytest
import xarray as xr

from pyralysis.optimization import ObjectiveFunction
from pyralysis.optimization.fi import Chi2, Entropy, L1Norm
from pyralysis.reconstruction import Image


class TestObjectiveFunction:

    @pytest.fixture
    def image(self):
        data = xr.DataArray(np.ones((3, 3, 2)))
        _image = Image(data=data, cellsize=1.0)
        return _image

    @pytest.fixture
    def fis(self):
        _fi_list = []
        attrs = {
            'function.return_value': 1.0,
            'gradient.return_value': da.ones((3, 3, 2)),
        }
        for i in range(4):
            _fi = Mock(penalization_factor=1.0, image_index=-1, **attrs)
            _fi_list.append(_fi)
        return _fi_list

    @pytest.fixture
    def of(self, image, fis):
        _of = ObjectiveFunction(fi_list=fis, image=image)
        return _of

    def test_init_img3d(self, image, fis):
        of = ObjectiveFunction(fi_list=fis, image=image)
        assert len(of.fi_list) == 4
        assert len(of.fi_values) == 4
        assert of.dphi is not None
        assert of.dphi.shape == (3, 3, 2)

    def test_init_img2d(self, fis):
        data = xr.DataArray(np.ones((16, 16)))
        image = Image(data=data, cellsize=1.0)
        of = ObjectiveFunction(fi_list=fis, image=image)
        assert len(of.fi_list) == 4
        assert len(of.fi_values) == 4
        assert of.dphi is not None
        assert of.dphi.shape == (16, 16)

    def test_init_img_shape(self, fis):
        of0 = ObjectiveFunction(fi_list=fis, image=(16, 16))
        assert len(of0.fi_list) == 4
        assert len(of0.fi_values) == 4
        assert of0.dphi is not None
        assert of0.dphi.shape == (16, 16)
        assert of0.dphi.chunksize == (16, 16)
        of1 = ObjectiveFunction(fi_list=fis, image=(16, 16), chunks=(8, 8))
        assert len(of1.fi_list) == 4
        assert len(of1.fi_values) == 4
        assert of1.dphi is not None
        assert of1.dphi.shape == (16, 16)
        assert of1.dphi.chunksize == (8, 8)

    @pytest.mark.parametrize(
        "penalization, expected", [
            (0.8, [0.8, 0.8, 0.8, 0.8]),
            ([0.8, 0.2, 0.3, 0.4], [0.8, 0.2, 0.3, 0.4]),
            (np.array([0.2, 0.5, 0.9, 0.7]), [0.2, 0.5, 0.9, 0.7]),
        ]
    )
    def test_fi_penalization(self, of, penalization, expected):
        first_pen = [fi.penalization_factor for fi in of.fi_list]
        assert first_pen == [1.0, 1.0, 1.0, 1.0]
        of.fi_penalization(penalization)
        upd_pen = [fi.penalization_factor for fi in of.fi_list]
        assert upd_pen == expected

    def test_fi_penalization_error(self, of):
        with pytest.raises(Exception):
            of.fi_penalization([0.8, 0.9])

    @pytest.mark.parametrize(
        "im, ch, exp_shape, exp_chunks",
        [
            # 3-dimensional images
            (
                Image(data=xr.DataArray(da.ones((100, 100, 2))), chunks=(50, 50, 1),
                      cellsize=1.0), None, (100, 100, 2), (50, 50, 1)
            ),
            ((100, 100, 2), None, (100, 100, 2), (100, 100, 2)),
            ([100, 100, 2], None, (100, 100, 2), (100, 100, 2)),
            ((100, 100, 2), (50, 50, 1), (100, 100, 2), (50, 50, 1)),
            ([100, 100, 2], (50, 50, 1), (100, 100, 2), (50, 50, 1)),
            ((100, 100, 2), {
                0: 50,
                1: 50,
                2: 1
            }, (100, 100, 2), (50, 50, 1)),
            ([100, 100, 2], {
                0: 50,
                1: 50,
                2: 1
            }, (100, 100, 2), (50, 50, 1)),
            # 2-dimensional images
            (
                Image(data=xr.DataArray(da.ones((100, 100))), chunks=(50, 50), cellsize=1.0), None,
                (100, 100), (50, 50)
            ),
            ((100, 100), None, (100, 100), (100, 100)),
            ([100, 100], None, (100, 100), (100, 100)),
            ((100, 100), (50, 50), (100, 100), (50, 50)),
            ([100, 100], (50, 50), (100, 100), (50, 50)),
            ((100, 100), {
                0: 50,
                1: 50
            }, (100, 100), (50, 50)),
            ([100, 100], {
                0: 50,
                1: 50
            }, (100, 100), (50, 50)),
        ]
    )
    def test_configure_image_size(self, of, im, ch, exp_shape, exp_chunks):
        assert of.dphi.shape == (3, 3, 2)
        if ch is None:
            of.configure_image_size(im)
        else:
            of.configure_image_size(im, ch)
        assert of.dphi.shape == exp_shape
        assert of.dphi.chunksize == exp_chunks

    @pytest.mark.parametrize("im", [(np.array([10, 10, 1])), (10), (10.), ("auto")])
    def test_configure_image_size_error(self, of, im):
        # Test with invalid type
        with pytest.raises(Exception):
            of.configure_image_size(im)

    @pytest.mark.parametrize(
        "im, exp_shape", [
            (Image(data=xr.DataArray(da.ones((100, 100, 2))), cellsize=1.0), (100, 100, 2)),
            (Image(data=xr.DataArray(da.ones((100, 100))), cellsize=1.0), (100, 100)),
        ]
    )
    def test_fi_image(self, of, im, exp_shape):
        # Test that dphi updates when a new image is set in every Fi
        assert of.dphi.shape == (3, 3, 2)
        # Update images of fi
        of.fi_image(im)
        # Check that the shape of dphi updates
        assert of.dphi.shape == exp_shape
        # Check that the images of the fi list updates
        for fi in of.fi_list:
            assert fi.image == im

    def test_restart_dphi(self, of):
        # Test that dphi is full the expected shape and chunks
        assert of.dphi.shape == (3, 3, 2)
        assert of.dphi.chunksize == (3, 3, 2)
        # Fill dphi with values
        of.dphi = da.ones_like(of.dphi, chunks=of.dphi.chunksize)
        assert (of.dphi == 1.0).all()
        # Call restart_dphi
        of.restart_dphi()
        assert of.dphi.shape == (3, 3, 2)
        assert of.dphi.chunksize == (3, 3, 2)
        assert (of.dphi == 0.0).all()

    def test_calculate_no_mask(self, of):
        # Test no error
        res_fun = of.calculate_function()
        for fi in of.fi_list:
            fi.function.assert_called_with(mask=None)

        of.calculate_gradient(iteration=10)
        for fi in of.fi_list:
            assert fi.iteration == 10
            fi.gradient.assert_called_with(iter=10, mask=None)

    def test_calculate_mask(self, of):
        mask = Mock(data=da.ones((3, 3, 2)))
        # Test no error with mask parameter
        res_fun = of.calculate_function(mask=mask)
        for fi in of.fi_list:
            fi.function.assert_called_with(mask=mask)

        of.calculate_gradient(iteration=10, mask=mask)
        for fi in of.fi_list:
            assert fi.iteration == 10
            fi.gradient.assert_called_with(iter=10, mask=mask)
