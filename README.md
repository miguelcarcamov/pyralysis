
<div style="text-align: center;">
  <img src="https://gitlab.com/miguelcarcamov/pyralysis/-/wikis/uploads/fd40975ce0601b5c52a3cf8b92f385a5/Pyralisis-25.png" height="400" alt="Pyralysis logo">
</div>

<div style="text-align: center;">

  [![codecov](https://codecov.io/gl/clirai/pyralysis/branch/development/graph/badge.svg?token=MVFAA1YKC7)](https://codecov.io/gl/clirai/pyralysis)
  [![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
  [![Documentation](https://img.shields.io/badge/docs-ReadTheDocs-blue)](https://pyralysis.readthedocs.io)
  [![GitLab Contributors](https://img.shields.io/gitlab/contributors/clirai/pyralysis)](https://gitlab.com/clirai/pyralysis/-/project_members)

  [![Python Version](https://img.shields.io/badge/python-3.9%20|%203.10%20|%203.11%20|3.12-blue)](https://www.python.org/)
  [![PyPI Version](https://img.shields.io/pypi/v/pyralysis)](https://pypi.org/project/pyralysis)
  [![PyPI Downloads](https://img.shields.io/pypi/dm/pyralysis)](https://pypi.org/project/pyralysis/)

  [![Docker Image Version](https://img.shields.io/badge/docker-latest-blue)](https://gitlab.com/clirai/pyralysis/container_registry)
  [![License](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
  [![Documentation Status](https://readthedocs.org/projects/pyralysis/badge/?version=latest)](https://pyralysis.readthedocs.io/en/latest/?badge=latest)

  [![Pipeline Status](https://gitlab.com/clirai/pyralysis/badges/development/pipeline.svg)](https://gitlab.com/clirai/pyralysis/-/pipelines?ref=development)

</div>

---

## The Origin of the Name "Pyralysis"

The name **Pyralysis** is a fusion of two fundamental pillars of the project: **Python** and **Radio Astronomy**. As a software framework dedicated to advancing image synthesis, data reduction, and analysis in radio interferometry, it was essential for the name to capture both the computational and astronomical dimensions of the project.

Beyond this, *Pyralysis* also evokes *paralysis*, but in a metaphorical sense—representing the ability to "freeze" raw radio data, deconstruct it, and extract meaningful astronomical insights through structured, high-performance computational techniques. This is made possible by its **object-oriented architecture**, which provides the flexibility needed to accommodate evolving algorithms, optimize performance, and seamlessly integrate new methodologies for interferometric imaging and Faraday tomography.

At its core, *Pyralysis* embodies the synergy between **flexibility, modularity, and scientific rigor**, leveraging Python’s power to push the boundaries of radio interferometric data processing. By combining object-oriented principles with state-of-the-art computational techniques, it creates a robust and extensible platform for the next generation of radio astronomical research.

## **Pyralysis Overview**

**Pyralysis** is an open-source Python library designed for **radio astronomy imaging and optimization**. It combines the flexibility of Python with the **scalability of Dask** (for CPU-based parallel processing) to perform **regularized maximum likelihood (RML)** imaging techniques, such as the **Maximum Entropy Method (MEM)**. Additionally, Pyralysis provides **gradient-based optimization routines** for image reconstruction.

### **Key Features**

- **Flexible Gridding Strategies**:
  - Count-in-cell gridding (simple binning method).
  - Convolutional gridding with multiple kernel options: Spline, Gaussian-Sinc, Sinc, PSWF1, Pillbox, Gaussian, Bicubic, Kaiser-Bessel.
- **Optimization Techniques**:
  - Regularized Maximum Likelihood (RML) imaging techniques.
  - Gradient-based optimization methods for image reconstruction.
- **Modular and Object-Oriented Architecture**:
  - Designed for scalability, extensibility, and performance optimization.
- **Upcoming GPU Support**:
  - Currently optimized for **CPU-based computation using Dask**.
  - Future support for **CUDA acceleration** is under development.

---

## **Table of Contents**

- [Features and Design](#features-and-design)
- [Installation Guide](#installation-guide)
  - [Prerequisites](#prerequisites)
  - [Installing Pyralysis](#installing-pyralysis)
- [Usage Overview](#usage-overview)
- [Testing and Contributions](#testing-and-contributions)
- [License](#license)
- [Contact](#contact)
- [Summary of Key Features](#summary-of-key-features)

---

## **Features and Design**

Pyralysis is built with a **modular architecture**, consisting of key components:

### **Model Visibilities**

Defines an **abstract class** for estimating model visibilities from an **inverse Fourier-transformed image**. Implemented methods include:

- **Degridding** – Uses **convolutional gridding** with a **half-kernel** approach.
- **Bilinear Interpolation** – Applies bilinear interpolation.
- **Nearest Neighbor Interpolation** – Uses nearest-neighbor interpolation.

### **Objective Function Framework**

Defines function terms used in **optimization**, including:

- **Chi-Squared ($\chi^2$)** – Measures goodness-of-fit.
- **L1 Norm** – Encourages sparsity in image reconstruction.
- **Entropy-Based Regularization** – Implements **maximum entropy methods (MEM)**.

These terms can be **combined** to create a **composite objective function**, which is used in optimization.

### **Optimization Methods**

Pyralysis provides **gradient-based optimization** methods for solving the inverse imaging problem, including:

- **Hager-Zhang** – A nonlinear conjugate gradient method.
- **Limited-memory Broyden–Fletcher–Goldfarb–Shanno (LBFGS)** – A quasi-Newton optimization algorithm for large-scale problems.
- **Fletcher-Reeves** – A conjugate gradient method.
- **Polak-Ribiere** – A conjugate gradient variant for non-linear optimization.
- **Dai-Yuan** – A modified conjugate gradient method for improved performance.
- **Hestenes-Stiefel** – A conjugate gradient method that ensures strong global convergence.

### **Line Search Techniques**

Implements **step size selection methods** for optimization, such as:

- **Brent’s Method** – A derivative-free method for root-finding.
- **Backtracking Line Search with Armijo Rule** – Ensures sufficient function decrease.
- **Fixed-step Search** – Uses a pre-defined step size.

### **Convolution Kernels**

Defines an **abstract class** for convolution kernels, which generate **gridding correction functions (GCF)** and **Fourier-space convolution kernels**. Available implementations include:

- **Spline**
- **Gaussian-Sinc**
- **Sinc**
- **Prolate Spheroidal Wave Function (PSWF1)**
- **Pillbox**
- **Gaussian**
- **Bicubic**
- **Kaiser-Bessel**

### **Additional Tools**

Pyralysis also provides utility functions for:

- **Fast Fourier Transforms (FFT)**
- **Padding and Phase Shifting**
- **Unit Conversions**
- **Hermitian Symmetry Handling**

---

## **Installation Guide**

### **Prerequisites**

Ensure your system meets the following requirements:

- **Python**: 3.9 ≤ Python < 3.13
- **Dask**: Required for parallel CPU computation.
- **CUDA (Optional)**: GPU support is in development. For installation, see [NVIDIA’s CUDA Guide](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html).

---

### Installing Pyralysis

#### Using pip

> **Note:** The **PyPI package is not updated**. Use the latest version from GitLab.

  ```bash
  pip install pyralysis
  ```

- **Latest development version from GitLab:**

  ```bash
  pip install -U git+https://gitlab.com/clirai/pyralysis.git
  ```

#### From Source (Developer Mode)

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/clirai/pyralysis.git
    cd pyralysis
    ```

2. Install in editable mode:

    ```bash
    pip install -e .
    ```

#### Using Docker

```bash
docker pull registry.gitlab.com/clirai/pyralysis:latest
```

## Usage Overview

Pyralysis is structured around several abstract interfaces that enable flexible integration of different methods.

## Abstract interfaces

- **Model Visibilities**:
An abstract class to estimate model visibilities from an FFT image. Subclasses include:

  - **Degridding**: Uses convolutional gridding (with half–kernels) to compute visibilities.
  - **BilinearInterpolation**: Uses bilinear interpolation.
  - **NearestNeighbor**: Uses nearest-neighbor interpolation.

- **Optimization Framework**:
The optimization module uses an abstract **Fi** interface for individual objective function terms (such as **Chi2** ($\chi^2$), **L1Norm**, **Entropy**) that compute function values and gradients. These can be combined into a composite objective function and solved using gradient-based optimizers (e.g., **Hager–Zhang**, **LBFGS**, **Polak–Ribiere**). Line search methods (e.g., **Brent**, **Backtracking**, **Fixed-step**) determine appropriate step sizes during optimization.
- **Convolution Kernels (CKernel)**:
Provides an abstract class for generating gridding correction functions (GCF) and Fourier-space convolution kernels. Available kernels include **Spline**, **Gaussian-Sinc**, **Sinc**, **PSWF1**, **Pillbox**, **Gaussian**, **Bicubic**, **Kaiser-Bessel**, etc.
- **Gridding**:
Pyralysis supports both count-in-cell gridding (a simple binning method) and convolutional gridding (using convolution kernels).

> **Note**: In this release, all operations are performed using Dask arrays on the CPU. GPU/CUDA acceleration using Dask is currently under development.

---

## Examples

Below are three examples that demonstrate key functionalities: primary beam calculation and gridding, visibility estimation, and image reconstruction using gradient-based optimization.

### Example 1: Primary Beam Calculation and Gridding

```python
import dask.array as da
import astropy.units as u
import numpy as np
import matplotlib.pyplot as plt
from pyralysis.io import DaskMS, FITS
from pyralysis.transformers import Gridder, DirtyMapper, HermitianSymmetry
from pyralysis.transformers.weighting_schemes import Uniform, Robust

# Load a Measurement Set (replace with your file path)
ms_file = "/path/to/your/ms"
x = DaskMS(input_name=ms_file, chunks={"row": 100000})
dataset = x.read(filter_flag_column=False, calculate_psf=False)

# Plot the phase direction cosines (pointings)
l = dataset.field.phase_direction_cosines[0]
m = dataset.field.phase_direction_cosines[1]
plt.plot(l.to(u.deg), m.to(u.deg), '.')
plt.title("Pointings in Degrees")
plt.xlabel("l (deg)")
plt.ylabel("m (deg)")
plt.show()

# Set gridding parameters
hermitian_symmetry = False
padding_factor = 1.2
imsize = 512
dx = dataset.theo_resolution / 7

# Primary Beam Calculation
pb = dataset.antenna.primary_beam
pb.cellsize = dx
chans = dataset.spws.dataset[0].CHAN_FREQ.data.squeeze(axis=0)
centers_l = (l / (-dx) + imsize // 2).astype(np.int32)
centers_m = (m / dx + imsize // 2).astype(np.int32)

p_beams = da.array([
    pb.beam(chans, (imsize, imsize), antenna=np.array([0]), imcenter=(centers_l[i], centers_m[i]))
    for i in range(centers_l.size)
])
p_beams_together = da.sum(p_beams, axis=(0, 1, 2))
plt.imshow(p_beams_together.compute(), origin="lower")
plt.title("Combined Primary Beam")
plt.colorbar()
plt.show()

# --- Gridding Example ---
# Here we use the DirtyMapper transformer to perform convolutional gridding.
# DirtyMapper internally computes the FFT of the (primary-beam-weighted) image.
from pyralysis.transformers import DirtyMapper

dirty_mapper = DirtyMapper(
    input_data=dataset,
    imsize=imsize,
    cellsize=dx,
    stokes=["I"],
    hermitian_symmetry=hermitian_symmetry,
    padding_factor=padding_factor,
    ckernel_object=None  # Use count-in-cell gridding if no kernel is provided; for convolutional gridding, provide a CKernel instance.
)

# Compute the gridded FFT image (dirty image and dirty beam)
dirty_images, dirty_beam = dirty_mapper.transform()
dirty_image = dirty_images.data[0].compute()  # Compute the first Stokes component

plt.imshow(dirty_image, origin="lower")
plt.title("Dirty Image (Gridded FFT)")
plt.colorbar()
plt.show()
```

### Example 2: Visibility Estimation with Different Methods

This example demonstrates how to estimate model visibilities from an FFT image using three different methods:

- Nearest Neighbor
- Bilinear Interpolation
- Degridding (Convolutional Gridding)

Each method is instantiated as a subclass of the abstract **ModelVisibilities** class. After instantiating the estimators, we call `transform()` to compute the model visibilities and then plot the estimated visibilities (using the first correlation).

```python
from pyralysis.transformers import Degridding
from pyralysis.io import DaskMS, FITS
from pyralysis.convolution import PSWF1
from pyralysis.models import PowerLawIntensityModel
import matplotlib.pyplot as plt

# Read dataset and image from FITS (replace paths accordingly)
ms_file = "/path/to/your/ms"
x = DaskMS(input_name=ms_file, chunks={"row": 100000})
dataset = x.read(filter_flag_column=False, calculate_psf=False)
fits_io = FITS()
image = fits_io.read()  # Read an image from a FITS file
image.cellsize = dataset.theo_resolution / 7
```

```python
# 1) Nearest Neighbor estimator
mv_nn = NearestNeighbor(
    input_data=dataset,
    image=image,
    hermitian_symmetry=False,
    padding_factor=1.0
)
```

```python
# 2) Bilinear Interpolation estimator
mv_bilinear = BilinearInterpolation(
    input_data=dataset,
    image=image,
    cellsize=image.cellsize,
    hermitian_symmetry=False,
    padding_factor=1.0
)
```

```python
# 3) Degridding estimator (using convolutional gridding with a half–kernel)
ckernel = PSWF1(size=3, cellsize=image.cellsize, oversampling_factor=3)
mv_degridding = Degridding(
    input_data=dataset,
    image=image,
    cellsize=image.cellsize,
    hermitian_symmetry=False,
    padding_factor=1.2,
    ckernel_object=ckernel,
)
```

```python
mv_degridding.transform()
# Visualize estimated model visibilities (using the first correlation)
for ms in dataset.ms_list:
    uvw = ms.visibilities.uvw.data
    vis_model = ms.visibilities.model.data.compute()
    plt.scatter(uvw[:, 0], uvw[:, 1], c=da.abs(vis_model[..., 0]).compute(), s=0.5)
plt.title("Estimated Model Visibilities (Degridding)")
plt.xlabel("u")
plt.ylabel("v")
plt.colorbar()
plt.show()
```

### Example 3: Image Reconstruction via Gradient-Based Optimization

This example shows how to reconstruct an image from visibilities using gradient‑based optimization. It demonstrates how to set up the objective function (using, for example, Chi2), choose a line search method (Brent), and run an optimizer such as Hager‑Zhang.

```python
from pyralysis.estimators import Degridding, BilinearInterpolation, NearestNeighbor
from pyralysis.transformers import DirtyMapper, ModelVisibilities
from pyralysis.optimization.fi import Chi2
from pyralysis.optimization import ObjectiveFunction
from pyralysis.optimization.linesearch import Brent
from pyralysis.optimization.optimizer import HagerZhang
from pyralysis.io import DaskMS, FITS
from pyralysis.models import PowerLawIntensityModel
from pyralysis.reconstruction import Image
from pyralysis.reconstruction.mask import Mask
import xarray as xr
import dask.array as da
import numpy as np
import astropy.units as u
import matplotlib.pyplot as plt

# Load dataset and a FITS image (replace with your files)
ms_file = "/path/to/your/ms"
x = DaskMS(input_name=ms_file, chunks={"row": 100000})
dataset = x.read(filter_flag_column=False, calculate_psf=False)
fits_io = FITS()
image_array = np.zeros((256, 256), dtype=np.float32)
image = Image(data=xr.DataArray(image_array), cellsize=0.003 * u.arcsec, name="I")
image.data = image.data.chunk(image_array.shape)

# Create a mask (e.g., based on a threshold from the primary beam)
mask = Mask(dataset=dataset, imsize=image.data.shape, threshold=0.2, cellsize=image.cellsize)

# Choose a ModelVisibilities estimator – for example, NearestNeighbor is used here.
mv = NearestNeighbor(
    input_data=dataset,
    image=image,
    hermitian_symmetry=False,
    padding_factor=1.0
)
mv.transform()

# Define the objective function using a Chi² term (additional Fi terms like L1Norm or Entropy can be added)
fi_list = [Chi2(model_visibility=mv, normalize=True)]
of = ObjectiveFunction(fi_list=fi_list, image=image, persist_gradient=True)

# Set up line search (Brent) and the optimizer (Hager–Zhang)
ls = Brent(objective_function=of)
niter = 10
optim = HagerZhang(
    image=image,
    objective_function=of,
    linesearch=ls,
    mask=mask,
    max_iter=niter,
    projection=None  # Optionally, a projection function can be provided.
)

# Run the optimization
result = optim.optimize(verbose=True, partial_image=True)
result_image = result.data.compute()

# Visualize the reconstructed image
plt.imshow(result_image, origin="lower")
plt.title("Reconstructed Image")
plt.colorbar()
plt.show()
```

## Testing and Contributions

## Running the tests

To ensure correct installation and functionality, run:

```bash
pytest -s tests/
```

## 🚀 How to contribute

We welcome contributions from the radio astronomy and signal processing communities! To contribute:

1. **Fork the Repository**
   - Go to the [Pyralysis GitLab page](https://gitlab.com/clirai/pyralysis).
   - Click the **"Fork"** button to create your own copy of the repository.

2. **Clone the Repository**
   Open a terminal and run:

   ```bash
   git clone https://gitlab.com/yourusername/pyralysis.git
   cd pyralysis
   ```

3. **Create a new branch**

    ```bash
    git checkout -b feature/my-new-feature
    ```

4. **Set up environment (Optional)**
    Pyralysis provides an `environment.yml` file to simplify dependency installation.
    You can create the environment using Conda, Miniconda, Mamba, or Micromamba:
    **Using conda or miniconda**

    ```bash
    conda env create -f environment.yml
    conda activate pyralysis-env
    ```

    **Using mamba (faster alternative to Conda)**

    ```bash
    mamba env create -f environment.yml
    mamba activate pyralysis-env
    ```

    **Using micromamba (lightweight Mamba alternative)**

    ```bash
    micromamba env create -f environment.yml
    micromamba activate pyralysis-env
    ```

5. **Install dependencies**
  Install the required packages before development:

    ```bash
    pip install -r requirements.txt
    ```

6. **Follow code standards**
  Pyralysis uses **pre-commit hooks** for code formatting (isort, yapf, markdown linters, etc.).
Install and enable pre-commit:

    ```bash
    pip install pre-commit
    pre-commit install
    ```

7. **Make your changes**
  Implement your feature or bug fix while following the Pyralysis coding style.
8. **Run tests**
  Ensure that your changes do not break existing functionality:

    ```bash
    pytest -s tests/
    ```

9. **Commit your changes**
  Write a clear commit message describing your changes:

    ```bash
    git add .
    git commit -m "Add feature XYZ"
    ```

10. **Push to fork**

    ```bash
    git push origin feature/my-new-feature
    ```

11. **Open a merge request**

- Go to the Pyralysis repository on GitLab.
- Click **"New Merge Request"** and select your branch.
- Follow the [merge request template](https://gitlab.com/clirai/pyralysis/-/blob/development/.gitlab/merge_request_templates/feature_template.md).

## 🐛 Reporting issues

If you find a bug or have a feature request, please open an issue on **GitLab** 👉 [Gitlab issues](https://gitlab.com/clirai/pyralysis/-/issues) following the [issue template](https://gitlab.com/clirai/pyralysis/-/blob/development/.gitlab/issue_templates/bug.md).

Please describe the issue clearly and include:

- Steps to reproduce the bug.
- Expected vs. actual behavior.
- Environment details (Python version, OS, dependencies).

---

## License

Pyralysis is distributed under the terms of the GNU General Public License version 3 (GPLv3). You should have received a copy of the GPLv3 along with this program. If not, please see <https://www.gnu.org/licenses/>.

```pgsql
Pyralysis
Copyright (C) 2021  Miguel Cárcamo

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
```

---

## Contact

For support or inquiries, you can reach out through the following channels:

📖 **Documentation**: [Pyralysis ReadTheDocs](https://pyralysis.readthedocs.io)

🐛 **Report Issues**: [GitLab Issues](https://gitlab.com/clirai/pyralysis/-/issues)

📧 **Lead Developer**: [miguel.carcamo@usach.cl](mailto:miguel.carcamo@usach.cl)

If you encounter bugs or would like to contribute, please open an issue on [GitLab Issues](https://gitlab.com/clirai/pyralysis/-/issues).
For general usage questions, check the **official documentation** first.

---

## Summary of Key Features

| Feature                                  | Description                                                                                                                                               |
|------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Flexible Gridding**                    | Supports both count‑in‑cell gridding and convolutional gridding using multiple convolution kernels (Spline, Gaussian‑Sinc, Sinc, PSWF1, Pillbox, etc.). |
| **Model Visibilities Estimation**        | Provides an abstract interface with several methods (Degridding, BilinearInterpolation, NearestNeighbor) for converting FFT images into visibilities.    |
| **Advanced Optimization Framework**      | Combines multiple objective function terms (Chi2, L1Norm, Entropy) that compute both function values and gradients, solved via gradient‑based optimizers.  |
| **Line Search Methods**                  | Includes Brent’s method, Backtracking with Armijo, and Fixed‑step approaches to determine optimal step lengths during optimization.                        |
| **Convolutional Kernels (CKernel)**      | Offers an abstract interface for generating gridding correction functions (GCF) and Fourier‑space convolution kernels.                                     |
| **Gridding Methods**                     | Capable of both count‑in‑cell and convolutional gridding to transform between image and visibility domains.                                               |
| **Dask‑based Parallel Processing (CPU)** | Built using Dask arrays for scalability on large datasets. **Note:** GPU/CUDA acceleration is under active development.|

---
Happy Imaging!

---
