Using Pyralysis
===============

This guide provides a **practical workflow** for using Pyralysis for radio astronomy imaging.
It covers the **main steps** required for **loading data, simulating visibilities, gridding, and optimization**.

For deeper explanations of each module, see:
- **Gridding techniques:** :doc:`gridding`
- **Optimization methods:** :doc:`optimization`
- **Full API reference:** :py:mod:`pyralysis`

---

Basic Workflow
--------------

A typical workflow in Pyralysis involves:

1. **Loading Measurement Sets**
2. **Simulating visibilities from an image (optional)**
3. **Applying gridding techniques**
4. **Running an optimization algorithm to reconstruct the image**
5. **Saving the final image**

---

Loading Measurement Sets
------------------------

Pyralysis processes radio interferometric data stored in **Measurement Set (MS) format**.
Under the hood, it leverages **`dask-ms`** for efficient handling of large datasets.

**Example: Loading a Measurement Set**

.. code-block:: python

   from pyralysis.io import DaskMS

   # Define the path to your Measurement Set file
   ms_file = "/path/to/data.ms"

   # Load the dataset using Dask-MS (lazy loading enabled)
   dataset = DaskMS(input_name=ms_file).read()

   print("Loaded dataset structure:", dataset)

---

Simulating Non-Parametric Sources
---------------------------------

Pyralysis allows simulating **non-parametric sources**, meaning we compute visibilities
from an image without assuming any predefined model.

**Methods for estimating visibilities from an image:**

- **Nearest Neighbor Interpolation** – Fastest but least accurate.
- **Bilinear Interpolation** – Uses linear interpolation for smoother results.
- **Degridding (Convolutional Gridding with a Half-Kernel)** – Most accurate method.

**Example: Simulating Model Visibilities**

.. code-block:: python

   from pyralysis.estimators import NearestNeighbor, BilinearInterpolation, Degridding
   from pyralysis.convolution import PSWF1

   # Use Degridding (Convolutional Gridding)
   kernel = PSWF1(size=3, cellsize=0.003, oversampling_factor=3)
   mv_degridding = Degridding(input_data=dataset, image=image, cellsize=0.003, ckernel_object=kernel)

   # Compute visibilities
   mv_degridding.transform()

---

Gridding Visibilities
----------------------

Gridding is essential for transforming irregularly sampled visibilities into a **uniform Fourier grid**.
Pyralysis provides **DirtyMapper** to compute a **dirty image** and **dirty beam**.

**Example: Applying Gridding**

.. code-block:: python

   from pyralysis.transformers import DirtyMapper
   from pyralysis.convolution import PSWF1

   # Define a convolution kernel
   kernel = PSWF1(size=3, cellsize=0.003, oversampling_factor=3)

   # Create a DirtyMapper object for gridding
   dirty_mapper = DirtyMapper(input_data=dataset, imsize=512, cellsize=0.003, padding_factor=1.2, ckernel_object=kernel)

   # Compute dirty image and dirty beam
   dirty_images, dirty_beam = dirty_mapper.transform()

🔹 **For more details, see** :doc:`gridding`.

---

Performing Image Reconstruction
-------------------------------

To reconstruct an image, Pyralysis uses **gradient-based optimization techniques**.

**Example: Running an Optimizer**

.. code-block:: python

   from pyralysis.optimization.optimizer import HagerZhang
   from pyralysis.optimization.fi import Chi2, L1Norm
   from pyralysis.optimization import ObjectiveFunction

   # Define the objective function
   fi_list = [Chi2(model_visibility=mv, normalize=True), L1Norm(penalization_factor=0.005)]
   of = ObjectiveFunction(fi_list=fi_list, image=image, persist_gradient=True)

   # Run the optimizer
   optim = HagerZhang(image=image, objective_function=of, max_iter=100)
   reconstructed_image = optim.optimize()

🔹 **For details on optimizers, see** :doc:`optimization`.

---

Saving Reconstructed Images
---------------------------

Pyralysis provides **I/O handlers** to save reconstructed images.

**Example: Saving Images in FITS or Zarr Format**

.. code-block:: python

   from pyralysis.io import FITS, ZarrArray

   ioh_fits = FITS()  # FITS format
   ioh_zarr = ZarrArray()  # Zarr format

   # Use the chosen I/O handler when running the optimizer
   optim = HagerZhang(image=image, objective_function=of, max_iter=100, io_handler=ioh_fits)

🔹 **For details on I/O handlers, see** :py:mod:`pyralysis`.

---

Next Steps
----------

Now that you've seen the basic usage of Pyralysis, explore the advanced topics:

- **Gridding techniques:** :doc:`gridding`
- **Optimization methods:** :doc:`optimization`
- **API reference:** :py:mod:`pyralysis`
