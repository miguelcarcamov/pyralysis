Gridding Techniques in Pyralysis
================================

Gridding is a fundamental step in **radio interferometric imaging**, where irregularly sampled
visibility data is mapped onto a regular grid in Fourier space.

Pyralysis provides **two main gridding approaches**:

1️⃣ **Count-in-Cell Gridding**
   - A simple **binning method** that places visibilities into the nearest grid cell.
   - Fast, but introduces **aliasing artifacts** due to the sharp edges in Fourier space.

2️⃣ **Convolutional Gridding (Recommended)**
   - Uses a **convolution kernel** to interpolate visibilities onto the Fourier grid.
   - Reduces aliasing artifacts and improves reconstruction quality.

---

Basic Gridding with `Gridder`
-----------------------------

The **`Gridder` class** in Pyralysis handles gridding:

.. code-block:: python

   from pyralysis.transformers import Gridder

   gridder = Gridder(
       imsize=512,
       cellsize=0.003,
       hermitian_symmetry=False,
       padding_factor=1.2
   )

---

Weighting Schemes
-----------------

Pyralysis supports **three weighting schemes** to improve gridding accuracy:

- **Uniform Weighting** – Assigns equal weights to all visibilities.
- **Natural Weighting** – Gives higher weight to baselines with more observations.
- **Robust Weighting** – A hybrid between natural and uniform weighting.

To apply weighting in `Gridder`, use:

.. code-block:: python

   from pyralysis.transformers.weighting_schemes import Uniform, Natural, Robust

   # Apply Robust weighting
   robust_weighting = Robust(gridder=gridder, robustness_parameter=0.5)
   imaging_weights = robust_weighting.apply()

---

Convolutional Kernels
---------------------

For **convolutional gridding**, Pyralysis provides multiple kernel options:

- **Spline**
- **Gaussian-Sinc**
- **Prolate Spheroidal Wave Function (PSWF1)**
- **Bicubic**
- **Kaiser-Bessel**

To use a **prolate spheroidal kernel**:

.. code-block:: python

   from pyralysis.convolution import PSWF1

   kernel = PSWF1(size=3, cellsize=0.003, oversampling_factor=3)

---

Gridding with Dirty Mapping
---------------------------

Pyralysis also provides a **Dirty Mapper** for direct imaging:

.. code-block:: python

   from pyralysis.transformers import DirtyMapper

   ckernel = kernel if kernel is not None else None

   dirty_mapper = DirtyMapper(
       input_data=dataset,
       imsize=512,
       cellsize=0.003,
       padding_factor=1.2,
       ckernel_object=ckernel
   )

   dirty_images, dirty_beam = dirty_mapper.transform()

---

Conclusion
----------

Gridding plays a crucial role in **radio interferometric imaging**, and Pyralysis provides
**flexible, high-performance implementations** that scale with modern datasets.

For more details, see the API reference: :doc:`modules`.
