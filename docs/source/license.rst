License
=======

Pyralysis is licensed under the **GNU General Public License (GPLv3)**.
Read the full license at https://www.gnu.org/licenses/gpl-3.0.en.html.
