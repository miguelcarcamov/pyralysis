Quickstart Guide
================

This guide walks you through **basic usage** of Pyralysis for radio astronomy imaging.

---

Loading Measurement Sets
------------------------

Pyralysis processes radio interferometric data stored in **Measurement Set (MS) format**.
Under the hood, it leverages the **`dask-ms`** Python library to handle large-scale datasets efficiently.

To load an MS file:

.. code-block:: python

   from pyralysis.io import DaskMS

   # Define the path to your Measurement Set file
   ms_file = "/path/to/data.ms"

   # Load the dataset using Dask-MS under the hood
   dataset = DaskMS(input_name=ms_file).read()

   print("Loaded dataset structure:", dataset)

**Why use `dask-ms`?**
- It enables **lazy loading**, meaning large datasets don’t consume memory immediately.
- It supports **distributed computing**, allowing Pyralysis to scale across multiple CPUs.
- It integrates seamlessly with **Dask arrays**, providing efficient parallel processing.

---

Performing Gridding
-------------------

Once the dataset is loaded, we can **grid the visibilities** onto a Fourier space grid.
Pyralysis provides a **DirtyMapper** transformer for convolutional gridding.

.. code-block:: python

   from pyralysis.transformers import DirtyMapper

   # Create a DirtyMapper object for gridding
   dirty_mapper = DirtyMapper(
       input_data=dataset,
       imsize=512,           # Image size in pixels
       cellsize=0.003,       # Pixel resolution
       padding_factor=1.2    # Padding for better Fourier resolution
   )

   # Perform gridding and obtain the dirty image & PSF (Point Spread Function)
   dirty_images, dirty_beam = dirty_mapper.transform()

   print("Dirty image shape:", dirty_images.data.shape)

---

Configuring Gridding Parameters
-------------------------------

Pyralysis offers **flexible gridding techniques** that can be customized using the `Gridder` transformer.

1️⃣ **Padding**
   - Applying a **padding factor** improves Fourier-space resolution.
   - Example: `padding_factor=1.2` increases the grid size by 20%.

2️⃣ **Convolution Kernels**
   - Pyralysis supports **multiple convolution kernels** for interpolation and extrapolation:
     - **Spline**
     - **Gaussian-Sinc**
     - **Prolate Spheroidal Wave Function (PSWF1)**
     - **Bicubic**
     - **Kaiser-Bessel**

   **Example: Using a Prolate Spheroidal Kernel**

   .. code-block:: python

      from pyralysis.convolution import PSWF1

      # Define the convolution kernel
      kernel = PSWF1(size=3, cellsize=0.003, oversampling_factor=3)

3️⃣ **Weighting Schemes (Configured in `Gridder`)**
   - Weighting is **not** an attribute of `DirtyMapper` but is set using **weighting scheme classes**.

---

Using Convolution Kernels in `ModelVisibilities` and `Degridding`
-----------------------------------------------------------------

In Pyralysis, **convolution kernels (`CKernel` subclasses)** are not only used for gridding, but also for estimating visibilities at **irregular Fourier coordinates**.

This process, known as **degridding**, allows us to **simulate visibilities from an image** (non-parametric simulation).
It is handled by the **`Degridding` class**, which is a subclass of `ModelVisibilities`.

.. code-block:: python

   from pyralysis.estimators import Degridding
   from pyralysis.convolution import PSWF1

   # Define the convolution kernel for degridding
   kernel = PSWF1(size=3, cellsize=0.003, oversampling_factor=3)

   # Create a Degridding object to estimate irregular visibilities
   degridding = Degridding(
       input_data=dataset,
       image=image,
       cellsize=0.003,
       hermitian_symmetry=False,
       padding_factor=1.2,
       ckernel_object=kernel  # Assign convolution kernel
   )

   # Compute visibilities at irregular uv-coordinates
   degridding.transform()

---

What is Non-Parametric Simulation?
-----------------------------------------------------------------

Degridding is also referred to as **non-parametric simulation** because:

- It **estimates visibilities** directly from an input image.
- It is crucial for **simulating visibilities for new observations** or validating imaging techniques.

---
