Frequently Asked Questions (FAQ)
================================

This section answers common questions about Pyralysis.

---

General Questions
-----------------

**Q1: What is Pyralysis and what is it used for?**
A: Pyralysis is an open-source Python library for **radio astronomy imaging and optimization**.
   It provides tools for **gridding, degridding, visibility estimation, and image reconstruction** using
   **regularized maximum likelihood (RML) methods** and **gradient-based optimization**.

**Q2: How does Pyralysis compare to other radio interferometric imaging software?**
A: Pyralysis differs from traditional radio interferometric imaging software in several key ways:

**1️⃣ Beyond CLEAN: A Shift to RML-Based Optimization**

* Most imaging software relies on **CLEAN**, which is fundamentally a **greedy algorithm**.
* CLEAN assumes that an image consists of **point sources and Gaussians** (when using Multi-Scale CLEAN).
* While widely used, CLEAN **does not solve a well-defined mathematical optimization problem**.
* Pyralysis, in contrast, **implements Regularized Maximum Likelihood (RML) methods**, which provide a more **mathematically rigorous** and **flexible** approach to image reconstruction.

**2️⃣ Readability and Object-Oriented Design**

* Many existing imaging software tools are **not object-oriented**, making them difficult to follow.
* They often contain **scattered functions across multiple files**, lacking clear organization.
* Pyralysis follows **modern software engineering practices**, with:

  * **Object-Oriented Design (OOP)** → Making it easier to integrate new functionalities.
  * **Well-structured code** → Improving readability and maintainability.
  * **Modular classes** → Allowing users to easily add **custom models, algorithms, and workflows**.

**3️⃣ Open-Source and Transparency**

* Not all imaging software is **open to the community**.
* Closed-source software acts like a **black box**, making it difficult to **debug, modify, or verify outputs**.
* Pyralysis is **fully open-source**, allowing researchers to **inspect, extend, and improve the codebase**.

**4️⃣ Built for Next-Generation Radio Interferometers**

* Upcoming interferometers like **ALMA (sensitivity upgrade), ngVLA, SKA, and LOFAR 2.0** will produce **massive datasets** that traditional software may struggle to handle.
* Pyralysis is designed to be **scalable**, leveraging:
  * **Dask** → Parallel computing for large-scale imaging.
  * **Zarr** → Efficient storage and handling of massive interferometric data.
  * **Numba** → Just-in-time (JIT) compilation for CPU acceleration.
  * **CuPy** → GPU acceleration **while maintaining a NumPy-like API**, making it easy to use.

By combining **RML-based imaging, an intuitive software architecture, full transparency, and scalable computing**,
Pyralysis is **future-proofed** for the next era of radio astronomy.

---

Installation and Setup
----------------------

**Q3: How can I install Pyralysis?**
A: You can install Pyralysis via pip (unstable) or from GitLab for the latest version:

.. code-block:: bash

   # Install from GitLab (Recommended)
   pip install -U git+https://gitlab.com/clirai/pyralysis.git

   # Install from PyPI (Not updated frequently)
   pip install pyralysis

**Q4: What are the system requirements for Pyralysis?**
A: Pyralysis requires **Python 3.9 - 3.12** and depends on the following libraries:

.. code-block:: bash

   astropy==6.0.0
   autopep8==2.2.0
   dafits==1.1.0
   dask==2024.8.2
   dask-ms[complete]==0.2.21
   distributed==2024.8.2
   matplotlib==3.9.0
   more-itertools==10.2.0
   multimethod==1.11.2
   numba==0.59.1
   numpy<2.0.0
   python-casacore==3.6.1
   radio-beam==0.3.7
   scipy==1.13.1
   snakeviz==2.2.0
   spectral-cube==0.6.5
   xarray==2024.01.1

**Q5: How do I enable GPU acceleration in Pyralysis?**
A: GPU acceleration is **under development**. When available, it will require:

   * A **NVIDIA GPU** with **CUDA support**.
   * **CuPy installed** (`pip install cupy-cuda11x`).
   * Dask GPU-enabled configurations.

---

Using Pyralysis
--------------

**Q6: How do I load a Measurement Set (MS) into Pyralysis?**
A: Pyralysis **relies on `dask-ms`** for reading Measurement Sets (MS) efficiently:

.. code-block:: python

   from pyralysis.io import DaskMS

   ms_file = "/path/to/data.ms"
   dataset = DaskMS(input_name=ms_file).read()

**Q7: How do I reconstruct an image from visibilities?**
A: You need to **define an objective function, select a line search method, and run an optimizer**:

.. code-block:: python

   from pyralysis.optimization import HagerZhang, ObjectiveFunction
   from pyralysis.optimization.fi import Chi2
   from pyralysis.optimization.linesearch import Brent
   from pyralysis.io import FITS

   fi_list = [Chi2(model_visibility=dataset)]
   objective_function = ObjectiveFunction(fi_list=fi_list, image=image)
   ls = Brent(objective_function=objective_function)

   optim = HagerZhang(image=image, objective_function=objective_function, linesearch=ls, io_handler=FITS())
   reconstructed_image = optim.optimize()

---

Optimization and Line Search
----------------------------

**Q8: Why does Pyralysis use derivative-free line search methods?**
A: Even though **the gradient is computed once per iteration** in Pyralysis’ gradient-based optimization algorithms, evaluating it multiple times during line search is **computationally prohibitive**.

   In radio interferometric imaging, datasets are extremely large, and performing multiple gradient evaluations
   **would drastically slow down convergence**.

   To avoid this, Pyralysis provides **efficient derivative-free line search methods**, including:

   * **Brent’s Method** – A derivative-free root-finding algorithm.
   * **Backtracking Line Search with Armijo Rule** – Ensures sufficient function decrease.
   * **Fixed Step** – Uses a predefined step size.

   These methods allow Pyralysis to efficiently optimize imaging solutions **without unnecessary recomputation of gradients**.

---

Data Storage and File I/O
-------------------------

**Q9: How do I save reconstructed images to disk?**
A: Pyralysis supports **FITS and Zarr** for storing images:

.. code-block:: python

   from pyralysis.io import FITS
   from pyralysis.io.zarr import ZarrArray

   fits_handler = FITS()      # Save in FITS format
   zarr_handler = ZarrArray() # Save in Zarr format

   # Use in an optimizer
   optim = HagerZhang(image=image, objective_function=objective_function, io_handler=fits_handler)

**Q10: What is the advantage of using Zarr over FITS?**
A: Zarr is optimized for **parallel and distributed data storage**, making it ideal for:
   * **Large-scale interferometric datasets** that don’t fit in memory.
   * **Fast parallel access** when using Dask.
   * **Multi-resolution imaging** (progressive refinement).

   FITS is the **astronomy standard** and is better for compatibility with tools like CASA and DS9.

---

Still Have Questions?
---------------------
If you have additional questions, feel free to:

* **Check the documentation:** `Pyralysis Docs <https://pyralysis.readthedocs.io>`_
* **Open an issue on GitLab:** `Pyralysis GitLab Issues <https://gitlab.com/clirai/pyralysis/-/issues>`_
* **Contact the lead developer:** `miguel.carcamo@usach.cl`
