Optimization Methods
====================

Pyralysis includes several **gradient-based optimization techniques** for image reconstruction.
These methods **minimize the objective function** by iteratively updating the image
to best match the observed visibilities while applying constraints such as sparsity, entropy, etc.

---

Gradient-Based Optimizers
-------------------------

Pyralysis supports the following **gradient-based optimization methods**:

- **Hager-Zhang** – A **nonlinear conjugate gradient method** that ensures better numerical stability.
- **Limited-memory Broyden–Fletcher–Goldfarb–Shanno (LBFGS)** – A **quasi-Newton** method that approximates second-order derivatives.
- **Fletcher-Reeves** – The **original conjugate gradient** method, best for quadratic functions.
- **Polak-Ribiere** – A variant of Fletcher-Reeves that dynamically adjusts gradient updates.
- **Dai-Yuan** – A **performance-optimized conjugate gradient** method.
- **Hestenes-Stiefel** – Ensures **strong global convergence**.

Each of these methods requires:
1. **An objective function** (combining data fidelity and regularization terms).
2. **A line search method** (step size selection strategy).
3. **A projection function (optional)** to enforce positivity, for example.

---

Defining the Objective Function
-------------------------------

Before running an optimizer, we must define an **objective function**.
For example, we can use a **weighted least squares loss with L1 sparsity**:

.. math::

   \Phi(I) = \sum_k \omega_k \left| V_{\text{obs}, k} - V_{\text{model}, k} \right|^2 + \lambda || I ||_1

where:

* :math:`V_{\text{obs}, k}` are the **observed visibilities**.
* :math:`V_{\text{model}, k}` are the **model-predicted visibilities** based on the reconstructed image.
* :math:`\omega_k` are **visibility weights** to normalize contributions.
* :math:`|| I ||_1` is the **L1 norm**, promoting sparsity.
* :math:`\lambda` is a **regularization parameter** controlling the sparsity constraint.

To instantiate this in Pyralysis:

.. code-block:: python

   from pyralysis.optimization.fi import Chi2, L1Norm
   from pyralysis.optimization import ObjectiveFunction

   # The normalize parameter normalizes the chi-squared function by the number of visibilities
   fi_list = [Chi2(model_visibility=mv, normalize=True),
              L1Norm(penalization_factor=0.005)]
   of = ObjectiveFunction(fi_list=fi_list, image=image, persist_gradient=True)

---

Choosing a Line Search Method
-----------------------------

Line search methods determine **how far** to move along the gradient direction at each iteration.
Pyralysis supports **three derivative-free line search methods**:

- **Fixed Step** – Uses a predefined step size.
- **Brent’s Method** – A **derivative-free root-finding** approach.
- **Backtracking Line Search with Armijo Rule** – Ensures sufficient function decrease.

To instantiate a line search method:

.. code-block:: python

   from pyralysis.optimization.linesearch import Brent, FixedStep, Backtracking

   ls = Brent(objective_function=of)  # Brent’s method
   # ls = FixedStep(step=0.1, objective_function=of)  # Fixed step size
   # ls = Backtracking(objective_function=of)  # Armijo rule

---

Running Different Optimizers
----------------------------

**1️⃣ Hager-Zhang Optimizer**
**Hager-Zhang** is a **nonlinear conjugate gradient method** that balances **convergence speed and numerical stability**.

**Example Usage:**

.. code-block:: python

   from pyralysis.optimization.optimizer import HagerZhang

   optim = HagerZhang(
       image=image,
       objective_function=of,
       linesearch=ls,
       max_iter=100,
       io_handler=ioh
   )

   # Run optimization
   reconstructed_image = optim.optimize()

   print("Reconstructed image shape:", reconstructed_image.data.shape)

---

**2️⃣ Limited-memory BFGS (LBFGS)**
LBFGS is a **quasi-Newton** method that approximates the Hessian matrix
without storing it explicitly, making it ideal for large-scale optimization.
It uses a **limited-memory approximation** controlled by the `max_corrections` parameter.

🔹 **Key Parameter in LBFGS:**
- **`max_corrections`** → Defines **how many past gradient updates** are used to approximate the Hessian.
  - A **higher value** improves the accuracy of the Hessian but increases memory usage.
  - A **lower value** makes LBFGS faster but less accurate.

**Example Usage:**

.. code-block:: python

   from pyralysis.optimization.optimizer import LBFGS

   optim = LBFGS(
       image=image,
       objective_function=of,
       linesearch=ls,
       max_iter=100,
       io_handler=ioh,
       max_corrections=10  # Number of past updates stored
   )

   reconstructed_image = optim.optimize()

---

**3️⃣ Fletcher-Reeves Conjugate Gradient**
Fletcher-Reeves is the **classic conjugate gradient method**,
well-suited for **quadratic functions**.

**Example Usage:**

.. code-block:: python

   from pyralysis.optimization.optimizer import FletcherReeves

   optim = FletcherReeves(
       image=image,
       objective_function=of,
       linesearch=ls,
       max_iter=100,
       io_handler=ioh
   )

   reconstructed_image = optim.optimize()

---

**4️⃣ Polak-Ribiere Conjugate Gradient**
Polak-Ribiere **modifies the gradient update** dynamically,
leading to **faster convergence in some cases**.

**Example Usage:**

.. code-block:: python

   from pyralysis.optimization.optimizer import PolakRibiere

   optim = PolakRibiere(
       image=image,
       objective_function=of,
       linesearch=ls,
       max_iter=100,
       io_handler=ioh
   )

   reconstructed_image = optim.optimize()

---

**5️⃣ Dai-Yuan Conjugate Gradient**
Dai-Yuan is a **variant of conjugate gradient** optimized for performance
by **adjusting gradient scaling factors**.

**Example Usage:**

.. code-block:: python

   from pyralysis.optimization.optimizer import DaiYuan

   optim = DaiYuan(
       image=image,
       objective_function=of,
       linesearch=ls,
       max_iter=100,
       io_handler=ioh
   )

   reconstructed_image = optim.optimize()

---

**6️⃣ Hestenes-Stiefel Conjugate Gradient**
Hestenes-Stiefel ensures **strong global convergence**
and is useful when the optimization function is **highly nonlinear**.

**Example Usage:**

.. code-block:: python

   from pyralysis.optimization.optimizer import HestenesStiefel

   optim = HestenesStiefel(
       image=image,
       objective_function=of,
       linesearch=ls,
       max_iter=100,
       io_handler=ioh
   )

   reconstructed_image = optim.optimize()

---

Saving Reconstructed Images
---------------------------

To store intermediate reconstructed images, we use **I/O handlers**.

**Using a FITS Handler**

.. code-block:: python

   from pyralysis.io import FITS

   ioh = FITS()  # Instantiate a FITS handler

**Using a Zarr Handler**

.. code-block:: python

   from pyralysis.io.zarr import ZarrArray

   ioh = ZarrArray()  # Instantiate a Zarr handler

---

Conclusion
----------

Pyralysis provides a **flexible and scalable optimization framework**
for radio interferometric imaging, allowing users to choose among
**various conjugate gradient and quasi-Newton methods**.

To learn more:

- **Check how gridding interacts with optimization:** See :doc:`gridding`.
- **Read the API reference:** See :py:mod:`pyralysis`.
