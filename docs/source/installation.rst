Installation
============

Pyralysis supports installation via **pip**, **source**, or **Docker**.

Prerequisites
-------------

Before installing Pyralysis, ensure your system meets the following requirements:

- **Python**: 3.9 ≤ Python < 3.13
- **Dask**: For parallel computing.
- **CUDA** (Optional): For future GPU acceleration.

Pyralysis depends on the following libraries:

.. code-block:: bash

   astropy==6.0.0
   autopep8==2.2.0
   dafits==1.1.0
   dask==2024.8.2
   dask-ms[complete]==0.2.21
   distributed==2024.8.2
   matplotlib==3.9.0
   more-itertools==10.2.0
   multimethod==1.11.2
   numba==0.59.1
   numpy<2.0.0
   python-casacore==3.6.1
   radio-beam==0.3.7
   scipy==1.13.1
   snakeviz==2.2.0
   spectral-cube==0.6.5
   xarray==2024.01.1

Installation Methods
--------------------

**1. Install from GitLab (Recommended)**
This ensures you have the **latest development version**:

.. code-block:: bash

   pip install -U git+https://gitlab.com/clirai/pyralysis.git

**2. Install from PyPI (Warning: Package may not be up to date)**

.. code-block:: bash

   pip install pyralysis

**3. Install from source**
For development purposes, install Pyralysis in **editable mode**:

.. code-block:: bash

   git clone https://gitlab.com/miguelcarcamov/pyralysis.git
   cd pyralysis
   pip install -e .

**4. Using Docker**
Pull the latest Docker image:

.. code-block:: bash

   docker pull registry.gitlab.com/miguelcarcamov/pyralysis:latest

---

Optional: Enabling GPU Support
------------------------------

If you have an **NVIDIA GPU with CUDA**, you can install CuPy for **GPU acceleration** (currently under development):

.. code-block:: bash

   pip install cupy-cuda11x  # For CUDA 11+
