Context: Why Pyralysis?
========================

The name **Pyralysis** is a fusion of two fundamental pillars of the project: **Python** and **Radio Astronomy**. As a software framework dedicated to advancing image synthesis, data reduction, and analysis in radio interferometry, it was essential for the name to capture both the computational and astronomical dimensions of the project.

Beyond this, *Pyralysis* also evokes *paralysis*, but in a metaphorical sense—representing the ability to "freeze" raw radio data, deconstruct it, and extract meaningful astronomical insights through structured, high-performance computational techniques. This is made possible by its **object-oriented architecture**, which provides the flexibility needed to accommodate evolving algorithms, optimize performance, and seamlessly integrate new methodologies for interferometric imaging and Faraday tomography.

At its core, *Pyralysis* embodies the synergy between **flexibility, modularity, and scientific rigor**, leveraging Python’s power to push the boundaries of radio interferometric data processing. By combining object-oriented principles with state-of-the-art computational techniques, it creates a robust and extensible platform for the next generation of radio astronomical research.

---

Making Software More Accessible to the Community
------------------------------------------------
One of the main goals of Pyralysis is to make **radio astronomy imaging software more accessible** by:

- **Ensuring modularity** – Pyralysis is built with an **object-oriented design**, making it easier for users
  to extend functionalities by adding **custom classes, functions, and algorithms**.
- **Improving readability** – Unlike legacy software, Pyralysis follows modern **software engineering practices**
  to ensure that the codebase is **well-structured, documented, and intuitive**.
- **Enabling extensibility** – Users can **easily integrate new imaging techniques**
  without modifying the core infrastructure.

These principles ensure that Pyralysis can be adopted by the community while remaining **flexible for future developments**.

---

Challenges in Existing Imaging Software
---------------------------------------
While several radio interferometric imaging tools exist, they often present challenges in **readability and maintainability**:

- **Lack of structure** – Many existing imaging software solutions are **not object-oriented**,
  making it difficult to **follow the execution flow**.
- **Scattered functions** – Instead of organized class-based designs, they rely on **functions spread throughout the code**,
  which makes it **harder to track dependencies and logic**.
- **Lack of coding standards** – Some tools do not follow **modern software engineering practices**,
  making it challenging to debug, modify, or extend functionalities.

Additionally, **not all imaging software is open-source**, meaning:

- **Limited transparency** – If a codebase is closed, users cannot **inspect or debug** its inner workings.
- **Black-box behavior** – Without access to the source, users must **trust the software’s output without validation**.

Pyralysis was designed to **solve these issues** by:

- **Being fully open-source** – Allowing researchers to inspect, modify, and contribute to the code.
- **Following an object-oriented design** – Making it easier to **understand, modify, and extend**.
- **Providing clear documentation** – Ensuring users can quickly adapt and integrate new methods.

Having an **object-oriented design** also allows developers to **design and implement software patterns**,
which helps keep the code **organized, scalable, and reusable**.

---

Beyond CLEAN: The Need for Mathematical Optimization
-----------------------------------------------------
Even though the **radio astronomy community** has explored advanced imaging techniques such as
**Regularized Maximum Likelihood (RML) methods** and **compressive sensing**, the dominant standard remains:

- **CLEAN** – The most widely used imaging algorithm in radio astronomy.
- **Multi-Scale CLEAN** – A variant that models sources as a combination of **point sources and Gaussians**.

However, CLEAN is fundamentally a **greedy algorithm** rather than a mathematically-driven optimization method:

- It iteratively subtracts bright sources from the visibility data.
- It does **not** solve an inverse problem based on a proper image model.
- It relies on heuristics rather than a well-defined cost function.

With the development of **next-generation interferometers**, the limitations of CLEAN
are becoming more apparent, creating a need for **more sophisticated imaging approaches**.

---

Scaling for the Future: Big Data & Big Computing
-------------------------------------------------
Upcoming and upgraded **radio interferometers** will produce **massive datasets**
that traditional imaging pipelines cannot efficiently handle.

These include:

- **ALMA Sensitivity Upgrade** – Higher sensitivity means **larger, higher-resolution datasets**.
- **Next-Generation VLA (ngVLA)** – Producing **petabyte-scale datasets** that demand distributed computing.
- **Square Kilometre Array (SKA)** – The largest radio interferometer, requiring **exascale computing**.
- **LOFAR and LOFAR 2.0** – Generating large **low-frequency interferometric datasets**.

To keep up with this **scale of data**, Pyralysis integrates **big-data and big-computing techniques**:

- **Dask** – Enables parallel computing and out-of-core processing for large datasets.
- **Zarr** – Efficiently stores and handles large-scale interferometric data.
- **Numba** – Speeds up CPU-bound operations using **Just-In-Time (JIT) compilation**.
- **CuPy** – Accelerates computations on **GPUs** while **maintaining a NumPy-like API**,
  making it **easy to adopt** for users familiar with NumPy.

By combining these **high-performance computing techniques**, Pyralysis can run **gradient-based
optimization algorithms** that scale with modern radio astronomy datasets.

---

A Future-Proof Imaging Framework
---------------------------------
Pyralysis is not just another imaging pipeline—it is a **flexible, scalable, and extensible framework**
designed to grow alongside the next generation of **radio astronomy instruments**.

It is built to:

- **Provide a mathematically solid alternative to CLEAN**.
- **Leverage parallel and distributed computing for large-scale imaging**.
- **Allow users to integrate custom models, algorithms, and workflows**.

With these principles in mind, Pyralysis aims to become a **community-driven**
solution for the future of **radio interferometric imaging**.
