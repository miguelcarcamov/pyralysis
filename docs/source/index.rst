.. Pyralysis documentation master file, created by
   sphinx-quickstart on Wed May 18 14:51:48 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: https://gitlab.com/miguelcarcamov/pyralysis/-/wikis/uploads/fd40975ce0601b5c52a3cf8b92f385a5/Pyralisis-25.png
   :width: 600px
   :height: 600px
   :scale: 100 %
   :alt: Pyralysis logo
   :align: center

Welcome to Pyralysis's Documentation!
=====================================

Pyralysis is an open-source Python library designed for **radio astronomy imaging and optimization**.
It provides:

* **Regularized Maximum Likelihood (RML) imaging techniques**, including Maximum Entropy Methods.
* **Advanced gridding strategies**, such as convolutional gridding and count-in-cell.
* **Gradient-based optimization** for image reconstruction.
* **Parallel processing with Dask**, supporting CPU (GPU support in development).

📖 **Full Documentation**: Available below.

.. note::
   This documentation is built using `Sphinx <https://www.sphinx-doc.org/>`_ and `sphinx-book-theme <https://sphinx-book-theme.readthedocs.io/>`_.

Contents:
---------

.. toctree::
   :maxdepth: 2
   :caption: Introduction

   context
   inverse_problem

.. toctree::
   :maxdepth: 2
   :caption: Getting Started

   installation
   quickstart
   usage

.. toctree::
   :maxdepth: 2
   :caption: Core Concepts

   gridding
   optimization

.. toctree::
   :maxdepth: 2
   :caption: API Reference

   autoapi/index

.. toctree::
   :maxdepth: 2
   :caption: Development

   development
   faq
   glossary
   license
