FROM nvidia/cuda:12.5.1-cudnn-devel-ubuntu22.04

RUN apt-get update -y && \
    apt-get upgrade -y python-pip && \
    apt-get install -y --no-install-recommends \
    build-essential \
    python3-dev \
    python3-pip \
    python3-wheel \
    python3-setuptools \
    python3-venv  \
    casacore-dev \
    libboost-python-dev \
    libcfitsio-dev \
    wcslib-dev \
    git && \
    rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*

RUN pip3 install --no-cache-dir -U setuptools setuptools-scm pip
RUN pip3 install --no-cache-dir -U cupy-cuda12x
RUN echo "Hello from pyralysis base image with cuPy"
LABEL org.opencontainers.image.source="https://gitlab.com/clirai/pyralysis"
LABEL org.opencontainers.image.description="Base container image for pyralysis"
LABEL org.opencontainers.image.licenses=GPL3
